//
//  Cache.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class Cache: NSObject
{
    struct CacheStatic
    {
        static let cacheDir = "2swy5rsdfe92"
        static let cachePass = ".@d908!2/&22q2"
    }
    
    class func cacheDirectory() -> NSString
    {
        let paths : NSArray = NSSearchPathForDirectoriesInDomains(.libraryDirectory,
            .userDomainMask, true) as NSArray
        let path = NSString(format: "%@/%@", paths.object(at: 0) as! NSString, CacheStatic.cacheDir)
        
        return path
    }
    
    class func createCacheDirectory()
    {
        let newDirectory = cacheDirectory()
        
        if (!FileManager.default.fileExists(atPath: newDirectory as String))
        {
            do {
                try FileManager.default.createDirectory(atPath: newDirectory as String, withIntermediateDirectories: true, attributes: nil)
            } catch _ {
            }
        }
    }
    
    class func pathWithName(_ name : NSString) -> NSString
    {
        let path : NSString = cacheDirectory()
        
        return path.appendingPathComponent(name as String) as NSString
    }
    
    class func saveWithName(_ name : NSString, collection : AnyObject?)
    {
        var collection = collection
        
        if (collection != nil)
        {
            if ((collection as? NSDictionary) != nil)
            {
                collection = (collection as! NSDictionary).replacingNullsWithBlanks() as AnyObject?
            }
            else
            {
                collection = (collection as! NSArray).replacingNullsWithBlanks() as AnyObject?
            }
            
            let path = pathWithName(name)
            var data = NSKeyedArchiver.archivedData(withRootObject: collection!)
            
            if (data.count > 0)
            {
                data = (data as NSData).aes256Encrypt(withKey: CacheStatic.cachePass)
                try? data.write(to: URL(fileURLWithPath: path as String), options: [.atomic])
            }
        }
    }
    
    class func loadDictionaryWithName(_ name : NSString) -> NSDictionary?
    {
        let path = pathWithName(name)
        var data = try? Data(contentsOf: URL(fileURLWithPath: path as String))
        
        data  = (data as NSData?)?.aes256Decrypt(withKey: CacheStatic.cachePass)
        
        if (data?.count > 0)
        {
            return NSKeyedUnarchiver.unarchiveObject(with: data!) as? NSDictionary
        }
        
        return nil
    }
    
    class func loadArrayWithName(_ name : NSString) -> NSArray?
    {
        let path = pathWithName(name)
        var data = try? Data(contentsOf: URL(fileURLWithPath: path as String))
        
        data  = (data as NSData?)?.aes256Decrypt(withKey: CacheStatic.cachePass)
        
        if (data?.count > 0)
        {
            return NSKeyedUnarchiver.unarchiveObject(with: data!) as? NSArray
        }
        
        return nil
    }
}
