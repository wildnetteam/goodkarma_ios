//
//  GKHallOfFameCollectionViewCell.swift
//  GoodKarma
//
//  Created by Shatakshi on 24/01/18.
//  Copyright © 2018 Teclalabs. All rights reserved.
//

import UIKit

class GKHallOfFameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: WebImageView!
    
    var data : GKDictionary!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.applyGradient(colours: [UIColor(rgb: 0x44c8c9), UIColor(rgb: 0x00a0e7)])

        let _border = CAShapeLayer()
        _border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius:5.0).cgPath
        _border.frame = self.bounds
        _border.strokeColor = UIColor.white.cgColor
        _border.fillColor = UIColor.clear.cgColor
        _border.lineWidth = 3.0
        self.layer.addSublayer(_border)
        
//        self.layer.cornerRadius = 5.0
//        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func loadData()
    {
        let firstName = data["first_name"] as! String
        let lastName = data["last_name"] as! String
        userNameLabel.text = firstName.capitalized + " " + lastName.capitalized

        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.profileImageView.layer.borderWidth = 2.0
        self.profileImageView.layer.borderColor = (UIColor.white).cgColor

        self.profileImageView.loadImageWithUrl(data["photo_url"] as? String as NSString?)
    }
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}
