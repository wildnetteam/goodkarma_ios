//
//  SendLocationViewController.swift
//  GoodKarma
//
//  Created by Shatakshi on 30/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import GoogleMaps
import SearchTextField
import GooglePlacesAPI

class SendLocationViewController: UIViewController, UITableViewDelegate , UITableViewDataSource, GMSMapViewDelegate {
    
    @IBOutlet weak var searchBarLocation: UISearchBar!
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var tableViewLocations: UITableView!
    @IBOutlet weak var textFieldSearch: SearchTextField!
    
    @IBOutlet weak var buttonSend: UIButton!

    fileprivate var locationManager = CLLocationManager()
    var mapView: GMSMapView!
    var currentLocation : CLLocationCoordinate2D?
    var places: [QPlace] = []
    let marker = GMSMarker()
    var arrSearchLocationList : NSMutableArray = []
    var dicSelectedLocation = [String:Any]()
    var response : QNearbyPlacesResponse?

    var isLoading = false
    var stringLocationSelected = String()
    var data: GKDictionary!
    
    let placeNearbyURLString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Send Location"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(SendLocationViewController.cancelRequest)
        )
       
        let nib = UINib(nibName: "SendLocationTableViewCell", bundle: nil)
        self.tableViewLocations.register(nib, forCellReuseIdentifier: "SendLocationTableViewCell")
        tableViewLocations.tableFooterView = UIView()
        
        customView()
        setMapView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSendClicked(_ sender: Any) {
        if self.textFieldSearch.text != nil && self.textFieldSearch.text != ""{
            sendLocationAndStatusUpdateService()
        }
        else{
            showAlert("", text: "Please select location to send.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
        }
    }
    
    // MARK: - Methods -
    
    @objc func cancelRequest()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func customView() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height:textFieldSearch.frame.height))
        let imageView = UIImageView(frame: CGRect(x: 3, y: 4, width: 16, height:paddingView.frame.height - 8));
        imageView.image = UIImage(named: "searchGray.png")
        paddingView.addSubview(imageView)
        textFieldSearch.leftView = paddingView;
        textFieldSearch.leftViewMode = .always
        textFieldSearch.tintColor = UIColor.black
        
        configureCustomSearchTextField()
    }
    
    func setMapView(){
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        
       // self.viewMap.backgroundColor = UIColor.red
        
        mapView = GMSMapView.map(withFrame: self.viewMap.frame, camera: camera)
        self.mapView.frame.origin.y = 0.0
        print(self.viewMap.frame.origin, mapView.frame.origin)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        self.viewMap.addSubview(mapView)
    }
    
    // GMSMapView Delegates
    
//    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
//    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print(marker.position.latitude)
        print(marker.position.longitude)
        
        getAddressFromLatLon(lat: marker.position.latitude, withLongitude: marker.position.longitude)

        return true
    }

    func getAddressFromLatLon(lat: CLLocationDegrees, withLongitude long: CLLocationDegrees) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(lat)")!
        let lon: Double = Double("\(long)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    var seperatorString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString  + pm.subLocality!
                    }
                    if pm.locality != nil {
                        if addressString.count != 0{
                            seperatorString = ", "
                        }
                        addressString = addressString + seperatorString + pm.locality!
                    }
                    if pm.administrativeArea != nil {
                        if addressString.count != 0{
                            seperatorString = ", "
                        }
                        addressString = addressString + ", " + pm.administrativeArea!
                    }
                    if pm.country != nil {
                        if addressString.count != 0{
                            seperatorString = ", "
                        }
                        addressString = addressString + ", " + pm.country!
                    }
                    if pm.postalCode != nil {
                        if addressString.count != 0{
                            seperatorString = ", "
                        }
                        addressString = addressString + ", " + pm.postalCode!
                    }
                    self.stringLocationSelected = addressString
                    print(self.stringLocationSelected)
                    
                    self.textFieldSearch.text = self.stringLocationSelected
                    self.dicSelectedLocation["latitude"] = lat
                    self.dicSelectedLocation["longitude"] = long
                    self.dicSelectedLocation["address"] = self.stringLocationSelected
                    self.arrSearchLocationList.add(self.stringLocationSelected)
                    
                    self.tableViewLocations.reloadData()
                }
        })
    }
    
    // 2 - Configure a custom search text view
    fileprivate func configureCustomSearchTextField() {
        // Set theme - Default: light
        textFieldSearch.theme = SearchTextFieldTheme.lightTheme()
        
        // Modify current theme properties
        textFieldSearch.theme.font = UIFont(name: "Roboto-Regular", size: 14)!
        textFieldSearch.theme.bgColor = UIColor.white.withAlphaComponent(1.0)
        textFieldSearch.theme.borderColor = UIColor.lightGray.withAlphaComponent(0.5)
        textFieldSearch.theme.separatorColor = UIColor.lightGray.withAlphaComponent(0.5)
        textFieldSearch.theme.cellHeight = 44
        textFieldSearch.theme.placeholderColor = UIColor.lightGray
        textFieldSearch.theme.fontColor = UIColor.darkGray
        
        // Max results list height - Default: No limit
        textFieldSearch.maxResultsListHeight = 300
        
        // Set specific comparision options - Default: .caseInsensitive
        textFieldSearch.comparisonOptions = [.caseInsensitive]
        
        // You can force the results list to support RTL languages - Default: false
        textFieldSearch.forceRightToLeft = false
        
        textFieldSearch.highlightAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):UIFont(name: "Roboto-Regular", size: 14)!]
        
        // Handle item selection - Default behaviour: item title set to the text field
        textFieldSearch.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            self.textFieldSearch.text = item.title
            self.stringLocationSelected = "\(item.title), \(item.subtitle ?? "")"
            self.arrSearchLocationList.add(self.stringLocationSelected)
            
            self.dicSelectedLocation["latitude"] = self.places[itemPosition].getLat()
            self.dicSelectedLocation["longitude"] = self.places[itemPosition].getLong()
            self.dicSelectedLocation["address"] = self.stringLocationSelected

            self.tableViewLocations.reloadData()
        }
        
        // Update data source when the user stops typing
        textFieldSearch.userStoppedTypingHandler = {
            if let criteria = self.textFieldSearch.text {
                if criteria.count > 1 {
                    // Show loading indicator
                    self.textFieldSearch.showLoadingIndicator()
                    
                    self.searchLocation(searchString:criteria, force:true) { results in
                        
                        // Set new items to filter
                        self.textFieldSearch.filterItems(results)
                        
                        // Stop loading indicator
                        self.textFieldSearch.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    // MARK: - UITableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrSearchLocationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SendLocationTableViewCell", for: indexPath) as! SendLocationTableViewCell
        
        cell.labelName.text = arrSearchLocationList[indexPath.row] as? String
        
        return cell
    }
    
    //MARK:------ WEBServices
    
    func searchLocation(searchString:String, force:Bool, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        
        if !force {
            if !canLoadMore() {
                return
            }
        }
        
        print("load more")
        isLoading = true
        NearbyPlacesController.getNearbyPlaces(by: searchString, coordinates: currentLocation!, rankby: "distance", token: self.response?.nextPageToken){ results in
            
            self.response = results
            self.places = []
            print(self.response!.status, self.response!.places!, searchString)
            if self.response?.status == "OK" {
                
                if let p = self.response?.places {
                    self.places.append(contentsOf: p)
                }
                print(self.places.count)
                var data = [SearchTextFieldItem]()
                for result in self.places {
                    data.append(SearchTextFieldItem(title: result.getName(), subtitle: result.getAddress(), image: UIImage(named: "acronym_icon")))
                }
                DispatchQueue.main.async {
                    callback(data)
                }
            }
            else {
                DispatchQueue.main.async {
                    callback([])
                }
            }
        }
    }
    
    func canLoadMore() -> Bool {
        if isLoading {
            return false
        }
        
        if let response = self.response {
            if (!response.canLoadMore()) {
                return false
            }
        }
        return true
    }
    
    func sendLocationAndStatusUpdateService()
    {
        self.textFieldSearch.resignFirstResponder()
        
        var params =  GKDictionary()
        params["token"] =  Static.sharedInstance.token() as AnyObject
        params["id"] =  self.data["related_post_id"] as AnyObject
        params["address"] =  dicSelectedLocation["address"] as AnyObject
        params["latitude"] =  dicSelectedLocation["latitude"] as AnyObject
        params["longitude"] =  dicSelectedLocation["longitude"] as AnyObject
        params["hugType"] =  self.data["hugType"] as AnyObject
        params["status"] =  2 as AnyObject
        params["post_type"] =   self.data["post_type"] as AnyObject
        
        print(params)
        
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
        
        GKServiceClient.call(GKRouterClient.sendLocationAndStatusUpdate(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
             LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response!)
                    let alertController = UIAlertController(title: "Success!", message: "Location shared successfully.", preferredStyle: .alert)
                    let OkButton: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
                        self.cancelRequest()
                    }
                    alertController.addAction(OkButton)
                    self.present(alertController, animated: false, completion: nil)
                }
                else{
                    showAlert("Alert!", text: "Location already shared.")
                }
            }
            else
            {
                
            }
        })
    }
}

// Delegates to handle events for the location manager.
extension SendLocationViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location: CLLocation = locations.last!
        print("Location: \(location)")
        self.currentLocation = location.coordinate
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: 15.0)
        
        mapView.isHidden = false
        mapView.camera = camera
        // Creates a marker in the center of the map.
        
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.title = "Current Location"
        marker.map = mapView
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
}
