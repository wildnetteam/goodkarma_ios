////
////  NumberPadTextField.m
////  Hawkr
////
////  Created by Eliezer de Armas on 26/6/16.
////  Copyright © 2016 TeclaLabs. All rights reserved.
////
//
//#import "NumberPadTextField.h"
//
//@implementation NumberPadTextField
//
//- (void)addAccessoryWithText:(NSString *)text
//{
//    UIToolbar *toolbar = [[UIToolbar alloc] init];
//    [toolbar sizeToFit];
//    
//    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc]
//                                   initWithTitle:text
//                                   style:UIBarButtonItemStyleBordered target:self
//                                   action:@selector(buttonItemAction)];
//    
//    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]
//                               initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
//                               target:nil action:nil];
//    spacer.width = 10;
//    
//    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    
//    [toolbar setItems:@[flexible, buttonItem, spacer]];
//    self.inputAccessoryView = toolbar;
//}
//
//#pragma mark - Actions
//
//- (void)buttonItemAction
//{
//    [self.delegate textFieldShouldReturn:self];
//}
//
//@end
