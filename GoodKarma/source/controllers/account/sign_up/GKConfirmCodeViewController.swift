//
//  GKConfirmCodeViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKConfirmCodeViewController: UIViewController, UITextFieldDelegate {

  // MARK: - IBOutlets -
  
  @IBOutlet weak var codeField: UITextField!
  
  // MARK: - Properties -

  var data: NSDictionary!
  var fromFacebook = false
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    cleanBackButton()
    
    self.title = "Sign Up"
    
//    let attributes = [NSFontAttributeName: UIFont.fontRoboto(.Regular, size: 30)]
//    codeField.attributedPlaceholder = NSAttributedString(string: "Code", attributes: attributes)
    
    self.codeField.addAccessoryView("Done")
    //codeField.text = UserDefaults.standard.object(forKey: "code") as? String
    self.updateText()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = false
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    
    super.viewWillDisappear(animated)
    
    self.view.endEditing(true)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    super.viewDidAppear(animated)
    
    codeField.becomeFirstResponder()
  }
  
  // MARK: - IBActions -
  
  @IBAction func textFieldDidChange(_ sender: AnyObject) {
    
    updateText()
  }
  
  @IBAction func actionNext(_ sender: AnyObject) {
    
    checkSendCode()
  }
  
    @IBAction func resendOtpClicked(_ sender: Any) {
        resendCode()
    }
    
    // MARK: - Protocol UITextFieldDelegate -
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    let postString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
    
    if postString.countPlainCharacters() <= 6 {
      
      return true
    }
    else if string.countPlainCharacters() == 0 {
      
      return true
    }
    else {
      
      return false
    }
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    codeField.resignFirstResponder()
//    self.checkSendCode()
    
    return false
  }
  
  // MARK: - Methods -
  
  func next() {
    
    let addPhotoController = GKAddPhotoViewController(
      nibName: "GKAddPhotoViewController", bundle: nil)
    addPhotoController.data = data
    self.navigationController?.pushViewController(addPhotoController, animated: true)
  }

  func checkSendCode() {
    
    self.view.endEditing(true)
    
    if validate() {
      
      sendCode()
    }
  }
  
  func sendCode() {
    
    LoadingView.showLoadingView("Verifying code...",
      parentView: self.navigationController?.view, backColor: true)
    
    var params = [String: Any]()
    
    params["token"] = self.data["token"] as? String
    params["code"]  = codeField.text
    
    print(params)
    //self.next()
    GKServiceClient.call(GKRouterAuth.validatePhone(), parameters:params as GKDictionary, callback: { (response: AnyObject?) in

      LoadingView.hideLoadingView(self.navigationController?.view)

      if let res: AnyObject = response {

        let envelope = ResponseSkeleton(res)

        if envelope.getSuccess() {

          self.next()
        }
        else {

          showMessageError("Alert", text: "Invalid code.", callback: { (type: NoInternetResultType) -> () in
            self.sendCode()
          })
        }
      }
      else
      {
        showConnectionError({ (type: NoInternetResultType) -> () in
          self.sendCode()
        })
      }
    })
  }
  
    func resendCode() {
        
        LoadingView.showLoadingView("Resending...",
                                    parentView: self.navigationController?.view, backColor: true)
        
        var params = [String: Any]()
        
        params["email"] = self.data["email"] as? String
        
        print(params)
        //self.next()
        GKServiceClient.call(GKRouterAuth.resendOtp(), parameters:params as GKDictionary, callback: { (response: AnyObject?) in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res: AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess() {
                    showAlert("Success", text: "Otp sent successfully.Please check your inbox.")
                }
                else {
                    showMessageError("Alert", text: "Something went wrong.", callback: { (type: NoInternetResultType) -> () in
                        self.sendCode()
                    })
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                    self.sendCode()
                })
            }
        })
    }
    
  func updateText() {
    
    let attributedString = NSMutableAttributedString(string: codeField.text!.plainText())
    attributedString.addAttribute(NSAttributedStringKey.kern, value: 8, range: NSMakeRange(0, codeField.text!.countPlainCharacters()))
    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont.fontRoboto(.Bold, size: 30), range: NSMakeRange(0, codeField.text!.countPlainCharacters()))
    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSMakeRange(0, codeField.text!.countPlainCharacters()))
    
    codeField.attributedText = attributedString
  }
  
  func validate() -> Bool {
    
    if self.codeField.text?.countPlainCharacters() != 6 {
      
      showMessageError("Alert", text: "Need to enter a code", callback: { (type: NoInternetResultType) in
        
      })
      
      return false
    }
    
    return true
  }
  
}
