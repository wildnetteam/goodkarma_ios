////
////  User.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/17/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation
//
//struct GKModelUser : GKModelProtocol {
//  
//    var uid: String!
//    private var firstName: String!
//    private var lastName: String!
//    private var birthday: String?
//  private var token: String!
//  private var facebookId: String!
//  private var profilePicUrl: String?
//  
//
////  static var access_token: String? {
////    
////    return GKLocalKeychain.fetchCredential(.AccessToken)
////  }
////  
//    // MARK: - Initializer
//    
////    init(uid: String = "", firstName: String = "", lastName: String = "",
////        token: String = "", facebookId: String = "")
////    {
////        self.uid = uid
////        self.firstName = firstName
////        self.lastName = lastName
////        self.token = token
////        self.facebookId = facebookId
////    }
//  
//    // MARK: - Getters
//    
//     func getUid() -> String
//    {
//        return uid
//    }
//    
//     func getFirstName() ->String
//    {
//        return firstName
//    }
//    
//     func getLastName() -> String
//    {
//        return lastName
//    }
//    
//     func getToken() -> String
//    {
//        return token
//    }
//    
//     func getFacebookId() -> String
//    {
//        return facebookId
//    }
//    
//    // MARK : - Class Methods
//    
//   static func parseJSON(data: AnyObject) -> GKModelUser?
//    {
//        let resource: AnyObject = ResponseSkeleton(data).getResource()
//        let resourceDict = resource as! [String:AnyObject]
//        
//        if let idString = resourceDict["id"] as? Int
//        {
//            let firstName = resourceDict["first_name"] as! String!
//            let lastName = resourceDict["last_name"] as! String!
//            let token = resourceDict["token"] as! String!
//            let facebookId = resourceDict["facebook_id"] as! String!
//            
////            let user = GKModelUser(uid: String(idString), firstName:
////                firstName, lastName: lastName,
////                token: token, facebookId: facebookId)
//          
//            return nil
////            return user
//        }
//        
//        return nil
//    }
//  
//  
//  static func logout() {
//    
//    GKStore.sharedInstance.clearCache()
//    GKLocalKeychain.removeLoginCredentials()
//  }
////  
//  // MARK: - GKModelProtocol -
//  
//  static func readFromDictionary(dictionary: GKDictionary) -> GKModelUser {
//    var user_data = GKModelUser(uid: dictionary["_id"] as! String)
//    
//    user_data.uid = dictionary["_id"] as! String
//    user_data.firstName  = dictionary["first_name"] as! String
//    
//    return user_data
//  }
//  
//  static func readFromSmallDictionary(data: GKDictionary) -> GKModelUser {
//  var user_data = GKModelUser(uid: data["_id"] as! String)
//  
//  user_data.firstName  = data["first_name"] as! String
//  user_data.lastName   = data["last_name"] as! String
//  user_data.profilePicUrl   = data["profile_pic"] as? String
//  
//  return user_data
//}
//
//  static func readFromDictionaryConversation(data: GKDictionary) -> GKModelUser {
//    var user_data = GKModelUser(uid: data["_id"] as! String)
//    
//    user_data.firstName      = data["first_name"] as! String
//    user_data.profilePicUrl = data["profile_pic"] as? String
//    
//    return user_data
//  }
//  
//  
//}
