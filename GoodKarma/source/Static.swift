//
//  Static.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

class Static {
    
    var userProfile: NSDictionary!
    
    class var sharedInstance: Static
    {
        struct StaticStruct
        {
            static let instance: Static = Static()
        }
        return StaticStruct.instance
    }
    
    func loadInitial()
    {
        Cache.createCacheDirectory()
        
        // User Profile
        self.userProfile = Cache.loadDictionaryWithName("userProfile")
    }
    
    func clearCache()
    {
        saveUserProfile(NSDictionary())
    }
    
    // MARK: - Login
    
    func isLogin() -> Bool
    {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "login")
    }
    
    func saveLogin(_ login: Bool)
    {
        if !login {
            clearCache()
        }
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(login, forKey: "login")
        userDefaults.synchronize()
    }
    
    func saveDeviceToken(_ deviceToken: String!)
    {
        if deviceToken != nil && deviceToken.count > 0 {
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(deviceToken, forKey: "deviceToken")
            userDefaults.synchronize()
        }
    }
    
    func deviceToken() -> String!
    {
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "deviceToken")
    }
    
    func saveEmail(_ email: String)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(email, forKey: "email")
        userDefaults.synchronize()
    }
    
    func email() -> String {
        
        let userDefaults = UserDefaults.standard
        
        guard let email = userDefaults.string(forKey: "email") else {
            
            return ""
        }
        return email
    }
    
    func savePassword(_ password: String)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(password, forKey: "password")
        userDefaults.synchronize()
    }
    
    func password() -> String!
    {
        let userDefaults = UserDefaults.standard
        
        return userDefaults.string(forKey: "password")
    }
    
    // MARK: - User Profile
    
    func saveUserProfile(_ userProfile: NSDictionary?)
    {
        print(userProfile!)
        self.userProfile = userProfile
        Cache.saveWithName("userProfile", collection: self.userProfile)
    }
    
    func token() -> String!
    {
        if self.userProfile != nil && self.userProfile.allKeys.count > 0 {
            
            return self.userProfile?["token"] as! String
        }
        return nil
    }
    
    func userId() -> Int!
    {
      //  return self.userProfile["id"] as! Int
        if UserDefaults.standard.object(forKey: "id") != nil{
            return UserDefaults.standard.object(forKey: "id") as! Int
        }
        return nil
    }
    
    func userFirstName() -> String
    {
        let value = self.userProfile.object(forKey: "first_name") as! String
        return value
    }
    
    func isMe(_ id: Int) -> Bool
    {
        return userId() == id
    }
    
    func isMe(_ userData: NSDictionary!) -> Bool
    {
        if (userData != nil)
        {
            if let userId = userData["id"] as? Int{
                 return isMe(userId)
            }
           return false
        }
        return false
    }
    
    func isLoginWithFacebook() -> Bool
    {
        if let fb = (UserDefaults.standard.object(forKey: "isFacebookId")) as? String
        {
            let isFacebookId = fb
            if isFacebookId != "NULL" {
                if self.userProfile != nil && isFacebookId.count > 0
                {
                    return true
                }
            }
        }

 
//        if self.userProfile != nil && self.userProfile["social"] as! Int == 1
//        {
//            return true
//        }
       
        return false
    }
    
    // MARK: - Instructions -
    
    func canShowAccessPositivityInstructions() -> Bool
    {
        let userDefaults = UserDefaults.standard
        return !userDefaults.bool(forKey: "HOME_INSTRUCTIONS_POSITIVITY")
    }
    
    func canShowAccessInstructions(_ typeInstructions: GKInstructionsType) -> Bool
    {
        let userDefaults = UserDefaults.standard
        switch typeInstructions {
        case .Gratitude:
            return !userDefaults.bool(forKey: "HOME_INSTRUCTIONS_GRATITUDE")
        case .OfferHelp:
            return !userDefaults.bool(forKey: "HOME_INSTRUCTIONS_OFFER_HELP")
        case .SeekHelp:
            return !userDefaults.bool(forKey: "HOME_INSTRUCTIONS_SEEK_HELP")
        case .History:
            return !userDefaults.bool(forKey: "HOME_HISTORY")
        case .AcceptOrCancel:
            return !userDefaults.bool(forKey: "ACCEPT_OR_CANCEL")
        case .FlakeOrSOS:
            return !userDefaults.bool(forKey: "FLAKE_OR_SOS")
        }
    }
    
    func saveShowPositivityInstructions()
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "HOME_INSTRUCTIONS_POSITIVITY")
        userDefaults.synchronize()
    }
    
    func saveShowInstructions(_ typeInstructions: GKInstructionsType)
    {
        let userDefaults = UserDefaults.standard
        
        switch typeInstructions {
        case .Gratitude:
            userDefaults.set(true, forKey: "HOME_INSTRUCTIONS_GRATITUDE")
        case .OfferHelp:
            userDefaults.set(true, forKey: "HOME_INSTRUCTIONS_OFFER_HELP")
        case .SeekHelp:
            userDefaults.set(true, forKey: "HOME_INSTRUCTIONS_SEEK_HELP")
        case .History:
            userDefaults.set(true, forKey: "HOME_HISTORY")
        case .AcceptOrCancel:
            userDefaults.set(true, forKey: "ACCEPT_OR_CANCEL")
        case .FlakeOrSOS:
            userDefaults.set(true, forKey: "FLAKE_OR_SOS")
        }
        
        userDefaults.synchronize()
    }
    
    
}
