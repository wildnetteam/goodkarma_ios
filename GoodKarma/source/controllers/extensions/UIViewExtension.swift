//
//  UIViewExtension.swift
//  Version3
//
//  Created by Paul Aguilar on 8/10/15.
//  Copyright (c) 2015 Alumnify Inc. All rights reserved.
//

import Foundation

public extension UIView {
    
    var rightSide: CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    var bottomSide: CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
}