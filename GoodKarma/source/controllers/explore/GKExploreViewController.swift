//
//  GKExploreViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class GKExploreViewController: GKViewController, GKMainFeedDelegate {
 
    // MARK: - IBOutlets -
    
    @IBOutlet weak var mainFeedContainer: GKMainFeedTableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    // MARK: - Properties -
    var anotherUserID: Int? = nil
    var isInMenu : Bool = true
    var onlyFinalized : Bool = false
    var onlyOpen : Bool = false
    var customTitle: String!
    var pageNo = 1

    // MARK: - Override UIViewController -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if (self.isInMenu)
        {
            self.title = "Explore"
            setMenuBarBadge()
        }
        else
        {
            self.title = "Posts"
        }
        
        if (self.customTitle != nil)
        {
            self.title = self.customTitle
        }
        
        self.mainFeedContainer.parentController = self
        self.mainFeedContainer.delegate = self
        self.mainFeedContainer.setupTableView()
        self.mainFeedContainer.requestData()
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKMainFeedDelegate -
    
    func GKRequestData(_ completion: @escaping ([GKDictionary]) -> ())
    {
        if (self.anotherUserID != nil)
        {
            requestRequestedData(completion)
        }
        else
        {
            requestExplorer(completion)
        }
    }
    
    func GKSendMessage(_ userID: Int, fullName: String)
    {
        let messageViewController = GKMessagesViewController(nibName:
            GKMessagesViewController.xibName(), bundle: nil)
        
        let anotherUser = GKMessageAnotherUser(uid: userID, fullName: fullName)
        messageViewController.anotherUser = anotherUser
        self.navigationController?.pushViewController(messageViewController, animated:true)
    }
    
    // MARK: - Methods -
    
    func checkEmptyState(_ count: Int)
    {
        emptyLabel.isHidden        = (count != 0)
        mainFeedContainer.isHidden = (count == 0)
    }
    
//    func requestRequestedData(_ completion: @escaping ([GKDictionary]) -> ())
//    {
//        var params = GKDictionary()
//        params["token"] = Static.sharedInstance.token() as AnyObject?
//
//        if anotherUserID != nil
//        {
//            params["recipientId"] = anotherUserID! as AnyObject?
//        }
//
//        LoadingView.showLoadingView(
//            "Loading...",
//            parentView: self.navigationController?.view, backColor: true
//        )
//
//        GKServiceClient.call(GKRouterClient.historyRequested(), parameters: params, callback: {
//            (response: AnyObject?) -> () in
//
//            LoadingView.hideLoadingView(self.navigationController?.view)
//
//            if let res:AnyObject = response
//            {
//                let envelope = ResponseSkeleton(res)
//
//                if envelope.getSuccess()
//                {
//                    if let data = envelope.getResource()["home"] as? [GKDictionary]
//                    {
//                        let d = self.filter(data as NSArray)
//                        //let items = self.removeDueDate(d)
//                        let items = d
//                        self.checkEmptyState(items.count)
//                        completion(items as! [GKDictionary])
//                    }
//                }
//            }
//        })
//    }
    
    func requestRequestedData(_ completion: @escaping ([GKDictionary]) -> ())
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["interval"] = "" as AnyObject
        params["distance"] = "" as AnyObject
        params["zip_code"] = "" as AnyObject
        params["difficulty"] = "" as AnyObject
        params["following"] = 1 as AnyObject
        params["follower"] = 1 as AnyObject as AnyObject
        params["skills"] = "" as AnyObject?
        params["number"] = ""  as AnyObject?
        params["post_type"] = "" as AnyObject? as AnyObject
        params["is_home"] = 1 as AnyObject
        params["page_number"] = 1  as AnyObject?
        params["is_search"] = "" as AnyObject?
        //30 nov
        if anotherUserID != nil
        {
            params["user_id"] = anotherUserID! as AnyObject?
        }
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.home(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["result"] as? [GKDictionary]
                    {
                        let d = data as NSArray
                        //  let d = self.filter(data as NSArray)
                        //let items = self.removeDueDate(d)
                        let items = d
                        self.checkEmptyState(items.count)
                        completion(items as! [GKDictionary])
                    }
                }
            }
        })
    }
    
    func requestExplorer(_ completion: @escaping ([GKDictionary]) -> ())
    {
        if self.mainFeedContainer.isSearch{
            self.pageNo = self.mainFeedContainer.currentPageSearch!
        }
        else{
            self.pageNo = self.mainFeedContainer.currentPage!
        }
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page_number"] = self.pageNo  as AnyObject?
        params["is_search"] = mainFeedContainer.searchBarMainFeed.text  as AnyObject?

        if self.pageNo==1 {
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
        }
        
        GKServiceClient.call(GKRouterClient.explore(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    // if let data = envelope.getResource()["explorer"] as? [GKDictionary]
                    if let data = envelope.getResource()["result"] as? [GKDictionary]
                    {
                        if self.mainFeedContainer.isSearch{
                            self.mainFeedContainer.currentPageSearch = self.mainFeedContainer.currentPageSearch! + 1
                            self.mainFeedContainer.totalCountSearch = envelope.getResource()["count"] as! NSInteger?
                            if data.count == 0{
                                showAlert("Sorry!", text: "No data found.")
                                self.mainFeedContainer.tableView.tableFooterView = nil

                            }
                            else{
                                self.mainFeedContainer.tableView.tableFooterView = self.mainFeedContainer.activityView
                            }
                        }
                        else{
                            self.mainFeedContainer.currentPage = self.mainFeedContainer.currentPage! + 1
                            self.mainFeedContainer.totalCount = envelope.getResource()["count"] as! NSInteger?
                            self.checkEmptyState(data.count)
                            self.mainFeedContainer.tableView.tableFooterView = self.mainFeedContainer.activityView
                        }
                        completion(data)
                    }
                }
            }
        })
    }
    
    func removeDueDate(_ data: NSArray) -> NSArray
    {
        let result = NSMutableArray()
        
        for item in data
        {
            let d = item as! NSDictionary
            let type = (d["type"] as! NSNumber).int32Value
            var add = true
            
            if (type == 1 || type == 3)
            {
                if let dateCompletion = d["date_completion"] as? String
                {
                    if (isDateExpired(dateCompletion))
                    {
                        add = false
                    }
                }
            }
            else if (type == 3)
            {
                add = false
            }
        
            if (add)
            {
                result.add(item)
            }
        }
        
        return result
    }
    
    func isDateExpired(_ dateStr: String) -> Bool
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date: Date! = formatter.date(from: dateStr)
        
        if (date.timeIntervalSinceNow < 0)
        {
            return true
        }
        
        return false
    }
    
    func filter(_ arrayData: NSArray) -> NSArray
    {
        if (self.onlyFinalized)
        {
            return arrayData.filter{ ($0 as! NSDictionary)["status"] as? Int == 4} as NSArray
        }
        else if (self.onlyOpen)
        {
            return arrayData.filter{ (($0 as! NSDictionary)["status"] as? Int) < 4} as NSArray
        }
        
        return arrayData
    }
}
