//
//  LoadingView.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

let kTagLoadingView = 6666

class LoadingView: UIView
{
    fileprivate var content: UIView!
    fileprivate var text: UILabel!
    fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var yPos: CGFloat = 0
    fileprivate var isSuccess = false
    
    class func showLoadingView(_ text: NSString, parentView: UIView?,
        backColor: Bool, yPos: CGFloat)
    {
        if (parentView != nil)
        {
            let loadingView = LoadingView(text: text)
            loadingView.yPos = yPos
            loadingView.tag = kTagLoadingView
            loadingView.frame = CGRect(x: 0, y: yPos, width: parentView!.frame.width,
                                           height: parentView!.frame.height)
            
            if (backColor)
            {
                loadingView.backgroundColor = UIColor.colorToPercent(0, green: 0,
                                                                     blue: 0, alpha: 0.3)
            }
            
            parentView?.addSubview(loadingView)
        }
    }
    
    class func showLoadingView(_ text: NSString, parentView: UIView?, backColor: Bool)
    {
        if (parentView != nil)
        {
            LoadingView.showLoadingView(text, parentView:parentView,
                                        backColor:backColor, yPos:0)
        }
    }
    
    class func hideLoadingView(_ parentView: UIView?)
    {
        let v = parentView?.viewWithTag(kTagLoadingView)
        
        if (v != nil)
        {
            v?.removeFromSuperview()
        }
    }
    
    class func hideWithSuccess(_ parentView: UIView?, text: String)
    {
        let v = parentView?.viewWithTag(kTagLoadingView)
        
        if (v != nil)
        {
            let loadingView = v as! LoadingView
            loadingView.success(text)
        }
    }
    
    init(text: NSString)
    {
        super.init(frame:CGRect.zero)
        setup(text)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder:aDecoder)
        setup("")
    }
    
    fileprivate func setup(_ text: NSString)
    {
        // Content
        self.content = UIView(frame: CGRect.zero)
        self.content.backgroundColor = UIColor.colorToPercent(12, green: 12, blue: 12)
        self.content.layer.cornerRadius = 6
        self.content.alpha = 0.9
        addSubview(self.content)
        
        // Loading indicator
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityIndicator.startAnimating()
        self.activityIndicator.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        self.content .addSubview(self.activityIndicator)
        
        // Text
        self.text = UILabel(frame: CGRect.zero)
        self.text.backgroundColor = UIColor.clear
        self.text.font = UIFont(name: "HelveticaNeue", size: 15)
        self.text.textColor = UIColor.white
        self.text.shadowColor = UIColor.black
        self.text.shadowOffset = CGSize(width: 0, height: 1)
        self.text.text = text as String
        self.content.addSubview(self.text)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.text.sizeToFit()
        
        if (self.isSuccess)
        {
            let width: CGFloat = self.text.frame.width + 35
            let height: CGFloat = 35
            let x: CGFloat = self.frame.width/2 - width/2
            let y: CGFloat = self.frame.height/2 - height/2 - self.yPos
            
            self.content.frame = CGRect(x: x, y: y, width: width, height: height)
            self.text.frame = CGRect(x: 0, y: 6, width: self.content.frame.width,
                height: self.text.frame.height);
        }
        else
        {
            let width: CGFloat = self.text.frame.width + 55
            let height: CGFloat = 35
            let x: CGFloat = self.frame.width/2 - width/2
            let y: CGFloat = self.frame.height/2 - height/2 - self.yPos
            
            self.content.frame = CGRect(x: x, y: y, width: width, height: height)
            self.activityIndicator.frame = CGRect(x: 18, y: 17, width: 1, height: 1);
            self.text.frame = CGRect(x: 40, y: 6, width: self.text.frame.width,
                height: self.text.frame.height);
        }
    }
    
    func success(_ text: String)
    {
        self.activityIndicator.isHidden = true
        self.text.text = text
        self.text.textAlignment = .center
        self.isSuccess = true
        setNeedsLayout()
        
        let delayTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.close()
        }
    }
    
    func close()
    {
        UIView.animate(withDuration: 0.3, delay:0,
            options:UIViewAnimationOptions(),
            animations:
            {
                self.alpha = 0
            },
            completion:
            { _ in
                self.removeFromSuperview()
        })
    }
}














