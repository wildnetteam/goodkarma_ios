//
//  GKRouterClient.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/27/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKRouterClient: ResourceRouter
{
    static func createGroup() -> String
    {
        return endpoint("karm/create/chat/group")
    }
    // MARK: - Hug -
    
    static func sendHug() -> String
    {
        return endpoint("karm/send/hug")
    }
    
    static func sendLocationAndStatusUpdate() -> String
    {
        return endpoint("karm/hug/status/update")
    }
    
    // MARK: - Register -
    
    static func signUp()->String
    {
        return endpoint("karm/register/")
    }
    
    static func validatePhoneNumber()->String
    {
        return endpoint("karm/Validate/phone")
    }
    
    static func registerProfile()->String
    {
        return endpoint("karm/profile/")
    }
    
    static func registerEditProfile()->String
    {
        return endpoint("karm/profile/edit")
    }
    
    static func registerUploadPhoto()-> String
    {
        return endpoint("karm/profile/upload-photo/")
    }
    
    // MARK: - Home -
    static func home()->String
    {
        return endpoint("karm/home/")
    }
    
    // MARK: - Explore -
    static func explore()->String
    {
        return endpoint("karm/explore/")
    }
    
    // MARK: - Friends -
    static func friends()->String
    {
        return endpoint("karm/friends/")
    }
    
    static func listHallOfFame() -> String
    {
        return endpoint("karm/hallOfFame/list")
    }
    
    // MARK: - Gratitude -
    
    static func createGratitude() -> String
    {
        return endpoint("karm/new/gratitude/")
    }
    
    // MARK: - Serve -
    
    static func joinServe()->String
    {
        return endpoint("karm/join/serve/")
    }
    
    static func createServe()->String
    {
        return endpoint("karm/create/serve/")
    }
    
    // MARK: - Help -
    
    static func joinHelpRequest()->String
    {
        return endpoint("karm/join/help/")
    }
    
    static func createHelpRequest()->String
    {
        return endpoint("karm/request/help/")
    }
    
    // MARK: - Positivity -
    
    static func createPositivity() -> String
    {
        return endpoint("karm/join/positivity/")
    }
    
    static func acceptPositivity() -> String
    {
        // "decline": True
        return endpoint("karm/accept/positivity/")
    }
    
    static func requestPositivity()->String
    {
        return endpoint("karm/request/positivity/")
    }
    
    //  static func listRespondPositivities() -> String  {
    //
    //    return endpoint("karm/list/positivity/")
    //  }
    
    // MARK: - Posts -
    
    static func hostPausePost()->String
    {
        return endpoint("karm/pause/post/")
    }
    
    static func hostUnpausePost()->String
    {
        return endpoint("karm/unpause/post/")
    }
    
    static func hostFinishPost()->String
    {
        return endpoint("karm/finish/post/")
    }
    
    static func clientFinishPost()->String
    {
        return endpoint("karm/client/post/finish/")
    }
    
    static func clientPost() -> String
    {
        return endpoint("karm/client/post/status/update")
    }
    static func hostPost() -> String
    {
        return endpoint("karm/update/status")
    }
    
    static func postDetail()->String
    {
        return endpoint("karm/post/detail/")
    }
    
    static func acceptParticipant()->String
    {
        return endpoint("karm/accept/joined/post/")
    }
    
    static func removeParticipant()->String
    {
        return endpoint("karm/client/post/remove/")
    }
    
    static func markFlakeParticipant()->String
    {
        return endpoint("karm/client/post/flake/")
    }
    
    static func reportParticipant()->String
    {
        return endpoint("karm/client/post/report/")
    }
    
    // MARK: - Skills -
    static func mySkills()->String
    {
        return endpoint("karm/skills/me/")
    }
    
    static func listSkills()->String
    {
        return endpoint("karm/skills/all/")
    }
    
    // MARK: - Menu -
    
    static func menuCounter()->String
    {
        return endpoint("karm/menu/counter/")
    }
    
    // MARK: - Like -
    
    static func like()->String
    {
        return endpoint("karm/like/")
    }
    
    // MARK: - Followers -
    
    static func checkFollow() -> String
    {
        return endpoint("karm/check/follow/")
    }
    
    static func follow() -> String
    {
        return endpoint("karm/follow/")
    }
    
    static func unfollow() -> String
    {
        return endpoint("karm/unfollow/")
    }
    
    static func followers() -> String
    {
        return endpoint("karm/list/followers/")
    }
    
    static func following() -> String
    {
        return endpoint("karm/list/followings/")
    }
    
    // MARK: - History -
    
    static func addTestimonial() -> String
    {
        return endpoint("karm/add/testimonial/")
    }
    
    static func historyRequested() -> String
    {
        
        return endpoint("karm/posted/history/")
    }
    
    static func historyOffered() -> String
    {
        return endpoint("karm/requested/history/")
    }
    
    // MARK: - Notifications -
    
    static func listNotifications() -> String
    {
        return endpoint("karm/notification/list")
    }
    
    //modified
    // MARK: - Company -
    static func addCompany() -> String
    {
        return endpoint("karm/company/add")
    }
    static func getCompanyList() -> String
    {
        return endpoint("karm/company/list")
    }
    static func searchEmployee() -> String
    {
        return endpoint("karm/company/employee/search")
    }
    static func addEmployee() -> String
    {
        return endpoint("karm/company/add/employee")
    }
    
    // MARK: - MyJobs -
    static func listJobsAssignedByMe() -> String
    {
        return endpoint("karm/job/assignedbyme/list/")
    }
    
    static func listJobsAssignedToMe() -> String
    {
        return endpoint("karm/job/assignedtome/list/")
    }
    static func jobStatus() -> String
    {
        return endpoint("karm/job/assign/status/")
    }
    static func addJob() -> String
    {
        return endpoint("karm/job/add")
    }
    // MARK: - SOS -
    static func listSOS() -> String
    {
        return endpoint("karm/sos/list")
    }
    static func sendSOS() -> String
    {
        return endpoint("karm/sos/send")
    }
    
    //new
    static func addSOS() -> String
    {
        return endpoint("karm/sos/add")
    }
    static func deleteSOS() -> String
    {
        return endpoint("karm/sos/delete")
    }
    
    // MARK: - Profile -
    
    static func profileBlock() -> String
    {
        return endpoint("karm/block/")
    }
    
    static func profileListMyGratitudes() -> String
    {
        return endpoint("karm/me/gratitude/")
    }
    
    static func profileListOtherGratitudes() -> String
    {
        return endpoint("karm/list/gratitude/")
    }
    
    static func profileListMyTestimonials() -> String
    {
        return endpoint("karm/testimonial/me/")
    }
    
    static func profileListOtherTestimonials() -> String
    {
        return endpoint("karm/testimonial/")
    }
    
    static func profileEdit() -> String
    {
        return endpoint("karm/edit/profile")
    }
    
    // MARK: - Settings -
    
    static func settingsDueDate() -> String
    {
        return endpoint("karm/due/settings/")
    }
    
    static func settingsExploreFilters() -> String
    {
        return endpoint("karm/explore/settings/")
    }
    
    static func settingsGratitude() -> String
    {
        return endpoint("karm/gratitude/privacy/")
    }
    
    static func settingsNotifications() -> String
    {
        return endpoint("karm/notification/setting")
    }
    
    static func settingsDeleteAccount() -> String
    {
        return endpoint("karm/delete/")
    }
    
    static func settingsBlockedUser() -> String
    {
        return endpoint("karm/block/list/")
    }
    
    static func resetPassword() -> String
    {
        return endpoint("karm/password/reset/")
    }
}
