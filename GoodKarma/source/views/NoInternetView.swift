//
//  NoInternetView.swift
//  Faveo
//
//  Created by Eliezer de Armas on 3/11/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

let kTagNoInternetView = 6667

enum NoInternetResultType
{
    case tryAgain
    case signOut
}

class NoInternetView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var signOutView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var signOutButton: UIButton!
    
    var resultBlock: ((NoInternetResultType) ->())!
    var isMessage: Bool = false
    
    init(frame: CGRect, isMessage: Bool)
    {
        super.init(frame:frame)
        self.isMessage = isMessage
        setup()
    }

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    fileprivate func setup()
    {
        self.tag = kTagNoInternetView
        self.backgroundColor = UIColor.clear
        self.alpha = 0
        
        view = Bundle.main.loadNibNamed("NoInternetView", owner: self,
            options: nil)?[0] as! UIView
        view.backgroundColor = UIColor.clear
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(view)
        
        // ContentView
        self.contentView.layer.cornerRadius = 4
        
        // Try Again
        self.tryAgainButton.layer.cornerRadius = 5
        
        // Is Login
        if (!Static.sharedInstance.isLogin() || self.isMessage) {
          
            self.signOutView.isHidden = true
            self.contentView.frame = CGRectSetHeight(self.contentView.frame, h: 185)
        }
        
        // SignOut Button
        setSignOutText("Sign Out")
    }
    
    func show(_ callback: @escaping (NoInternetResultType) ->()) -> ()
    {
        self.resultBlock = callback
        
        UIView.animate(withDuration: 0.5, delay:0,
            options:UIViewAnimationOptions(),
            animations:
            {
                self.alpha = 1
            },
            completion:
            { _ in
        })
    }
    
    func close()
    {
        UIView.animate(withDuration: 0.3, delay:0,
            options:UIViewAnimationOptions(),
            animations:
            {
                self.alpha = 0
            },
            completion:
            { _ in
                self.removeFromSuperview()
        })
    }
    
    func setSignOutText(_ signOutText: String)
    {
        let or = "or "
        let fullText = NSString(format: "%@ %@", or, signOutText)
        
        let attributedString = NSMutableAttributedString(string: fullText as String)
        attributedString.setAttributes(
            [NSAttributedStringKey.foregroundColor: RGBCOLOR(180, green: 180, blue: 180),
                NSAttributedStringKey.font: (UIFont(name: "HelveticaNeue-Medium", size: 14))!],
            range: fullText.range(of: String(or)))
        attributedString.setAttributes(
            [NSAttributedStringKey.foregroundColor: RGBCOLOR(1, green: 156, blue: 222),
            NSAttributedStringKey.font: (UIFont(name: "HelveticaNeue-Medium", size: 14))!],
            range: fullText.range(of: signOutText))
        
        self.signOutButton.setAttributedTitle(attributedString, for: UIControlState())
    }
    
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: AnyObject)
    {
        close()
    }
    
    @IBAction func tryAgainAction(_ sender: AnyObject)
    {
        if (self.resultBlock != nil)
        {
            self.resultBlock(.tryAgain)
        }
        
        close()
    }
    
    @IBAction func signOutAction(_ sender: AnyObject)
    {
        if (self.resultBlock != nil)
        {
            self.resultBlock(.signOut)
        }
        
        close()
    }
}
