//
//  SOSAddContactsViewController.swift//  GoodKarma
//
//  Created by Shivani on 03/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import AddressBookUI
import CoreLocation

typealias listSosServiceResponse = ([GKDictionary]) -> Void
typealias deleteSosServiceResponse = (Bool) -> Void
typealias addSosServiceResponse = (Bool) -> Void

class SOSAddContactsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource ,GKSosContactDelegate,GKSosAddContactDelegate, ABPeoplePickerNavigationControllerDelegate
{
    @IBOutlet weak var tableView: UITableView!

    var data = [NSDictionary]()
    var listData : NSDictionary!
    var info : NSDictionary!
    var count = 0
    let addContactsViewObject = Bundle.main.loadNibNamed(
        "GKSosAddContactsView", owner: self, options: nil)?[0] as! GKSosAddContactsView
  
    fileprivate var locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let nib = UINib(nibName: "SOSAddContactsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SOSAddContactsCell")
        self.title = "SOS"
        addContactsViewObject.delegate = self
//        (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(addContactsViewObject)
        self.addContact()
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SOSAddContactsCell", for: indexPath) as! SOSAddContactsTableViewCell
        cell.loadData(data : data[indexPath.row] as! GKDictionary)
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  SOSAddContactsTableViewCell.heightForCell()
    }
    
    
    func addContactClicked() {
        let c = ABPeoplePickerNavigationController()
        c.peoplePickerDelegate = self
        c.displayedProperties = [
            NSNumber(value: kABPersonPhoneProperty as Int32)
        ]
        
        ABAddressBookRequestAccessWithCompletion(c, { success, error in
            if success {
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(c, animated: true, completion: nil)
            }
        })
    }
    
    // MARK: - ABAddressBook Methods
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier)
    {
        peoplePicker.dismiss(animated: true, completion: nil)
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        let contact = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
        let nameCFString: CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
        let name: NSString = nameCFString as NSString
        let firstName = name.components(separatedBy: " ")[0]
        let lastName = name.components(separatedBy: " ")[1]
        
        //self.nameTextField.text = (firstName as String) + (lastName as String)
        //self.phoneNumberTextField.text = contact
    }
    
    func addContact()
    {
        self.sosList()
            {
                (object) in
                if object.count == 2
                {
                    self.data = object as [NSDictionary]
                    let alertController = UIAlertController(title: "Message", message: "Maximum two contacts can be added", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    self.count = 2
                    self.tableView.reloadData()
                }
                else
                {
                    self.addContactClicked()
//                    self.addContactsViewObject.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
//                    self.addContactsViewObject.setUp()
//                  //  (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(self.addContactsViewObject)
//                    self.view.addSubview(self.addContactsViewObject)
//                    
//                    if object.count == 1
//                    {
//                        self.listData = object[0] as NSDictionary
//                        self.count = 1
//                        self.data.append(self.listData! )
//                        self.tableView.reloadData()
//                    }
//                    else
//                    {
//                        self.count = 1
//                    }
                    
                }
        }
    
    }
    func passDeleteData(index : Int)
    {
        self.sosList()
            {
                (object) in
                let dataAtIndex = object[index] 
                let idAtIndex = dataAtIndex["id"] as? Int
                self.deleteContact(id: idAtIndex!)
                {
                    (isDeleted) in
                    if isDeleted == true
                    {
                        self.data.remove(at: index)
                        self.count = self.data.count
                        self.tableView.reloadData()
                    }
                    
                }
                
           }
        
        
    }
    
    func passDataToBeAdded(info : GKDictionary)
   {
    self.info = info as NSDictionary?
    self.uploadSOSContact()
        {
            (isUpdated) in
            self.data.append(self.info)
            self.count = self.data.count
            self.tableView.reloadData()
        }
    }
    func passSendLocationData(index : Int)
    {
        self.uploadLocation()
    }
    func uploadSOSContact(onCompletion: @escaping addSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["name"] = info.object(forKey: "name") as AnyObject?
        params["country_code"] = info.object(forKey: "country_code") as AnyObject?
        params["phn_number"] =  info.object(forKey: "phn_no")  as AnyObject?
        if params["phn_number"] == nil
        {
            params["phn_no"] = " " as AnyObject
        }
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.addSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response!)
                    onCompletion(true)
                }
                else
                {
                   print("error")
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
                
            }
        })
        
    }
    func sosList(onCompletion: @escaping listSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.listSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    let object = response?.object(forKey: "object") as! [GKDictionary]
               
                    onCompletion(object)
            
                }
                else
                {
                    print("error")
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
                
            }
           
        })
        
    }
    
    
    func deleteContact(id : Int , onCompletion: @escaping deleteSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["id"] = id as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.deleteSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response as Any)
                    onCompletion(true)
                }
                else{
                    print("error")
                    
                    
                }
                
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
                
            }
        }
        )
        
    }
    
    func uploadLocation()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["msg"] = ("I am in SOS condition and my current location=" + " " + String(format: "http://maps.google.com/maps?q=loc:%@,%@",(self.currentLocation?.latitude)!,(self.currentLocation?.longitude)!)) as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.sendSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                print(res)
            }
            else
            {
                print("error")
            }
        })
    }
    
}

// Delegates to handle events for the location manager.
extension SOSAddContactsViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        self.currentLocation = location.coordinate
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
}
}

