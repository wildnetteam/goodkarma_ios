//
//  GKServiceUser.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation
import Alamofire

struct GKServiceClient {
  
  // MARK: - General API -
  
  static func call(_ router: String, parameters: GKDictionary, callback:@escaping (AnyObject?) ->()) -> (){
    Alamofire.request(router, method: .post,
      parameters: parameters, encoding:  JSONEncoding.default)
      .responseJSON { response in
        callback(response.result.value as AnyObject?)
    }
  }
  
    static func callGet(_ router: String, param: [String: Any], callback:@escaping (AnyObject?) ->()) -> (){
        Alamofire.request(router, method: .get, parameters: nil,  encoding: JSONEncoding.default).validate().responseJSON { response in
            callback(response.result.value as AnyObject?)
        }
    }
    
  static func uploadWithAlamofire(_ parameters: [String: String], imageData: Data, callback:@escaping (AnyObject?)->()) -> () {
    
    // Begin upload
    Alamofire.upload( multipartFormData: { multipartFormData in
        
        multipartFormData.append(imageData, withName: "file", fileName: "myImage.png", mimeType: "image/png")
        
        // import parameters
        for (key, value) in parameters {
            multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
        }}
        , to: GKRouterClient.registerUploadPhoto(), method: .post,

                     encodingCompletion: { encodingResult in
                      
                      switch encodingResult {
                        
                      case .success(let upload, _, _):
                        
                        upload.responseJSON { response in
                          
                          callback(response.result.value as AnyObject?)
                        }
                      case .failure(let encodingError):
                        
                        callback(nil)
                        print("**ERROR")
                        print(encodingError)
                      }
    })
  }
}
