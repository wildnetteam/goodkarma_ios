//
//  GKHallOfFameTableViewCell.swift
//  GoodKarma
//
//  Created by Shivani on 23/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKHallOfFameTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: WebImageView!
    
    var data : GKDictionary!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.clipsToBounds = true
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  class func heightForCell() -> CGFloat
    {
        return 65.0
    }
    func loadData()
    {
        let firstName = data["first_name"] as! String
        let lastName = data["last_name"] as! String
         self.profileImageView.loadImageWithUrl(data["photo_url"] as? String as NSString?)
        userNameLabel.text = firstName.capitalized + "  " + lastName.capitalized
        
    }
}
