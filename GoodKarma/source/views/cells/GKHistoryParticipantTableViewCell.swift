//
//  GKHistoryParticipantTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/26/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKHistoryParticipantTableViewCell: UITableViewCell
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var pictureImageView : WebImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var testimonialButton: UIButton!
    
    // MARK: - Properties -
    
    var data: GKDictionary!
    var delegate: (GKShowUserProfileDelegate & GKTestimonialDelegate)?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
        self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
        self.pictureImageView.isUserInteractionEnabled = true
        
        self.pictureImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
        self.pictureImageView.isUserInteractionEnabled = true
        
        self.testimonialButton.titleLabel?.textAlignment = .center
        
        self.selectionStyle = .none
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String
    {
        return "GKHistoryParticipantTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat
    {
        return 90.0
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionTestimonial(_ sender: AnyObject)
    {
        if let testimonial = self.data["testimonial"] as? GKDictionary
        {
            let authorName = (self.data["first_name"] as! String).capitalized +  " " + (self.data["last_name"] as! String).capitalized
            var testimonialMessage = ""
            
            if let detail = testimonial["detail"] as? [String] {
                
                if detail.contains("1")
                {
                    testimonialMessage += (authorName + " was able to help.\n\n")
                }
                if detail.contains("2")
                {
                    testimonialMessage += (authorName + " is very skilled.\n\n")
                }
                if detail.contains("3")
                {
                    testimonialMessage += (authorName + " is a punctual person.\n\n")
                }
                
                if detail.contains("4")
                {
                    testimonialMessage += (authorName + " is such a friendly person.\n\n")
                }
                
                if detail.contains("5")
                {
                    testimonialMessage += ("I am grateful I met " + authorName + ".\n\n")
                }
                
                if detail.contains("6")
                {
                    testimonialMessage += (authorName + " was thoughtful.\n\n")
                }
                
                if detail.contains("7")
                {
                    testimonialMessage += (authorName + " was able to put my needs first..\n\n")
                }
                
                if detail.contains("8")
                {
                    testimonialMessage += (authorName + " had a kind demeanor.\n\n")
                }
                
                if detail.contains("9")
                {
                    testimonialMessage += (authorName + " was able to pass along helpful thoughts and prayers.\n\n")
                }
                
                if detail.contains("10")
                {
                    testimonialMessage += (authorName + " was able to take time out to go the extra mile for me.\n\n")
                }
            }
            
            TestimonialView.show(testimonialMessage)
        }
        else
        {
            self.delegate?.GKLeaveTestimonial(self.data)
        }
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary, detail: String, postType: Int, postStatus: Int)
    {
        self.data = data
        
        if ((postType == 1 || postType == 3) && postStatus == 4)
        {
            testimonialButton.isHidden = false
            
            if let _ = self.data["testimonial"] as? GKDictionary
            {
                testimonialButton.setTitle("View Testimonial", for: UIControlState())
            }
            else
            {
                testimonialButton.setTitle("Leave Testimonial", for: UIControlState())
            }
        }
        else
        {
            testimonialButton.isHidden = true
        }
        
        let author = data
        
        let nameAttrString = NSAttributedString(
            string: "\((author["first_name"] as! String).capitalized +  " " + (author["last_name"] as! String).capitalized): ",
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.black
            ]
        )
        let gratitudeAttrString = NSAttributedString(
            string: detail,
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Light, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.darkGray
            ]
        )
        
        let attrString = NSMutableAttributedString()
        attrString.append(nameAttrString)
        attrString.append(gratitudeAttrString)
        
        self.titleLabel.attributedText = attrString
        
        self.pictureImageView.loadImageWithUrl(author["photo_url"] as? String as NSString?)
        self.timestampLabel.text = secondsToFormatted(data["date_created"] as? Int)
    }
    
    @objc func tapImageView()
    {
        self.delegate?.GKShowProfile(self.data["id"] as! Int)
    }
}
