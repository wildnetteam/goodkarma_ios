
//
//  GKGratitudePrivacyViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/5/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKGratitudePrivacyViewController: GKViewController, UITableViewDataSource, UITableViewDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Properties -
  
  var titleSettings: [String] = []
  var gratitudeSelected: Int = -1
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Privacy for Gratitude"
    
    let cancelBarButton = UIBarButtonItem(
      title: "Cancel", style: .plain, target: self, action: #selector(self.cancel)
    )
//    cancelBarButton.tintColor = UIColor.baseColor()
    self.navigationItem.leftBarButtonItem = cancelBarButton
    
    let saveBarButton = UIBarButtonItem(
      title: "Save", style: .plain, target: self, action: #selector(self.save)
    )
//    saveBarButton.tintColor = UIColor.baseColor()
    self.navigationItem.rightBarButtonItem = saveBarButton
    
    self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "GratitudePrivacyCell")
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
//    self.tableView.separatorStyle = .None
    
    requestData()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return privacyOptions().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(withIdentifier: "GratitudePrivacyCell", for: indexPath)
    
    cell.textLabel?.text = self.privacyOptions()[indexPath.row]
    cell.selectionStyle = .none
    cell.tintColor = UIColor.baseColor()
    
    if (indexPath.row + 1) == gratitudeSelected
    {
      cell.textLabel?.textColor = UIColor.baseColor()
      cell.accessoryType = .checkmark
    }
    else
    {
      cell.textLabel?.textColor = UIColor.darkGray
      cell.accessoryType = .none
    }
    
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
  {
    
    if (indexPath.row + 1) != self.gratitudeSelected
    {
      self.gratitudeSelected = (indexPath.row + 1)
    }
    
    self.tableView.reloadData()
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return 50.0
  }
  
  // MARK: - Methods -
  
  @objc func cancel()
  {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
  
  func requestData()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.settingsGratitude(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res , string : "gratitude_privacy")
        
        if envelope.getSuccess() {
          
          if let data = envelope.getResource() as? [GKDictionary] {
            
            let info = data[0]
            self.gratitudeSelected = info["gratitude_privacy"] as! Int
            self.tableView.reloadData()
          }
        }
      }
    })
  }
  
  @objc func save() {
    
    var params = GKDictionary()
    params["token"]             = Static.sharedInstance.token() as AnyObject?
    params["gratitude_privacy"] = gratitudeSelected as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    print(params)
    GKServiceClient.call(GKRouterClient.settingsGratitude(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          self.navigationController?.dismiss(animated: true, completion: nil)
        }
      }
    })
  }
  
  func privacyOptions() -> [String] {
    
    return [
      "Only me",
      "Following",
      "Followers",
      "Public"
    ]
  }
}
