//
//  NotificationView.swift
//  Faveo
//
//  Created by Eliezer de Armas on 29/12/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit
import AudioToolbox

protocol NotificationDelegate: NSObjectProtocol
{
    func notificationDidTap(_ data: NSDictionary!)
}

class NotificationView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var faveoIcon: UIImageView!
    @IBOutlet weak var text: UILabel!
    
    var customText: String!
    var data: NSDictionary!
    weak var delegate: NotificationDelegate!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup()
    {
        view = Bundle.main.loadNibNamed("NotificationView", owner: self,
            options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.clipsToBounds = true
        view.frame = CGRectSetY(view.frame, y: -68)
        self.addSubview(view)
        self.backgroundColor = UIColor.clear
        
        // Faveo Icon
        self.faveoIcon.clipsToBounds = true
        self.faveoIcon.layer.cornerRadius = 5
    }
    
    func show()
    {
        AudioServicesPlayAlertSound(1315)
        
        if (self.customText != nil)
        {
            self.text.text = self.customText;
        }
        else
        {
           self.text.text = self.data?.value(forKeyPath: "aps.alert") as? String
        }
        
        UIView.animate(withDuration: 0.5, delay:0,
            options:UIViewAnimationOptions(),
            animations:
            {
                self.view.frame = CGRectSetY(self.view.frame, y: 0)
            },
            completion:
            { _ in
                delay(3) {
                        self.hide()
                }
        })
    }
    
    func hide()
    {
        UIView.animate(withDuration: 0.3, delay:0,
            options:UIViewAnimationOptions(),
            animations:
            {
                self.view.frame = CGRectSetY(self.view.frame, y: -68)
            },
            completion:
            { _ in
                self.removeFromSuperview()
        })
    }
    
    // MARK: - Actions
    
    @IBAction func tapAction(_ sender: AnyObject)
    {
        self.delegate.notificationDidTap(self.data)
        hide()
    }
}
