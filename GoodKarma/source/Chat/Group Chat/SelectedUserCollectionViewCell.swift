//
//  SelectedUserCollectionViewCell.swift
//  GoodKarma
//
//  Created by Anuj Jha on 04/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class SelectedUserCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var crossButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.layer.masksToBounds = true
    }

    @IBAction func crossButtonTapped(_ sender: UIButton) {
    }
    
    
}
