//
//  GKLoginViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/31/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKLoginViewController: UIViewController, NIAttributedLabelDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var fbButton: UIButton!
  
  @IBOutlet weak var accessAttributedLabel: NIAttributedLabel!
  @IBOutlet weak var termsAttributedLabel: NIAttributedLabel!
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    self.title = "Login"
    cleanBackButton()
    // fbButton.layer.cornerRadius = 5.0
    fbButton.imageView?.contentMode = .scaleAspectFit
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = .center
    
    // Access
    
    let accessToken1 = "Or, "
    let accessLink1 = "create an account"
    let accessToken2 = " or "
    let accessLink2 = "sign in"
    let accessToken3 = " with email"
    
    let countAccessToken1 = accessToken1.count
    let countAccessLink1 = accessLink1.count
    
    let accessAttrString = accessToken1 + accessLink1 + accessToken2 + accessLink2 + accessToken3
    
    accessAttributedLabel.highlightedLinkBackgroundColor = UIColor.clear
    accessAttributedLabel.attributedText = NSAttributedString(
      string: accessAttrString,
      attributes: [
        NSAttributedStringKey.paragraphStyle: paragraphStyle,
        NSAttributedStringKey.font: UIFont.fontRoboto(.Regular, size: 15),
        NSAttributedStringKey.foregroundColor: UIColor.white
      ]
    )
    accessAttributedLabel.attributesForLinks = [
      NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 15),
      NSAttributedStringKey.foregroundColor: UIColor.white,
      NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
    ]
    
    accessAttributedLabel.addLink(URL(string: "sign_up") , range: NSRange(location: countAccessToken1, length: countAccessLink1))
    
    let startNextLinkAccess = countAccessToken1 + countAccessLink1 + accessToken2.count
    let countAccessLink2 = accessLink2.count
    
    accessAttributedLabel.addLink(URL(string: "sign_in"), range: NSRange(location: startNextLinkAccess, length: countAccessLink2))
    
    // Terms
    
    let termsToken1 = "By using GoodKarms you agree to the "
    let termsLink1 = "Privacy Policy"
    let termsToken2 = " and "
    let termsLink2 = "Terms & Conditions"
    
    let countTermsToken1 = termsToken1.count
    let countTermsLink1 = termsLink1.count
    
    let termsString = termsToken1 + termsLink1 + termsToken2 + termsLink2
    
    termsAttributedLabel.highlightedLinkBackgroundColor = UIColor.clear
    termsAttributedLabel.attributedText = NSAttributedString(
      string: termsString,
      attributes: [
        NSAttributedStringKey.paragraphStyle: paragraphStyle,
        NSAttributedStringKey.font: UIFont.fontRoboto(.Regular, size: 13),
        NSAttributedStringKey.foregroundColor: UIColor.white
      ]
    )
    termsAttributedLabel.attributesForLinks = [
      NSAttributedStringKey.foregroundColor: UIColor.white,
      NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 15),
      NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
    ]
    
    termsAttributedLabel.addLink(URL(string: "privacy_policy"), range: NSRange(location: countTermsToken1, length: countTermsLink1))
    
    let startNextLinkterms = countTermsToken1 + countTermsLink1 + termsToken2.count
    let countTermsLink2 = termsLink2.count
    
    termsAttributedLabel.addLink(URL(string: "terms_conditions"), range: NSRange(location: startNextLinkterms, length: countTermsLink2))
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = true
  }
  
  // MARK: - Protocols
  // MARK: - Protocol NIAttributedLabelDelegate -
  
  func attributedLabel(_ attributedLabel: NIAttributedLabel!, didSelect result: NSTextCheckingResult!, at point: CGPoint) {
    
    if result.resultType == .link {
      
      let s = result.url!.absoluteString
      
      switch s {
        
      case "sign_in": goSignIn()
      case "sign_up": goSignUp()
      case "privacy_policy": goPrivacyPolicy()
      case "terms_conditions": goTerms()
      default: break
      }
    }
  }
  
  func attributedLabel(_ attributedLabel: NIAttributedLabel!, shouldPresent actionSheet: UIActionSheet!, with result: NSTextCheckingResult!, at point: CGPoint) -> Bool {
    
    return false
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionFacebook(_ sender: AnyObject) {
    
//    let alertController = UIAlertController(title: "Wait!", message: "Work in progress", preferredStyle: UIAlertControllerStyle.alert)
//    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
//    self.present(alertController, animated: false, completion: nil)
    
    self.view.endEditing(true)

    FacebookHandler.login(

      self.navigationController, callback: { (data: NSDictionary?) -> () in

        if data == nil {

          showConnectionError({ (type: NoInternetResultType) -> () in

            self.actionFacebook(0 as AnyObject)
          })
        }
        else {

          self.loginWithFacebook(data!)
        }
    })
  }
  
  // MARK: - Navigation -
  
  func goSignIn() {
    
    let signInController = GKSignInViewController(nibName: "GKSignInViewController", bundle: nil)
    self.navigationController?.pushViewController(signInController, animated: true)
  }
  
  func goSignUp() {
    
    let signUpController = GKSignUpViewController(nibName: "GKSignUpViewController", bundle: nil)
    self.navigationController?.pushViewController(signUpController, animated: true)
  }
  
  func goPrivacyPolicy() {
    
    let privacyViewController = GKPrivacyViewController(nibName: "GKPrivacyViewController", bundle: nil)
    self.navigationController?.pushViewController(privacyViewController, animated: true)
  }
  
  func goTerms() {
    //    self.navigationController?.navigationBarHidden = false
    
    let termsViewController = GKTermsViewController(nibName: "GKTermsViewController", bundle: nil)
    self.navigationController?.pushViewController(termsViewController, animated: true)
  }
  
  // MARK: - Methods -
  
  func loginWithFacebook(_ data: NSDictionary)
  {
    LoadingView.showLoadingView("Loading...",
                                parentView: self.navigationController?.view,
                                backColor: true)
    
    var params = [String: String]()
    params["first_name"]  = (data["first_name"] as? String)?.capitalized
    params["last_name"]   = (data["last_name"] as? String)?.capitalized
    params["facebook_id"] = data["id"] as? String
    params["uid"] = UIDevice.current.identifierForVendor!.uuidString
    params["email"] = data["email"] as? String
   // params["type"] = "Client"
    
    params["device_type"] = "ios" as String?

    let fromFacebook = true
    
    GKServiceClient.call(GKRouterAuth.signWithFacebook(), parameters:params as GKDictionary, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          let data = envelope.getResource() as? NSDictionary
            UserDefaults.standard.set((data?["id"] as AnyObject), forKey: "id") // added
            UserDefaults.standard.set((data?["facebook_id"] as AnyObject), forKey: "isFacebookId") // added

          let hasPhoto         = (data?["has_photo"] as! NSNumber).boolValue
          let hasProfile       = (data?["has_profile"] as! NSNumber).boolValue
          let hasPhoneVerified = (data?["has_phone_verified"] as! NSNumber).boolValue
          
          if (!hasProfile) {
            
            let completeInfo = GKRegistrationViewController(nibName: "GKRegistrationViewController",
              bundle: nil)
            completeInfo.data = data
            completeInfo.fromFacebook = fromFacebook
            self.navigationController?.pushViewController(completeInfo, animated: true)
          }
          else if !hasPhoneVerified {
            
            let confirmCodeController = GKConfirmCodeViewController(nibName: "GKConfirmCodeViewController",
              bundle: nil)
            confirmCodeController.data = data
            confirmCodeController.fromFacebook = fromFacebook
            self.navigationController?.pushViewController(confirmCodeController, animated: true)
          }
          else if (!hasPhoto) {
            
            let photoController = GKAddPhotoViewController(nibName: "GKAddPhotoViewController",
              bundle: nil)
            photoController.data = data
            photoController.fromFacebook = fromFacebook
            self.navigationController?.pushViewController(photoController, animated: true)
          }
          else {
            var recovery = false
            
            if let comeback = envelope.getResource()["recovery"] as? Bool
            {
              recovery = comeback
            }
            
            Static.sharedInstance.saveUserProfile(envelope.getResource() as? NSDictionary)
            Static.sharedInstance.saveLogin(true)
            (UIApplication.shared.delegate as! AppDelegate).showHome(self.navigationController!, loginOrRegister: true, recovery: recovery) // fromFacebook
          }
        }
        else {
          
          showCommonError({ (type: NoInternetResultType) -> () in
            
            self.actionFacebook(0 as AnyObject)
          })
        }
      }
      else {
        
        showConnectionError({ (type: NoInternetResultType) -> () in
          
          self.actionFacebook(0 as AnyObject)
        })
      }
    })
  }
}
