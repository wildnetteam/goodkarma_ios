//
//  AppDelegate.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/17/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import AVFoundation
import GoogleMaps
import Photos
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NotificationDelegate
{
    // MARK: - Properties -
    
    var window: UIWindow?
    var container: GKMenuViewcontroller!
    var isLogin: Bool?
    var videoLink = String()
    var initialNavigation:UINavigationController?
    
    // MARK: - Protocols
    // MARK: - Protocol NotificationDelegate -./
    
    class func appDelegate() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func notificationDidTap(_ data: NSDictionary!)
    {
        notificationHandler(data)
    }
    
    // MARK: - Protocol UIApplicationDelegate -
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        NSLog("### Running FB SDK Version: %@", FBSDKSettings.sdkVersion())
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        GMSServices.provideAPIKey(Google_APIKey)

        AFNetworkReachabilityManager.shared().startMonitoring()
        OldVimeoUploader.sharedInstance.applicationDidFinishLaunching() // Ensure init is called on launch

//        let msgRef = Database.database().reference(withPath: "Messages")
//        msgRef.keepSynced(true)
        
        container = GKMenuViewcontroller()
        
        // Load data saved in cache
        Static.sharedInstance.loadInitial()
        
        // Window
        window?.rootViewController = initialRootViewController()
        window?.makeKeyAndVisible()
        
        // Notification
//        if let launchOpts = launchOptions
//        {
//            if let userInfo = launchOpts[UIApplicationLaunchOptionsKey.remoteNotification]
//            {
//                if (Static.sharedInstance.isLogin())
//                {
//                    notificationHandler(userInfo as! NSDictionary)
//                }
//            }
//        }
        
        // Notification
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
        
        // Removed
//        showLocalNotification()
        do { if #available(iOS 10.0, *) {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with:AVAudioSessionCategoryOptions.allowAirPlay)
        } else {
            // Fallback on earlier versions
            }}
        catch{}
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        if let userId = (Static.sharedInstance.userId()){
            FireBaseManager.defaultManager.setUserOnlineOfflineStatus(id: "\(userId)", status: 0)
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if let userId = (Static.sharedInstance.userId()){
            FireBaseManager.defaultManager.setUserOnlineOfflineStatus(id: "\(userId)", status: 1)
        }
    }
    

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication:  sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let cleanToken = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        if Static.sharedInstance.isLogin() {
            var params = [String: AnyObject]()
            params["token"] = Static.sharedInstance.token() as AnyObject?
            params["device_token"] = cleanToken as AnyObject?
            params["device_type"] = "ios" as AnyObject
            //      #if DEBUG
            //        params["service"] = 3
            //      #else
            params["service"] = 1 as AnyObject?
            //      #endif
            
            print("****REGISTER")
            print(params)
            
            GKServiceClient.call(GKRouterAuth.deviceToken(), parameters: params, callback: {
                (response: AnyObject?) -> () in
                
                //   print(response!)
            })
        }
        else {
            
            Static.sharedInstance.saveDeviceToken(cleanToken)
        }
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Failed to get token; error: %@", error)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any])
    {
//        if (Static.sharedInstance.isLogin())
//        {
//            
//            GKNotificationStore.number_messages      = userInfo["message"] as! Int
//            GKNotificationStore.number_notifications = userInfo["notification"] as! Int
//            
//            var isJobCompleted = false;
//            
//            if let type = userInfo["type"] as? Int
//            {
//                // Message
//                if type == 1
//                {
//                    GKNotificationStore.new_message_user_id = userInfo["user"] as! Int
//                }
//                else if type == 7
//                {
//                    isJobCompleted = true
//                }
//            }
//            
//            if (application.applicationState == .active)
//            {
//                if (isJobCompleted)
//                {
//                    let customText = "Congratulations \(String(describing: (userInfo["object"] as! NSDictionary)["helper_name"])) has marked this as complete! Click here to confirm, and leave gratitude!"
//                    showNotificationView(userInfo as NSDictionary, customText: customText)
//                }
//                else
//                {
//                    showNotificationView(userInfo as NSDictionary)
//                }
//            }
//            else
//            {
//                notificationHandler(userInfo as NSDictionary)
//            }
//        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification)
    {
        self.application(application, didReceiveRemoteNotification: notification.userInfo!)
        application.cancelAllLocalNotifications()
    }
        
    // MARK: - Methods -
    
    func updateBadgeCount()
    {
        // TODO: check this
        //        let badgeCount = (Static.sharedInstance.menuCounter?["shouts_unread_events"] as! NSNumber).integerValue + (Static.sharedInstance.menuCounter?["messages"] as! NSNumber).integerValue
        
        UIApplication.shared.applicationIconBadgeNumber = GKNotificationStore.totalNotifications()
        
        //        NSNotificationCenter.defaultCenter().postNotificationName(NotificationType.UPDATE_BADGE, object: nil)
    }
    
    func showHello() {
        
        print("SHOW HELLO")
        
    }
    
    // MARK: - Custom -
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void)
    {
        if OldVimeoUploader.sharedInstance.descriptorManager.handleEventsForBackgroundURLSession(identifier: identifier, completionHandler: completionHandler) == false
        {
            assertionFailure("Unhandled background events")
        }
    }
    
    private func requestCameraRollAccessIfNecessary()
    {
        PHPhotoLibrary.requestAuthorization { status in
            switch status
            {
            case .authorized:
                print("Camera roll access granted")
            case .restricted:
                print("Unable to present camera roll. Camera roll access restricted.")
            case .denied:
                print("Unable to present camera roll. Camera roll access denied.")
            default:
                // place for .NotDetermined - in this callback status is already determined so should never get here
                break
            }
        }
    }
    
    func setDetaultSettings() {
        let plistPath = Bundle.main
            .path(forResource: "DefaultSettings", ofType: "plist")
        
        let dic = NSDictionary(contentsOfFile: plistPath!)
        UserDefaults.standard
            .register(defaults: dic as! [String : AnyObject])
    }
    
    func initialRootViewController() -> UIViewController
    {
        guard Static.sharedInstance.isLogin() else {
            
            var controller: UIViewController
            
            //      let userDefaults = NSUserDefaults.standardUserDefaults()
            
            //      if !userDefaults.boolForKey("hasRunBefore") {
            //
            //        userDefaults.setBool(true, forKey: "hasRunBefore")
            //        userDefaults.synchronize()
            
            controller = GKWalkthroughViewController(nibName: "GKWalkthroughViewController", bundle: nil)
            //      }
            //      else {
            //
            //        controller = GKLoginViewController(nibName: "GKLoginViewController", bundle: nil)
            //      }
            
             initialNavigation = UINavigationController(rootViewController: controller)
            initialNavigation?.isNavigationBarHidden = true
            
            return  initialNavigation!
        }
        
        return container
    }
    
    func showHome(_ currentController: UIViewController, loginOrRegister: Bool, recovery: Bool)
    {
        let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        container.view.addSubview(overlayView)
        
        self.window?.rootViewController = container
        container.showHome(loginOrRegister, recovery: recovery)
        
        UIView.animate(
            withDuration: 0.5, delay:0,
            options:UIViewAnimationOptions.transitionCrossDissolve,
            animations: {
                
                overlayView.alpha = 0
                
        }, completion: { _ in
            
            overlayView.removeFromSuperview()
            currentController.removeFromParentViewController()
        })
        
        let deviceToken = Static.sharedInstance.deviceToken()
        
        if deviceToken != nil && (deviceToken?.count)! > 0 {
            
            var params = [String: AnyObject]()
            params["token"] = Static.sharedInstance.token() as AnyObject?
            params["device_token"] = deviceToken as AnyObject?
            params["service"] = 1 as AnyObject?
            
            print("****SHOW HOME")
            print(params)
            
            GKServiceClient.call(GKRouterAuth.deviceToken(), parameters:params, callback: {
                (response: AnyObject?) -> () in
                
                //print(response!)
            })
        }
    }
    
    func showLogin(_ currentController: UIViewController) {
        
        //    let walkthrough = GKWalkthroughViewController(nibName: "GKWalkthroughViewController", bundle: nil)
        //    walkthrough.moveToEnd = true
        
        let loginController = GKLoginViewController(nibName: "GKLoginViewController", bundle: nil)
        //    loginController.navigationController?.navigationBarHidden = true
        
        
        let controller = UINavigationController(rootViewController: loginController)
        controller.isNavigationBarHidden = true
        
        let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
        controller.view.addSubview(overlayView)
        self.window?.rootViewController = controller
        
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions.transitionCrossDissolve,
                                   animations:
            {
                overlayView.alpha = 0
            },
                                   completion:
            { _ in
                overlayView.removeFromSuperview()
                currentController.removeFromParentViewController()
        })
    }
    
    func showHistory()
    {
        container.showHistory()
    }
    
    func showDetailPost(_ userInfo: NSDictionary, isParticipant: Bool)
    {
        let object = userInfo["object"]
        let type = (object as! NSDictionary)["type"] as! Int
        
            //if (type == 1 || type == 2 || type == 3)
        //{
        container.showDetailPost((object as! NSDictionary)["id"] as! Int, type: type,
                                 isParticipant: isParticipant)
        //}
    }
    
    func showTestimonials()
    {
        container.showTestimonials()
    }
    
    func showHistoryWithTestimonial(_ userInfo: NSDictionary, byClient: Bool)
    {
        let object = userInfo["object"]
        let name = (object as! NSDictionary)["helper_name"] as! String
        let relatedId = (object as! NSDictionary)["related_id"] as! Int
        let type = (object as! NSDictionary)["type"] as! Int
        container.showHistoryWithTestimonial(name, relatedId: relatedId,
                                             type: type, byClient: byClient)
    }
    
    func showProfile(_ userInfo: NSDictionary)
    {
        container.showProfile((userInfo["object"] as! NSDictionary)["receiver_id"] as! Int)
    }
    
    func showMessages()
    {
        container.showMessages()
    }
    
    func showConversation(_ userID: Int, fullName: String) {
        
        container.showConversation(userID, fullName: fullName)
    }
    
    func showHistoryRequestedPost(_ dataPost: GKDictionary, typePost: Int)
    {
        container.showHistory(dataPost, typePost: typePost)
    }
    
    func showLocalNotification()
    {
        let notification = UILocalNotification()
        notification.fireDate = Date(timeInterval: 45, since: Date())
        notification.alertBody = "Test Notification"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["object": ["object_id": 88, "type": 3],
                                 "type": 17,
                                 "message": 0,
                                 "notification": 0]
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    // MARK: - Handle Push -
    
    
    func showNotificationView(_ userInfo: NSDictionary)
    {
        showNotificationView(userInfo, customText: nil);
    }
    
    func showNotificationView(_ userInfo: NSDictionary, customText: String!)
    {
        let windowFrame = self.window?.frame
        let notificationView = NotificationView(frame: CGRect(x: 0, y: 0,
            width: windowFrame!.width, height: 68))
        notificationView.customText = customText;
        notificationView.data = userInfo
        notificationView.delegate = self
        self.window?.addSubview(notificationView)
        notificationView.show()
    }
    
    func notificationHandler(_ userInfo: NSDictionary)
    {
        let type = userInfo["type"] as! Int
        
        // messages
        if type == 1
        {
            showMessages()
        }
        
        // Join Serve
        if type == 2
        {
            showHistory()
        }
        
        // Accept Join Serve
        if type == 3
        {
            showHistory()
        }
        
        // Send Positivity
        if type == 4
        {
            showHistory()
        }
        
        // Join Help
        if type == 5
        {
            showHistory()
        }
        
        // Completed Job
        if type == 7
        {
            showHistoryWithTestimonial(userInfo, byClient: false)
        }
        
        // New Connection
        if type == 14
        {
            showProfile(userInfo)
        }
        
        // Finalize
        if type == 15
        {
            showTestimonials()
        }
        
        if (type == 16)
        {
            showDetailPost(userInfo, isParticipant: false)
        }
        
        // Finalize
        if (type == 17)
        {
           showDetailPost(userInfo, isParticipant: true)
        }
    }
}
