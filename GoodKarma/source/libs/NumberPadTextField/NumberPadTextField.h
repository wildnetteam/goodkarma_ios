////
////  NumberPadTextField.h
////  Hawkr
////
////  Created by Eliezer de Armas on 26/6/16.
////  Copyright © 2016 TeclaLabs. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//@interface NumberPadTextField : UITextField
//
//- (void)addAccessoryWithText:(NSString *)text;
//
//@end
