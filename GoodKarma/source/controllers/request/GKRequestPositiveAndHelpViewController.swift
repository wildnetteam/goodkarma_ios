//
//  GKRequestPositiveAndHelpViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/15/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import VimeoUpload
import Alamofire
import MobileCoreServices
import Photos
import VimeoNetworking
import FBSDKShareKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class GKRequestPositiveAndHelpViewController: GKViewController, GKManageSkillsDelegate, UITextFieldDelegate, UITextViewDelegate, GKPostFacebookDelegate,
    FBSDKSharingDelegate, UICollectionViewDelegate , UICollectionViewDataSource, UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var positivityScrollView: UIScrollView!
    @IBOutlet weak var helpScrollView: UIScrollView!
    
    // Positivity
    @IBOutlet weak var positivityFormView: UIView!
    @IBOutlet weak var positivityCounterLabel: UILabel!
    @IBOutlet weak var positivityContentField: UITextView!
    
    // Help
    @IBOutlet weak var helpFormView: UIView!
    
    @IBOutlet weak var helpTitleContainer: UIView!
    @IBOutlet weak var helpTitleField: UITextField!
    
    @IBOutlet weak var helpDificultySlider: UISlider!
    @IBOutlet weak var helpDifficultyLabel: UILabel!
    
    @IBOutlet weak var helpSkillsContainer: UIView!
    
    @IBOutlet weak var helpZipDateContainer: UIView!
    @IBOutlet weak var helpZipField: UITextField!
    @IBOutlet weak var helpDateCompletionField: UITextField!
    @IBOutlet weak var helpDateCompletionView: UIView!
    
    @IBOutlet weak var helpBodyContainer: UIView!
    @IBOutlet weak var helpBodyContentField: UITextView!
    @IBOutlet weak var helpBodyPlaceholderLabel    : UILabel!
    
    @IBOutlet weak var helpPhysicalImageView: UIImageView!
    @IBOutlet weak var helpVirtualImageView: UIImageView!
    
    @IBOutlet weak var helpDistanceField: UITextField!
    
    @IBOutlet weak var helpDistanceView: UIView!
    
    @IBOutlet weak var helpSkillsContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonCamera: UIButton!
    @IBOutlet weak var buttonGallery: UIButton!
    @IBOutlet weak var buttonVideo: UIButton!
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    var imageArray : [UIImage] = []
    var videoArray = NSMutableArray()
    var collectionViewArray : [UIImage] = []
    var tagArray = NSMutableArray()

    @IBOutlet weak var buttonCameraPositivity: UIButton!
    @IBOutlet weak var buttonGalleryPositivity: UIButton!
    @IBOutlet weak var buttonVideoPositivity: UIButton!
    @IBOutlet weak var collectionViewAttachmentsPositivity: UICollectionView!
    
    var imageArrayPositivity : [UIImage] = []
    var videoArrayPositivity = NSMutableArray()
    var collectionViewArrayPositivity : [UIImage] = []
    var tagArrayPositivity = NSMutableArray()

    let picker = UIImagePickerController()

    // MARK: - Properties -
    
    var isRequestPositiveSelected = false
    var barTitle: String!
    var isOnlyPositive = false
    weak var delegate: GKCreatePostDelegate!
    
    var showImageScrollView:ShowImageScrollView?

    // Positivity
    
    var positivityErrorView: ErrorView!
    let positivityContentLimit = 80
    
    // Help
    
    var helpErrorView: ErrorView!
    
    var helpSkillsSelected: [GKModelSkill] = []
    
    var helpDifficultyValue = 3
    
    var helpManageSkillView: GKManageSkillsView!
    
    var helpCompletionDate =  Date()
    
    var helpIsPhysicalSelected = true
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        videoUrlResponse = ""
        
        if (self.barTitle != nil)
        {
            self.title = self.barTitle
        }
        else
        {
            self.title = "Request"
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(GKRequestPositiveAndHelpViewController.cancelRequest)
        )
        
        // Difficulty
        helpDificultySlider.tintColor = UIColor.darkGray
        helpDificultySlider.minimumValue = 1.0
        helpDificultySlider.maximumValue = 5.0
        helpDificultySlider.setValue(Float(helpDifficultyValue), animated: false)
        
        helpDifficultyLabel.text = difficultyToString(helpDifficultyValue)
        
        // Content
        helpBodyContentField.layer.borderColor = UIColor.superLightGray().cgColor
        helpBodyContentField.layer.cornerRadius = 5.0
        helpBodyContentField.layer.borderWidth = 1.0
        
        updateViews()
        updatedKindHelp()
        
        // Error views
        self.positivityErrorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
        self.helpErrorView       = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
        
        // Forms
        self.positivityFormView.addSubview(self.positivityErrorView)
        self.helpFormView.addSubview(self.helpErrorView)
        
        // Zip
        self.helpZipField.addAccessoryView("Done")
        
        // Date
        helpDateCompletionView.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(self.showSelectDates)
            )
        )
        
        // Distance
        self.helpDistanceField.addAccessoryView("Done")
        
        helpManageSkillView = GKManageSkillsView(frame: helpSkillsContainer.bounds, editable: true, delegate: self)
        helpSkillsContainer.addSubview(helpManageSkillView)
        
        if (self.isOnlyPositive)
        {
            self.segmentedControl.isHidden = true
            self.segmentedControl.selectedSegmentIndex = 0
            self.isRequestPositiveSelected = true
            updateViews()
        }
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionViewAttachments.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
        collectionViewAttachmentsPositivity.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        
        if videoUrlResponse != "" {
            if isRequestPositiveSelected
            {
                tagArrayPositivity.add(collectionViewArrayPositivity.count)
                collectionViewArrayPositivity.append(UIImage(named: "PlayButtonOverlayLarge")!)
                collectionViewAttachmentsPositivity.reloadData()
                videoArrayPositivity.add(videoUrlResponse)
                videoUrlResponse = ""
                print(videoArrayPositivity)
            }
            else{
                 tagArray.add(collectionViewArray.count)
                collectionViewArray.append(UIImage(named: "PlayButtonOverlayLarge")!)
                collectionViewAttachments.reloadData()
                videoArray.add(videoUrlResponse)
                videoUrlResponse = ""
                print(videoArray)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
//        if isRequestPositiveSelected
//        {
//            positivityContentField.becomeFirstResponder()
//        }
        
        updateSkillsView()
        
        self.helpScrollView.contentSize = CGSize(width: self.helpScrollView.frame.width, height: self.helpFormView.bottomSide + 30.0)
    }
    
    
    // MARK: - Protocols
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        helpSkillsSelected = helpSkillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView()
    {
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.helpSkillsSelected
        skillsController.creatingPos = true
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill])
    {
        // Nothing for now
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == helpDistanceField
        {
            textField.resignFirstResponder()
            return false
        }
        else if textField == helpZipField
        {
            textField.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == helpZipField
        {
            let postString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if postString.countPlainCharacters() <= 5
            {
                return true
            }
            else if string.countPlainCharacters() == 0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return true
        }
    }
    
    // MARK: - Protocol UITextViewDelegate -
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == self.helpBodyContentField
        {
            self.helpBodyPlaceholderLabel.isHidden = true
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.helpBodyContentField
        {
            self.helpBodyPlaceholderLabel.isHidden = textView.text.count > 0
        }
        else if textView == self.positivityContentField
        {
            refreshPositivityCount()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == self.helpBodyContentField
        {
            self.helpBodyPlaceholderLabel.isHidden = textView.text.countPlainCharacters() > 0
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionBottomButton(_ sender: AnyObject)
    {
        self.positivityErrorView.hide()
        self.helpErrorView.hide()
        
        self.view.endEditing(true)
        
        // Positivity
        if isRequestPositiveSelected
        {
            if validatePositivity() {
                
                LoadingView.showLoadingView(
                    "Loading...",
                    parentView: self.navigationController?.view, backColor: true
                )
                
                var params = getPositivityParams()
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    if self.videoArrayPositivity.count > 0 {
                        
                        var videoInfo  = ""
                        for i in 0 ..< (self.videoArrayPositivity.count) {
                            if i > 0{
                                videoInfo +=  "," + (self.videoArrayPositivity[i] as! String)
                            }
                            else{
                                videoInfo +=  self.videoArrayPositivity[i] as! String
                            }
                        }
                        let info = "\(videoInfo)"
                        params["videos"] = info as AnyObject?
                    }
                    for (key, value) in params {
                        multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                    }
                    for i in 0 ..< (self.imageArrayPositivity.count) {
                        
                        if let imageData = UIImageJPEGRepresentation(self.imageArrayPositivity[i] , 0.6)
                        {
                            multipartFormData.append(imageData, withName: "image[]", fileName: "image\(i).jpeg", mimeType: "image/png")
                        }
                    }
                    print(params)
                }, to: GKRouterClient.requestPositivity(), method: .post, headers: nil,
                   encodingCompletion: {
                    
                    encodingResult in
                    switch encodingResult
                    {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            LoadingView.hideLoadingView(self.navigationController?.view)
                            print(response.result.value!)
                            if response.result.value != nil
                            {
                                self.imageArray.removeAll()
                                if (self.delegate != nil)
                                {
                                    self.delegate.createPostDidFinish()
                                }
                                self.showPostFacebook(1)
                            }
                        }
                        break
                        
                    case .failure(let encodingError):
                        LoadingView.hideLoadingView(self.navigationController?.view)
                        print(encodingError)
                        showCommonError({ (type: NoInternetResultType) -> () in
                            self.actionBottomButton(0 as AnyObject)
                        })
                        break
                    }
                })
            }
        }
        else
        {
            if validateHelp()
            {
                LoadingView.showLoadingView(
                    "Loading...",
                    parentView: self.navigationController?.view, backColor: true
                )
                
                var params = getHelpParams()
                print(params)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    if self.videoArray.count > 0 {
                        
                        var videoInfo  = ""
                        for i in 0 ..< (self.videoArray.count) {
                            if i > 0{
                                videoInfo +=  "," + (self.videoArray[i] as! String)
                            }
                            else{
                                videoInfo +=  self.videoArray[i] as! String
                            }
                        }
                        let info = "\(videoInfo)"
                        params["videos"] = info as AnyObject?
                    }
                    for (key, value) in params {
                        multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                    }
                    for i in 0 ..< (self.imageArray.count) {
                        
                        if let imageData = UIImageJPEGRepresentation(self.imageArray[i] , 0.6)
                        {
                            multipartFormData.append(imageData, withName: "image[]", fileName: "image\(i).jpeg", mimeType: "image/png")
                        }
                    }
                    print(params)
                }, to: GKRouterClient.createHelpRequest(), method: .post, headers: nil,
                   encodingCompletion: {
                    
                    encodingResult in
                    switch encodingResult
                    {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            LoadingView.hideLoadingView(self.navigationController?.view)
                            print(response.result.value!)
                            if response.result.value != nil
                            {
                                self.imageArray.removeAll()
                                if (self.delegate != nil)
                                {
                                    self.delegate.createPostDidFinish()
                                }
                                self.showPostFacebook(2)
                            }
                        }
                        break
                        
                    case .failure(let encodingError):
                        LoadingView.hideLoadingView(self.navigationController?.view)
                        print(encodingError)
                        showCommonError({ (type: NoInternetResultType) -> () in
                           self.actionBottomButton(0 as AnyObject)
                        })
                        break
                    }
                })
            }
        }
    }
    
    @IBAction func actionSelectPhysical(_ sender: AnyObject)
    {
        guard !helpIsPhysicalSelected else
        {
            return
        }
        
        self.helpIsPhysicalSelected = !self.helpIsPhysicalSelected
        updatedKindHelp()
    }
    
    @IBAction func actionSelectVirtual(_ sender: AnyObject)
    {
        guard helpIsPhysicalSelected else
        {
            return
        }
        
        self.helpIsPhysicalSelected = !self.helpIsPhysicalSelected
        updatedKindHelp()
    }
    
    @IBAction func actionSlider(_ sender: UISlider)
    {
        if sender == helpDificultySlider
        {
            helpDifficultyValue = Int(sender.value)
            helpDifficultyLabel.text = difficultyToString(helpDifficultyValue)
            
            helpDificultySlider.setValue(Float(helpDifficultyValue), animated: false)
        }
    }
    
    @IBAction func actionSwitchViews(_ sender: UISegmentedControl)
    {
        isRequestPositiveSelected = (sender.selectedSegmentIndex == 0)
        
        updateViews()
    }
    
    @IBAction func buttonCameraClicked(_ sender: Any) {
        self.pickFromCamera()
    }
    
    @IBAction func buttonGalleryClicked(_ sender: Any) {
        self.pickFromGallery()
    }
    
    @IBAction func buttonVideoClicked(_ sender: Any) {
        self.requestCameraRollAccessIfNecessary()
        let alertController = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        picker.navigationBar.tintColor = UIColor.black;
        
        let objcancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(objcancel)
        
        let objCamera = UIAlertAction(title: "Record video", style: .default) { (action:UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.sourceType = .camera
                self.picker.videoMaximumDuration = 60
                self.picker.delegate = self
                self.picker.mediaTypes = NSArray(objects: kUTTypeMovie) as! [String]
                self.present(self.picker, animated: true, completion: nil)
            }
        }
        alertController.addAction(objCamera)
        
        let objGallery = UIAlertAction(title: "Choose Existing", style: .default) { (action:UIAlertAction!) in
            self.vimeoInitialization()
        }
        alertController.addAction(objGallery)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    private func requestCameraRollAccessIfNecessary()
    {
        PHPhotoLibrary.requestAuthorization { status in
            switch status
            {
            case .authorized:
                print("Camera roll access granted")
            case .restricted:
                print("Unable to present camera roll. Camera roll access restricted.")
            case .denied:
                print("Unable to present camera roll. Camera roll access denied.")
            default:
                // place for .NotDetermined - in this callback status is already determined so should never get here
                break
            }
        }
    }
    
    @IBAction func buttonCameraPositivityClicked(_ sender: Any) {
        self.pickFromCamera()
    }
    
    @IBAction func buttonGalleryPositivityClicked(_ sender: Any) {
        self.pickFromGallery()
    }
    
    @IBAction func buttonVideoPositivityClicked(_ sender: Any) {
        self.requestCameraRollAccessIfNecessary()
        let alertController = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        picker.navigationBar.tintColor = UIColor.black;
        
        let objcancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(objcancel)
        
        let objCamera = UIAlertAction(title: "Record video", style: .default) { (action:UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.sourceType = .camera
                self.picker.videoMaximumDuration = 60
                self.picker.delegate = self
                self.picker.mediaTypes = NSArray(objects: kUTTypeMovie) as! [String]
                self.present(self.picker, animated: true, completion: nil)
            }
        }
        alertController.addAction(objCamera)
        
        let objGallery = UIAlertAction(title: "Choose Existing", style: .default) { (action:UIAlertAction!) in
            self.vimeoInitialization()
        }
        alertController.addAction(objGallery)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    // MARK: - Vimeo Upload
    
    func vimeoInitialization(){
        let cameraRollViewController = CameraRollViewController(nibName: BaseCameraRollViewController.NibName, bundle:Bundle.main)
        
        let cameraNavController = UINavigationController(rootViewController: cameraRollViewController)
        cameraNavController.view.backgroundColor = UIColor.white
        
        self.present(cameraNavController, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePicker Methods
    
    func pickFromCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func pickFromGallery()
    {
        let imagePicker = UIImagePickerController()
        //  imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = (info[UIImagePickerControllerEditedImage] as? UIImage) {
            if isRequestPositiveSelected{
                imageArrayPositivity.append(pickedImage)
                collectionViewArrayPositivity.append(pickedImage)
                collectionViewAttachmentsPositivity.reloadData()
            }
            else{
                imageArray.append(pickedImage)
                collectionViewArray.append(pickedImage)
                collectionViewAttachments.reloadData()
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let pickedVideo = (info[UIImagePickerControllerMediaURL] as? URL)
            self.dismiss(animated: true, completion: nil)
            
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: pickedVideo!)
            }) { saved, error in
                if saved {
                    self.didSelectVideo( self.PHAssetForFileURL(url: pickedVideo!))
                }
            }
        }

    }
    
    func PHAssetForFileURL(url: URL) -> VIMPHAsset {
        
        let imageRequestOptions = PHVideoRequestOptions()
        imageRequestOptions.version = .current
        imageRequestOptions.deliveryMode = .fastFormat
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        let fetchResult = PHAsset.fetchAssets(with: .video, options: options).lastObject
        
        var assets = PHAsset()
        assets = fetchResult!
        
        PHImageManager().requestAVAsset(forVideo: fetchResult!, options: nil, resultHandler: { (avurlAsset, audioMix, dict) in
            let newObj = avurlAsset as! AVURLAsset
            print(newObj.url)
            PHPhotoLibrary.shared().performChanges({
                let videoAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: [newObj.url], options: nil)
                PHAssetChangeRequest.deleteAssets(videoAssetToDelete)
            }, completionHandler: {success, error in
                print(success ? "Deleted Successfully!" : error! )
            })
        })
        
        return (VIMPHAsset(phAsset: assets))
    }
    
    func didSelectVideo(_ asset: VIMPHAsset)
    {
        let viewController = VideoSettingsViewController(asset: asset)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.backgroundColor = UIColor.white
        self.present(navigationController, animated: true, completion: nil)
        //topMostController().present(viewController, animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDelegate Methods
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
         if isRequestPositiveSelected{
            return collectionViewArrayPositivity.count
        }
         else{
            return collectionViewArray.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
        if isRequestPositiveSelected{
            if (collectionViewArrayPositivity[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
                cell.imageView.contentMode = .center
            }
            else{
                cell.imageView.contentMode = .scaleToFill
            }
            cell.imageView.image = collectionViewArrayPositivity[indexPath.row]
        }
        else{
            if (collectionViewArray[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
                cell.imageView.contentMode = .center
            }
            else{
                cell.imageView.contentMode = .scaleToFill
            }
            cell.imageView.image = collectionViewArray[indexPath.row]
        }
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isRequestPositiveSelected{
            if (collectionViewArrayPositivity[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
                let privacyViewController = GKWebViewController(nibName: "GKWebViewController", bundle: nil)
                for (index, tag) in (tagArrayPositivity.enumerated()) {
                    if (tag as! Int) == indexPath.row{
                        privacyViewController.url = videoArrayPositivity.object(at: index) as! String
                    }
                }
                self.navigationController?.pushViewController(privacyViewController, animated: true)
            }
            else{
                addImageScrollView(image:  (collectionViewArrayPositivity[indexPath.row]))
            }
        }
        else{
            if (collectionViewArray[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
                let privacyViewController = GKWebViewController(nibName: "GKWebViewController", bundle: nil)
                for (index, tag) in (tagArray.enumerated()) {
                    if (tag as! Int) == indexPath.row{
                        privacyViewController.url = videoArray.object(at: index) as! String
                    }
                }
                self.navigationController?.pushViewController(privacyViewController, animated: true)
            }
            else{
                addImageScrollView(image:  (collectionViewArray[indexPath.row]))
            }
        }
   
    }
    // MARK: - Methods -
    
    @objc func cancelRequest()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func getPositivityParams() -> GKDictionary
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["desc"]  = positivityContentField.text?.encodeEmoji() as AnyObject?
        
        return params
    }
    
    func getHelpParams() -> GKDictionary
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["title"] = helpTitleField.text?.encodeEmoji() as AnyObject?
        params["desc"]  = helpBodyContentField.text?.encodeEmoji() as AnyObject?
        params["difficulty"] = helpDifficultyValue as AnyObject?
        
        if helpZipField.text?.count > 1
        {
            params["zip_code"] = helpZipField.text as AnyObject?
        }
        
        params["skills"] = "\((helpSkillsSelected.map{ $0.uid! }))"  as AnyObject //helpSkillsSelected.map{ $0.uid } as AnyObject?
        
        if let indexElement = (helpSkillsSelected.map{ $0.uid }.index(of: 63))
        {
            params["other"] = helpSkillsSelected[indexElement].otherText as AnyObject?
        }
        
        params["help_type"] = helpIsPhysicalSelected ? (1 as AnyObject?) : (2 as AnyObject?) 
        
        // Distance
        let distanceValueOptional: Int? = Int(helpDistanceField.text!)
        
        if let distanceValue = distanceValueOptional, distanceValue > 0
        {
            params["distance"] = distanceValue as AnyObject?
        }
        else
        {
            params["distance"] = 1000 as AnyObject?
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        params["date_completion"] = formatter.string(from: self.helpCompletionDate) as AnyObject?
        
        return params
    }
    
    func refreshPositivityCount()
    {
        positivityCounterLabel.text = "\(positivityContentLimit - positivityContentField.text.count)"
    }
    
    @objc func showSelectDates()
    {
        self.view.endEditing(true)
        self.helpErrorView.hide()
        
        let selectAction = RMAction<UIDatePicker>(title: "Select", style: .done) { controller in
            
            if let dateController = controller as? RMDateSelectionViewController {
                
                let picker = dateController.datePicker
                self.helpCompletionDate = (picker?.date)!
                
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.helpDateCompletionField.text = formatter.string(from: self.helpCompletionDate)
            }
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .cancel, andHandler: nil)
        
        let pickerController = RMDateSelectionViewController(style: .default, title: nil, message: nil, select: selectAction , andCancel: cancelAction as! RMAction<UIDatePicker>?)
        
        pickerController?.disableBlurEffects = true
        pickerController?.datePicker.datePickerMode = .date;
        pickerController?.datePicker.date = helpCompletionDate
        pickerController?.datePicker.minimumDate = Date()
        
        self.present(pickerController!, animated: true, completion: nil)
    }
    
    func validatePositivity() -> Bool
    {
        let baseY = self.positivityErrorView.frame.size.height;
        
        if self.positivityContentField.text!.count < 10
        {
            self.positivityErrorView.showInPos(
                CGPoint(
                    x: self.positivityContentField.frame.minX - 8,
                    y: self.positivityContentField.frame.minY - baseY
                ),
                text: "Description should contain at least 10 characters."
            )
            
            return false;
        }
        
        return true
    }
    
    func validateHelp() -> Bool
    {
        let baseY = self.helpErrorView.frame.size.height;
        
        if self.helpTitleField.text!.count < 5
        {
            self.helpErrorView.showInPos(
                CGPoint(
                    x: self.helpTitleField.frame.minX - 8,
                    y: self.helpTitleContainer.convert(
                        self.helpTitleField.frame, to: self.helpFormView).minY - baseY
                ),
                text: "Title should contain at least 5 characters."
            )
            
            return false;
        }
        
        if self.helpSkillsSelected.count == 0
        {
            self.helpErrorView.showInPos(
                CGPoint(
                    x: self.helpSkillsContainer.frame.minX,
                    y: self.helpSkillsContainer.frame.minY - baseY
                ),
                text: "Please select atleast one skill.")
            
            return false
        }
        
        if (self.helpZipField.text?.countPlainCharacters() != 5)
        {
            self.helpErrorView.showInPos(
                CGPoint(x: self.helpZipField.frame.minX - 14,
                    y: self.helpZipDateContainer.convert(
                        self.helpZipField.frame, to: self.helpFormView).minY - baseY
                ),
                text: "Zip code must be of 5 digits.")
            
            return false
        }
        
        if self.helpDateCompletionField.text?.count < 4
        {
            self.helpErrorView.showInPos(
                CGPoint(x: self.helpDateCompletionField.frame.minX - 14,
                    y: self.helpZipDateContainer.convert(
                        self.helpDateCompletionField.frame, to: self.helpFormView).minY  - baseY
                ),
                text: "You need to set a completion date.")
            
            return false
        }
        
        //    let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
        //    let components = calendar?.components([.Year], fromDate: helpDate, toDate: NSDate(), options: [])
        //
        //    if components?.year < 18 {
        //
        //      self.helpErrorView.showInPosFromCenter(
        //        CGPointMake(CGRectGetMinX(self.helpDateField.frame) - 8,
        //          CGRectGetMinY(self.helpDateField.frame) - baseY),
        //        text: "You must be 18 years or older to be a GoodKarma.")
        //      return false
        //    }
        
        if self.helpBodyContentField.text!.count < 10
        {
            self.helpErrorView.showInPos(
                CGPoint(
                    x: self.helpBodyContentField.frame.minX - 8,
                    y: self.helpBodyContainer.convert(
                        self.helpBodyContentField.frame, to: self.helpFormView).minY - baseY
                ),
                text: "Content should contain at least 10 characters."
            )
            
            return false;
        }
        
        return true
    }
    
    func updateViews()
    {
        self.view.endEditing(true)
        
        if isRequestPositiveSelected
        {
            refreshPositivityCount()
            
            actionButton.setTitle("SEND", for: UIControlState())
        }
        else
        {
            actionButton.setTitle("POST", for: UIControlState())
        }
        
        positivityScrollView.isHidden = !isRequestPositiveSelected
        helpScrollView.isHidden       = isRequestPositiveSelected
    }
    
    func updatedKindHelp()
    {
        self.helpPhysicalImageView.image = UIImage(named: self.helpIsPhysicalSelected ? "check-on" : "check-off")
        self.helpVirtualImageView.image = UIImage(named: !self.helpIsPhysicalSelected ? "check-on" : "check-off")
        
        helpDistanceView.isHidden = !helpIsPhysicalSelected
    }
    
    func updateSkillsView()
    {
        helpManageSkillView.frame = helpSkillsContainer.bounds
        helpManageSkillView.setupSkills(helpSkillsSelected)
        
        self.helpSkillsContainerHeightConstraint.constant = helpManageSkillView.bounds.size.height
    }
    
    func showPostFacebook(_ tag: NSInteger)
    {
        if Static.sharedInstance.isLoginWithFacebook()
        {
            let parentView = self.navigationController?.view;
            let frame =  CGRect(x:0, y:0, width: parentView!.frame.width,
                                height: parentView!.frame.height);
            let postFacebook = GKPostFacebookView(frame: frame)
            postFacebook.tag = tag;
            postFacebook.delegate = self
            parentView!.addSubview(postFacebook)
            postFacebook.show()
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - GKPostFacebookDelegate
    
    func postFacebookYes(_ postFacebookView: GKPostFacebookView)
    {
        LoadingView.showLoadingView("Sending...",
                                    parentView: self.navigationController?.view,
                                    backColor: true)
        
        if (postFacebookView.tag == 1)
        {
            FacebookHandler.publishPositivity(self, content: positivityContentField.text)
            { (result) in
                if (result)
                {
                    postFacebookView.hide()
                }
                else
                {
                    LoadingView.hideLoadingView(self.navigationController?.view)
                    
                    showCommonError({ (type: NoInternetResultType) in
                        self.postFacebookYes(postFacebookView)
                    })
                }
            }
        }
        else
        {
            FacebookHandler.publishHelpRequest(self, content: helpBodyContentField.text)
            { (result) in
                if (result)
                {
                    postFacebookView.hide()
                }
                else
                {
                    LoadingView.hideLoadingView(self.navigationController?.view)
                    
                    showCommonError({ (type: NoInternetResultType) in
                        self.postFacebookYes(postFacebookView)
                    })
                }
            }
        }
    }
    
    func postFacebookNo(_ postFacebookView: GKPostFacebookView)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - FBSDKSharingDelegate
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!)
    {
        print("%@", results)
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("%@", error)
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!)
    {
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
}

extension GKRequestPositiveAndHelpViewController : ShowImageScrollViewDelegate{
    func addImageScrollView(image: UIImage!){
        showImageScrollView = (Bundle.main.loadNibNamed("ShowImageScrollView", owner: self, options: nil)?[0] as? ShowImageScrollView)!
        
        showImageScrollView?.delegate = self
        showImageScrollView?.setImage(image: image)
        showImageScrollView?.alpha = 0
        showImageScrollView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        (UIApplication.shared.delegate as! AppDelegate).window!.addSubview(showImageScrollView!)
        UIView.animate(withDuration: 0.7, animations: {
            self.showImageScrollView?.alpha = 1
        } ,completion: { (done) in
            
        } )
    }
    
    // MARK: - ShowImageScrollViewDelegate
    func removeShowImageScrollView()  {
        self.showImageScrollView?.alpha = 1
        UIView.animate(withDuration: 0.5, animations: {
            self.showImageScrollView?.alpha = 0
        } ,completion: { (done) in
            self.showImageScrollView?.removeFromSuperview()
            self.showImageScrollView = nil
        } )
    }
}
