                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             //
//  GKNotificationsViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNotificationsViewController: GKViewController, GKShowUserProfileDelegate, UITableViewDataSource, UITableViewDelegate
{
  // MARK: - IBOutlets -
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var virtualHugView: UIImageView!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backgroundButton: UIButton!
    
    @IBOutlet weak var acceptRejectView: UIView!
    @IBOutlet weak var hugView: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonReject: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
  // MARK: - Properties -
  
    var notifications: [GKDictionary] = []
    var isAccept = Bool()
    var locationData: GKDictionary!
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Notifications"

    self.tableView.register(
      UINib(nibName: GKNotificationTableViewCell.cellIdentifier(),bundle: nil),
      forCellReuseIdentifier: GKNotificationTableViewCell.cellIdentifier()
    )
  //  self.tableView.tableFooterView = UIView()
    
    GKNotificationStore.clearAll()
    cleanBackButton()
    setMenuBarBadge()
    
    // Load Data
    reloadData()
    
  }
  
  deinit
  {
    NotificationCenter.default.removeObserver(self)
  }
  
    override func viewWillAppear(_ animated: Bool) {
        isAccept = false
        locationData = [:]
    }
    
  override func viewDidAppear(_ animated: Bool)
  {
    super.viewDidAppear(animated)
    
    // Request Menu
//    GKNotificationStore.requestMenuCount()
    // Request Data
    requestNotificationsList()
  }
  
  // MARK: - Notifications -
  
  override func receiveFromNotificationStore(_ notification: Notification)
  {
    updateMenuBarButton()
    // FIXME: fix this
//    requestNotificationsList()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol GKShowUserProfileDelegate -
  
  func GKShowProfile(_ userID: Int)
  {
    let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
    profileController.anotherUserID = userID
    self.present(
      UINavigationController(rootViewController: profileController), animated: true, completion: nil
    )
  }
  // MARK: - Protocol UITableViewDelegate -
  
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
  {
    return true
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return self.notifications.count
  }
  
    func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(
      withIdentifier: GKNotificationTableViewCell.cellIdentifier(), for: indexPath
    ) as! GKNotificationTableViewCell
    
    cell.delegate = self
    cell.loadData(self.notifications[indexPath.row])
    
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
  {
    let notification = self.notifications[indexPath.row];
    
    if (notification["details"] is NSNull) ||
        (notification["post"] is NSNull) ||
        (notification["notification_type"]) is NSNull
    {
        return
    }
    
    let detail = notification["details"] as! String
    let typePost = notification["notification_type"] as! Int
    var title = "View post"
    
    if (typePost == 4)
    {
        title = "View Gratitude"
    }
    
    if (typePost == 3)
    {
        if (notification["hugType"] as! Int) == 1{
            if (notification["status"] as! Int) == 2{
                if detail.contains("accepted"){
                    openGoogleMap(latitude: notification["latitude"] as! NSNumber, longitude: notification["longitude"] as! NSNumber)
                }
                else{
                    showAlert("Alert!", text: "Already accepted")
                }
            }
            else if (notification["status"] as! Int) == 1{
                showAlert("Alert!", text: "Already rejected")
            }
           else{
                labelName.text = detail
                locationData = notification
                self.acceptRejectView.isHidden = false
            }
        }
        else{
            showVirtualHugFromNotification()
        }
    }
    else{
        GKMoreDetailView.show(detail, buttonTitle: title)
        {
            //        let notification = self.notifications[indexPath.row]
            //        let dataPost: GKDictionary! = notification["post"] as! GKDictionary
            //        let typePost = notification["notification_type"] as! Int
            //
            //        if (dataPost != nil)
            //        {
            //            // Serve
            //            if typePost == 1
            //            {
            //                self.loadPostServe(dataPost)
            //            }
            //            // Request Positivity
            //            else if typePost == 2
            //            {
            //                self.loadPostRequestPositivity(dataPost)
            //            }
            //            // Request Help
            //            else if typePost == 3
            //            {
            //                self.loadPostHelpRequest(dataPost)
            //            }
            //            // Gratitude
            //            else if typePost == 4
            //            {
            //                GKMoreDetailView.show(dataPost["desc"] as! String)
            //            }
            //        }
        }
    }
  }
  
    func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return GKNotificationTableViewCell.heightForCell()
  }
  
  // MARK: - Methods -
    
    func openGoogleMap(latitude: NSNumber, longitude: NSNumber){
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: Float(truncating: latitude))),\(String(describing: Float(truncating: longitude)))&directionsmode=driving")! as URL)
        } else
        {
            NSLog("Can't use com.google.maps://");
            showAlert("Sorry!", text: "Can't use Google maps")
        }
    }
    
    @IBAction func buttonAcceptClicked(_ sender: Any) {
        let notificationsViewController = SendLocationViewController(nibName: "SendLocationViewController", bundle: nil)
        notificationsViewController.data = self.locationData
        self.acceptRejectView.isHidden = true
        self.present(
            UINavigationController(rootViewController: notificationsViewController), animated: true, completion: nil
        )
    }
    
    @IBAction func buttonRejectClicked(_ sender: Any) {
        
        var params =  GKDictionary()
        params["token"] =  Static.sharedInstance.token() as AnyObject
        params["id"] =  self.locationData["related_post_id"] as AnyObject
        params["hugType"] =  self.locationData["hugType"] as AnyObject
        params["status"] =  1 as AnyObject
        params["post_type"] =   self.locationData["post_type"] as AnyObject
        
        print(params)
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.sendLocationAndStatusUpdate(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                 print(response!)
                if envelope.getSuccess()
                {
                    self.acceptRejectView.isHidden = true
                }
                else{
                    showAlert("Alert!", text: "Already rejected.")
                }
            }
            else
            {
                
            }
        }
        )
    }
    
    @IBAction func buttonCancelClicked(_ sender: Any) {
        self.acceptRejectView.isHidden = true
    }
    
    @IBAction func backgroundButtonClicked(_ sender: Any) {
        self.acceptRejectView.isHidden = true
    }
  
    @objc func showVirtualHugFromNotification()
    {
        self.virtualHugView.isHidden = false
        let heartGif = UIImage.gifImageWithName("virtual_hug_final")
        virtualHugView.image = heartGif
        UIView.animate(withDuration: 1, delay:5, options:UIViewAnimationOptions.transitionFlipFromTop, animations: {
            self.virtualHugView.alpha = 0
        }, completion: { finished in
            self.virtualHugView.isHidden = true
        })
    }
    
  func checkEmptyState(_ count: Int)
  {
    emptyLabel.isHidden = (count != 0)
    tableView.isHidden  = (count == 0)
  }
  
  func reloadData()
  {
    self.tableView.reloadData()
  }
  
  func requestNotificationsList()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["page_number"] = 1 as AnyObject

//    params["token"] = "HYrYe6ij0tqkWdmqv57njVGODZRbyQBJNEk"
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.listNotifications(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
        
      if let res:AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
            let data = envelope.getResource() as? NSDictionary
            (print(data!))
            if data != nil
            {
                self.notifications = data!.object(forKey: "result") as! [GKDictionary]
                self.checkEmptyState(self.notifications.count)
                self.tableView.reloadData()
            }
        }
      }
        GKNotificationStore.clearAll()
        self.setMenuBarBadge()
    })
  }
    
    func loadPostHelpRequest(_ data: GKDictionary!)
    {
        let authorId = data["author"]!["id"] as! Int
        
        let detailViewController = GKDetailRequestHelpViewController(nibName: "GKDetailRequestHelpViewController", bundle: nil)
        detailViewController.data  = data
        detailViewController.isParticipant = !Static.sharedInstance.isMe(authorId)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func loadPostServe(_ data: GKDictionary)
    {
        let authorId = data["author"]!["id"] as! Int
        
        let detailViewController = GKDetailServeViewController(nibName: "GKDetailServeViewController", bundle: nil)
        detailViewController.data  = data
        detailViewController.isParticipant = !Static.sharedInstance.isMe(authorId)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func loadPostRequestPositivity(_ data: GKDictionary)
    {
        let detailViewController = GKDetailRequestPositivityViewController(nibName: "GKDetailRequestPositivityViewController", bundle: nil)
        detailViewController.data  = data
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
