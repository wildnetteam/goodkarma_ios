//
//  GKSignInViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/31/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSignInViewController: UIViewController,UITextFieldDelegate
{
  // MARK: - IBOutlets -
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var containerTextFields: UIView!
  
  // MARK: - Protocols -
  
  var errorView: ErrorView!
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    
    self.title = "Sign In"
    cleanBackButton()
    
    self.automaticallyAdjustsScrollViewInsets = false
    
    ////      self.scrollView.contentInset = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0)
    //      self.scrollView.contentInset = UIEdgeInsetsZero
    //      self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    //      self.scrollView.contentOffset = CGPointMake(0.0, 0.0);
    
    self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
    self.containerTextFields.addSubview(self.errorView)
    
    self.emailTextField.text = Static.sharedInstance.email()
    self.passwordTextField.delegate = self
    // FIXME: delete his
//    emailTextField.text = "andresgarciadev@hotmail.com"
//    passwordTextField.text = "1234"
  }
  
  override func viewWillAppear(_ animated: Bool)
  {
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = false
  }
  
//  override func viewDidAppear(animated: Bool) {
//    super.viewDidAppear(animated)
//    
////    self.navigationController?.navigationBarHidden = false
  //  }
  
  // MARK: - Protocol UITextFieldDelegate -
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool
  {
    
    if textField == emailTextField
    {
        self.passwordTextField.becomeFirstResponder()
    }
    else
    {

        self.passwordTextField.resignFirstResponder()
         self.actionSingIn(0 as AnyObject)
    }
//    if NSObject.isEqual(self.emailTextField)
//    {
//      self.passwordTextField.becomeFirstResponder()
//    }
//    else if NSObject.isEqual(self.passwordTextField)
//    {
//      self.actionSingIn(0 as AnyObject)
//    }

    return false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField)
  {
    self.errorView.hide()
  }
  
  // MARK: - Protocol UIScrollViewDelegate -
  
  func scrollViewDidScroll(_ scrollView: UIScrollView)
  {
    self.errorView.hide()
  }
  
  // MARK: - IBActions -

  @IBAction func actionForgotPassword(_ sender: AnyObject)
  {
    self.view.endEditing(true)
    self.errorView.hide()
    
    let forgotViewController = GKForgotPasswordViewController(nibName: "GKForgotPasswordViewController", bundle: nil)
    self.navigationController?.pushViewController(forgotViewController, animated: true)
  }
  
    
    @IBAction func actionSingIn(_ sender: AnyObject)
    {
        
        Static.sharedInstance.saveEmail(self.emailTextField.text!)
        
        self.view.endEditing(true)
        self.errorView.hide()
        
        if validate() {
            
            var params = [String: String]()
            params["uid"] = UIDevice.current.identifierForVendor!.uuidString
            params["email"]    = self.emailTextField.text
            params["password"] = self.passwordTextField.text
            params["device_type"] = "ios"
            
            LoadingView.showLoadingView(
                "Sending...",
                parentView: self.navigationController?.view, backColor: true
            )
            
            GKServiceClient.call(GKRouterAuth.signIn(), parameters:params as GKDictionary, callback: {
                (response: AnyObject?) -> () in
                
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res:AnyObject = response {
                    
                    let envelope = ResponseSkeleton(res , string : "login")
                    print(res)
                    if envelope.getSuccess()
                    {
                        Static.sharedInstance.saveEmail(self.emailTextField.text!)
                        let data = envelope.getResource() as? NSDictionary
                        if data != nil
                        {
                            UserDefaults.standard.set((data?["code"] as! String), forKey: "code")
                            if let phone = data?["phone_number"] as? String
                            {
                                UserDefaults.standard.set(phone, forKey: "phone_number")
                            }
                            if let zip = data?["_zip"]
                            {
                                UserDefaults.standard.set(zip, forKey: "zip_code")
                            }
                            if let lat = data?["latitude"] as? NSNumber
                            {
                                UserDefaults.standard.set("\(lat )", forKey: "latitude")
                                //  UserDefaults.standard.set(data?["latitude"] as! String, forKey: "latitude")
                            }
                            if let long = data?["latitude"] as? NSNumber
                            {
                                UserDefaults.standard.set("\(long )", forKey: "longitude")
                                //  UserDefaults.standard.set(data?["longitude"] as! String, forKey: "longitude")
                            }
                            
                            if let fb = data?["facebook_id"] as? String
                            {
                                UserDefaults.standard.set(fb, forKey: "isFacebookId")
                            }
                            UserDefaults.standard.set((data?["interval"] as AnyObject), forKey: "interval")
                            UserDefaults.standard.set((data?["difficulty"] as AnyObject), forKey: "difficulty")
                            UserDefaults.standard.set((data?["distance"] as AnyObject), forKey: "distance")
                            UserDefaults.standard.set((data?["filter_following"] as AnyObject), forKey: "following")
                            UserDefaults.standard.set((data?["filter_follower"] as AnyObject), forKey: "follower")
                            UserDefaults.standard.set((data?["id"] as AnyObject), forKey: "id")
                            UserDefaults.standard.set(" ", forKey: "birthdate")
                        }
                        
                        let hasPhoto         = (data?["has_photo"] as! NSNumber).boolValue
                        let hasProfile       = (data?["has_profile"] as! NSNumber).boolValue
                        let hasPhoneVerified = (data?["has_phone_verified"] as! NSNumber).boolValue
                        
                        if !hasProfile
                        {
                            let completeInfo = GKRegistrationViewController(nibName: "GKRegistrationViewController", bundle: nil)
                            completeInfo.data = data
                            completeInfo.fromFacebook = false
                            self.navigationController?.pushViewController(completeInfo, animated: true)
                        }
                        else if !hasPhoneVerified
                        {
                            let confirmCodeController = GKConfirmCodeViewController(nibName: "GKConfirmCodeViewController", bundle: nil)   // uncomment later
                            //  let confirmCodeController = GKRegistrationViewController(nibName: "GKRegistrationViewController", bundle: nil)
                            confirmCodeController.data = data
                            confirmCodeController.fromFacebook = false
                            self.navigationController?.pushViewController(confirmCodeController, animated: true)
                        }
                        else if !hasPhoto
                        {
                            let photoController = GKAddPhotoViewController(nibName: "GKAddPhotoViewController", bundle: nil)
                            photoController.data = data
                            photoController.fromFacebook = false
                            self.navigationController?.pushViewController(photoController, animated: true)
                        }
                        else
                        {
                            var recovery = false
                            
                            if let comeback = envelope.getResource()["recovery"] as? Bool
                            {
                                recovery = comeback
                            }
                            
                            Static.sharedInstance.saveUserProfile(envelope.getResource() as? NSDictionary)
                            Static.sharedInstance.saveLogin(true)
                            (UIApplication.shared.delegate as! AppDelegate).showHome(self.navigationController!, loginOrRegister: true, recovery: recovery) // false
                        }
                    }
                    else if (envelope.getResource() as? String != nil)
                    {
                        showMessageError("Alert", text: envelope.getResource() as! String, callback: { (type: NoInternetResultType) in
                        })
                    }
                    else
                    {
                        showCommonError({ (type: NoInternetResultType) -> () in
                            self.actionSingIn(0 as AnyObject)
                        })
                    }
                }
                else
                {
                    showConnectionError({ (type: NoInternetResultType) -> () in
                        self.actionSingIn(0 as AnyObject)
                    })
                }
            })
        }
    }
  
  // MARK: - Methods -
  
  func validate() -> Bool
  {
    let baseY = self.errorView.frame.size.height;
    if !self.emailTextField.text!.isEmailValid()
    {
      self.errorView.showInPos(
        CGPoint(
          x: self.emailTextField.frame.minX - 8,
          y: self.emailTextField.frame.minY - baseY
        ),
        text: "Need to set a valid email.")
      
      return false;
    }
    
    if self.passwordTextField.text!.count == 0
    {
      self.errorView.showInPos(
        CGPoint(
          x: self.passwordTextField.frame.minX - 8,
          y: self.passwordTextField.frame.minY - baseY
        ),
        text: "Need to write a password."
      )
      
      return false;
    }
    return true
  }
}
