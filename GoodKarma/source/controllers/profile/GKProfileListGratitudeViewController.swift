//
//  GKProfileListGratitudeViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKProfileListGratitudeViewController: GKViewController, UITableViewDataSource, UITableViewDelegate
{
  // MARK: - IBOutlets -
  
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Properties -
  
  var gratitudes: [GKDictionary] = []
  var anotherUserID: Int?
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Gratitude"
    
    self.tableView.register(
      UINib(nibName: GKProfileGratitudeTableViewCell.cellIdentifier(),bundle: nil),
      forCellReuseIdentifier: GKProfileGratitudeTableViewCell.cellIdentifier()
    )
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    
    requestData()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return gratitudes.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(withIdentifier: GKProfileGratitudeTableViewCell.cellIdentifier(), for: indexPath) as! GKProfileGratitudeTableViewCell
    
    cell.loadData(self.gratitudes[indexPath.row])
    
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
  {
    let descGratitude = self.gratitudes[indexPath.row]["desc"] as! String
    GKMoreDetailView.show(descGratitude)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return GKProfileGratitudeTableViewCell.heightForCell()
  }
  
  // MARK: - Methods -
  
  func requestData()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["page_number"] = 1 as AnyObject
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    var routeService = ""
    
    if anotherUserID != nil
    {
      routeService = GKRouterClient.profileListOtherGratitudes()
      params["user_id"] = anotherUserID! as AnyObject?
    }
    else
    {
      routeService = GKRouterClient.profileListMyGratitudes()
    }
    
    GKServiceClient.call(routeService, parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
          if let data = envelope.getResource()["result"] as? [GKDictionary]
          {
            self.gratitudes = data
            self.tableView.reloadData()
          }
        }
      }
    })
  }
}
