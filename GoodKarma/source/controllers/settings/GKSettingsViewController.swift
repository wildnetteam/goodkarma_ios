//
//  GKSettingsViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSettingsViewController: GKViewController, GKHomeFilterDelegate, UITableViewDelegate, UITableViewDataSource
{
    enum GKSettingOptions: Int
    {
        case filters    = 0
        case dueDate
        case notifications
        case blockUsers
        case privacyGratitude
        case changePassword
        case contact
        case logout
        case delete
    }
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties -
    
    var titleSettings: [String] = []
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Settings"
        
        setMenuBarBadge()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "SettingsCell")
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.reload()
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKHomeFilterDelegate -
    
    func GKFilterUpdateParams(_ filterParams: GKDictionary)
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        for (k,v) in filterParams
        {
            params[k] = v
        }
        
        print(params)
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.settingsExploreFilters(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "explore_filters_settings")
                
                if envelope.getSuccess()
                {
                    print("SAVED!")
                    //          print(envelope.getResource())
                }
            }
        })
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titleSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath)
        
        cell.textLabel?.text = self.titleSettings[indexPath.row]
        cell.selectionStyle = .none
        
        if indexPath.row < (titleSettings.count - 2)
        {
            let arrowImageView = UIImageView(image: UIImage(named: "arrow-r"))
            arrowImageView.bounds = CGRect(x: 0.0, y: 0.0, width: 15.0, height: 15.0)
            arrowImageView.contentMode = .scaleAspectFit
            
            cell.accessoryView = arrowImageView
        }
        else
        {
            cell.accessoryView = nil
        }
        
        if indexPath.row == GKSettingOptions.delete.rawValue
        {
            cell.textLabel?.textColor = UIColor.red
        }
        else
        {
            cell.textLabel?.textColor = UIColor.black
        }
        
        if (indexPath.row == GKSettingOptions.changePassword.rawValue &&
            Static.sharedInstance.isLoginWithFacebook())
        {
            cell.contentView.alpha = 0.3;
            cell.accessoryView = nil
        }
        return cell
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.row
        {
        case GKSettingOptions.filters.rawValue:
            loadFilters()
            
        case GKSettingOptions.dueDate.rawValue:
            loadDueDate()
            
        case GKSettingOptions.notifications.rawValue:
            loadNotificationPreferences()
            
        case GKSettingOptions.blockUsers.rawValue:
            loadBlockedUsers()
            
        case GKSettingOptions.privacyGratitude.rawValue:
            loadGratitudeSettings()
            
        case GKSettingOptions.changePassword.rawValue:
            loadChangePassword()
            
        case GKSettingOptions.contact.rawValue:
            loadContactView()
            
        case GKSettingOptions.logout.rawValue:
            let alertController = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            let destroyAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                if (Static.sharedInstance.isLoginWithFacebook())
                {
                    logout(true)
                }
                else{
                    logout(false)
                }
            }
            alertController.addAction(cancelAction)
            alertController.addAction(destroyAction)
            
            self.present(alertController, animated: false, completion: nil)
            
        case GKSettingOptions.delete.rawValue:
            loadDeleteForm()
            
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        return 50.0
    }
    
    // MARK: - Methods -
    
    func deleteAccount()
    {
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        GKServiceClient.call(GKRouterClient.settingsDeleteAccount(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    if (Static.sharedInstance.isLoginWithFacebook())
                    {
                        logout(true)
                    }
                    else{
                        logout(false)
                    }
                }
            }
        })
    }
    
    func reload()
    {
        titleSettings = [
            "Explore filters",
            "Post due-date preferences",
            "Notifications",
            "Blocked users",
            "Privacy for gratitude",
            "Change Password",
            "Contact support team",
            "Log-Out",
            "Delete Account",
        ]
        
        tableView.reloadData()
    }
    
    func loadContactView()
    {
        let contactController = GKTeamContactViewController(nibName: "GKTeamContactViewController", bundle: nil)
        self.navigationController?.pushViewController(contactController, animated: true)
    }
    
    func loadDueDate()
    {
        let dueDateController = GKDueDateViewController(nibName: "GKDueDateViewController", bundle: nil)
        self.navigationController?.pushViewController(dueDateController, animated: true)
    }
    
    func loadDeleteForm()
    {
        let alertController = UIAlertController(title: "Are you sure you want to delete your account?", message: "Type \"Goodkarms\" to delete you account", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let destroyAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
            
            if let goodKarmsText = alertController.textFields?.first?.text
            {
                if goodKarmsText == "Goodkarms"
                {
                    self.deleteAccount()
                }
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(destroyAction)
        
        alertController.addTextField { (textField) in
            
            textField.placeholder = "Goodkarms"
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loadGratitudeSettings()
    {
        let gratitudeController = GKGratitudePrivacyViewController(nibName: "GKGratitudePrivacyViewController", bundle: nil)
        self.navigationController?.present(UINavigationController(rootViewController: gratitudeController), animated: true, completion: nil)
    }
    
    func loadChangePassword()
    {
        if (!Static.sharedInstance.isLoginWithFacebook())
        {
            let changePassword = GKChangePasswordViewController(nibName: "GKChangePasswordViewController", bundle: nil)
            self.navigationController?.pushViewController(changePassword, animated: true)
        }
    }
    
    func loadNotificationPreferences()
    {
        let notificationsController = GKNotificationPreferencesViewController(nibName: "GKNotificationPreferencesViewController", bundle: nil)
        self.navigationController?.present(UINavigationController(rootViewController: notificationsController), animated: true, completion: nil)
    }
    
    func loadBlockedUsers()
    {
        let blockedUsersController = GKBlockedUsersViewController(nibName: "GKBlockedUsersViewController", bundle: nil)
        //    self.navigationController?.presentViewController(UINavigationController(rootViewController: blockedUsersController), animated: true, completion: nil)
        self.navigationController?.pushViewController(blockedUsersController, animated: true)
    }
    
    func loadFilters()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.settingsExploreFilters(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "explore_filters_settings")
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["object"] as? GKDictionary
                    {
                        var filterParams = data
                        // Skills: [GKDictionary] to [Int]
                        if let skills = filterParams["skills"] as? [GKDictionary]
                        {
                            var tempSkills: [Int] = []
                            
                            // Get ID of skills
                            for dic in skills {
                                
                                if let uid = dic["id"] as? Int
                                {
                                    tempSkills.append(uid)
                                }
                            }
                            filterParams["skills"] = tempSkills as AnyObject?
                        }
                        self.showFilters(filterParams)
                    }
                }
            }
        })
    }
    
    func showFilters(_ filterParams: GKDictionary)
    {
        let filterController = GKHomeFilterViewController(nibName: "GKHomeFilterViewController", bundle: nil)
        filterController.homeState = nil
        filterController.delegate  = self
        filterController.params    = filterParams
        filterController.isSetting = true

        //    dispatch_async(dispatch_get_main_queue()) {
        self.navigationController?.present(UINavigationController(rootViewController: filterController), animated: true, completion: nil)
        //    }
    }
}
