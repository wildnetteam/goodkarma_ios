//
//  SendAudioTableViewCell.swift
//  Bizzalley
//
//  Created by Anuj Jha on 13/04/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit
import AVFoundation


protocol SendPlayAudioDelegate {
    
    func playAudioSend(_ tag: Int)
}

class SendAudioTableViewCell: UITableViewCell {

    @IBOutlet weak var tickImageVIewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tickMsgImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var loaderView: InstagramActivityIndicator!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var audioDurationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var delegate: SendPlayAudioDelegate?
    //var deleteImageDelegate: deleteMsgDelegate?
    var infoDict:Message?
    var timer:Timer?
    var seconds:Int = 0
    var player:AVAudioPlayer?
    
    var downloadTask:URLSessionDownloadTask?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.progressView.progress = 0.0
        
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        bgView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)//UIColor(red:0.96, green:0.71, blue:0, alpha:1)
        cornerView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)//UIColor(red:0.96, green:0.71, blue:0, alpha:1)
    }
    
    @IBAction func playAudioButtonTapped(_ sender: UIButton) {
        if delegate != nil{
            delegate?.playAudioSend(sender.tag)
        }
        DispatchQueue.main.async {
            self.playButton.isSelected = false
        }
        
        if player != nil && (player?.isPlaying)! {
            
            if self.timer != nil {
                self.timer?.invalidate()
            }
            if downloadTask != nil {
                downloadTask?.cancel()
            }
            player?.stop()
            self.progressView.progress = 0.0
            
        } else {
            downloadAndPlayAudioFromURL()
        }
    }
    
    func setLoaderUI(){
        loaderView.lineWidth = 2
        //loaderView.strokeColor = UIColor.blue
        loaderView.numSegments = 15
        loaderView.rotationDuration = 10;
        loaderView.animationDuration = 1.0;
        loaderView.startAnimating()
    }
    
    func downloadAndPlayAudioFromURL(){
        setLoaderUI()
        let urlStr  = infoDict?.audioURL
        let url = NSURL(string: urlStr!)
        //print("the url = \(url!)")
        weak var weakSelf = self
        
        player?.stop()
        downloadTask?.cancel()
        //self.timer?.invalidate()
        //self.progressView.progress = 0.0
        
        downloadTask = URLSession.shared.downloadTask(with: url! as URL, completionHandler: { (URL, response, error) -> Void in
            
            if error == nil {
                weakSelf!.play(url: URL! as NSURL)
            }
        })
        downloadTask?.resume()
    }
    
    func moveSlider(){
        DispatchQueue.main.async {
            self.loaderView.stopAnimating()
            self.progressView.progress = 0.0
            if #available(iOS 10.0, *) {
                self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                    self.progressView.progress += Float(1.0) / Float(self.seconds * 10)
                    if self.progressView.progress == 1.0 {
                        DispatchQueue.main.async {
                        timer.invalidate()
                         self.progressView.progress = 0.0
                            self.playButton.isSelected = false
                        }
                    }
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func play(url:NSURL) {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
        } catch {
        }
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url as URL)
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
            DispatchQueue.main.async {
                self.playButton.isSelected = true
            }
            moveSlider()
        } catch _ as NSError {
            //self.player = nil
            //print(error.localizedDescription)
        } catch {
            //print("AVAudioPlayer init failed")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func deleteMessageActionTapped(sender:Any){
//        if deleteImageDelegate != nil{
//            deleteImageDelegate?.deleteMsg()
//        }
        
    }
    

    
}
