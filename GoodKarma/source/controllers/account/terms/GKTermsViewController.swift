//
//  GKTermsViewController.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

class GKTermsViewController: GKViewController, UIWebViewDelegate
{
  
  // MARK: - IBOutlets -
  
//  @IBOutlet weak var scrollView: UIScrollView!
//  @IBOutlet weak var textView: UITextView!
//  @IBOutlet weak var webView: UIWebView!
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Terms of Use"
    cleanBackButton()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = false
//    self.scrollView.contentSize = CGSizeMake(
//      CGRectGetWidth(self.scrollView.frame),
//      CGRectGetMaxY(self.textView.frame) + 20
//    )
//    webView.hidden = true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    super.viewDidAppear(animated)
    
    let path = Bundle.main.path(forResource: "terms", ofType: "pdf")
    let targetURL = URL(fileURLWithPath: path!)
    let request = URLRequest(url: targetURL)
    
    let webView = UIWebView(
      frame: CGRect(
        x: 0.0,
        y: 64.0,
        width: self.view.bounds.size.width,
        height: self.view.bounds.size.height - 64
      )
    )
    
    webView.loadRequest(request)
    webView.scalesPageToFit = true
    self.view.addSubview(webView)
  }
}
