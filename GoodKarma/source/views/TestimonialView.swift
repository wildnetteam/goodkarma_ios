//
//  TestimonialView.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 13/1/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class TestimonialView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    fileprivate func setup()
    {
        view = Bundle.main.loadNibNamed("TestimonialView", owner: self,
                                                  options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.backgroundColor = UIColor.clear
        self.addSubview(view)
        self.backgroundColor = UIColor.clear
        
        self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self,
            action: #selector(self.close)))
        self.contentView.layer.cornerRadius = 6
    }
    
    @objc func close()
    {
        UIView.animate(withDuration: 0.3, delay:0, options:UIViewAnimationOptions(), animations:
            {
                self.alpha = 0
            },
                                   completion:
            { _ in
                self.removeFromSuperview()
        })
    }
    
    func size()
    {
        self.text.sizeToFit()
        self.contentView.frame = CGRectSetHeight(self.contentView.frame,
                                                 h: self.text.frame.maxY + 20)
    }
    
    // MARK: - Actions
    
    @IBAction func closeAction(_ sender: AnyObject)
    {
        close()
    }
    
    // MARK: - Static
    
    static func show(_ testimonialText: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window!
        
        let testimonialView = TestimonialView(frame: CGRect(x: 0.0, y: 0.0,
            width: window.frame.width, height: window.frame.height))
        testimonialView.alpha = 0
        testimonialView.text.text = testimonialText.trimmingCharacters(in: CharacterSet.newlines)
        testimonialView.size()
        window.addSubview(testimonialView)
        
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                testimonialView.alpha = 1
            },
                                   completion:
            { _ in
        })
    }
}
