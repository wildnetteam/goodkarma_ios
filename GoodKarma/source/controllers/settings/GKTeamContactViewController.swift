//
//  GKTeamContactViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/15/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import MessageUI

class GKTeamContactViewController: GKViewController, MFMailComposeViewControllerDelegate {
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Contact Info"
  }
  
  // MARK: - Protocols -
  // MARK: - protocol MFMailComposeViewController
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
  {
    switch result.rawValue {
    case MFMailComposeResult.cancelled.rawValue:
      break
    case MFMailComposeResult.failed.rawValue:
      break
    case MFMailComposeResult.sent.rawValue:
      break
    case MFMailComposeResult.saved.rawValue:
      break
    default : break
    }
    
    controller.dismiss(animated: true, completion: nil)
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionPhone()
  {
    UIApplication.shared.openURL(URL(string: "http://www.goodkarms.com")!)
  }
  
  @IBAction func actionMail()
  {
    if !MFMailComposeViewController.canSendMail() {
        let  alertVC = UIAlertController(title: "Alert" , message: "Sorry!! Can't Send Mail", preferredStyle: UIAlertControllerStyle.alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertVC, animated: false, completion: nil)
        print("Mail services are not available")
        return
    }
    else{
        let composeEmail = MFMailComposeViewController()
        composeEmail.mailComposeDelegate = self
        composeEmail.setMessageBody("", isHTML: false)
        composeEmail.setSubject("Contact Goodkarms")
        composeEmail.setToRecipients(["support@goodkarms.com"])
        self.present(composeEmail, animated: true, completion: nil)
    }
    
  }
}
