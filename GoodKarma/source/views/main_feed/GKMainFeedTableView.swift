
//
//  GKMainFeedTableView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/9/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import MediaBrowser
import AVFoundation
import AFNetworking
import CoreLocation

class GKMainFeedTableView: UIView,
    // Profile
    GKShowUserProfileDelegate,
    // Send Message
    GKStartMessageDelegate,
    // Like
    GKLikePostDelegate,
    // Follow
    GKFollowDelegate,
    // Skills
    GKManageSkillsDelegate,
    // Participate
    GKServeParticipateDelegate,
    GKHelpRequestParticipateDelegate,
    GKRespondPositiveRequestDelegate,
    GKHugDelegate,
    GKSosContactDelegate,
    GKShowSosContact,
    GKShareDelegate,
    // Tableview
    UITableViewDelegate,
    UITableViewDataSource,
    UISearchBarDelegate
{

    // MARK: - IBOutlets -
    
    @IBOutlet var view: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var searchBarMainFeed: UISearchBar!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    var deleteDelegate : deleteSosData!
    // MARK: - Properties -
    
    weak var parentController: UIViewController!
    var delegate: GKMainFeedDelegate!
    var exploreList: GKMutableDictionary = []
    var searchList: GKMutableDictionary = []
    var isSearch = Bool()
    
    var activityView : UIView!
    var activity : UIActivityIndicatorView!
    var lastPageBool : Bool?
    var currentPage : NSInteger?
    var totalCount : NSInteger?
    var currentPageSearch : NSInteger?
    var totalCountSearch  : NSInteger?
    var lastPageBoolSearch : Bool?

    var imageArray : NSMutableArray = []

    var mediaArray = [Media]()
    var selections = [Bool]()
    
    fileprivate var locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D?
    
    // MARK: - Initializer -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK: - Protocols     // MARK: - Protocol GKShowUserProfileDelegate -
    
    func GKShowProfile(_ userID: Int)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.anotherUserID = userID
        
        self.parentController.present(
            UINavigationController(rootViewController: profileController), animated: true, completion: nil
        )
    }
    
    // MARK: - Protocol GKStartMessageDelegate -
    
    func GKSendMessage(_ userID: Int, fullName: String) {
        
        if let sendMessage = delegate.GKSendMessage
        {
            sendMessage(userID, fullName)
        }
        else
        {
            self.parentController.appDelegate().showConversation(userID, fullName: fullName)
        }
    }
    
    // MARK: - Protocol GKSharePostDelegate -
    
    func GKSharePost(share_text: NSAttributedString) {
        if (AFNetworkReachabilityManager.shared().isReachable){
            
            let imageUrlStr = "http://goodkarms.com/"
            
            // set up activity view controller
            let textToShare = [ share_text, imageUrlStr ] as [Any]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // present the view controller
            self.parentController.present(activityViewController, animated: true, completion: { () in
                UIBarButtonItem.appearance().tintColor = UIColor.black
            })
        }
        else{
            showConnectionError({ (type: NoInternetResultType) -> () in
            })
        }
    }
    
    // MARK: - Protocol GKLikePostDelegate -
    
    func GKSendLike(_ typePost: Int, postID: Int)
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["type"]  = typePost as AnyObject?
        params["id"]    = postID as AnyObject?
        
        print(params)
        
        LoadingView.showLoadingView("Loading...", parentView: self.parentController.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.like(), parameters: params) { (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    self.lastPageBool = false
                    self.currentPage = 1
                    self.currentPageSearch = 1
                    self.lastPageBoolSearch = false
                    self.totalCountSearch = 0
                    self.totalCount = 0
                    self.exploreList = []
                    self.requestData()
                }
            }
        }
    }
    
    // MARK: - Protocol GKHugDelegate -
    func GKPassHug(full_name : String , recipientId : Int , authorId :  Int , positivityId : Int , post_type : Int)
    {
        let view = Bundle.main.loadNibNamed(
            "GKHugView", owner: self, options: nil)?[0] as! GKHugView
        view.setUp()
        view.positivityId = positivityId
        view.recipientId = recipientId
        view.authorId = authorId
        view.postType = post_type
        view.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        (UIApplication.shared.delegate as! AppDelegate).window?.addSubview(view)
        view.appreciateFirstLabel.text = "Want to Appreciate "  + "\(full_name)"
        view.virtualHugLabel.text = "You have sent a hug to" + " " + "\(full_name)"
    }
    
    // MARK: - Protocol GKFollowDelegate -
    
    func GKFollow(_ userID: Int) {
        
        var params = GKDictionary()
        params["token"]   = Static.sharedInstance.token() as AnyObject?
        params["user_id"] = userID as AnyObject?
        
        LoadingView.showLoadingView("Loading...", parentView: self.parentController.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.follow(), parameters: params) { (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    self.lastPageBool = false
                    self.currentPage = 1
                    self.currentPageSearch = 1
                    self.lastPageBoolSearch = false
                    self.totalCountSearch = 0
                    self.totalCount = 0
                    self.exploreList = []
                    
                    self.requestData()
                }
            }
        }
    }
    
    func GKUnfollow(_ userID: Int, name: String) {
        
        let alertController = UIAlertController(title: nil, message: "Unfollow \(name)?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let destroyAction = UIAlertAction(title: "Unfollow", style: .destructive) { (action) in
            self.unfollow(userID)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(destroyAction)
        
        self.parentController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill) {
        // Nothing for now
    }
    
    func GKShowAddSkillsView() {
        // Nothing for now
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill]) {
        
        GKMoreSkillsView.show(skills)
    }
    
    // MARK: - Protocol GKHelpRequestParticipateDelegate -
    
    func GKParticipateInHelpRequest(_ helpRequestData: GKDictionary) {
        print("HELP REQUEST")
        
        let alertController = UIAlertController(title: nil, message: "Help Request", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let acceptAction = UIAlertAction(title: "Accept", style: .default ) { (action) in
            self.confirmParticipateHelpRequest(helpRequestData)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(acceptAction)
        
        self.parentController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Protocol GKRespondPositiveRequestDelegate -
    
    func GKRespondPositiveRequest(_ requestPositivityID: Int) {
        
        let writePositivityView = GKWritePositivityView(frame: self.parentController.view.bounds)
        writePositivityView.requestPositivityID = requestPositivityID
        writePositivityView.alpha = 0.0
        
        self.parentController.view.addSubview(writePositivityView)
        
        UIView.animate(withDuration: 0.3, animations: {
            writePositivityView.alpha = 1.0
        })
    }
    
    // MARK: - Protocol GKServeParticipateDelegate -
    
    func GKParticipateInServe(_ serveData: GKDictionary) {
        print("SERVE")
        
        let alertController = UIAlertController(title: nil, message: "Serve", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let acceptAction = UIAlertAction(title: "Accept", style: .default ) { (action) in
            self.confirmParticipateServe(serveData)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(acceptAction)
        
        self.parentController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Protocol UITableViewDataSource -
    
    //new
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if isSos == true
        {
            let alertController = UIAlertController(title: "Alert!!", message: "Want to delete contact", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.passDeleteData(index: indexPath.row)}))
            alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil ))
            AppDelegate.appDelegate().window?.rootViewController?.present(alertController, animated: true, completion: nil)
//            self.passDeleteData(index: indexPath.row)
        }
        
        return []
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        if isSos == true
        {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: SOSAddContactsTableViewCell.cellIdentifier(), for: indexPath) as! SOSAddContactsTableViewCell
            //new
            //     cell.deleteButton.tag = indexPath.row + 101
            cell.delegate = self
            cell.tag = indexPath.row + 1
            cell.loadData(data: exploreList[indexPath.row] as! GKDictionary)
            cell.frame.size.height = 150.0
            return cell
        }
            
            // Serve
        else
        {
            let type = (exploreList[indexPath.row] as! GKDictionary)["post_type"] as! Int
            if type == 1 {
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: GKExploreServeTableViewCell.cellIdentifier(), for: indexPath) as! GKExploreServeTableViewCell
                
                cell.delegate = self
                cell.tag = indexPath.row + 1
                cell.loadData((exploreList[indexPath.row]) as! GKDictionary)
                cell.manageSkillView?.delegate = self
                
                //Images
                self.imageArray = []
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray = (imageDta.mutableCopy()) as! NSMutableArray
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray.addObjects(from: ((imageDta.mutableCopy()) as! NSMutableArray) as! [Any])
                    }
                }
                if imageArray.count > 0{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                else{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                
                if (isSearch) {
                    if(lastPageBoolSearch==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCountSearch!)
                            {
                                if self.searchBarMainFeed == nil && self.currentPageSearch == 2
                                {
                                    self.tableView.tableFooterView = self.activityView
                                }
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBoolSearch=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                else{
                    if(lastPageBool==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCount!)
                            {
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBool=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                return cell
            }
                // Request Positivity
            else if type == 2  {
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: GKExploreRequestPositivityTableViewCell.cellIdentifier(), for: indexPath) as! GKExploreRequestPositivityTableViewCell
                
                cell.delegate = self
                cell.tag = indexPath.row + 1
                cell.loadData((exploreList[indexPath.row]) as! GKDictionary)
                //Images
                self.imageArray = []
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray = (imageDta.mutableCopy()) as! NSMutableArray
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray.addObjects(from: ((imageDta.mutableCopy()) as! NSMutableArray) as! [Any])
                    }
                }
                if imageArray.count > 0{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                else{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                
                if (isSearch) {
                    if(lastPageBoolSearch==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCountSearch!)
                            {
                                if self.searchBarMainFeed == nil && self.currentPageSearch == 2
                                {
                                    self.tableView.tableFooterView = self.activityView
                                }
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBoolSearch=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                else{
                    if(lastPageBool==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCount!)
                            {
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBool=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                return cell
            }
                // Request Help
            else if type == 3 {
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: GKExploreHelpTableViewCell.cellIdentifier(), for: indexPath) as! GKExploreHelpTableViewCell
                
                cell.delegate = self
                cell.tag = indexPath.row + 1
                cell.manageSkillView?.delegate = self
                cell.loadData(exploreList[indexPath.row] as! GKDictionary)
                //Images
                self.imageArray = []
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray = (imageDta.mutableCopy()) as! NSMutableArray
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray.addObjects(from: ((imageDta.mutableCopy()) as! NSMutableArray) as! [Any])
                    }
                }
                if imageArray.count > 0{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                else{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                
                if (isSearch) {
                    if(lastPageBoolSearch==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCountSearch!)
                            {
                                if self.searchBarMainFeed == nil && self.currentPageSearch == 2
                                {
                                    self.tableView.tableFooterView = self.activityView
                                }
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBoolSearch=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                else{
                    if(lastPageBool==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCount!)
                            {
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBool=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                return cell
            }
                // Gratitude
            else if type == 4 {
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: GKExploreGratitudeTableViewCell.cellIdentifier(), for: indexPath) as! GKExploreGratitudeTableViewCell
                
                cell.delegate = self
                cell.tag = indexPath.row + 1
                cell.loadData(exploreList[indexPath.row] as! GKDictionary)
                //Images
                 self.imageArray = []
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray = (imageDta.mutableCopy()) as! NSMutableArray
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray.addObjects(from: ((imageDta.mutableCopy()) as! NSMutableArray) as! [Any])
                    }
                }
                if imageArray.count > 0{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                else{
                    cell.collectionViewHeightConstraint.constant = 120
                }
                
                if (isSearch) {
                    if(lastPageBoolSearch==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCountSearch!)
                            {
                                if self.searchBarMainFeed == nil && self.currentPageSearch == 2
                                {
                                    self.tableView.tableFooterView = self.activityView
                                }
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBoolSearch=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                else{
                    if(lastPageBool==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCount!)
                            {
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBool=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                return cell
            }
                // Positivity
            else {
                
                let cell = tableView.dequeueReusableCell(
                    withIdentifier: GKExplorePositivityTableViewCell.cellIdentifier(), for: indexPath) as! GKExplorePositivityTableViewCell
                
                cell.delegate = self
                cell.tag = indexPath.row + 1
                cell.loadData(exploreList[indexPath.row] as! GKDictionary)
                //Images
                self.imageArray = []
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray = (imageDta.mutableCopy()) as! NSMutableArray
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count > 0 {
                        self.imageArray.addObjects(from: ((imageDta.mutableCopy()) as! NSMutableArray) as! [Any])
                    }
                }
                if imageArray.count > 0{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                else{
                    cell.collectionViewHeightConstraint.constant = 130
                }
                
                if (isSearch) {
                    if(lastPageBoolSearch==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCountSearch!)
                            {
                                if self.searchBarMainFeed == nil && self.currentPageSearch == 2
                                {
                                    self.tableView.tableFooterView = self.activityView
                                }
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBoolSearch=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                else{
                    if(lastPageBool==false)
                    {
                        if(indexPath.row==self.exploreList.count-1 )
                        {
                            if(self.exploreList.count < totalCount!)
                            {
                                activity.isHidden=false
                                activity.startAnimating()
                                self.requestData()
                            }
                            else
                            {
                                lastPageBool=true
                                activity.isHidden=true
                                activity.stopAnimating()
                                
                                self.tableView.contentSize=CGSize(width: self.tableView.contentSize.width, height: self.tableView.contentSize.height-100)
                            }
                        }
                    }
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.exploreList.count
    }
    
    func reloadTable()
    {
        self.reloadData()
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
     func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        
        if !isSos{
            let type = (exploreList[indexPath.row] as! GKDictionary)["post_type"] as! Int
            
            if type == 1 {
                guard let tableViewCellServe = cell as? GKExploreServeTableViewCell else { return }
                tableViewCellServe.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            }
            else if type == 2 {
                let tableViewCellRequestPositivity = cell as? GKExploreRequestPositivityTableViewCell
                tableViewCellRequestPositivity?.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            }
                // Request help
            else if type == 3 {
                guard let tableViewCellHelp = cell as? GKExploreHelpTableViewCell else { return }
                tableViewCellHelp.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            }
                // Gratitude
            else if type == 4 {
                guard let tableViewCellGratitude = cell as? GKExploreGratitudeTableViewCell else { return }
                tableViewCellGratitude.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            }
            else{
                guard let tableViewCellPositivity = cell as? GKExplorePositivityTableViewCell else { return }
                tableViewCellPositivity.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        if isSos == true
        {
            return SOSAddContactsTableViewCell.heightForCell()
        }
        else
        {
            let type = (exploreList[indexPath.row] as! GKDictionary)["post_type"] as! Int

            // Serve
            if type == 1 {
                GKExploreServeTableViewCell.isImages = false
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExploreServeTableViewCell.isImages = true
                    }
                }
                else{
                    if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                        if imageDta.count == 0 {
                            GKExploreServeTableViewCell.isImages = true
                        }
                        else{
                            GKExploreServeTableViewCell.isImages = false
                        }
                    }
                }
                return GKExploreServeTableViewCell.heightForCell()
            }
                // Request Positivity
            else if type == 2 {
                GKExploreRequestPositivityTableViewCell.isImages = false
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExploreRequestPositivityTableViewCell.isImages = true
                    }
                }
                else{
                    if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                        if imageDta.count == 0 {
                            GKExploreRequestPositivityTableViewCell.isImages = true
                        }
                        else{
                            GKExploreRequestPositivityTableViewCell.isImages = false
                        }
                    }
                }

                return GKExploreRequestPositivityTableViewCell.heightForCell()
            }
                // Request help
            else if type == 3 {
                GKExploreHelpTableViewCell.isImages = false
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExploreHelpTableViewCell.isImages = true
                    }
                }
                else{
                    if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                        if imageDta.count == 0 {
                            GKExploreHelpTableViewCell.isImages = true
                        }
                        else{
                            GKExploreHelpTableViewCell.isImages = false
                        }
                    }
                }
                return GKExploreHelpTableViewCell.heightForCell()
            }
                // Gratitude
            else if type == 4 {
 
                GKExploreGratitudeTableViewCell.isImages = false
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExploreGratitudeTableViewCell.isImages = true
                    }
                }
                else{
                    if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                        if imageDta.count == 0 {
                            GKExploreGratitudeTableViewCell.isImages = true
                        }
                        else{
                            GKExploreGratitudeTableViewCell.isImages = false
                        }
                    }
                }

                return GKExploreGratitudeTableViewCell.heightForCell()
            }
                // Positivity
            else {
                GKExplorePositivityTableViewCell.isImages = false
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["images"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExplorePositivityTableViewCell.isImages = true
                    }
                }
                if let imageDta = (exploreList[indexPath.row] as! GKDictionary)["videos"] as? NSArray{
                    if imageDta.count == 0 {
                        GKExplorePositivityTableViewCell.isImages = true
                    }
                    else{
                        GKExplorePositivityTableViewCell.isImages = false
                    }
                }
                return GKExplorePositivityTableViewCell.heightForCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if isSos == false
        {
            let type = (exploreList[indexPath.row] as! GKDictionary)["post_type"] as! Int
            
            if (type == 2)
            {
                let cell: GKExploreRequestPositivityTableViewCell
                    = tableView.cellForRow(at: indexPath) as! GKExploreRequestPositivityTableViewCell
                
                if (cell.titleLabel.willBeTruncated())
                {
                    GKMoreDetailView.showWithAttributed((cell.titleLabel?.attributedText)!)
                }
                
            }
            else if (type == 4)
            {
                let cell: GKExploreGratitudeTableViewCell
                    = tableView.cellForRow(at: indexPath) as!
                GKExploreGratitudeTableViewCell
                
                if (cell.titleLabel.willBeTruncated())
                {
                    GKMoreDetailView.showWithAttributed((cell.titleLabel?.attributedText)!)
                }
            }
            else if (type == 5)
            {
                let cell: GKExplorePositivityTableViewCell
                    = tableView.cellForRow(at: indexPath) as!
                GKExplorePositivityTableViewCell
                
                if (cell.titleLabel.willBeTruncated())
                {
                    GKMoreDetailView.showWithAttributed((cell.titleLabel?.attributedText)!)
                }
            }
        }
    }
    
    //MARK: - SEARCHBAR DELEGATE -
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count > 1 {
            if !isSearch{
                self.searchList = self.exploreList
            }
            currentPageSearch = 1;
            lastPageBoolSearch = false
            self.exploreList = []
            isSearch = true
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(requestData), object: self)
            self.perform(#selector(requestData), with: self, afterDelay: 2.0)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        isSearch = false
        currentPageSearch = 1
        lastPageBoolSearch = false
        totalCountSearch = 0
        
        if searchList.count > 0{
            exploreList = searchList
            self.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
//        isSearch = false
//        currentPageSearch = 1
//        lastPageBoolSearch = false
//        totalCountSearch = 0
        self.view.endEditing(true)
    }
    
    // MARK: - Methods -
    
    func confirmParticipateHelpRequest(_ helpRequestData: GKDictionary)
    {
        var params = GKDictionary()
        params["token"]        = Static.sharedInstance.token() as AnyObject?
        params["request_help"] = helpRequestData["id"] as! Int as AnyObject?
        
        LoadingView.showLoadingView("Sending...", parentView: self.parentController.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.joinHelpRequest(), parameters:params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    self.continueMessage("Help request", authorData: helpRequestData["author"] as! GKDictionary)
                }
                else if envelope.getStatusCode() == 400
                {
                    let d = res as? NSDictionary
                    showAlert("Already Applied", text: d?.value(forKeyPath: "object.error_message") as! String)
                }
            }
        })
    }
    
    func confirmParticipateServe(_ serveData: GKDictionary) {
        
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["serve"] = serveData["id"] as! Int as AnyObject?
        
        LoadingView.showLoadingView("Sending...", parentView: self.parentController.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.joinServe(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            print(response!)
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    
                    self.continueMessage("Serve", authorData: serveData["author"] as! GKDictionary)
                }
                else if envelope.getStatusCode() == 400
                {
                    let d = res as? NSDictionary
                    showAlert("Already Applied", text: d?.value(forKeyPath: "object.error_message") as! String)
                }
            }
        })
    }
    
    func continueMessage(_ title: String, authorData: GKDictionary) {
        
        let authorID   = authorData["id"] as! Int
        let authorName = ((authorData["first_name"] as! String).capitalized +  " " + (authorData["last_name"] as! String).capitalized)
        
        let alertController = UIAlertController(title: title, message: "Great! Make sure to write to \(authorName) to coordinate the details", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let sendAction = UIAlertAction(title: "Send", style: .default ) { (action) in
            self.parentController.appDelegate().showConversation(authorID, fullName: authorName)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(sendAction)
        
        self.parentController.present(alertController, animated: true, completion: nil)
    }
    
    func cleanData()
    {
        isSearch = false
        
        lastPageBool = false
        currentPage = 1
        currentPageSearch = 1
        lastPageBoolSearch = false
        totalCountSearch = 0
        totalCount = 0
        
        self.exploreList = []
        self.reloadData()
    }
    
    
    func updateData(data : [GKDictionary])
    {
        self.exploreList = GKMutableDictionary(array: data)
        self.tableView.reloadData()
    }
    
    @objc func requestData() {
        
        self.delegate.GKRequestData { (data) in
            let value : GKDictionary!
            if data.count != 0
            {
                value = data[0]
                let valueExists = value["country_code"] as? String != nil
                if valueExists{
                    isSos = true
                }
                else
                {
                    isSos = false
                }
             }
            self.exploreList.addObjects(from: data)
            self.reloadData()
        }
    }
    
    func reloadData() {
        //        let predicate = NSPredicate(format: "SELF.type != 5")
        //        let mutableArray = NSMutableArray(array: self.exploreList)
        //        self.exploreList = mutableArray.filteredArrayUsingPredicate(predicate) as! [GKDictionary]
        
        if isSos == true {
            tableViewTop.constant = -56
        }
        else{
            tableViewTop.constant = 0
        }
        self.tableView.reloadData()
    }
    
    func unfollow(_ userID: Int) {
        
        var params = GKDictionary()
        params["token"]   = Static.sharedInstance.token() as AnyObject?
        params["user_id"] = userID as AnyObject?
        
        LoadingView.showLoadingView("Loading...", parentView: self.parentController.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.unfollow(), parameters: params) { (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    self.lastPageBool = false
                    self.currentPage = 1
                    self.currentPageSearch = 1
                    self.lastPageBoolSearch = false
                    self.totalCountSearch = 0
                    self.totalCount = 0
                    self.exploreList = []
                    
                    self.requestData()
                }
            }
        }
    }
    
   
    // MARK: - Setup -
    
    fileprivate func setup()
    {
        view = Bundle.main.loadNibNamed(
            "GKMainFeedTableView", owner: self, options: nil)?[0] as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(view)
        
        setupTableView()
        
        lastPageBool = false
        currentPage = 1
        currentPageSearch = 1
        lastPageBoolSearch = false
        totalCountSearch = 0
        totalCount = 0
        
        activityView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100))
        activity = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activity.center = activityView.center
        activity.color=UIColor.black
        activityView.addSubview(activity)
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    func setupTableView() {
        
        self.tableView.register(
            UINib(nibName: GKExploreGratitudeTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKExploreGratitudeTableViewCell.cellIdentifier()
        )
        self.tableView.register(
            UINib(nibName: GKExplorePositivityTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKExplorePositivityTableViewCell.cellIdentifier()
        )
        self.tableView.register(
            UINib(nibName: GKExploreHelpTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKExploreHelpTableViewCell.cellIdentifier()
        )
        self.tableView.register(
            UINib(nibName: GKExploreServeTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKExploreServeTableViewCell.cellIdentifier()
        )
        self.tableView.register(
            UINib(nibName: GKExploreRequestPositivityTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKExploreRequestPositivityTableViewCell.cellIdentifier()
        )
        
        self.tableView.register(
            UINib(nibName: "SOSAddContactsTableViewCell",bundle: nil),
            forCellReuseIdentifier: SOSAddContactsTableViewCell.cellIdentifier()
        )
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.estimatedRowHeight = 320.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    func passDeleteData(index : Int)
    {
        self.sosList()
            {
                (object) in
                let dataAtIndex = object[index]
                let idAtIndex = dataAtIndex["id"] as? Int
                self.deleteContact(id: idAtIndex!)
                {
                    (isDeleted) in
                    if isDeleted == true
                    {
                        self.exploreList.removeObject(at: index)
                        
                        self.reloadData()
                        if self.deleteDelegate != nil
                        {
                            self.deleteDelegate.delete(index: index)
                        }
                    }
                    
                }
        }
    }
    
    func sosList(onCompletion: @escaping listSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.parentController.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.listSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    let object = response?.object(forKey: "object") as! [GKDictionary]
                    
                    onCompletion(object)
                    
                }
                else
                {
                    print("error")
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
            }
        })
    }
    
    
    func deleteContact(id : Int , onCompletion: @escaping deleteSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["id"] = id as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.parentController.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.deleteSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response as Any)
                    onCompletion(true)
                }
                else{
                    print("error")
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
            }
        }
        )
    }
    
    func passSendLocationData(index : Int)
    {
        self.uploadLocation()
    }
    
    
    func uploadLocation()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["msg"] = ("I am in SOS condition and my current location=" + " " + String(format: "http://maps.google.com/maps?q=loc:%f,%f",Double((self.currentLocation?.latitude)!),Double((self.currentLocation?.longitude)!))) as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.parentController.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.sendSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.parentController.navigationController?.view)
            
            if let res:AnyObject = response
            {
                print(res)
                let alertController = UIAlertController(title: "Success!", message: "SOS sent successfully.", preferredStyle: .alert)
                let OkButton: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
                }
                alertController.addAction(OkButton)
                AppDelegate.appDelegate().window?.rootViewController?.present(alertController, animated: true, completion: nil)
            }
            else
            {
                print("error")
                showConnectionError()
            }
        })
    }
    
    func showSosData(info : [GKDictionary])
    {
        self.exploreList = info as! GKMutableDictionary
        self.reloadData()
    }

}

extension GKMainFeedTableView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
        
        if (imageArray[indexPath.row] as? String) != nil {
            if (imageArray[indexPath.row] as! String).contains("vimeo"){
                cell.imageView.image = UIImage(named: "PlayButtonOverlayLarge")
                cell.imageView.contentMode = .center
            }
            else{
                cell.imageView.contentMode = .scaleToFill
            }
        }
        cell.imageView.loadImageWithUrl( imageArray[indexPath.row] as? String as NSString?)

        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tableRow = (collectionView.cellForItem(at: indexPath))?.superview?.tag
        print(tableRow as Any)
        if collectionView.cellForItem(at: indexPath as IndexPath) != nil {
            mediaArray = [Media]()
            var photo = Media()
            for img in ((exploreList[tableRow!] as! GKDictionary)["images"] as? NSArray)!
            {
                if img is String
                {
                    photo = DemoData.webMediaPhoto(url: img as! String, caption: "")
                    mediaArray.append(photo)
                }
            }
            for img in ((exploreList[tableRow!] as! GKDictionary)["videos"] as? NSArray)!
            {
                if img is String
                {
                    photo = DemoData.webMediaVideo(url: img as! String)
                    self.mediaArray.append(photo)
                }
            }
            
            let browser = MediaBrowser(delegate: self)
            browser.displayActionButton = false
            browser.displayMediaNavigationArrows = true
            browser.displaySelectionButtons = false
            browser.alwaysShowControls = true
            browser.zoomPhotosToFill = true
            browser.enableGrid = false
            browser.startOnGrid = false
            browser.enableSwipeToDismiss = true
            browser.autoPlayOnAppear = true
            browser.cachingImageCount = 2
            browser.setCurrentIndex(at: indexPath.row)
            
            let nc = UINavigationController.init(rootViewController: browser)
            nc.modalTransitionStyle = .crossDissolve
            self.parentController.present(nc, animated: true, completion: nil)
        }
    }
    
}

//MARK: MediaBrowserDelegate
extension GKMainFeedTableView: MediaBrowserDelegate {

    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < mediaArray.count {
            return mediaArray[index]
        }
        return DemoData.localMediaPhoto(imageName: "MotionBookIcon", caption: "Photo at index is Wrong")
    }
    
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return mediaArray.count
    }
}

// Delegates to handle events for the location manager.
extension GKMainFeedTableView: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        self.currentLocation = location.coordinate
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        // Display the map using the default location.
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
