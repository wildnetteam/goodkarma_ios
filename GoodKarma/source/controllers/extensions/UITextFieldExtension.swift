//
//  UITextFieldExtension.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/18/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation


public extension UITextField {
  
  func addAccessoryView(_ text: String) {
    
    let toolbar = UIToolbar()
    toolbar.sizeToFit()
    
    let buttonItem = UIBarButtonItem(title: text, style: .plain, target: self, action: #selector(self.buttonItemAction))
    
    let spacer   = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    spacer.width = 10.0
    
    let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
    toolbar.setItems([flexible, buttonItem, spacer], animated: false)
    self.inputAccessoryView = toolbar
  }
  
  @objc func buttonItemAction() {
    
    self.delegate?.textFieldShouldReturn!(self)
  }
    
}
