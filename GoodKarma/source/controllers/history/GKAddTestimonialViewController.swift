//
//  GKAddTestimonialViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKAddTestimonialViewController: UIViewController
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var scrollViewAdd: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var optionFirstImageView  : UIImageView!
    @IBOutlet weak var optionSecondImageView : UIImageView!
    @IBOutlet weak var optionThirdImageView  : UIImageView!
    @IBOutlet weak var optionFourthImageView : UIImageView!
    @IBOutlet weak var optionFifthImageView  : UIImageView!
    @IBOutlet weak var optionSixthImageView  : UIImageView!
    @IBOutlet weak var optionSeventhImageView  : UIImageView!
    @IBOutlet weak var optionEighthImageView  : UIImageView!
    @IBOutlet weak var optionNinthImageView  : UIImageView!
    @IBOutlet weak var optionTenthImageView  : UIImageView!
    
    @IBOutlet weak var optionFirstLabel  : UILabel!
    @IBOutlet weak var optionSecondLabel : UILabel!
    @IBOutlet weak var optionThirdLabel  : UILabel!
    @IBOutlet weak var optionFourthLabel : UILabel!
    @IBOutlet weak var optionFifthLabel  : UILabel!
    @IBOutlet weak var optionSixthLabel  : UILabel!
    @IBOutlet weak var optionSeventhLabel  : UILabel!
    @IBOutlet weak var optionEighthLabel  : UILabel!
    @IBOutlet weak var optionNinthLabel  : UILabel!
    @IBOutlet weak var optionTenthLabel  : UILabel!
    
    // MARK: - Properties -
    
    var selectionsOptions = [false, false, false, false, false, false, false,
                             false, false, false]
    var relatedID : Int!
    var type      : Int!
    var firstName : String!
    var byClient: Bool = false
    var customMessage : String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Testimonials"
        
        scrollViewAdd.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: 600)
        
        //    cleanBackButton()
        
        loadOptions()
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionOption(_ sender: UIButton)
    {
        selectionsOptions[sender.tag - 1] = !selectionsOptions[sender.tag - 1]
        updateSelections()
    }
    
    @IBAction func buttonCustomMessageClicked(_ sender: Any) {
        let addcustomMessgaeViewController = GKThankYouViewController(nibName: "GKThankYouViewController", bundle: nil)
        addcustomMessgaeViewController.isTestimonial = true
        addcustomMessgaeViewController.parentObject = self
        self.navigationController?.present(UINavigationController(rootViewController: addcustomMessgaeViewController), animated: true, completion: nil)
    }
    
    @IBAction func actionSend(_ sender: UIButton)
    {
        if validate()
        {
            var params = GKDictionary()
            params["token"] = Static.sharedInstance.token() as AnyObject?
            
            var selectionsParams: [String] = []
            for (index, selectionOption) in selectionsOptions.enumerated()
            {
                if selectionOption
                {
                    selectionsParams.append("\(index + 1)")
                }
            }
            selectionsParams.append(customMessage.encodeEmoji())
            
            params["detail"]     = "\(selectionsParams)" as AnyObject?
            params["related_id"] = relatedID as AnyObject?
            params["type"]       = type as AnyObject?
            
            if (self.byClient)
            {
                params["by_client"] = true as AnyObject?
            }
            
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
            
            GKServiceClient.call(GKRouterClient.addTestimonial(),
                                 parameters: params,
                                 callback:
                { (response: AnyObject?) -> () in
                    
                    LoadingView.hideLoadingView(self.navigationController?.view)
                    
                    if let res:AnyObject = response
                    {
                        let envelope = ResponseSkeleton(res)
                        
                        if envelope.getSuccess()
                        {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
            })
        }
    }
    
    // MARK: - Methods -
    
    func loadOptions()
    {
        optionFirstLabel.text  = "I am grateful " + firstName + " was able to help me."
        optionSecondLabel.text = "I am grateful " + firstName + " is very skilled."
        optionThirdLabel.text  = "I am grateful " + firstName + " is a punctual person."
        optionFourthLabel.text = "I am grateful " + firstName + " is such a friendly person."
        optionFifthLabel.text  = "I am grateful I met " + firstName + "."
        optionSixthLabel.text  = "I am grateful " + firstName + " was thoughtful."
        optionSeventhLabel.text  = "I am grateful " + firstName + " was able to put my needs first."
        optionEighthLabel.text  = "I am grateful " + firstName + " had a kind demeanor."
        optionNinthLabel.text  = "I am grateful " + firstName +
        " was able to pass along helpful thoughts and prayers."
        optionTenthLabel.text  = "I am grateful " + firstName +
        " was able to take time out to go the extra mile for me."
    }
    
    func updateSelections()
    {
        self.optionFirstImageView.image  = UIImage(named: (selectionsOptions[0]) ? "check-on" : "check-off")
        self.optionSecondImageView.image = UIImage(named: (selectionsOptions[1]) ? "check-on" : "check-off")
        self.optionThirdImageView.image  = UIImage(named: (selectionsOptions[2]) ? "check-on" : "check-off")
        self.optionFourthImageView.image = UIImage(named: (selectionsOptions[3]) ? "check-on" : "check-off")
        self.optionFifthImageView.image  = UIImage(named: (selectionsOptions[4]) ? "check-on" : "check-off")
        self.optionSixthImageView.image  = UIImage(named: (selectionsOptions[5]) ? "check-on" : "check-off")
        self.optionSeventhImageView.image  = UIImage(named: (selectionsOptions[6]) ? "check-on" : "check-off")
        self.optionEighthImageView.image  = UIImage(named: (selectionsOptions[7]) ? "check-on" : "check-off")
        self.optionNinthImageView.image  = UIImage(named: (selectionsOptions[8]) ? "check-on" : "check-off")
        self.optionTenthImageView.image  = UIImage(named: (selectionsOptions[9]) ? "check-on" : "check-off")
    }
    
    func validate() -> Bool
    {
        for selOption in selectionsOptions
        {
            if selOption
            {
                return true
            }
        }
        
        // TODO: should show error
        
        return false
    }
}
