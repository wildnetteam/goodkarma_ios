//
//  GKCancelMyJobViewController.swift
//  GoodKarma
//
//  Created by Shivani on 26/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKCancelMyJobViewController: UIViewController , UITextViewDelegate
{

    @IBOutlet weak var reasonOfCancellationTextView: UITextView!
    
    @IBOutlet weak var reasonPlaceHolderLabel: UILabel!
    var delegate : updateStatusDelegate!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        reasonOfCancellationTextView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        self.errorView.hideWithoutAnimation()
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
       
            self.reasonPlaceHolderLabel.isHidden = true
        
       
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        
            self.reasonPlaceHolderLabel.isHidden = textView.text.count > 0
       
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
     
            self.reasonPlaceHolderLabel.isHidden = textView.text.countPlainCharacters() > 0
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            
             reasonOfCancellationTextView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    
    
    
    @IBAction func actionCancelDismiss(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCancelButton(_ sender: Any)
    {
        if reasonOfCancellationTextView.text != ""
        {
            if delegate != nil
            {
                delegate.updateStatus(reason: reasonOfCancellationTextView.text)
            }
        }
        else
        {
            let alertController = UIAlertController(title: "Message", message: "Please enter Reason of Cancellation", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(alertController, animated: false, completion: nil)
        }
    }
    
    
}
