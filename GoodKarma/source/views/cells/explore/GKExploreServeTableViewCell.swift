//
//  GKExploreServeTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/22/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKExploreServeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var containerView : UIView!
    
    @IBOutlet weak var userImageView : WebImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var followButton : UIButton!
    
    @IBOutlet weak var checkImage : UIImageView!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var typeLabel : UILabel!
    
    @IBOutlet weak var skillsContainer: UIView!
    
    @IBOutlet weak var descTextView : UITextView!
    @IBOutlet weak var timestampLabel : UILabel!
    
    @IBOutlet weak var likeButton : UIButton!
    @IBOutlet weak var likeCountLabel : UILabel!
    
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    
    @IBOutlet weak var collectionViewPost: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties -
    
    var manageSkillView: GKManageSkillsView?
    var skills: [GKModelSkill] = []
    
    var delegate: (GKShowUserProfileDelegate & GKLikePostDelegate & GKFollowDelegate & GKServeParticipateDelegate & GKStartMessageDelegate & GKShareDelegate)?
    var data: GKDictionary!
    var popTip: AMPopTip!
    var tapGesture: UITapGestureRecognizer!
    
    static var isImages = Bool()

    // MARK: - Methods -
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        userImageView.layer.cornerRadius = userImageView.bounds.height * 0.5
        userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
        userImageView.isUserInteractionEnabled = true
        
        followButton.layer.cornerRadius = 4.0
        followButton.layer.borderColor = UIColor.baseColor().cgColor
        followButton.layer.borderWidth = 1.0
        
        self.selectionStyle = .none
        
        self.manageSkillView?.removeFromSuperview()
        
        self.manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: false)
        self.skillsContainer.addSubview(manageSkillView!)
        
        // Tap Gesture
        self.tapGesture = UITapGestureRecognizer(target: self,
                                              action: #selector(self.tapImageView))
        
        // User Name Label
        self.userNameLabel.addGestureRecognizer(self.tapGesture)
        self.userNameLabel.isUserInteractionEnabled = true
        
        // PopTip
        self.popTip = AMPopTip()
        self.popTip.popoverColor = UIColor.black
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionViewPost.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
    }
    
    deinit
    {
        self.userNameLabel.removeGestureRecognizer(self.tapGesture)
        self.tapGesture = nil
    }
    
    // MARK: - IBActions -
    
    @IBAction func followAction(_ sender: AnyObject)
    {
        let authorData = self.data["author"] as! GKDictionary
        
        if (self.data["follow"] as! Bool) {
            
            self.delegate?.GKUnfollow(authorData["id"] as! Int, name: ((authorData["first_name"] as! String).capitalized + (authorData["last_name"] as! String).capitalized))
        }
        else {
            
            self.delegate?.GKFollow(authorData["id"] as! Int)
        }
    }
    
    @IBAction func buttonShareClicked(_ sender: Any) {
        if delegate != nil
        {
            delegate?.GKSharePost(share_text: self.titleLabel.attributedText!)
        }
    }
    
    @IBAction func sendLike(_ sender: AnyObject) {
        
        let postID = self.data["id"] as! Int
        
        self.delegate?.GKSendLike(1, postID: postID)
    }
    
    @IBAction func participateInServe(_ sender: AnyObject) {
        
        self.delegate?.GKParticipateInServe(self.data)
    }
    
    @IBAction func sendMessage(_ sender: AnyObject) {
        
        let authorData = data["author"] as! GKDictionary
        
        let userID = authorData["id"] as! Int
        let userName = (authorData["first_name"] as! String).capitalized +  " " + (authorData["last_name"] as! String).capitalized
        
        delegate?.GKSendMessage(userID, fullName: userName)
    }
    
    @IBAction func tooltipAction(_ sender: AnyObject)
    {
        popTip.showText("Offering Help", direction: .left, maxWidth: 200, in: self,
                        fromFrame: (sender as! UIView).frame, duration: 2)
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKExploreServeTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat {
        
        if isImages {
            return 580.0
        }
        return 580.0
    }
    
    // MARK: - UICollectionView Methods
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewPost.delegate = dataSourceDelegate
        collectionViewPost.dataSource = dataSourceDelegate
        collectionViewPost.tag = row
        collectionViewPost.reloadData()
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        self.popTip.hideForced(true)
        print(self.data)
        let author = data["author"] as! GKDictionary
        let fullName = "\(author["first_name"] as! String)".capitalized + " " + "\(author["last_name"] as! String)".capitalized
        
        self.userNameLabel.text = fullName
        self.userImageView.loadImageWithUrl(author["photo_url"] as? String as NSString?)
        
        titleLabel.text = (self.data["title"] as? String)?.decodeEmoji()
        
        if self.data["help_type"] as? Int == 1
        {
            typeLabel.text = "Physical"
        }
        else {
            typeLabel.text = "Virtual"
        }
        
        // Skills
        var skillsData: [GKModelSkill] = []
        
        for skillData in (self.data["skills"] as! [GKDictionary]) {
            
            skillsData.append(GKModelSkill.readFromExploreDictionary(skillData))
        }
        
        self.skills = skillsData
        
        self.manageSkillView?.frame = skillsContainer.bounds
        self.manageSkillView?.setupSkills(self.skills, hasLimits: true)
        
        // Date
        timestampLabel.text = secondsToFormatted(self.data["date_created"] as? Int)
        // Desc
        descTextView.text = (self.data["desc"] as! String).decodeEmoji()
        // Follow
        self.updateFollow(self.data["follow"] as! Bool)
    
        // Like
        self.updateLike(self.data["like_status"] as! Bool, likeCount: self.data["like_count"] as! Int)
        
        // Zip Code
        let helpType: Int? = self.data["help_type"] as? Int
        let zipCode: String? = self.data["zip_code"] as? String
        
        if (helpType != nil && helpType == 1 && zipCode != nil)
        {
            self.zipView.isHidden = false
            self.zipLabel.text = zipCode
        }
        else
        {
            self.zipView.isHidden = true
        }
        
        // Disable buttons 
        let isMePost = Static.sharedInstance.isMe(author as NSDictionary)
        enabledView(self.followButton, enabled: !isMePost)
        enabledView(self.playButton, enabled: !isMePost)
        enabledView(self.messageButton, enabled: !isMePost)
        
        if (self.data["is_blocked"] as! Bool == true)
        {
            self.messageButton.isEnabled = false
            self.playButton.isEnabled = false
            self.followButton.isEnabled = false
            self.followButton.alpha = 0.3
        }
        else
        {
            self.messageButton.isEnabled = true
            self.playButton.isEnabled = true
            self.followButton.isEnabled = true
            self.followButton.alpha = 1
        }
    }
    
    @objc func tapImageView()
    {
        let authorData = self.data["author"] as! GKDictionary
        self.delegate?.GKShowProfile(authorData["id"] as! Int)
    }
    
    func updateLike(_ isLike: Bool, likeCount: Int) {
        
        likeCountLabel.text = "\(likeCount)"
        
        if isLike
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-on"), for: UIControlState())
            likeCountLabel.textColor = UIColor.white
        }
        else
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-off"), for: UIControlState())
            likeCountLabel.textColor = UIColor.red
        }
    }
    
    func updateFollow(_ isFollowing: Bool) {
        
        if isFollowing {
            
            checkImage.isHidden = false
            
            followButton.setTitleColor(UIColor.white, for: UIControlState())
            followButton.backgroundColor   = UIColor.baseColor()
            followButton.layer.borderColor = UIColor.clear.cgColor
            followButton.setTitle("FOLLOWING", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        }
        else {
            checkImage.isHidden = true
            
            followButton.setTitleColor(UIColor.baseColor(), for: UIControlState())
            followButton.backgroundColor   = UIColor.white
            followButton.layer.borderColor = UIColor.baseColor().cgColor
            followButton.setTitle("FOLLOW", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsets.zero
        }
    }
}
