//
//  GKSosAddContactsView.swift
//  GoodKarma
//
//  Created by Shivani on 02/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKSosAddContactsView: UIView,UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate
{
    @IBOutlet weak var addContactView: UIView!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var addContactViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var cancelButton: UIButton!
    var callingCode: CallingCode!
    var allCodes: NSArray!
    var code = "US"
    var delegate : GKSosAddContactDelegate!
    let button = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 1))
    
    func setUp()
    {
       // self.phoneNumberTextField.setFormatting("(###) ###-####-####", replacementChar: "#")
        phoneNumberTextField.keyboardType = .phonePad
        self.showPickerView()
        self.isHidden = false
        //self.phoneNumberTextField.addAccessoryView("Done")
        
        button.setTitle("+1", for: UIControlState())
        button.setTitleColor(self.phoneNumberTextField.textColor, for: UIControlState())
        button.titleLabel?.font = self.phoneNumberTextField.font
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(countryCodeAction),
                         for: .touchUpInside)
        self.phoneNumberTextField.leftView = button
        self.phoneNumberTextField.leftViewMode = .always
        self.callingCode = CallingCode();
        self.allCodes = self.callingCode.allCodes() as NSArray!;
        self.addContactView.layer.cornerRadius = 20.0
        self.addContactView.clipsToBounds = true
        self.cancelButton.layer.cornerRadius = 7.0
        self.cancelButton.clipsToBounds = true
        self.addButton.layer.cornerRadius = 7.0
        self.addButton.clipsToBounds = true
        
        nameTextField.delegate = self
        phoneNumberTextField.delegate = self
        
        let hideKeyboardOnTapGesture = UITapGestureRecognizer( target:  self, action: #selector(GKSosAddContactsView.hideKeyboardOnTap(_:)) )
        self.addGestureRecognizer( hideKeyboardOnTapGesture )
    }
    
    @objc func countryCodeAction()
    {
        let selectAction = RMAction(title: "Select", style: .done)
        { (controller) in
            
            let pickerController = (controller as! RMActionController<UIPickerView>) as! RMPickerViewController
            let row = pickerController.picker.selectedRow(inComponent: 0)
            let d = self.allCodes[row] as! NSDictionary
            let b = self.countryCodeButton()
            
            self.code = d["code"] as! String
            b.setTitle(d["dial_code"] as? String, for: UIControlState())
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .cancel)
        {
            (controller) in
        }
        
        let pickerController = RMPickerViewController(style: .default, select: selectAction as! RMAction<UIPickerView>?, andCancel: cancelAction as! RMAction<UIPickerView>?)!
        pickerController.picker.dataSource = self
        pickerController.picker.delegate   = self
        pickerController.disableBlurEffects = true
        pickerController.picker.selectRow(indexAtCode(code),
                                          inComponent: 0,
                                          animated: false)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(pickerController, animated: true, completion: nil)
    }
    
    func countryCodeButton() -> UIButton
    {
        return self.phoneNumberTextField.leftView as! UIButton
    }
    
    func currentCountryCode() -> String
    {
        let button = countryCodeButton()
        
        return button.title(for: UIControlState())!
    }
    
    func indexAtCode(_ code: String) -> Int
    {
        var index = 0
        
        for (idx, obj) in self.allCodes.enumerated()
        {
            let d = obj as? NSDictionary
            
            if (d?["code"] as! String == code)
            {
                index = idx
                break
            }
        }
        return index
    }
    
    
    @objc func hideKeyboardOnTap( _ sender : UITapGestureRecognizer )
    {
        nameTextField.resignFirstResponder()
        phoneNumberTextField.resignFirstResponder()
        addContactViewTop.constant = -30
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        let nextTag  = textField.tag + 1
        // Try to find next responder
        var nextResponder : UIResponder?
        nextResponder = ((textField.superview?.viewWithTag(nextTag)))
        if((nextResponder) != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
            addContactViewTop.constant = -30
        }
  
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        addContactViewTop.constant = -90
        return true
    }
    
    // MARK: - UIPickerViewDataSource & UIPickerViewDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.allCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int,
                    forComponent component: Int) -> String?
    {
        let d = self.allCodes[row] as? NSDictionary
        
        return NSString(format: "%@ %@", d?["dial_code"] as! String,
                        d?["name"] as! String) as String
    }
    
    @objc func donePicker(sender: UITextField)
    {
        addContactViewTop.constant = -30
        phoneNumberTextField.endEditing(true)
    }
    
    func showPickerView()
    {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action:  #selector(donePicker(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        phoneNumberTextField.inputAccessoryView = toolBar
    }
    
    // MARK: - IBACTION

    
    @IBAction func actionAdd(_ sender: Any)
    {
        addContactView.endEditing(true)
        var message : String!
        if nameTextField.text == ""
        {
           message = "Please enter name"
        }
        else if phoneNumberTextField.text == ""
        {
            message = "Please enter phone number"
        }
         else
        {
            message = "success"
        }
        if message == "success"
        {
           if delegate != nil
           {
            var info = GKDictionary()
            self.isHidden = true
            info["phn_no"] = phoneNumberTextField.text as AnyObject?
            info["country_code"] = button.titleLabel?.text as AnyObject?
            info["name"] = nameTextField.text as AnyObject?
            phoneNumberTextField.text = ""
            nameTextField.text = ""
            button.setTitle("+1", for: UIControlState())
            delegate?.passDataToBeAdded(info: info)
            }
        }
        else
        {
            let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alertController, animated: false, completion: nil)
        }
    }
    
    @IBAction func actionCancel(_ sender: Any)
    {
        self.isHidden = true
    }
    
}
