//
//  GKRegistrationViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class GKRegistrationViewController: UIViewController, CLLocationManagerDelegate, GKManageSkillsDelegate, NIAttributedLabelDelegate, UITextFieldDelegate,
    UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate
{
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var formView: UIView!
    
    @IBOutlet weak var adultImageView: UIImageView!
    @IBOutlet weak var locateImageView: UIImageView!
    @IBOutlet weak var acceptTermImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: Fields
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    //  @IBOutlet weak var birthField: UITextField!
    //  @IBOutlet weak var birthView: UIView!
    
    @IBOutlet weak var collegeMajorField: UITextField!
    @IBOutlet weak var aboutContentField: UITextView!
    @IBOutlet weak var phoneNumberTextField: VSTextField!
    
    @IBOutlet weak var zipField: UITextField!
    
    @IBOutlet weak var questionContentField: UITextView!
    
    @IBOutlet weak var skillsContainer: UIView!
    @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var termsAttributedLabel: NIAttributedLabel!
    
    // MARK: Placeholders
    
    @IBOutlet weak var aboutPlaceholderLabel    : UILabel!
    @IBOutlet weak var questionPlaceholderLabel : UILabel!
    
    // MARK: - Properties -
    
    var data: NSDictionary!
    var fromFacebook = false
    var skillsSelected: [GKModelSkill] = []
    var zipLocated: String?
    var callingCode: CallingCode!
    var allCodes: NSArray!
    var code = "US"
    
    
    //  let aboutPlaceholderText    = "About me"
    //  let questionPlaceholderText = "I would save the world by..."
    
    //location
    fileprivate var locationManager:CLLocationManager!
    fileprivate var currentLocation:CLLocation!
    
    // Error
    fileprivate var errorView: ErrorView!
    var manageSkillView: GKManageSkillsView!
    
    //  private var birthDate =  NSDate()
    
    fileprivate var isAdult       = false
    fileprivate var isCheckLocate = false
    fileprivate var isCheckAcceptTerms = false
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Sign Up"
        zipField.text = ""
        cleanBackButton()
        
        //    self.automaticallyAdjustsScrollViewInsets = false
        
        //     Phone TextField
        self.phoneNumberTextField.setFormatting("(###) ###-####-####", replacementChar: "#")
        self.phoneNumberTextField.addAccessoryView("Done")
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 1))
        button.setTitle("+1", for: UIControlState())
        button.setTitleColor(self.phoneNumberTextField.textColor, for: UIControlState())
        button.titleLabel?.font = self.phoneNumberTextField.font
        button.contentHorizontalAlignment = .right
        button.addTarget(self, action: #selector(countryCodeAction),
                         for: .touchUpInside)
        self.phoneNumberTextField.leftView = button
        self.phoneNumberTextField.leftViewMode = .always
        
        self.callingCode = CallingCode();
        self.allCodes = self.callingCode.allCodes() as NSArray!;
        
        updateLocation()
        
        self.zipField.addAccessoryView("Next")
        
        // Error View
        self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
        self.formView.addSubview(self.errorView)
        
        // Load Data
        if (self.fromFacebook) {
            
            self.firstNameField.text = (self.data["first_name"] as? String)?.capitalized
            self.lastNameField.text  = (self.data["last_name"] as? String)?.capitalized
        }
        
        aboutContentField.layer.cornerRadius = 5.0
        aboutContentField.layer.borderColor = UIColor.hightLightGray().cgColor
        aboutContentField.layer.borderWidth = 0.5
        
        questionContentField.layer.cornerRadius = aboutContentField.layer.cornerRadius
        questionContentField.layer.borderColor = aboutContentField.layer.borderColor
        questionContentField.layer.borderWidth = aboutContentField.layer.borderWidth
        
        //    birthView.addGestureRecognizer(
        //      UITapGestureRecognizer(
        //        target: self,
        //        action: #selector(GKRegistrationViewController.showSelectDates)
        //      )
        //    )
        
        manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: true, delegate: self)
        skillsContainer.addSubview(manageSkillView)
        
        // Term & Conditions
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        let termsToken1 = "By using Goodkarms, you agree to the "
        let termsLink1  = "Privacy Policy."
        
        let termsToken2 = " and the "
        let termsLink2  = "Terms and Conditions"
        
        let countTermsToken1 = termsToken1.count
        let countTermsLink1  = termsLink1.count
        
        let termsString = termsToken1 + termsLink1 + termsToken2 + termsLink2
        
        termsAttributedLabel.highlightedLinkBackgroundColor = UIColor.clear
        
        termsAttributedLabel.attributedText = NSAttributedString(
            string: termsString,
            attributes: [
                NSAttributedStringKey.paragraphStyle: paragraphStyle,
                NSAttributedStringKey.font: UIFont.fontRoboto(.Regular, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.black
            ]
        )
        termsAttributedLabel.attributesForLinks = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
        ]
        
        termsAttributedLabel.addLink(NSURL(string: "privacy_policy")! as URL, range: NSRange(location: countTermsToken1, length: countTermsLink1))
        
        let startNextLinkterms = countTermsToken1 + countTermsLink1 + termsToken2.count
        let countTermsLink2 = termsLink2.count
        
        termsAttributedLabel.addLink(NSURL(string: "terms_conditions")! as URL, range: NSRange(location: startNextLinkterms, length: countTermsLink2))
        
        //    checkPlaceholders()
        
        requestProfile()
        
        self.navigationItem.hidesBackButton = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if self.skillsSelected.count != 0
        {
            errorView.hideWithoutAnimation()
        }
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        updateSkillsView()
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: self.nextButton.bottomSide)
    }
    
    // MARK: - Protocols
    // MARK: - Protocol CLLocationManagerDelegate -
    
    //  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    //
    //    if status == .AuthorizedWhenInUse {
    //
    //      manager.startUpdatingLocation()
    ////      self.mapView.myLocationEnabled = true
    //    }
    //  }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        guard let location: CLLocation = locations.last else {
            
            return
        }
    
        self.currentLocation = location
       if !self.isCheckLocate  {
            return
        }
        self.isCheckLocate = true
        self.locateImageView.image = UIImage(named: self.isCheckLocate ? "check-on" : "check-off")

        getZipCode()
    }
    
//    func locationManager(_ manager: CLLocationManager,
//                         didUpdateToLocation newLocation: CLLocation,
//                                             fromLocation oldLocation: CLLocation)
//    {
//        
//        self.currentLocation = newLocation
//    }
    
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill) {
        
        skillsSelected = skillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView() {
        
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill]) {
        // Nothing for now
    }
    
    // MARK: - Protocol NIAttributedLabelDelegate -
    
    func attributedLabel(_ attributedLabel: NIAttributedLabel!, didSelect result: NSTextCheckingResult!, at point: CGPoint) {
        
        if result.resultType == .link {
            
            let s = result.url!.absoluteString
            
            switch s {
                
            case "privacy_policy": goPrivacyPolicy()
            case "terms_conditions": goTerms()
            default: break
            }
        }
    }
    
    func attributedLabel(_ attributedLabel: NIAttributedLabel!, shouldPresent actionSheet: UIActionSheet!, with result: NSTextCheckingResult!, at point: CGPoint) -> Bool {
        
        return false
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let baseY = self.errorView.frame.size.height;
        errorView.hideWithoutAnimation()
        
        if textField == firstNameField {
            if firstNameField.text?.countPlainCharacters() == 0 {
                
                self.errorView.showInPos(
                    CGPoint(x: self.firstNameField.frame.minX - 8,
                        y: self.firstNameField.frame.minY - baseY),
                    text: "Please enter your first name.")
                
                return false
            }
            else {
                
                lastNameField.becomeFirstResponder()
            }
        }
        if textField == lastNameField
        {
            if lastNameField.text?.countPlainCharacters() == 0
            {
                self.errorView.showInPosFromCenter(
                    CGPoint(x: self.lastNameField.frame.minX - 48,
                        y: self.lastNameField.frame.minY - baseY),
                    text: "Please enter your last name.")
                
                return false
            }
            else
            {
                collegeMajorField.becomeFirstResponder()
            }
        }
        
        if textField == phoneNumberTextField {
            
            if (self.phoneNumberTextField.text?.countPlainCharacters() == 0) {
                
                self.errorView.showInPos(
                    CGPoint(x: self.phoneNumberTextField.frame.minX - 8,
                        y: self.phoneNumberTextField.frame.minY - baseY),
                    text: "Please enter your phone number.")
                
                self.scrollToPointY(self.phoneNumberTextField.frame.minY - baseY)
                return false
            }
            else {
                
                phoneNumberTextField.resignFirstResponder()
            }
        }
        
        if textField == zipField {
            if zipField.text?.countPlainCharacters() != 5 {
                
                self.errorView.showInPos(
                    CGPoint(x: self.zipField.frame.minX - 8,
                        y: self.zipField.frame.minY - baseY),
                    text: "Zip code must be of 5 digits.")
                
                return false
            }
            else {
                
                questionContentField.becomeFirstResponder()
            }
        }
        
        return true
    }
    
    // MARK: - Protocol UITextViewDelegate -
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.errorView.hideWithoutAnimation()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.phoneNumberTextField
        {
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            let limitNumber = startString.characters.count
            if limitNumber > 10
            {
                return false
            }
            else
            {
                return true;
            }
        }
        else{
            return true;
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == self.aboutContentField {
            
            self.aboutPlaceholderLabel.isHidden = true
        }
        else if textView == self.questionContentField {
            
            self.questionPlaceholderLabel.isHidden = true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == self.aboutContentField {
            
            self.aboutPlaceholderLabel.isHidden = textView.text.characters.count > 0
        }
        else if textView == self.questionContentField {
            
            self.questionPlaceholderLabel.isHidden = textView.text.characters.count > 0
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == self.aboutContentField {
            
            self.aboutPlaceholderLabel.isHidden = textView.text.countPlainCharacters() > 0
        }
        else if textView == self.questionContentField {
            
            self.questionPlaceholderLabel.isHidden = textView.text.countPlainCharacters() > 0
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            
            if textView == aboutContentField {
                
                phoneNumberTextField.becomeFirstResponder()
            }
            else if textView == questionContentField {
                
                questionContentField.resignFirstResponder()
            }
            
            return false
        }
        
        return true
    }
    
    // MARK: - IBActions -
    
    //  @IBAction func actionAddPhoto(sender: AnyObject) {
    //
    //  }
    //
    //  @IBAction func actionNext(sender: AnyObject) {
    //
    //  }
    
    @IBAction func actionCheckLocate(_ sender: AnyObject) {
        
        if !self.isCheckLocate {
            self.isCheckLocate = !self.isCheckLocate
            self.locateImageView.image = UIImage(named: self.isCheckLocate ? "check-on" : "check-off")
            updateLocation()
        }
        else {
            
            self.isCheckLocate = !self.isCheckLocate
            self.locateImageView.image = UIImage(named: self.isCheckLocate ? "check-on" : "check-off")
        }
        //    self.locateImageView.image = UIImage(named: self.isCheckLocate ? "check-on" : "check-off")
    }
    
    @IBAction func actionAdult(_ sender: AnyObject) {
        
        self.isAdult = !self.isAdult
        self.adultImageView.image = UIImage(named: self.isAdult ? "check-on" : "check-off")
    }
    
    @IBAction func actionAcceptTerms(_ sender: AnyObject) {
        
        self.isCheckAcceptTerms = !self.isCheckAcceptTerms
        self.acceptTermImageView.image = UIImage(named: self.isCheckAcceptTerms ? "check-on" : "check-off")
    }
    
    @IBAction func actionNext(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        self.errorView.hideWithoutAnimation()
        
        if validate() {
            
            LoadingView.showLoadingView("Sending...",
                                        parentView: self.navigationController?.view,
                                        backColor: true)
            
            let params = getParams()
            print(params)
            GKServiceClient.call(GKRouterClient.registerEditProfile(), parameters: params, callback: {
                (response: AnyObject?) -> () in
                
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res: AnyObject = response
                {
                    print(res)
                    let envelope = ResponseSkeleton(res)
                    
                    if envelope.getSuccess ()
                    {
                        let data = envelope.getResource() as? NSDictionary
                        let hasPhoto         = (data?["has_photo"] as! NSNumber).boolValue
                        let hasPhoneVerified = (data?["has_phone_verified"] as! NSNumber).boolValue
                        
                        if (!hasPhoneVerified)
                        {
                            let confirmController = GKConfirmCodeViewController(nibName: "GKConfirmCodeViewController",
                                bundle: nil)
                            confirmController.data = data
                            confirmController.fromFacebook = self.fromFacebook
                            self.navigationController?.pushViewController(confirmController, animated: true)
                        }
                        else if (!hasPhoto)
                        {
                            let photoController = GKAddPhotoViewController(nibName: "GKAddPhotoViewController",
                                bundle: nil)
                            photoController.data = data
                            photoController.fromFacebook = self.fromFacebook
                            self.navigationController?.pushViewController(photoController, animated: true)
                        }
                        else
                        {
                            Static.sharedInstance.saveUserProfile(envelope.getResource() as? NSDictionary)
                            Static.sharedInstance.saveLogin(true)
                            (UIApplication.shared.delegate as! AppDelegate)
                                .showHome(self.navigationController!, loginOrRegister: true, recovery: false) //self.fromFacebook
                        }
                    }
                    else
                    {
                        showCommonError({ (type: NoInternetResultType) -> () in
                            
                            self.actionNext(0 as AnyObject)
                        })
                    }
                }
                else
                {
                    showConnectionError({ (type: NoInternetResultType) -> () in
                        
                        self.actionNext(0 as AnyObject)
                    })
                }
            })
        }
    }
    
    @IBAction func actionAddSkills(_ sender: AnyObject)
    {
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    @objc func countryCodeAction()
    {
        let selectAction = RMAction(title: "Select", style: .done)
        { (controller) in
            
            let pickerController = (controller as! RMActionController<UIPickerView>) as! RMPickerViewController
            let row = pickerController.picker.selectedRow(inComponent: 0)
            let d = self.allCodes[row] as! NSDictionary
            let b = self.countryCodeButton()
            
            self.code = d["code"] as! String
            b.setTitle(d["dial_code"] as? String, for: UIControlState())
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .cancel)
        { (controller) in
        }
        
        let pickerController = RMPickerViewController(style: .default, select: selectAction as! RMAction<UIPickerView>?, andCancel: cancelAction as! RMAction<UIPickerView>?)!
        pickerController.picker.dataSource = self
        pickerController.picker.delegate   = self
        pickerController.disableBlurEffects = true
        pickerController.picker.selectRow(indexAtCode(code),
                                          inComponent: 0,
                                          animated: false)
        self.present(pickerController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation -
    
    func goPrivacyPolicy()
    {
        let privacyViewController = GKPrivacyViewController(nibName: "GKPrivacyViewController", bundle: nil)
        self.navigationController?.pushViewController(privacyViewController, animated: true)
    }
    
    func goTerms()
    {
        let termsViewController = GKTermsViewController(nibName: "GKTermsViewController", bundle: nil)
        self.navigationController?.pushViewController(termsViewController, animated: true)
    }
    
    // MARK: - Methods -
    
    func checkPhoneNumber() {
        
    }
    
    func getZipCode()
    {
//        LoadingView.showLoadingView("Loading...",
//                                    parentView: self.navigationController?.view,
//                                    backColor: true)
        
        var params = GKDictionary()
        //    params["token"]     = Static.sharedInstance.token()
        params["latitude"]  = self.currentLocation.coordinate.latitude as AnyObject?
        params["longitude"] = self.currentLocation.coordinate.longitude as AnyObject?
        
        GKServiceClient.call(GKRouterAuth.getZipcode(), parameters:params, callback: { (response: AnyObject?) -> () in
            
            //LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res: AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess () {
                    
                    let data = envelope.getResource() as! NSDictionary
                    self.zipField.text = String(describing: data["zip_code"] as! NSNumber)
                    print("Updated zip")
                }
                else {
                    showCommonError({ (type: NoInternetResultType) -> () in
                        
                        self.getZipCode()
                    })
                }
            }
            else {
                
                showConnectionError({ (type: NoInternetResultType) -> () in
                    
                    self.getZipCode()
                })
            }
        })
    }
    
    func getParams() -> [String: AnyObject] {
        
        var params = [String: AnyObject]()
        
        params["token"] = self.data["token"] as? String as AnyObject?
        params["first_name"] = firstNameField.text as AnyObject?
        params["last_name"] = lastNameField.text as AnyObject?
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        params["birthdate"] = formatter.string(from: Date()) as AnyObject?
        params["college"] = collegeMajorField.text as AnyObject?
       
        params["skills"]  = "\((skillsSelected.map{ $0.uid! }))"  as AnyObject
    
        params["description"]  = aboutContentField.text as AnyObject?
        //    params["state"] = stateField.text
        //    params["city"]  = cityField.text
        params["_zip"]  = zipField.text as AnyObject?
        //    params["formatted_address"] = streetField.text
        
        if (self.currentLocation != nil) {
            
            params["latitude"]  = "\(self.currentLocation.coordinate.latitude)" as AnyObject?
            params["longitude"] = "\(self.currentLocation.coordinate.longitude)" as AnyObject?
        }
        
        params["save_world"] = questionContentField.text as AnyObject?
        params["phone_number"] = (currentCountryCode() + phoneNumberTextField.text!)  as String as AnyObject?
        params["isUpdate"] = "0" as AnyObject?

        print(params)
        
        return params
    }
    
    func loadInfo(_ dictionary: NSDictionary?)
    {
        guard let params = dictionary else {
            
            return
        }
        print(params)
        firstNameField.text = (params["first_name"] as? String)?.capitalized
        lastNameField.text  = (params["last_name"] as? String)?.capitalized
        
        collegeMajorField.text = params["college"] as? String
        
        skillsSelected = GKModelSkill.readFromDictionaryGetProfile(params["gkarm_skill_users"] as! [GKDictionary])
        
        if params["description"] as? String != "null" && params["description"] as? String != "NULL"{
            aboutContentField.text = params["description"] as? String
        }
        if params["_zip"] as? String != "null" && params["_zip"] as? String != "NULL"{
            zipField.text = params["_zip"] as? String
        }
        questionContentField.text = params["question"] as? String
        if params["phone_number"] as? String != "null" && params["phone_number"] as? String != "NULL"{
            phoneNumberTextField.text = params["phone_number"] as? String
        }
        
        updateSkillsView()
    }
    
    func scrollToPointY(_ pointY: CGFloat)
    {
        if scrollView.contentSize.height - pointY > scrollView.bounds.height
        {
            scrollView.setContentOffset(CGPoint(x: 0.0, y: pointY - 64.0), animated: true)
        }
        else
        {
            print("not scroll")
        }
    }
    
    func updateSkillsView() {
        
        manageSkillView.frame = skillsContainer.bounds
        manageSkillView.setupSkills(skillsSelected)
        
        self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    
    func validate() -> Bool
    {
        let baseY = self.errorView.frame.size.height;
        
        if (self.firstNameField.text?.countPlainCharacters() < 1)
        {
            self.errorView.showInPos(
                CGPoint(x: self.firstNameField.frame.minX - 8,
                    y: self.firstNameField.frame.minY - baseY),
                text: "Please enter your first name.")
            
            self.scrollToPointY(self.firstNameField.frame.minY - baseY)
            return false
        }
        
        if (self.lastNameField.text?.countPlainCharacters() < 1)
        {
            self.errorView.showInPosFromCenter(
                CGPoint(x: self.lastNameField.frame.minX - 48,
                    y: self.lastNameField.frame.minY - baseY),
                text: "Please enter your last name.")
            
            self.scrollToPointY(self.lastNameField.frame.minY - baseY)
            return false
        }
        if self.phoneNumberTextField.text?.count == 0
        {
            self.errorView.showInPos(
                CGPoint(x: self.phoneNumberTextField.frame.minX - 8,
                        y: self.phoneNumberTextField.frame.minY - baseY),
                text: "Please enter your Phone Number.")
            
            self.scrollToPointY(self.phoneNumberTextField.frame.minY - baseY)
            return false
        }
        
        if (self.phoneNumberTextField.text?.count !=  10)
        {
            self.errorView.showInPos(
                CGPoint(x: self.phoneNumberTextField.frame.minX - 8,
                        y: self.phoneNumberTextField.frame.minY - baseY),
                text: "Phone number must be of 10 digits.")
            
            self.scrollToPointY(self.phoneNumberTextField.frame.minY - baseY)
            return false
        }
        //    if !isAdult
        //    {
        //      showMessageError(
        //        "Alert",
        //        text: "You must be 18 years or older to use GoodKarms.",
        //        callback: { (type: NoInternetResultType) in
        //      })
        //
        //      return false
        //    }
        
        if (self.phoneNumberTextField.text?.countPlainCharacters() == 0)
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.phoneNumberTextField.frame.minX - 8,
                    y: self.phoneNumberTextField.frame.minY - baseY
                ),
                text: "Please enter your phone number.")
            
            self.scrollToPointY(self.phoneNumberTextField.frame.minY - baseY)
            return false
        }
        
        if self.skillsSelected.count == 0
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.skillsContainer.frame.minX,
                    y: self.skillsContainer.frame.minY - baseY
                ),
                text: "Please select atleast one skill.")
            
            self.scrollToPointY(self.skillsContainer.frame.minY - baseY)
            return false
        }
        
        if (self.zipField.text?.countPlainCharacters() != 5)
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.zipField.frame.minX - 8,
                    y: self.zipField.frame.minY - baseY
                ),
                text: "Zip code must be of 5 digits.")
            
            self.scrollToPointY(self.zipField.frame.minY - baseY)
            return false
        }
        
        if !isCheckAcceptTerms
        {
            showMessageError(
                "Alert",
                text: "Please read and accept the Terms of Use and Privacy Policy before continuing.",
                callback: { (type: NoInternetResultType) in
            })
            
            return false
        }
        
        return true
    }
    
    func requestProfile()
    {
        if (self.data == nil)
        {
            return
        }
        
        LoadingView.showLoadingView("Loading...",
                                    parentView: self.navigationController?.view,
                                    backColor: true)
        
        var params = [String: AnyObject]()
        
        
        params["token"] = self.data["token"] as? String as AnyObject?
        
        GKServiceClient.call(GKRouterClient.registerProfile(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            print(response!)
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    let resource = (envelope.getResource() as? NSDictionary) //?.object(at: 0)) as! NSDictionary
                    self.loadInfo(resource)
                }
                else {
                    showCommonError({ (type: NoInternetResultType) -> () in
                        self.requestProfile()
                    })
                }
            }
            else {
                
                showConnectionError({ (type: NoInternetResultType) -> () in
                    self.requestProfile()
                })
            }
        })
    }
    
    func indexAtCode(_ code: String) -> Int
    {
        var index = 0
        
        for (idx, obj) in self.allCodes.enumerated()
        {
            let d = obj as? NSDictionary
            
            if (d?["code"] as! String == code)
            {
                index = idx
                break
            }
        }
        
        return index
    }
    
    func countryCodeButton() -> UIButton
    {
        return self.phoneNumberTextField.leftView as! UIButton
    }
    
    func currentCountryCode() -> String
    {
        let button = countryCodeButton()
        
        return button.title(for: UIControlState())!
    }
    
    // MARK: - Location -
    
    func initLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 50
        //    if  (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0 {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        //    }
        //    locationManager.startUpdatingLocation()
    }
    
    func updateLocation()
    {
        if locationManager == nil
        {
            initLocation()
        }
        else
        {
            locationManager.stopUpdatingLocation()
            initLocation()
        }
        
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - UIPickerViewDataSource & UIPickerViewDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.allCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int,
                    forComponent component: Int) -> String?
    {
        let d = self.allCodes[row] as? NSDictionary
        
        return NSString(format: "%@ %@", d?["dial_code"] as! String,
                        d?["name"] as! String) as String
    }
}
