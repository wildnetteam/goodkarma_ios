//
//  GKNotificationPreferenceTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/9/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNotificationPreferenceTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var descLabel: UILabel!
  @IBOutlet weak var cellSwitch: UISwitch!
  
  // MARK: - Override UITableViewCell -
  
  override func awakeFromNib() {
    
    super.awakeFromNib()
    
    self.cellSwitch.onTintColor = UIColor.baseColor()
    self.selectionStyle = .none
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKNotificationPreferenceTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 50.0
  }
  
  // MARK: - Methods -
  
  func loadSwitch(_ onOff: Bool)
  {
    cellSwitch.isOn = onOff
  }
}
