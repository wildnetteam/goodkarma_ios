//
//  GKWalkthroughViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/30/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKWalkthroughViewController: UIViewController, UIScrollViewDelegate {
    
    struct SliderInfo {
      var title: String
      var desc: String
    }
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
  
  var moveToEnd = false
  
    override func viewDidLoad() {
      
      super.viewDidLoad()
      
      self.automaticallyAdjustsScrollViewInsets = false
      
//      scrollView.scrollsToTop = false
      scrollView.isPagingEnabled = true
      scrollView.bounces = false
      scrollView.clipsToBounds = true
      scrollView.delegate = self
      scrollView.backgroundColor = UIColor.white
      
      scrollView.showsVerticalScrollIndicator = false
      scrollView.showsHorizontalScrollIndicator = false
      scrollView.scrollsToTop = false
      
      pageControl.currentPage = 0
      pageControl.numberOfPages = getInfoSliders().count
      
//      self.navigationController?.navigationBarHidden = true
      
      self.startButton.alpha = 0.0
      //      self.view.backgroundColor = UIColor.redColor()
      
//            setupSliders()
    }
  
//  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = true
  }
      override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        setupSliders()
//        let bounds = scrollView.bounds
//        
//        scrollView.contentSize = CGSizeMake(
//          bounds.size.width * CGFloat(getInfoSliders().count),
//          bounds.size.height
        //        )
        
        if self.moveToEnd {
          scrollView.setContentOffset(
            CGPoint(
              x: scrollView.contentSize.width - scrollView.bounds.size.width, y: 0
            ),
            animated: false
          )
        }
      }
//
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
      let widthScrollView = scrollView.frame.size.width
      let fractionalPage  = Double(scrollView.contentOffset.x / widthScrollView)
      let page = lround(fractionalPage)
      
      pageControl.currentPage = page
      
    
//      print(alphaButton)
//      print(fractionalPage)
      
      if page == (getInfoSliders().count - 1) {
        
        startButton.alpha = 1.0
      }
      else {
        
        startButton.alpha = 0.0
      }
//
//      print("\(CGFloat(getInfoSliders().count) / CGFloat(fractionalPage))")
    }
  
  // MARK: - Methods -
  
    func getInfoSliders() -> [SliderInfo] {
      
      return [
        SliderInfo(
          title: "Seek Help",
          desc: "GoodKarms is a network of positive, generous users who want to help each other selflessly. Rather than completing tasks for money, users complete tasks for GoodKarms and a more enriching lifestyle."
        ),
        SliderInfo(
          title: "Offer Help",
          desc: "Can't find someone to help? GoodKarms makes it simple. Offer assistance based on the areas and the skills you feel comfortable with."
        ),
        SliderInfo(
          title: "Gratitude Log",
          desc: "Our gratitude log lets you broadcast all the things you’re grateful for. It’s a great way to get inspired and can be used as a virtual journal to extend your gratitude to the Universe."
        ),
        SliderInfo(
          title: "Positivity",
          desc: "Feeling a little off, or your energy not where it is supposed to be? Ask our community of generous users to send you positive vibes through our messaging options."
        )
      ]
    }
    
    func setupSliders() {
      
      let infoSliders = getInfoSliders()
      
      var sliderFrame = self.scrollView.bounds
      sliderFrame.origin.y = 0.0
      
      for (index, infoSlider) in infoSliders.enumerated() {
        
        let sliderView = Bundle.main.loadNibNamed("GKWalkthroughView", owner: self, options: nil)?[0] as! GKWalkthroughView
        sliderView.frame = sliderFrame
        sliderView.setupTitle(infoSlider.title, desc: infoSlider.desc, index: index + 1)
        scrollView.addSubview(sliderView)
        
        sliderFrame.origin.x += self.scrollView.bounds.size.width
      }
      scrollView.contentSize = CGSize(
        width: self.scrollView.bounds.width * CGFloat(getInfoSliders().count),
        height: self.scrollView.bounds.height
      )
  }
  
  @IBAction func letsStartAction() {
    
    let loginController = GKLoginViewController(nibName: "GKLoginViewController", bundle: nil)
    loginController.navigationController?.isNavigationBarHidden = true
    self.navigationController?.pushViewController(loginController, animated: true)
    
  }
  
}
