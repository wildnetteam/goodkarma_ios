//
//  GKFollowSocialFriendsView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/5/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKFollowSocialFriendsView: UIView, GKFollowDelegate, UITableViewDataSource, UITableViewDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet var view: UIView!
  @IBOutlet var contentView: UIView!
  @IBOutlet weak var tableView: UITableView!
//  @IBOutlet weak var skipButton: UIButton!
  
  // MARK: - Properties -
  
  fileprivate var facebookIDs: [String] = []
  fileprivate var friends          : [GKDictionary] = []
  var parentController: UIViewController?
  var delegate: GKFriendSocialDelegate?
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol GKFollowDelegate -
  
  func GKFollow(_ userID: Int) {
    
    var params = GKDictionary()
    params["token"]   = Static.sharedInstance.token() as AnyObject?
    params["user_id"] = userID as AnyObject?
    
    LoadingView.showLoadingView("Loading...", parentView: self.view, backColor: true)
    
    GKServiceClient.call(GKRouterClient.follow(), parameters: params) { (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          self.checkFollow()
        }
      }
    }
  }
  
  func GKUnfollow(_ userID: Int, name: String) {
    
    let alertController = UIAlertController(title: nil, message: "Unfollow \(name)?", preferredStyle: .actionSheet)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
    }
    
    let destroyAction = UIAlertAction(title: "Unfollow", style: .destructive) { (action) in
      self.unfollow(userID)
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(destroyAction)
    
    self.parentController?.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return self.friends.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    
    let cell = tableView.dequeueReusableCell(
      withIdentifier: GKFollowerTableViewCell.cellIdentifier(), for: indexPath) as! GKFollowerTableViewCell
    
    let dataFriend = self.friends[indexPath.row]
    
    cell.delegate = self
    cell.loadData(dataFriend, mutual_follow: dataFriend["following"] as! Bool)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return GKFollowerTableViewCell.heightForCell()
  }
  
  // MARK: - IBActions -

  @IBAction func actionSkip() {
    
    self.delegate?.GKFriendSocialSkip()
  }
  
  // MARK: - Methods -
  
  func checkFollow() {
    
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["facebook_id_list"] = self.facebookIDs as AnyObject?
    
//    LoadingView.showLoadingView(
//      "Loading...",
//      parentView: self.view, backColor: true
//    )
//    
    GKServiceClient.call(GKRouterClient.checkFollow(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          if let data = envelope.getResource() as? [GKDictionary] {
            
            self.friends = []
            
            for dataFriend in data {
              
              let following = dataFriend["following"] as! Bool
              
              if !following {
                
                self.friends.append(dataFriend)
              }
            
            }
            self.reloadData()
            
            if self.friends.count == 0 {
              
              self.delegate?.GKFriendSocialSkip()
            }
          }
        }
      }
    })
  }

  func close() {
    
    self.removeFromSuperview()
  }

  func loadfriendsData(_ resultFriends: [GKDictionary]? ) {
    
    guard let friendsArray = resultFriends else {
      
      return
    }
    
    self.friends = friendsArray
    
    self.reloadData()
  }
  
  func requestFriendsList() {
    
    LoadingView.showLoadingView("Loading...", parentView: self.view, backColor: true)
    
    let params = ["fields": "id, first_name, last_name, name, email, picture,cover,name,location"]

    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me/friends?fields=cover,name,location", parameters:params)
    
    graphRequest.start {
      (connection, result, error) -> Void in
      
      if (result != nil) {

        let dataResult  = (result as! NSDictionary)["data"] as! [GKDictionary]
        
        self.facebookIDs = dataResult.map{ $0["id"] as! String }
        self.checkFollow()
      }
      else {
        
        LoadingView.showLoadingView("Loading...", parentView: self.view, backColor: true)
        self.delegate?.GKFriendSocialSkip()
        
        print("error")
      }
    }
  }

  func reloadData() {
    
    tableView.reloadData()
  }
  
  func unfollow(_ userID: Int) {
    
    var params = GKDictionary()
    params["token"]   = Static.sharedInstance.token() as AnyObject?
    params["user_id"] = userID as AnyObject?
    
    GKServiceClient.call(GKRouterClient.unfollow(), parameters: params) { (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          self.checkFollow()
        }
      }
    }
  }

  // MARK: - Setup -
  
  func setup() {
    
    view = Bundle.main.loadNibNamed("GKFollowSocialFriendsView", owner: self,
                                              options: nil)?[0] as! UIView
    view.frame = self.bounds
    view.clipsToBounds = true
    view.frame = self.bounds
    self.addSubview(view)
    
    self.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
    
    contentView.layer.cornerRadius = 8.0
    
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    self.tableView.register(
      UINib(nibName: GKFollowerTableViewCell.cellIdentifier() ,bundle: nil),
      forCellReuseIdentifier: GKFollowerTableViewCell.cellIdentifier()
    )
  }
}
