//
//  GKMoreDetailView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/31/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKMoreDetailView: UIView {
  
  // MARK: - IBOutlets -
  
  @IBOutlet var view: UIView!
  @IBOutlet var backView: UIView!
  @IBOutlet var containerView: UIView!
  @IBOutlet var detailLabel: UILabel!
  
  @IBOutlet var moreOverlay: UIView!
  @IBOutlet var moreButton: UIButton!
  
  // MARK: - Properties -
  
  var moreCallback: (() -> (Void))?
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionClose(_ sender: AnyObject)
  {
    closeView()
  }
  
  @IBAction func actionMore(_ sender: AnyObject)
  {
    moreCallback!()
    closeView(false)
  }
  
  // MARK: - Static Methods -
  
  static func show(_ detail: String, alignmentLeft: Bool = false, buttonTitle: String? = nil, callback: (() -> ())? = nil)
  {  
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    
    let moreDetailView = GKMoreDetailView(frame: CGRect(x: 0.0, y: 0.0, width: window.frame.width, height: window.frame.height))
    moreDetailView.alpha = 0
    moreDetailView.detailLabel.text = detail.decodeEmoji()
    
    if alignmentLeft
    {
      moreDetailView.detailLabel.textAlignment = .left
    }
    
    if let title = buttonTitle
    {
      moreDetailView.moreCallback = callback
      moreDetailView.moreButton.setTitle(title, for: UIControlState())
    }
    else
    {
      moreDetailView.moreOverlay.isHidden = true
      moreDetailView.moreButton.isHidden = true
    }

    window.addSubview(moreDetailView)
    
    UIView.animate(withDuration: 0.5, delay:0,
                               options:UIViewAnimationOptions(),
                               animations:
      {
        moreDetailView.alpha = 1
      },
                               completion:
      { _ in
    })
  }
    
    static func showWithAttributed(_ detail: NSAttributedString, alignmentLeft: Bool = false, buttonTitle: String? = nil, callback: (() -> ())? = nil)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window!
        
        let moreDetailView = GKMoreDetailView(frame: CGRect(x: 0.0, y: 0.0, width: window.frame.width, height: window.frame.height))
        moreDetailView.alpha = 0
        moreDetailView.detailLabel.attributedText = detail
        
        if alignmentLeft
        {
            moreDetailView.detailLabel.textAlignment = .left
        }
        
        if let title = buttonTitle
        {
            moreDetailView.moreCallback = callback
            moreDetailView.moreButton.setTitle(title, for: UIControlState())
        }
        else
        {
            moreDetailView.moreOverlay.isHidden = true
            moreDetailView.moreButton.isHidden = true
        }
        
        window.addSubview(moreDetailView)
        
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                moreDetailView.alpha = 1
            },
                                   completion:
            { _ in
        })
    }
  
  // MARK: - Methods -
  
  @objc func closeView(_ animated: Bool = true)
  {
    if animated
    {
      UIView.animate(
        withDuration: 0.3, delay:0, options:UIViewAnimationOptions(), animations:
        {
          self.alpha = 0
        },
        completion:
        { _ in
          self.removeFromSuperview()
      })
    }
    else
    {
      self.alpha = 0
      self.removeFromSuperview()
    }
  }
  
  fileprivate func setup()
  {
    view = Bundle.main.loadNibNamed("GKMoreDetailView", owner: self,
                                              options: nil)?[0] as! UIView
    view.frame = self.bounds
    view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    self.addSubview(view)
    
//    self.backgroundColor = UIColor.clearColor()
    
    self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.closeView)))
    self.containerView.layer.cornerRadius = 6
    
    self.moreButton.layer.cornerRadius = 6
  }
}
