//
//  GKCompanyAddPeopleTableViewCell.swift
//  GoodKarma
//
//  Created by Shatakshi on 07/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKCompanyAddPeopleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRole: UILabel!
    
    @IBOutlet weak var buttonGroupadmin: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
