////
////  GKInitialViewController.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/20/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import MBProgressHUD
//
//class GKInitialViewController: UIViewController, FBSDKAppInviteDialogDelegate {
//  
//  @IBOutlet var imageIcon: UIImageView!
//  
//  //    var welcomeView:WelcomeView!
//  //    var welcomeBusinessView:WelcomeBusinessView!
//  //    var inviteView:InviteFriendsView!
//      var initialHud: MBProgressHUD!
//  var userHasBusiness:Bool = false
//  
//  let facebookReadPermissions = ["public_profile",
//                                 "email",
//                                 "user_friends",
//                                 "user_about_me",
//                                 "user_education_history"
//    , "user_location"
//    , "user_likes"
//    ,"user_relationships"
//  ]
//  
//  let facebookWritePermissions = [
//    "manage_pages",
//    "publish_actions"
//  ]
//  
//  
//  override func viewDidLoad() {
//    super.viewDidLoad()
//    
//    // Do any additional setup after loading the view.
//    let imageLogo = UIImage(named: "logo-vetted-green.png")
//    let imageWidth = imageLogo?.size.width
//    let imageHeight = imageLogo?.size.height
//    
//    let imgViewLogo = UIImageView(frame: CGRectMake(PGCView.Metrics.screen.width / 2 - (imageWidth! * 0.12) / 2 - 5, 90, imageWidth! * 0.12, imageHeight! * 0.12))
//    imgViewLogo.image = UIImage(named: "logo-vetted-green.png")
//    view.addSubview(imgViewLogo)
//    
//    var yTopFirstButton = CGFloat(330)
//    var spaceBetweenButtons = CGFloat(30)
//    
//    if PGCView.Metrics.screen.width > 320
//    {
//      yTopFirstButton = 360
//      spaceBetweenButtons = 60
//    }
//    
//    let buttonWhatis = UIButton(frame: CGRectMake(PGCView.Metrics.screen.width / 2 - 280 / 2, yTopFirstButton, 280, 50))
//    //buttonWhatis.setImage(UIImage(named: "what-is-show-out.png"), forState: .Normal)
//    buttonWhatis.backgroundColor  = UIColor(red: 120.0 / 255.0, green: 170.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
//    buttonWhatis.setTitle("What is Vetted?", forState: .Normal)
//    buttonWhatis.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
//    buttonWhatis.layer.cornerRadius = 4
//    buttonWhatis.addTarget(self,
//                           action: #selector(GKInitialViewController.actionButton(_:)),
//                           forControlEvents: UIControlEvents.TouchUpInside		)
//    buttonWhatis.tag = 1
//    view.addSubview(buttonWhatis)
//    
//    
//    let buttonSigninFacebook = UIButton(frame: CGRectMake(PGCView.Metrics.screen.width / 2 - 280 / 2, buttonWhatis.frame.origin.y + buttonWhatis.frame.size.height + spaceBetweenButtons, 280, 50))
//    //buttonSigninFacebook.setImage(UIImage(named: "connect-with-facebook-3006.png"), forState: .Normal)
//    buttonSigninFacebook.backgroundColor  = UIColor(red: 68 / 255, green: 127 / 255, blue: 215 / 255, alpha: 1.0)
//    buttonSigninFacebook.setTitle("Connect with Facebook", forState: .Normal)
//    buttonSigninFacebook.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
//    buttonSigninFacebook.layer.cornerRadius = 4
//    buttonSigninFacebook.addTarget(self,
//                                   action: #selector(GKInitialViewController.actionButton(_:)),
//                                   forControlEvents: UIControlEvents.TouchUpInside)
//    buttonSigninFacebook.tag = 2
//    view.addSubview(buttonSigninFacebook)
//    
//    if PGCView.Metrics.screen.width > 320
//    {
//      if PGCView.Metrics.screen.width == 414
//      {
//        let myNormalAttributedTitle = NSAttributedString(string: "Terms of Service",
//                                                         attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//        
//        let buttonTerms = UIButton(frame: CGRectMake(45, PGCView.Metrics.screen.height - 40, 90, 30))
//        buttonTerms.titleLabel?.font = UIFont(name: "Arial", size: 11)
//        buttonTerms.setAttributedTitle(myNormalAttributedTitle, forState: .Normal)
//        buttonTerms.tag = 4
//        buttonTerms.addTarget(self,
//                              action: #selector(GKInitialViewController.actionButton(_:)),
//                              forControlEvents: UIControlEvents.TouchUpInside)
//        view.addSubview(buttonTerms)
//        
//        
//        let myNormalAttributedTitle2 = NSAttributedString(string: "Privacy Policy",
//                                                          attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//        
//        let buttonPrivacy = UIButton(frame: CGRectMake(145, PGCView.Metrics.screen.height - 40, 90, 30))
//        buttonPrivacy.titleLabel?.font = UIFont(name: "Arial", size: 11)
//        buttonPrivacy.setAttributedTitle(myNormalAttributedTitle2, forState: .Normal)
//        buttonPrivacy.tag = 5
//        buttonPrivacy.addTarget(self,
//                                action: #selector(GKInitialViewController.actionButton(_:)),
//                                forControlEvents: UIControlEvents.TouchUpInside)
//        view.addSubview(buttonPrivacy)
//        
//        
//        let lblTextInfo = UILabel(frame: CGRectMake(0, PGCView.Metrics.screen.height - 40, PGCView.Metrics.screen.width, 30))
//        lblTextInfo.text = "                          |                            |  Vetted 2016 ©  |  V0.4"
//        lblTextInfo.font = UIFont(name: "Arial", size: 11)
//        lblTextInfo.textColor = UIColor.grayColor()
//        lblTextInfo.textAlignment = .Center
//        view.addSubview(lblTextInfo)
//      }
//      else
//      {
//        let myNormalAttributedTitle = NSAttributedString(string: "Terms of Service",
//                                                         attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//        
//        let buttonTerms = UIButton(frame: CGRectMake(30, PGCView.Metrics.screen.height - 40, 90, 30))
//        buttonTerms.titleLabel?.font = UIFont(name: "Arial", size: 11)
//        buttonTerms.setAttributedTitle(myNormalAttributedTitle, forState: .Normal)
//        buttonTerms.tag = 4
//        buttonTerms.addTarget(self,
//                              action: #selector(GKInitialViewController.actionButton(_:)),
//                              forControlEvents: UIControlEvents.TouchUpInside)
//        view.addSubview(buttonTerms)
//        
//        
//        let myNormalAttributedTitle2 = NSAttributedString(string: "Privacy Policy",
//                                                          attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//        
//        let buttonPrivacy = UIButton(frame: CGRectMake(125, PGCView.Metrics.screen.height - 40, 90, 30))
//        buttonPrivacy.titleLabel?.font = UIFont(name: "Arial", size: 11)
//        buttonPrivacy.setAttributedTitle(myNormalAttributedTitle2, forState: .Normal)
//        buttonPrivacy.tag = 5
//        buttonPrivacy.addTarget(self,
//                                action: #selector(GKInitialViewController.actionButton(_:)),
//                                forControlEvents: UIControlEvents.TouchUpInside)
//        view.addSubview(buttonPrivacy)
//        
//        
//        let lblTextInfo = UILabel(frame: CGRectMake(0, PGCView.Metrics.screen.height - 40, PGCView.Metrics.screen.width, 30))
//        lblTextInfo.text = "                          |                            |  Vetted 2016 ©  |  V0.4"
//        lblTextInfo.font = UIFont(name: "Arial", size: 11)
//        lblTextInfo.textColor = UIColor.grayColor()
//        lblTextInfo.textAlignment = .Center
//        view.addSubview(lblTextInfo)
//      }
//      
//    }
//    else
//    {
//      let myNormalAttributedTitle = NSAttributedString(string: "Terms of Service",
//                                                       attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//      
//      let buttonTerms = UIButton(frame: CGRectMake(5, PGCView.Metrics.screen.height - 40, 90, 30))
//      buttonTerms.titleLabel?.font = UIFont(name: "Arial", size: 11)
//      buttonTerms.setAttributedTitle(myNormalAttributedTitle, forState: .Normal)
//      buttonTerms.tag = 4
//      buttonTerms.addTarget(self,
//                            action: #selector(GKInitialViewController.actionButton(_:)),
//                            forControlEvents: UIControlEvents.TouchUpInside)
//      view.addSubview(buttonTerms)
//      
//      
//      let myNormalAttributedTitle2 = NSAttributedString(string: "Privacy Policy",
//                                                        attributes: [NSForegroundColorAttributeName : UIColor.grayColor()])
//      
//      let buttonPrivacy = UIButton(frame: CGRectMake(100, PGCView.Metrics.screen.height - 40, 90, 30))
//      buttonPrivacy.titleLabel?.font = UIFont(name: "Arial", size: 11)
//      buttonPrivacy.setAttributedTitle(myNormalAttributedTitle2, forState: .Normal)
//      buttonPrivacy.tag = 5
//      buttonPrivacy.addTarget(self,
//                              action: #selector(GKInitialViewController.actionButton(_:)),
//                              forControlEvents: UIControlEvents.TouchUpInside)
//      view.addSubview(buttonPrivacy)
//      
//      
//      let lblTextInfo = UILabel(frame: CGRectMake(0, PGCView.Metrics.screen.height - 40, PGCView.Metrics.screen.width, 30))
//      lblTextInfo.text = "                             |                            |  Vetted 2016 ©  |  V0.4"
//      lblTextInfo.font = UIFont(name: "Arial", size: 11)
//      lblTextInfo.textColor = UIColor.grayColor()
//      lblTextInfo.textAlignment = .Center
//      view.addSubview(lblTextInfo)
//    }
//    
//    //      if (self.delegate().gettingPhoneNumber)
//    //      {
//    //        NSNotificationCenter.defaultCenter().addObserver(self,
//    //                                                         selector: #selector(GKInitialViewController.finishGetPhoneNumberObserver), name: "FinishGetPhoneNumber",
//    //                                                         object: nil)
//    //        self.initialHud = MBProgressHUD.showHUDAddedTo(view, animated: true)
//    //      }
//  }
//  
//  deinit
//  {
//    NSNotificationCenter.defaultCenter().removeObserver(self)
//  }
//  
//  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
//    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
//  }
//  
//  required init?(coder aDecoder: NSCoder) {
//    super.init(coder: aDecoder)!
//  }
//  
//  
//  override func didReceiveMemoryWarning() {
//    super.didReceiveMemoryWarning()
//    // Dispose of any resources that can be recreated.
//  }
//  
//  
//  override func viewDidAppear(animated: Bool) {
//    //super.viewDidAppear(Bool)
//    super.viewDidAppear(animated)
//    
//    
//  }
//  override func viewWillAppear(animated: Bool) {
//    //super.viewDidAppear(Bool)
//    super.viewWillAppear(animated)
//    
//    
//    
//  }
//  static func xibName() -> String {
//    return "GKInitialViewController"
//  }
//  
//  @IBAction func actionButton(sender: UIButton) {
//    
//    
//    //      switch sender.tag {
//    //      case 1:
//    //        self.presentViewController(WhatIsViewController(nibName: WhatIsViewController.xibName(), bundle: nil), animated: true, completion: nil)
//    //        break
//    //      case 2:
//    //        connectWithFacebookForPublishPermissions()
//    //        break
//    //      case 3:
//    //
//    //        break
//    //      case 4:
//    //        self.presentViewController(TermsViewController(nibName: TermsViewController.xibName(), bundle: nil), animated: true, completion: nil)
//    //        break
//    //      case 5:
//    //        self.presentViewController(PrivacyViewController(nibName: PrivacyViewController.xibName(), bundle: nil), animated: true, completion: nil)
//    //        break
//    //      default :
//    //        break
//    //      }
//    
//    
//    
//  }
//  
//  
//  func connectWithFacebookNew(){
//    self.loginToFacebookWithSuccessNew({
//      (response:()) -> () in
////      self.returnUserData()
//      
//      }, andFailure: {
//        (error:NSError?) -> () in
//    })
//  }
//  
//  
//  func loginToFacebookWithSuccessNew(successBlock: () -> (),
//                                     andFailure failureBlock: (NSError?) -> ()) {
//    
//    
//    //            if FBSDKAccessToken.currentAccessToken() != nil {
//    //
//    //                //Otherwise do:
//    //                self.returnUserData()
//    //
//    //                return
//    //            }
//    
//    FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
//      if error != nil {
//        //According to Facebook:
//        //Errors will rarely occur in the typical login flow because the login dialog
//        //presented by Facebook via single sign on will guide the users to resolve any errors.
//        
//        // Process error
//        FBSDKLoginManager().logOut()
//        failureBlock(error)
//      } else if result.isCancelled {
//        // Handle cancellations
//        FBSDKLoginManager().logOut()
//        failureBlock(nil)
//      } else {
//        // If you ask for multiple permissions at once, you
//        // should check if specific permissions missing
//        var allPermsGranted = true
//        
//        //result.grantedPermissions returns an array of _NSCFString pointers
//        let perms = result.grantedPermissions as NSSet
//        
//        let grantedPermissions = perms.allObjects.map( {"\($0)"} )
//        //        let grantedPermissions = result.grantedPermissions.allObjects.map( {"\($0)"} )
//        
//        for permission in self.facebookReadPermissions {
//          if !grantedPermissions.contains(permission){
//            allPermsGranted = false
//            break
//          }
//        }
//        
//        if allPermsGranted {
//          // Do work
//          let _ = result.token.tokenString
//          let _ = result.token.userID
//          
//          successBlock()
//        }else {
//          //The user did not grant all permissions requested
//          //Discover which permissions are granted
//          //and if you can live without the declined ones
////          showConnectionError()
//          failureBlock(nil)
//        }
//      }
//    })
//    
//  }
//  
//  func connectWithFacebookForPublishPermissions(){
//    self.loginToFacebookWithPusblihPermissionsSuccess({
//      (response:()) -> () in
////      self.getUserPagesData()
//      
//      }, andFailure: {
//        (error:NSError?) -> () in
//    })
//  }
//  
//  func loginToFacebookWithPusblihPermissionsSuccess(successBlock: () -> (),
//                                                    andFailure failureBlock: (NSError?) -> ()) {
//    
//    
//    /* if FBSDKAccessToken.currentAccessToken() != nil {
//     
//     //Otherwise do:
//     println(FBSDKAccessToken.currentAccessToken().tokenString)
//     self.getUserPagesData()
//     
//     return
//     }*/
//    
//    FBSDKLoginManager().logInWithPublishPermissions(self.facebookWritePermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
//      if error != nil {
//        //According to Facebook:
//        //Errors will rarely occur in the typical login flow because the login dialog
//        //presented by Facebook via single sign on will guide the users to resolve any errors.
//        
//        // Process error
//        FBSDKLoginManager().logOut()
//        failureBlock(error)
//      } else if result.isCancelled {
//        // Handle cancellations
//        FBSDKLoginManager().logOut()
//        failureBlock(nil)
//      } else {
//        // If you ask for multiple permissions at once, you
//        // should check if specific permissions missing
//        var allPermsGranted = true
//        
//        //result.grantedPermissions returns an array of _NSCFString pointers
//        let perms = result.grantedPermissions as NSSet
//        
//        let grantedPermissions = perms.allObjects.map( {"\($0)"} )
//        //        let grantedPermissions = result.grantedPermissions.allObjects.map( {"\($0)"} )
//        
//        
//        for permission in self.facebookWritePermissions {
//          if !grantedPermissions.contains(permission) {
//            allPermsGranted = false
//            break
//          }
//        }
//        
//        if allPermsGranted {
//          successBlock()
//        }else {
//          //The user did not grant all permissions requested
//          //Discover which permissions are granted
//          //and if you can live without the declined ones
////          showConnectionError()
//          failureBlock(nil)
//        }
//      }
//    })
//    
//  }
//  
//  //    func getUserPagesData()
//  //    {
//  //      let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//  //
//  //      let graphRequest : FBSDKGraphRequest =
//  //        FBSDKGraphRequest(graphPath: "me/accounts", parameters:nil)
//  //
//  //      graphRequest.startWithCompletionHandler {
//  //        (connection, result, error) -> Void in
//  //        hud.hide(true)
//  //
//  //        if (result != nil)
//  //        {
//  //          //Static.sharedInstance.saveFriendsList(result["data"] as? NSArray)
//  //          //self.loadUsers()
//  //          self.connectWithFacebookNew()
//  //        }
//  //        else
//  //        {
//  //          showConnectionError()
//  //
//  //        }
//  //      }
//  //    }
//  //
//  
//  
//  //    func returnUserData()
//  //    {//,location,address,city,state
//  //      //let graphRequest2 : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,picture,first_name,last_name,birthday,email,location", parameters: nil, tokenString: FBSDKAccessToken.currentAccessToken().tokenString, version: nil, HTTPMethod: nil)
//  //
//  //      let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
//  //
//  //      let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,picture,first_name,last_name,birthday,email,location,friends,likes,about", parameters: nil, tokenString: FBSDKAccessToken.currentAccessToken().tokenString, version: nil, HTTPMethod: nil)
//  //      graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
//  //
//  //        //SAVE DATA
//  //
//  //        if ((error) != nil)
//  //        {
//  //          hud.hide(true)
//  //          showConnectionError()
//  //        }
//  //        else
//  //        {
//  //          Authentication.saveDataFacebook(result)
//  //
//  //          let userDefault = NSUserDefaults.standardUserDefaults()
//  //          userDefault.setBool(false, forKey: "InApp")
//  //          userDefault.setBool(true, forKey: "Facebook")
//  //          userDefault.synchronize()
//  //
//  //          let dicFacebook:NSDictionary = Authentication.getDatafacebook()!
//  //          var params = [String:String]()
//  //
//  //          params["first_name"] = dicFacebook.objectForKey("first_name") as? String
//  //          params["last_name"] = dicFacebook.objectForKey("last_name") as? String
//  //          params["facebook_id"] = dicFacebook.objectForKey("uid") as? String
//  //          params["birthday"] = dicFacebook.objectForKey("birthday") as? String
//  //
//  //          if (dicFacebook.valueForKey("about") != nil)
//  //          {
//  //            params["about"] = dicFacebook.objectForKey("about") as? String
//  //          }
//  //          if (dicFacebook.valueForKey("detail") != nil)
//  //          {
//  //            params["detail"] = dicFacebook.objectForKey("detail") as? String
//  //          }
//  //
//  //          UsersService().create(params, callback: {
//  //            (response:AnyObject?) -> () in
//  //
//  //            if let res:AnyObject = response {
//  //
//  //              let envelope = ResponseSkeleton(res)
//  //
//  //              hud.hide(true)
//  //
//  //              if let _ = envelope.getErrors() as? [String:AnyObject]{
//  //                //TODO error
//  //                showConnectionError()
//  //              }
//  //              else {
//  //
//  //                let user = envelope.getResource() as? NSDictionary
//  //
//  //                if let _ = user?.objectForKey("new_user") as? Bool{
//  //
//  //                  let userIdAux:NSNumber = (user?.objectForKey("id") as? NSNumber)!
//  //                  //let recipientId: NSNumber = dicRecipient.objectForKey("id") as! NSNumber
//  //                  let userId = userIdAux.stringValue
//  //
//  //                  // When users indicate they are Giants fans, we subscribe them to that channel.
//  //                  let currentInstallation = PFInstallation.currentInstallation()
//  //                  currentInstallation.addUniqueObject("vetted_" + userId, forKey: "channels")
//  //                  currentInstallation.saveInBackground()
//  //                  Authentication.saveChannel("vetted_" + userId)
//  //
//  //                  Authentication.save(res)
//  //                  self.getUserHasBusiness()
//  //
//  //                  if (self.delegate().recipient != nil)
//  //                  {
//  //                    self.delegate().requestVetMobile(self.delegate().recipient)
//  //                  }
//  //
//  //                  if (self.delegate().author != nil)
//  //                  {
//  //                    self.delegate().receiveRequest(self.delegate().author)
//  //                  }
//  //
//  //                  if (self.delegate().shareParams != nil)
//  //                  {
//  //                    self.delegate()
//  //                      .showProfile(self.delegate().shareParams)
//  //                  }
//  //                }
//  //              }
//  //            }
//  //            else {
//  //              hud.hide(true)
//  //              showConnectionError()
//  //            }
//  //          })
//  //
//  //        }
//  //      })
//  //    }
//  //
//  //    private func getUserHasBusiness()
//  //    {
//  //      let graphRequest : FBSDKGraphRequest =
//  //        FBSDKGraphRequest(graphPath: "me/accounts", parameters:nil)
//  //      let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
//  //
//  //      graphRequest.startWithCompletionHandler {
//  //        (connection, result, error) -> Void in
//  //
//  //        if (result != nil)
//  //        {
//  //
//  //          let arrBusinessFacebook:NSArray = result["data"] as! NSArray
//  //
//  //          hud.hide(true)
//  //          if(arrBusinessFacebook.count == 0)
//  //          {
//  //            self.userHasBusiness = false
//  //          }
//  //          else
//  //          {
//  //            self.userHasBusiness = true
//  //          }
//  //        }
//  //        else
//  //        {
//  //          hud.hide(true)
//  //          showConnectionError()
//  //
//  //        }
//  //        self.showInviteView()
//  //
//  //      }
//  //    }
//  
//  func delegate() -> AppDelegate {
//    return UIApplication.sharedApplication().delegate as! AppDelegate
//  }
//  
//  //    func showWelcomeView(){
//  //      welcomeView = WelcomeView(frame: self.view.frame)
//  //      welcomeView.buttonSend.addTarget(self,
//  //                                       action: #selector(GKInitialViewController.sendShout),
//  //                                       forControlEvents: UIControlEvents.TouchUpInside)
//  //      welcomeView.buttonRequest.addTarget(self, action: #selector(GKInitialViewController.sendRequest), forControlEvents: UIControlEvents.TouchUpInside)
//  //      welcomeView.buttonContinue.addTarget(self, action: #selector(GKInitialViewController.actionSkip), forControlEvents: UIControlEvents.TouchUpInside)
//  //      self.view.addSubview(welcomeView)
//  //    }
//  //
//  //    func showWelcomeBusinessView(){
//  //      welcomeBusinessView = WelcomeBusinessView(frame: self.view.frame)
//  //      /*welcomeBusinessView.buttonSend.addTarget(self,
//  //       action: "sendShout",
//  //       forControlEvents: UIControlEvents.TouchUpInside)
//  //       welcomeBusinessView.buttonRequest.addTarget(self, action: "sendRequest", forControlEvents: UIControlEvents.TouchUpInside)
//  //       */
//  //      welcomeBusinessView.buttonContinue.addTarget(self, action: #selector(GKInitialViewController.actionDone), forControlEvents: UIControlEvents.TouchUpInside)
//  //      self.view.addSubview(welcomeBusinessView)
//  //    }
//  
//  //    func showInviteView(){
//  //      inviteView = InviteFriendsView(frame: self.view.frame, target: self)
//  //      inviteView.buttonSkip.addTarget(self, action: #selector(GKInitialViewController.showWelcome), forControlEvents: UIControlEvents.TouchUpInside)
//  //      self.view.addSubview(inviteView)
//  //      inviteView.buttonInvite.addTarget(self, action: #selector(GKInitialViewController.showDialog), forControlEvents: UIControlEvents.TouchUpInside)
//  //    }
//  //
//      func showWelcome(){
////        inviteView.removeFromSuperview()
////        showWelcomeView()
//      }
//  
//  //    func showMenu(){
//  //      delegate().selectedTab = "all-vets"
//  //      delegate().container = GKMenuViewcontroller()
//  //      self.presentViewController(self.delegate().container, animated: true, completion: nil)
//  //    }
//  
//  //    func showDialog(){
//  //      let content = FBSDKAppInviteContent()
//  //      content.appLinkURL = NSURL(string: "https://fb.me/853604708051949")
//  //      FBSDKAppInviteDialog.showWithContent(content, delegate: self)
//  //    }
//  //
//  
////  func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!){
////    showWelcome()
////    
////  }
////  
////  func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!){
////    
////    
////    
////  }
//  //    func sendShout(){
//  //      delegate().selectedTab = "get-vetted"
//  //      delegate().container = GKMenuViewcontroller()
//  //      self.presentViewController(self.delegate().container, animated: true, completion: nil)
//  //
//  //    }
//  //
//  //    func sendRequest(){
//  //      delegate().selectedTab = "vet-someone"
//  //      delegate().container = GKMenuViewcontroller()
//  //      self.presentViewController(self.delegate().container, animated: true, completion: nil)
//  //
//  //    }
//  
//  //    func actionSkip(){
//  //      if (self.userHasBusiness)
//  //      {
//  //        welcomeView.removeFromSuperview()
//  //        showWelcomeBusinessView()
//  //      }
//  //      else
//  //      {
//  //        delegate().selectedTab = "all-vets"
//  //        delegate().container = GKMenuViewcontroller()
//  //        self.presentViewController(self.delegate().container, animated: true, completion: nil)
//  //      }
//  //    }
//  //
//  //    func actionDone(){
//  //      delegate().selectedTab = "all-vets"
//  //      delegate().container = GKMenuViewcontroller()
//  //      self.presentViewController(self.delegate().container, animated: true, completion: nil)
//  //    }
//  //
//  
//  // MARK: - Observers
//  
////  func finishGetPhoneNumberObserver()
////  {
////    self.initialHud.hide(true)
////  }
//  
//}
