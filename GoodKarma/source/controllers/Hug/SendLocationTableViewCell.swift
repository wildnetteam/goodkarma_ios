//
//  SendLocationTableViewCell.swift
//  GoodKarma
//
//  Created by Shatakshi on 05/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class SendLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
