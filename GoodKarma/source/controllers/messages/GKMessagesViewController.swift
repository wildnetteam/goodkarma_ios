//
//  GKMessagesViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//
import AVFoundation
import SDWebImage
import UIKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


struct GKMessageAnotherUser {
    var uid: Int!
    var fullName: String!
}

class GKMessagesViewController: UIViewController, UITextFieldDelegate,
    UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, zoomImageDelegate, playAudioDelegate, SendPlayAudioDelegate //, LoadingViewDelegate
{
    
    // MARK: - IBOutlets -
    
//    @IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var contentTextField: UIView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var attachmentButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var msgTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
   // @IBOutlet weak var contentViewText: UIView!
    @IBOutlet weak var typingLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    
    // MARK: - Properties -
    
    fileprivate var indicatorView: UIActivityIndicatorView!
    //    private var loadingView: LoadingView!
    fileprivate var page: Int = 1
    //    var data    : GKDictionary!
    //var messages: NSArray!
    var showKeyboard: Bool = false
    var backToRoot: Bool = true
    //var modal: Bool = false
    var anotherUser: GKMessageAnotherUser!
    
    //************************************
    var showImageScrollView:ShowImageScrollView?
    var imagePicker = UIImagePickerController()
    var selectedImage: UIImage?
    var userImageURL = ""
    var messages:[Message]! = []
    var msgType = ""
    var message = ""
    var lastMsg = ""
    var imageURL = ""
    var senderImg = ""
    var anotherUserImg = ""
    var senderId = ""
    var groupName = ""
    var senderName = ""
    var msgStatus = ""
    var momName = "Mom"
    var forumName = ""
    var groupMembersName = ""
    var isRecording = false
    var timer:Timer = Timer()
    var startTime = TimeInterval()
    var audioDuration = 0
    var documentAudioURL: String!
    var audioURL = ""
    var lastIndex:NSInteger?
    var isTyping = false
    var groupNode: String?
    var groupImage: String?
    var isGroupChat: Bool = false
    //*************************************
    
    static func xibName() -> String
    {
        return "GKChatViewController"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msgTextField.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        
        micButton.isHidden = false
        sendButton.isHidden = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.layer.masksToBounds = true
        userImageView.layer.borderWidth = 0.3
        userImageView.layer.borderColor = UIColor.gray.cgColor
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        
        micButton.addGestureRecognizer(longGesture)
        
        tableView.register(UINib.init(nibName: "SendTextMsgCell", bundle : nil), forCellReuseIdentifier: "senderCell")
        tableView.register(UINib.init(nibName: "RecieveTextMsgCell", bundle : nil), forCellReuseIdentifier: "recieverCell")
        tableView.register(UINib.init(nibName: "SendPhotoMsgCell", bundle : nil), forCellReuseIdentifier: "sendPhotoMsgCell")
        tableView.register(UINib.init(nibName: "RecievePhotoMsgCell", bundle : nil), forCellReuseIdentifier: "recievePhotoMsgCell")
        tableView.register(UINib.init(nibName: "SendAudioTableViewCell", bundle : nil), forCellReuseIdentifier: "sendAudio")
        tableView.register(UINib.init(nibName: "RecieveAudioTableViewCell", bundle : nil), forCellReuseIdentifier: "recieveAudio")
        
        senderId = "\(String(describing: UserDefaults.standard.object(forKey: "id")!))"
        
        let user = Static.sharedInstance.userProfile
        let lastName = user?.object(forKey: "last_name") as! String
        let firstName = user?.object(forKey: "first_name") as! String
        if let senderImage = (user?["photo_url"] as? String){
            senderImg = senderImage
        }
        senderName = "\(firstName)" + " " + "\(lastName)"
        
        FireBaseManager.defaultManager.messageManagerDelegate = (self as FirebaseMessageManagerDelegate)
        FireBaseManager.defaultManager.messages = []
        
        if isGroupChat{
            userNameLabel.text = groupName
            typingLabel.text = groupMembersName
            typingLabel.textColor = UIColor.gray
            userImageView.sd_setImage(with: URL(string: groupImage!))
        }else{
            userNameLabel.text = anotherUser.fullName
            typingLabel.textColor = UIColor.black
            requestData()
            FireBaseManager.defaultManager.setUnreadCountToZero(node: getMsgNode(), id: senderId)
            typingLabel.text = "Offline"
            FireBaseManager.defaultManager.setUnreadCountToZero(node: getMsgNode(), id: senderId)
            FireBaseManager.defaultManager.observeOnlineStatus(id: "\(anotherUser.uid!)") { (status) in
                self.typingLabel.text = status
                print("status---\(status)")
            }
            
        }
        FireBaseManager.defaultManager.fetchMessages(node:getMsgNode(), id: senderId)
        // Do any additional setup after loading the view.
        
        
        print(sendButton)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func userImageButton(){
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func scrollAtBottom() {
        if messages.count > 0 {
            let lastIndex = IndexPath(row: messages.count-1, section: 0)
            print(lastIndex)
            tableView.scrollToRow(at: lastIndex, at: .bottom, animated: false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == msgTextField{
            msgTextField.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ((((string.count) > 0) || (string == ""))){
            sendButton.isHidden = false
            micButton.isHidden = true
            if isTyping == false{
                isTyping = true
                FireBaseManager.defaultManager.setUserOnlineOfflineStatus(id: senderId, status: 2)
            }
            tableView.setNeedsUpdateConstraints()
            tableView.layoutIfNeeded()
        }
        if((((textField.text?.count)! - 1) == 0) && (string == "")){
            sendButton.isHidden = true
            micButton.isHidden = false
            isTyping = false
            FireBaseManager.defaultManager.setUserOnlineOfflineStatus(id: senderId, status: 1)
        }
        return true
    }
    
    func requestData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        if anotherUser.uid != nil
        {
            params["user_id"] = anotherUser.uid! as AnyObject?
        }
        
        //        LoadingView.showLoadingView(
        //            "Loading...",
        //            parentView: self.navigationController?.view, backColor: true
        //        )
        DispatchQueue.main.async {
            GKServiceClient.call(GKRouterClient.profileEdit(), parameters: params, callback: {
                (response: AnyObject?) -> () in
                
                //LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res:AnyObject = response
                {
                    let envelope = ResponseSkeleton(res , string : "profile")
                    
                    if envelope.getSuccess()
                    {
                        if envelope.getResource() is GKDictionary
                        {
                            print(response as! NSDictionary)
                            
                            if let image = ((response as! NSDictionary).object(forKey: "object") as? NSDictionary)?.object(forKey: "photo_url") as? String{
                                self.anotherUserImg = image
                                self.userImageView.sd_setImage(with: URL(string: image))
                            }
                        }
                    }
                }
            })
        }
    }
    
    @IBAction func attachmentButtonClicked(_ sender: Any) {
        print("attachmentButtonTapped")
        
        let actionSheet: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        let cameraButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(cameraButton)
        
        let galleryButton = UIAlertAction(title: "Photo Library", style: .default)
        { _ in
            print("Photo & Video Library")
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(galleryButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion:nil)
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage?
        
        if (selectedImage != nil){
            msgType = "picture"
            lastMsg = "[Image]"
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
            FireBaseManager.defaultManager.uploadImageOnFirebase(contact: getMsgNode(), image: selectedImage!, onCompletion: { (String) in
                self.imageURL = String
                self.setMsgData()
                LoadingView.hideLoadingView(self.navigationController?.view)
            })
        }
    }
    
    @IBAction func micButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Please tap and hold to record audio.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sendMsgButtonTapped(_ sender: UIButton) {
        
        msgType = "text"
        lastMsg = msgTextField.text!
        message = msgTextField.text!
        if message != ""{
            setMsgData()
        }
        msgTextField.text = ""
    }
    
    //MARK:- Set Message Data.
    func setMsgData(){
        
        let createdAt = ServerValue.timestamp()//NSDate().timeIntervalSince1970
        let nodeStr = getMsgNode()
        msgStatus = "sent"
        
        
        let messageItem: NSMutableDictionary = [
            "audio":self.audioURL,
            "audio_duration":self.audioDuration,
            "createdAt":createdAt,
            "picture":self.imageURL,
            "senderId":senderId,
            "senderName":senderName,
            "status":self.msgStatus,
            "text": message,
            "type":self.msgType,
            ]
 
        if isGroupChat{
            let updateDict: NSMutableDictionary = [
                "last_msngr_name":senderName,
                "time":createdAt as ([AnyHashable : Any]),
                "last_msg": lastMsg
            ]
            FireBaseManager.defaultManager.updateGroupMessage(atNode: nodeStr, updateDict: updateDict)
        }else{
            var count = 0
            
            FireBaseManager.defaultManager.incrementNotificationCount(node: nodeStr, id: "\(anotherUser.uid!)") { (cnt) in
                
                count = cnt
            }
            
            let unreadCountDict: NSMutableDictionary = [
                senderId:0,
                "\(anotherUser.uid!)":count
            ]
            
            let messageRecent: NSMutableDictionary = [
                "senderId":senderId,
                "senderImg":senderImg,
                "senderName":senderName,
                "last_msg":lastMsg,
                "msgType":msgType,
                "online":true,
                "time":createdAt,
                "recieverId":"\(anotherUser.uid!)",
                "recieverImg":anotherUserImg,
                "recieverName":anotherUser.fullName,
                "unreadCount":unreadCountDict
            ]
            
            FireBaseManager.defaultManager.setRecentData(atNode: nodeStr, recentDict: messageRecent as NSMutableDictionary)
            
            FireBaseManager.defaultManager.setUserOnlineOfflineStatus(id: senderId, status: 1)
            isTyping = false
        }
        FireBaseManager.defaultManager.sendNewMsg(atNode: nodeStr, detailDict: messageItem as NSMutableDictionary)
        
        audioDuration = 0
        audioURL = ""
        imageURL = ""
        sendButton.isHidden = true
        micButton.isHidden = false
    }
    
    
    @objc func longTap(_ sender: UIGestureRecognizer){
        if sender.state == .ended {
            
            if self.isRecording {
//                LoadingView.showLoadingView(
//                    "Sending...",
//                    parentView: self.navigationController?.view, backColor: true
//                )
                self.isRecording = false
                let _: Bool =  AudioManager.sharedInstance.finishRecording(success: true)
                if self.audioDuration > 0 {
                    self.timer.invalidate()
                    self.msgTextField.text = ""
                    let nodeStr = getMsgNode()
                    
                    if self.documentAudioURL != ""{
                        msgType = "audio"
                        lastMsg = "[Audio]"
                        
                        LoadingView.showLoadingView(
                            "Loading...",
                            parentView: self.navigationController?.view, backColor: true
                        )
                        
                        FireBaseManager.defaultManager.uploadAudioOnFirebase(nodeStr: nodeStr, ducumentDirectoryUrl: self.documentAudioURL, onCompletion: { (String) in
                            self.audioURL = String
                            self.setMsgData()
                            LoadingView.hideLoadingView(self.navigationController?.view)
                        })
                    }
                }else{
                    //LoadingView.hideLoadingView(self.navigationController?.view)
                }
            }
        }
        else if sender.state == .began {
            if checkPermissionStatus() {
                DispatchQueue.main.async {
                    AudioManager.sharedInstance.recordingSetup()
                    self.documentAudioURL = ((AudioManager.sharedInstance.directoryURL())?.absoluteString)!
                    self.isRecording = true
                    if (!self.timer.isValid) {
                        let aSelector : Selector = #selector(self.updateTime)
                        self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
                        self.startTime = Date.timeIntervalSinceReferenceDate
                    }
                }
            } else {
                //self.showAlert(with: "Error", message: "Use of microphone permission is not granted. Please enable the from the settings")
            }
        }
    }
    
    @objc func updateTime() {
        
        //print(isRecording)
        if isRecording == false {
            timer.invalidate()
            msgTextField.text = ""
        }
        
        let currentTime = Date.timeIntervalSinceReferenceDate
        
        //Find the difference between current time and start time.
        var elapsedTime: TimeInterval = currentTime - startTime
        
        //calculate the minutes in elapsed time.
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (TimeInterval(minutes) * 60)
        
        audioDuration = Int(elapsedTime)
        
        //print("audioDuration ---------- \(audioDuration)")
        
        //calculate the seconds in elapsed time.
        let seconds = UInt8(elapsedTime)
        elapsedTime -= TimeInterval(seconds)
        
        //find out the fraction of milliseconds to be displayed.
        //let fraction = UInt8(elapsedTime * 100)
        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
        
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        //let strFraction = String(format: "%02d", fraction)
        
        //concatenate minuets, seconds and milliseconds as assign it to the UILabel
        msgTextField.text = "\(strMinutes):\(strSeconds)"
    }
    
    func checkPermissionStatus() -> Bool {
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            //print("Permission granted")
            return true
        case AVAudioSessionRecordPermission.denied:
            //print("Pemission denied")
            
            let alert = UIAlertController(title: "Error", message: "Use of microphone permission is not granted. Please enable from the settings", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return false
        case AVAudioSessionRecordPermission.undetermined:
            //print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) -> Void in
            })
            return false
        default:
            return false
        }
    }
    
    func playAudio(_ tag: Int) {
        if lastIndex != nil {
            if lastIndex! != NSInteger(tag) {
                DispatchQueue.main.async {
                    let indexPath = IndexPath(row: self.lastIndex!, section: 0)
                    
                    if indexPath == nil {
                        return
                    }
                    
                    if self.tableView.cellForRow(at: indexPath) is RecieveAudioTableViewCell {
                        let recivedAudioCell = self.tableView.cellForRow(at: indexPath) as! RecieveAudioTableViewCell
                        
                        if recivedAudioCell == nil {
                            return
                        }
                        
                        if  (recivedAudioCell.player != nil) && (recivedAudioCell.player?.isPlaying)! {
                            recivedAudioCell.player?.stop()
                            
                            recivedAudioCell.playButton.isSelected = false
                            
                        }
                        
                        if recivedAudioCell.downloadTask != nil {
                            recivedAudioCell.downloadTask?.cancel()
                        }
                        
                        if recivedAudioCell.timer != nil {
                            recivedAudioCell.timer?.invalidate()
                        }
                        
                        recivedAudioCell.progressView.progress = 0
                        self.lastIndex = NSInteger(tag)
                    }else if self.tableView.cellForRow(at: indexPath) is SendAudioTableViewCell {
                        let sendAudioCell = self.tableView.cellForRow(at: indexPath) as! SendAudioTableViewCell
                        
                        if sendAudioCell == nil {
                            return
                        }
                        
                        if  (sendAudioCell.player != nil) && (sendAudioCell.player?.isPlaying)! {
                            sendAudioCell.player?.stop()
                            sendAudioCell.playButton.isSelected = false
                        }
                        
                        if sendAudioCell.downloadTask != nil {
                            sendAudioCell.downloadTask?.cancel()
                        }
                        
                        if sendAudioCell.timer != nil {
                            sendAudioCell.timer?.invalidate()
                        }
                        
                        sendAudioCell.progressView.progress = 0
                        
                        self.lastIndex = NSInteger(tag)
                    }
                }
            }
        }else{
            lastIndex = NSInteger(tag)
        }
    }
    
    func playAudioSend(_ tag: Int) {
        if lastIndex != nil {
            if lastIndex! != NSInteger(tag) {
                DispatchQueue.main.async {
                    let indexPath = IndexPath(row: self.lastIndex!, section: 0)
                    
                    if indexPath == nil {
                        return
                    }
                    
                    if self.tableView.cellForRow(at: indexPath) is RecieveAudioTableViewCell {
                        let recivedAudioCell = self.tableView.cellForRow(at: indexPath) as! RecieveAudioTableViewCell
                        
                        if recivedAudioCell == nil {
                            return
                        }
                        
                        if  (recivedAudioCell.player != nil) && (recivedAudioCell.player?.isPlaying)! {
                            recivedAudioCell.player?.stop()
                            
                            recivedAudioCell.playButton.isSelected = false
                            
                        }
                        
                        if recivedAudioCell.downloadTask != nil {
                            recivedAudioCell.downloadTask?.cancel()
                        }
                        
                        if recivedAudioCell.timer != nil {
                            recivedAudioCell.timer?.invalidate()
                        }
                        
                        recivedAudioCell.progressView.progress = 0
                        self.lastIndex = NSInteger(tag)
                    }else if self.tableView.cellForRow(at: indexPath) is SendAudioTableViewCell {
                        let sendAudioCell = self.tableView.cellForRow(at: indexPath) as! SendAudioTableViewCell
                        
                        if sendAudioCell == nil {
                            return
                        }
                        
                        if  (sendAudioCell.player != nil) && (sendAudioCell.player?.isPlaying)! {
                            sendAudioCell.player?.stop()
                            sendAudioCell.playButton.isSelected = false
                        }
                        
                        if sendAudioCell.downloadTask != nil {
                            sendAudioCell.downloadTask?.cancel()
                        }
                        
                        if sendAudioCell.timer != nil {
                            sendAudioCell.timer?.invalidate()
                        }
                        
                        sendAudioCell.progressView.progress = 0
                        
                        self.lastIndex = NSInteger(tag)
                    }
                }
            }
        }else{
            lastIndex = NSInteger(tag)
        }
    }
    
    //MARK:- Create Node.
    func getMsgNode() -> String{
        if isGroupChat{
            return self.groupNode!
        }else{
            var nodeStr = ""
            let num1 = Int(senderId)
            let num2 = Int(anotherUser.uid)
            
            if(num1<num2){
                nodeStr = "\(String(describing: num1!))-\(num2)"
            }else{
                nodeStr = "\(num2)-\(String(describing: num1!))"
            }
            return nodeStr
        }
        
    }
    
    //MARK:- Delegate to zoom an image in the chat.
    func zoomImage(image: UIImage!) {
        if (image != nil){
            if msgTextField.isFirstResponder {
                msgTextField.resignFirstResponder()
            }
            addImageScrollView(image: image)
        }
        
    }
    
    //MARK: TABLEVIEW DELEGATES AND DATASOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        
        if messages[indexPath.row].senderId! == senderId{
            if messages[indexPath.row].type! == "text"{
                let senderCell = tableView.dequeueReusableCell(withIdentifier: "senderCell") as? SendTextMsgCell
                
                if isGroupChat{
                    senderCell?.tickMsgImageView.isHidden = true
                    senderCell?.tickImageVIewWidthConstraint.constant = 0
                }else{
                    senderCell?.tickMsgImageView.isHidden = false
                    senderCell?.tickImageVIewWidthConstraint.constant = 18
                }
                
                //senderCell?.delegate = self
                senderCell?.senderMessageLabel.text = messages[indexPath.row].text
                senderCell?.timeLabel.text = messages[indexPath.row].createdAt
                if messages[indexPath.row].status == "delivered"{
                    senderCell?.tickMsgImageView.image = UIImage(named: "doubleTickGrey")
                }else if messages[indexPath.row].status == "read"{
                    senderCell?.tickMsgImageView.image = UIImage(named: "doubleTickIcon")
                }
                cell = senderCell!
            }else if messages[indexPath.row].type! == "picture"{
                let sendPhotoCell = tableView.dequeueReusableCell(withIdentifier: "sendPhotoMsgCell") as? SendPhotoMsgCell
                
                if isGroupChat{
                    sendPhotoCell?.tickMsgImageView.isHidden = true
                    sendPhotoCell?.tickImageVIewWidthConstraint.constant = 0
                }else{
                    sendPhotoCell?.tickMsgImageView.isHidden = false
                    sendPhotoCell?.tickImageVIewWidthConstraint.constant = 18
                }
                
                sendPhotoCell?.delegate = self
                if let url = URL(string:(messages[indexPath.row].picture!)){
                    sendPhotoCell?.sendPhotoMsgImageView.sd_setImage(with: url)
                    sendPhotoCell?.timeLabel.text = messages[indexPath.row].createdAt
                    if messages[indexPath.row].status == "delivered"{
                        sendPhotoCell?.tickMsgImageView.image = UIImage(named: "doubleTickGrey")
                    }else if messages[indexPath.row].status == "read"{
                        sendPhotoCell?.tickMsgImageView.image = UIImage(named: "doubleTickIcon")
                    }
                    cell = sendPhotoCell!
                }
            }else{
                let audioSenderCell = tableView.dequeueReusableCell(withIdentifier: "sendAudio") as? SendAudioTableViewCell
                
                if isGroupChat{
                    audioSenderCell?.tickMsgImageView.isHidden = true
                    audioSenderCell?.tickImageVIewWidthConstraint.constant = 0
                }else{
                    audioSenderCell?.tickMsgImageView.isHidden = false
                    audioSenderCell?.tickImageVIewWidthConstraint.constant = 18
                }
                
                audioSenderCell?.delegate = self as SendPlayAudioDelegate
                
                audioSenderCell?.timeLabel.text = messages[indexPath.row].createdAt
                audioSenderCell?.infoDict =  messages[indexPath.row]
                if messages[indexPath.row].status == "delivered"{
                    audioSenderCell?.tickMsgImageView.image = UIImage(named: "doubleTickGrey")
                }else if messages[indexPath.row].status == "read"{
                    audioSenderCell?.tickMsgImageView.image = UIImage(named: "doubleTickIcon")
                }
                let totalSeconds = messages[indexPath.row].audio_Duration
                audioSenderCell?.seconds = totalSeconds!
                let minutes = totalSeconds! / 60
                var minutesDuration = ""
                if minutes < 10{
                    minutesDuration = "0\(minutes)"
                }else{
                    minutesDuration = "\(minutes)"
                }
                
                let seconds = totalSeconds! % 60
                var secondsDuration = "\(seconds)"
                
                if seconds < 10{
                    secondsDuration = "0\(seconds)"
                }
                let duration = "\(minutesDuration):\(secondsDuration)"
                
                audioSenderCell?.audioDurationLabel.text = duration
                audioSenderCell?.playButton.tag = indexPath.row
                
//                let url = ""
//                
//                if url != ""{
//                    //let data = NSData(contentsOf: URL(string:url)!)
//                    //let image:UIImage! = UIImage(data: data as! Data)
//                    //if image != nil {
//                    
//                    //audioSenderCell?.userImage.setImageWith(URL(string:url)!, placeholderImage: nil)
//                    
//                    audioSenderCell?.userImage.sd_setImage(with: URL(string:url)!)
//                }
                cell = audioSenderCell!
            }
        }else{
            if messages[indexPath.row].type! == "text"{
                
                let recieverCell = tableView.dequeueReusableCell(withIdentifier: "recieverCell") as? RecieveTextMsgCell
                //recieverCell?.delegate = self
                //recieverCell?.spamButton.tag = indexPath.row
                
                if isGroupChat{
                    recieverCell?.userNameLabel.isHidden = false
                    recieverCell?.userNameLabelHeightConstraint.constant = 21
                    recieverCell?.userNameLabel.text = messages[indexPath.row].senderName
                }else{
                    recieverCell?.userNameLabel.isHidden = true
                    recieverCell?.userNameLabelHeightConstraint.constant = 0
                }
                
                recieverCell?.recieverMessageLabel.text = messages[indexPath.row].text
                //recieverCell?.userNameLabel.text = messages[indexPath.row].senderName
//                if let url = URL(string:(messages[indexPath.row].senderImg!)){
//                    recieverCell?.userImage.sd_setImage(with: url)
//                }
                recieverCell?.timeLabel.text = messages[indexPath.row].createdAt
                cell = recieverCell!
            }else if messages[indexPath.row].type! == "picture"{
                let recievePhotoCell = tableView.dequeueReusableCell(withIdentifier: "recievePhotoMsgCell") as? RecievePhotoMsgCell
                
                if isGroupChat{
                    recievePhotoCell?.userNameLabel.isHidden = false
                    recievePhotoCell?.userNameLabelHeightConstraint.constant = 21
                    recievePhotoCell?.userNameLabel.text = messages[indexPath.row].senderName
                }else{
                    recievePhotoCell?.userNameLabel.isHidden = true
                    recievePhotoCell?.userNameLabelHeightConstraint.constant = 0
                }
                
                recievePhotoCell?.delegate = self
                if let url = URL(string:(messages[indexPath.row].picture!)){
                    recievePhotoCell?.recievePhotoMsgImageView.sd_setImage(with: url)
                }
                //recievePhotoCell?.userNameLabel.text = messages[indexPath.row].senderName
//                if let url = URL(string:(messages[indexPath.row].senderImg!)){
//                    recievePhotoCell?.userImageView.sd_setImage(with: url)
//                }
                recievePhotoCell?.timeLabel.text = messages[indexPath.row].createdAt
                cell = recievePhotoCell!
                
            }else{
                let audioRecieverCell = tableView.dequeueReusableCell(withIdentifier: "recieveAudio") as? RecieveAudioTableViewCell
                
                if isGroupChat{
                    audioRecieverCell?.userNameLabel.isHidden = false
                    audioRecieverCell?.bgViewHeightConstraint.constant = 108
                    audioRecieverCell?.userNameLabelHeightConstraint.constant = 21
                    audioRecieverCell?.userNameLabel.text = messages[indexPath.row].senderName
                }else{
                    audioRecieverCell?.userNameLabel.isHidden = true
                    audioRecieverCell?.bgViewHeightConstraint.constant = 88
                    audioRecieverCell?.userNameLabelHeightConstraint.constant = 0
                }
                
                audioRecieverCell?.delegate = self as playAudioDelegate
                audioRecieverCell?.timeLabel.text = messages[indexPath.row].createdAt
                
                let totalSeconds = messages[indexPath.row].audio_Duration
                audioRecieverCell?.infoDict =  messages[indexPath.row]
                audioRecieverCell?.seconds = totalSeconds!
                let minutes = totalSeconds! / 60
                var minutesDuration = ""
                if minutes < 10{
                    minutesDuration = "0\(minutes)"
                }else{
                    minutesDuration = "\(minutes)"
                }
                
                let seconds = totalSeconds! % 60
                var secondsDuration = "\(seconds)"
                
                if seconds < 10{
                    secondsDuration = "0\(seconds)"
                }
                let duration = "\(minutesDuration):\(secondsDuration)"
                
                audioRecieverCell?.audioDurationLabel.text = duration
                audioRecieverCell?.playButton.tag = indexPath.row
                //audioRecieverCell?.userImageView.image = profileImage.image
                //audioRecieverCell?.selectionStyle = UITableViewCellSelectionStyle.none
                cell = audioRecieverCell!
            }
        }
        cell.selectionStyle = .none
        return cell
    }
}

extension GKMessagesViewController : FirebaseMessageManagerDelegate
{
    
    func fireBaseManagerDidCompleteFetchingMessages() {
        
        messages = FireBaseManager.defaultManager.messages
        tableView.reloadData()
        scrollAtBottom()
    }
}

extension GKMessagesViewController : ShowImageScrollViewDelegate{
    func addImageScrollView(image: UIImage!){
        showImageScrollView = (Bundle.main.loadNibNamed("ShowImageScrollView", owner: self, options: nil)?[0] as? ShowImageScrollView)!
        
        showImageScrollView?.delegate = self
        showImageScrollView?.setImage(image: image)
        showImageScrollView?.alpha = 0
        showImageScrollView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        (UIApplication.shared.delegate as! AppDelegate).window!.addSubview(showImageScrollView!)
        UIView.animate(withDuration: 0.7, animations: {
            self.showImageScrollView?.alpha = 1
        } ,completion: { (done) in
            
        } )
    }
    
    // MARK: - ShowImageScrollViewDelegate
    func removeShowImageScrollView()  {
        self.showImageScrollView?.alpha = 1
        UIView.animate(withDuration: 0.7, animations: {
            self.showImageScrollView?.alpha = 0
        } ,completion: { (done) in
            self.showImageScrollView?.removeFromSuperview()
            self.showImageScrollView = nil
        } )
    }
}

    // MARK: - Override UIViewController -
    
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//
//        self.title = anotherUser.fullName
//
//        // Content View Text
//        let gesture = UIPanGestureRecognizer(target:self, action:#selector(GKMessagesViewController.panGestureAction))
//        self.contentViewText.addGestureRecognizer(gesture)
//
//        // Content TextField
//        self.contentTextField.layer.cornerRadius = 3
//
//        // Send Btn
//        self.sendBtn.layer.cornerRadius = 3
//
//        // TableView
//        let header = UIView(frame:CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 30))
//        header.backgroundColor = UIColor.clear
//        self.tableView.tableHeaderView = header
//
//        // Empty Label
//        self.emptyLabel.text = NSString(format: "You have not yet written %@. Say \"Hello\" and get the conversation started!", anotherUser.fullName) as String
//        self.emptyLabel.isHidden = true
//
//        // Activity Indicator
//        self.indicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
//        self.indicatorView.center = header.center
//        self.indicatorView.isHidden = true
//        header.addSubview(self.indicatorView)
//
//        let footer = UIView(frame:CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 20))
//        footer.backgroundColor = UIColor.clear
//        self.tableView.tableFooterView = footer
//
//        // Loading View
//        //      let frameLoadingView = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 130)
//        //      self.loadingView = LoadingView(frame:frameLoadingView)
//        //      self.loadingView.backgroundColor = UIColor.clearColor()
//        //      self.loadingView.view.backgroundColor = UIColor.clearColor()
//        //      self.loadingView.noDataLabel.text = "You don't have messages"
//        //      self.loadingView.delegate = self
//        //      self.tableView.addSubview(self.loadingView)
//
//        // Nib
//        var classBundle = Bundle(for:MessagesCell.self)
//        var nib = UINib(nibName:"MessagesCell", bundle:classBundle)
//        self.tableView.register(nib, forCellReuseIdentifier:"MessagesCell")
//
//        classBundle = Bundle(for:MessagesRightCell.self)
//        nib = UINib(nibName:"MessagesRightCell", bundle:classBundle)
//        self.tableView.register(nib, forCellReuseIdentifier:"MessagesRightCell")
//
//        // Load Data
//        loadData([])
//
//        // Notification change
//        let defaultCenter = NotificationCenter.default
//        defaultCenter.addObserver(self, selector:#selector(GKMessagesViewController.handleTextFieldChange),
//                                  name:NSNotification.Name.UITextFieldTextDidChange, object:self.textField)
//
//        NotificationCenter.default.addObserver(self,
//                                                         selector:#selector(GKMessagesViewController.keyboardWillShow(_:)),
//                                                         name:NSNotification.Name.UIKeyboardWillShow,
//                                                         object:nil)
//
//        NotificationCenter.default.addObserver(self,
//                                                         selector:#selector(GKMessagesViewController.keyboardWillHide(_:)),
//                                                         name:NSNotification.Name.UIKeyboardWillHide,
//                                                         object:nil)
//        cleanBackButton()
//
//        listenToNotifications(GKNotificationStore.notification_kind_message)
//    }
//
//    deinit
//    {
//        NotificationCenter.default.removeObserver(self)
//    }
    
//    override func viewDidAppear(_ animated: Bool)
//    {
//        super.viewDidAppear(animated)
//
//        // Request Data
//        requestMessages()
//
//        // Show Keyboard
//        if (self.showKeyboard)
//        {
//            self.textField.becomeFirstResponder()
//            self.showKeyboard = false
//        }
//
//        // Menu Counts
//        //      Static.sharedInstance.menuCounterRemove("messages")
//        //      removeBadge()
//    }
    
    
    // MARK: - Notifications -
    
    
//    override func receiveFromNotificationStore(_ notification: Notification)
//    {
//        if anotherUser.uid == GKNotificationStore.new_message_user_id {
//
//            self.page = 1
//            self.requestMessages()
//        }
//    }
    
    // MARK: - UITableViewDelegate & UITableViewDataSource
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
//    {
//        return false
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return self.messages?.count ?? 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt
//        indexPath: IndexPath) -> UITableViewCell
//    {
//        var cell : UITableViewCell!
//        let messageData = self.messages[indexPath.row] as! NSDictionary
//
////        if (isOther(messageData["author"] as! Int))
////        {
//            let customCell = tableView.dequeueReusableCell(withIdentifier: "MessagesCell",
//                                                                         for: indexPath) as! MessagesCell
//            customCell.loadData(messageData)
//            cell = customCell
//        //}
////        else
////        {
////            let customCell = tableView.dequeueReusableCell(withIdentifier: "MessagesRightCell",
////                                                                         for: indexPath) as! MessagesRightCell
////            customCell.loadData(messageData)
////            cell = customCell
//       // }
//
//        if (self.page > 1 && indexPath.row == 1 && self.indicatorView.isHidden)
//        {
//            self.indicatorView.startAnimating()
//            self.indicatorView.isHidden = false
//            requestMessages()
//        }
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt
//        indexPath: IndexPath) -> CGFloat
//    {
//        return getHeightForText((self.messages[indexPath.row] as! NSDictionary)["detail"] as! NSString)
//
//    }
//
//    // MARK: - IBActions -
//
//    @IBAction func sendAction(_ sender: AnyObject)
//    {
//        let messageData = NSMutableDictionary()
//       // messageData["author"] = Static.sharedInstance.userProfile["id"] as! Int
//        messageData["detail"] = self.textField.text
//
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        messageData["date_created"] = dateFormatter.string(from: Date())
//
//        if self.messages == nil
//        {
//            self.messages = NSMutableArray()
//        }
//
//        let mutable: NSMutableArray = self.messages.mutableCopy() as! NSMutableArray
//        mutable.add(messageData)
//        self.messages = mutable
//
//        let sortDescriptor = NSSortDescriptor(key:"date_created", ascending:true)
//        self.messages = self.messages.sortedArray(using: [sortDescriptor]) as NSArray
//
//        reloadAndScroll()
//        //      self.loadingView.setState(.LoadingViewStateHide)
//
//        sendMessage(self.textField.text!)
//        self.textField.endEditing(true)
//    }
//
//    // MARK: - Notifications
//
//    @objc func handleTextFieldChange()
//    {
//        if (self.textField.text?.countPlainCharacters() > 0)
//        {
//            self.sendBtn.alpha = 1
//            self.sendBtn.isEnabled = true
//        }
//        else
//        {
//            self.sendBtn.alpha = 0.5
//            self.sendBtn.isEnabled = false
//        }
//    }
//
//    // MARK: - UITextFieldDelegate
//
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
   //     sendAction(0 as AnyObject)

        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
       // reloadAndScroll()
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        self.textField.text = ""
//        self.sendBtn.alpha = 0.5
//        self.sendBtn.isEnabled = false
//
//        reloadAndScroll()
    }
//
//    // MARK: - Gesture
//
//    @objc func panGestureAction()
//    {
//        self.textField.endEditing(true)
//    }
//
//    // MARK: - Observer
//
//    @objc func keyboardWillShow(_ notification: Notification)
//    {
//        if let userInfo = notification.userInfo
//        {
//            if let keyboardSize =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
//            {
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.scrollView.frame = CGRectSetHeight(
//                        self.scrollView.frame,
//                        h: self.view.frame.height - keyboardSize.height)  //- 60)
//                    }, completion: { _ in
//
//                        self.reloadAndScroll()
//                })
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(_ notification: Notification)
//    {
//        self.reloadAndScroll()
//
//        if let userInfo = notification.userInfo
//        {
//            if let _ =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
//            {
//                UIView.animate(withDuration: 0.3, animations: {
//
//                    self.scrollView.frame = CGRectSetHeight(
//                        self.scrollView.frame,
//                        h: self.view.frame.height)  // - 60
//                    }, completion: { _ in
//
//                })
//            }
//        }
//    }
    
    // MARK: - LoadingViewDelegate
    
    //    func loadingViewReload()
    //    {
    //      self.page = 1
    //      requestMessages()
    //    }
    
    // MARK: - Methods -
    
//    fileprivate func loadData(_ messages: NSArray)
//    {
//        //      self.messages = Static.sharedInstance
//        //        .getMessages(extractConversationId())
//        self.messages = messages
//
//        if (self.messages != nil)
//        {
//            let sortDescriptor = NSSortDescriptor(key:"date_created", ascending:true)
//            self.messages = self.messages.sortedArray(using: [sortDescriptor]) as NSArray
//
//            reloadAndScroll()
//            //        self.loadingView.setState(.LoadingViewStateHide)
//        }
//    }
    
//    fileprivate func loadMore(_ messages: NSArray)
//    {
//        let mutableArray: NSMutableArray = self.messages.mutableCopy() as! NSMutableArray
//        mutableArray.addObjects(from: messages as [AnyObject])
//        self.messages = mutableArray
//
//        if (self.messages != nil)
//        {
//            let sortDescriptor = NSSortDescriptor(key:"date_created", ascending:true)
//            self.messages = self.messages.sortedArray(using: [sortDescriptor])  as NSArray
//
//            let y = self.tableView.contentOffset.y + calculateLoadMoreHeigh(messages)
//            self.tableView.reloadData()
//            self.tableView.setContentOffset(CGPoint(x: 0, y: y), animated: false)
//        }
//    }
    
//    fileprivate func requestMessages()
//    {
//        self.emptyLabel.isHidden = true
//
//        if (self.messages == nil || self.messages.count == 0)
//        {
//            //        self.loadingView.setState(.LoadingViewStateLoading)
//        }
//
//        if (self.page == -1)
//        {
//            self.page = 1
//        }
//
//        //      let dicUser: NSDictionary = Authentication.getUserDictionary()!
//
//        var params = [String:AnyObject]()
//        params["token"] = Static.sharedInstance.token() as AnyObject?
//        params["recipient"] = anotherUser.uid as AnyObject?
//        params["page"]      = self.page as AnyObject?
//
//        GKServiceClient.call(GKRouterChat.detailMessage(), parameters:params, callback: {
//            (response: AnyObject?) -> () in
//
//            self.indicatorView.isHidden = true
//
//            if let res:AnyObject = response {
//
//                let envelope = ResponseSkeleton(res)
//
//                if envelope.getSuccess() {
//
//                    let data = envelope.getResource() as! GKDictionary
//
//                    let current = data["current"] as! Int
//
//                    if (current == 1)
//                    {
//                        //                Static.sharedInstance.addMessages(jsonDic["object"]! as? NSArray, conversationId: self.extractConversationId())
//
//                        self.loadData(data["data"] as! NSArray)
//                    }
//                    else
//                    {
//                        self.loadMore(data["data"] as! NSArray)
//                    }
//
//                    if (data["total_pages"] as! Int > self.page)
//                    {
//                        self.page += 1
//                    }
//                    else
//                    {
//                        self.page = -1
//                    }
//
//                    self.emptyLabel.isHidden = self.messages != nil && self.messages.count > 0
//                }
//            }
//        })
//
//        //          if (response != nil)
//        //          {
//        //            let jsonDic : NSDictionary = response as! NSDictionary
//        //
//        //            if (jsonDic["success"]?.boolValue == true)
//        //            {
//        //              print(jsonDic)
//        //
//        //              let pagination = jsonDic["pagination"] as! NSDictionary
//        //
//        //              if (pagination["current"]?.intValue == 1)
//        //              {
//        ////                Static.sharedInstance.addMessages(jsonDic["object"]! as? NSArray, conversationId: self.extractConversationId())
//        //
//        //                self.loadData()
//        //              }
//        //              else
//        //              {
//        //               self.loadMore(jsonDic["object"]! as! NSArray)
//        //              }
//        //
//        //              if (pagination["pages"]?.integerValue > self.page)
//        //              {
//        //                self.page += 1
//        //              }
//        //              else
//        //              {
//        //                self.page = -1
//        //              }
//        //            }
//        //            else
//        //            {
//        //              if (self.messages == nil)
//        //              {
//        ////                self.loadingView.setState(.LoadingViewStateError)
//        //              }
//        //            }
//        //          }
//        //          else
//        //          {
//        //            if (self.messages == nil)
//        //            {
//        ////              self.loadingView.setState(.LoadingViewStateError)
//        //            }
//        //          }
//        //      })
//    }
    
    
    
    //    private func removeBadge()
    //    {
    //      if (Authentication.isUserLoggedIn())
    //      {
    //        let dicUser: NSDictionary = Authentication.getUserDictionary()!
    //        let token = dicUser.objectForKey("token") as? String
    //
    //        var params = [String:AnyObject]()
    //        params["token"] = token
    //        params["message_badge"] = true
    //
    //        if (Static.sharedInstance.isBusiness())
    //        {
    //          params["business_id"] = Static.sharedInstance.businessId()
    //        }
    //
    //        UsersService().removeBadge(params, callback: {
    //          (response:AnyObject?) -> () in
    //
    //          Static.sharedInstance.requestMenuCount()
    //            {
    //          }
    //        })
    //      }
    //    }
    
//    fileprivate func sendMessage(_ text: String)
//    {
//        var params = [String: AnyObject]()
//        params["token"] = Static.sharedInstance.token() as AnyObject?
//        params["recipient"] = anotherUser.uid as AnyObject?
//        params["detail"] = text.encodeEmoji() as AnyObject?
//
//        GKServiceClient.call(GKRouterChat.createConversation(), parameters:params, callback: {
//            (response: AnyObject?) -> () in
//
//            if let res:AnyObject = response {
//
//                let envelope = ResponseSkeleton(res)
//
//                if envelope.getSuccess() {
//
//                    self.page = 1
//                    self.requestMessages()
//                }
//            }
//        })
//    }
//
//    fileprivate func reloadAndScroll()
//    {
//
//        self.tableView.reloadData()
//
//        if (self.messages != nil && self.messages.count > 0)
//        {
//            let indexPath = IndexPath(row:self.messages.count - 1, section:0)
//            self.tableView.scrollToRow(at: indexPath, at:.top,
//                                                  animated:true)
//        }
//    }
//
//    fileprivate func getHeightForText(_ postTitle: NSString) -> CGFloat
//    {
//        let constraintSize = CGSize(width: 200, height: CGFloat.greatestFiniteMagnitude)
//        let attributes = [NSAttributedStringKey.font: UIFont.fontRoboto(.Regular, size: 14)]
//        let labelSize = postTitle.boundingRect(with: constraintSize,
//                                                       options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                                       attributes: attributes,
//                                                       context: nil)
//
//        return max(labelSize.height + 59, 80)
//    }
//
//    fileprivate func calculateLoadMoreHeigh(_ array: NSArray) -> CGFloat
//    {
//        var height: CGFloat = 0.0
//
//        for d in array
//        {
//            height += getHeightForText((d as! NSDictionary)["detail"] as! NSString)
//        }
//
//        return height
//    }
//
//    fileprivate func isOther(_ authorId: Int) -> Bool
//    {
//
//        let userId = Static.sharedInstance.userProfile["id"] as! Int
//
//        if (userId == authorId)
//        {
//            return false
//        }
//
//        return true
//    }

//}
