//
//  CreateGroupViewController.swift
//  GoodKarma
//
//  Created by Anuj Jha on 01/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class CreateGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var participantsCountLabel: UILabel!
    
    var selectedMembersIdArray: NSMutableArray? = []
    var selectedMembersDetailedArray: NSMutableArray? = []
    var participantsListArray: NSArray? = []
    var filteredArray: NSArray? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewHeightConstraint.constant = 0
        requestFollowings()
        // Do any additional setup after loading the view.
        tableView.register(UINib.init(nibName: "AddParticipantsTableViewCell", bundle: nil), forCellReuseIdentifier: "ParticipantsListCell")
        collectionView.register(UINib.init(nibName: "SelectedUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "selectedUserCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        selectedMembersDetailedArray = []
        selectedMembersIdArray = []
        tableView.reloadData()
        collectionView.reloadData()
        collectionViewHeightConstraint.constant = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func requestFollowings()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.following(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                // let envelope = ResponseSkeleton(res)
                let envelope = ResponseSkeleton(res , string : "following")
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource() as? [GKDictionary]
                    {
                        print("data--\(data)")
                        self.participantsListArray = (response as! NSDictionary).object(forKey: "object") as? NSArray
                        self.filteredArray = self.participantsListArray
                        self.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text != ""{
            let filter1  = NSPredicate(format: "first_name contains[c] %@", searchText)
            let filter2  = NSPredicate(format: "last_name contains[c] %@", searchText)
            let finalFilter = NSCompoundPredicate(orPredicateWithSubpredicates: [filter1,filter2])
            let tempArray = filteredArray?.filtered(using: finalFilter) as NSArray?
            
            if (tempArray != nil) && (tempArray != nil){
                participantsListArray = tempArray
            }
        }else{
            participantsListArray = filteredArray
        }
        
        tableView.reloadData()
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        let createGroup = FillDetailsAndCreateGroupViewController(nibName: "FillDetailsAndCreateGroupViewController", bundle: nil)
        createGroup.selectedMembersDetailedArray = selectedMembersDetailedArray
        createGroup.selectedMembersIdArray = selectedMembersIdArray
        self.navigationController?.pushViewController(createGroup, animated: true)
    }
    
    @objc func crossButtonTapped(sender: UIButton){
        print("sender.tag---\(sender.tag)")
        
        for index in 0..<participantsListArray!.count{
            if (String((selectedMembersDetailedArray![sender.tag] as! NSDictionary).object(forKey: "id") as! Int)) == String((participantsListArray![index] as! NSDictionary).object(forKey: "id") as! Int){
             
                let indexPath = IndexPath(row: index, section: 0)
                let cell = self.tableView.cellForRow(at: indexPath) as! AddParticipantsTableViewCell
                var imageView : UIImageView
                imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
                imageView.image = UIImage(named:"circle-hollow")
                cell.accessoryView = imageView as UIView
                selectedMembersDetailedArray?.removeObject(at: sender.tag)
                selectedMembersIdArray?.remove(String((participantsListArray![sender.tag] as! NSDictionary).object(forKey: "id") as! Int))
                if selectedMembersDetailedArray?.count != 0{
                    collectionViewHeightConstraint.constant = 80
                }else{
                    collectionViewHeightConstraint.constant = 0
                }
                collectionView.reloadData()
                break
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (participantsListArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        let id = ((participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "id") as? Int)
        if (UserDefaults.standard.object(forKey: "id")! as! Int) != id{
            let listCell = tableView.dequeueReusableCell(
                withIdentifier: "ParticipantsListCell", for: indexPath) as! AddParticipantsTableViewCell
            
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = UIImage(named:"circle-hollow")
            listCell.accessoryView = imageView as UIView
            
            var fullName = ""
            
            if let firstName = (participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "first_name") as? String{
                fullName = firstName
            }
            if let lastName = (participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "last_name") as? String{
                fullName = "\(fullName) \(lastName)"
            }
            
            listCell.nameLabel.text = fullName
            listCell.userImageView.sd_setImage(with: URL(string: (participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "photo_url") as! String))
            cell = listCell
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let listCell = tableView.cellForRow(at: indexPath) as? AddParticipantsTableViewCell{
        var imageView : UIImageView
        imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
        
        if (selectedMembersIdArray?.contains(String((participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "id") as! Int)))!{
            
            selectedMembersIdArray?.remove(String((participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "id") as! Int))
            selectedMembersDetailedArray?.remove(participantsListArray![indexPath.row] as! NSDictionary)
            print("selectedMembersIdArray---\(String(describing: selectedMembersIdArray!))")
            imageView.image = UIImage(named:"circle-hollow")
            
        }else{
            selectedMembersIdArray?.add(String((participantsListArray![indexPath.row] as! NSDictionary).object(forKey: "id") as! Int))
            selectedMembersDetailedArray?.add(participantsListArray![indexPath.row] as! NSDictionary)
            print("selectedMembersIdArray---\(String(describing: selectedMembersIdArray!))")
            imageView.image = UIImage(named:"check-circle")
        }
        
        if selectedMembersDetailedArray?.count != 0{
            collectionViewHeightConstraint.constant = 80
        }else{
            collectionViewHeightConstraint.constant = 0
        }
        collectionView.reloadData()
        participantsCountLabel.text = "\(String(describing: selectedMembersIdArray!.count))/100"
        listCell.accessoryView = imageView as UIView
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (selectedMembersDetailedArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell = UICollectionViewCell()
        
        let selectedUserCell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectedUserCell", for: indexPath) as! SelectedUserCollectionViewCell
        selectedUserCell.crossButton.tag = indexPath.item
        selectedUserCell.crossButton.addTarget(self, action: #selector(crossButtonTapped), for: .touchUpInside)
        selectedUserCell.userImageView.sd_setImage(with: URL(string: (selectedMembersDetailedArray![indexPath.row] as! NSDictionary).object(forKey: "photo_url") as! String))
        selectedUserCell.userNameLabel.text = (selectedMembersDetailedArray![indexPath.row] as! NSDictionary).object(forKey: "first_name") as? String
        cell = selectedUserCell
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        return CGSize(width: 75.0 , height: collectionView.frame.size.height - 10)
//    }
    
func createGroup()
{
    //group_name
    //group_user
    //group_image
    
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    LoadingView.showLoadingView(
        "Loading...",
        parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.createGroup(), parameters: params, callback: {
        (response: AnyObject?) -> () in
        
        LoadingView.hideLoadingView(self.navigationController?.view)
        
        if let res:AnyObject = response
        {
            // let envelope = ResponseSkeleton(res)
            let envelope = ResponseSkeleton(res , string : "following")
            if envelope.getSuccess()
            {
                if let data = envelope.getResource() as? [GKDictionary]
                {
                    print("data--\(data)")
                    self.participantsListArray = (response as! NSDictionary).object(forKey: "object") as? NSArray
                    self.tableView.reloadData()
                }
            }
        }
    })
}
}
