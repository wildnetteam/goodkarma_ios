//
//  GKCreateServeViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import VimeoUpload
import Alamofire
import MobileCoreServices
import Photos
import VimeoNetworking
import FBSDKShareKit

class GKCreateServeViewController: GKViewController, UITextFieldDelegate, GKManageSkillsDelegate, GKPostFacebookDelegate, FBSDKSharingDelegate,UITextViewDelegate, UICollectionViewDelegate , UICollectionViewDataSource, UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    /*!
     @abstract Sent to the delegate when the sharer encounters an error.
     @param sharer The FBSDKSharing that completed.
     @param error The error.
     */
    public func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print(error!)
    }

    // MARK: - IBOutlets -
    
    @IBOutlet weak var serveScrollView: UIScrollView!
    
    @IBOutlet weak var serveFormView: UIView!
    @IBOutlet weak var serveDistanceView: UIView!
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var bodyContentField: UITextView!
    @IBOutlet weak var bodyPlaceholderLabel : UILabel!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var physicalImageView: UIImageView!
    @IBOutlet weak var virtualImageView: UIImageView!
    
    @IBOutlet weak var distanceField: UITextField!
    
    @IBOutlet weak var skillsContainer: UIView!
    @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonCamera: UIButton!
    @IBOutlet weak var buttonGallery: UIButton!
    @IBOutlet weak var buttonVideo: UIButton!
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    var imageArray : [UIImage] = []
    var videoArray = NSMutableArray()
    var collectionViewArray : [UIImage] = []
    var tagArray = NSMutableArray()

    // MARK: - Properties -
    
    var errorView: ErrorView!
    var manageSkillView: GKManageSkillsView!
    weak var delegate: GKCreatePostDelegate!
    
    var isPhysicalSelected = true
    var skillsSelected: [GKModelSkill] = []
    
    let picker = UIImagePickerController()

    var showImageScrollView:ShowImageScrollView?

    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        videoUrlResponse = ""
        
        self.title = "Serve"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(GKCreateServeViewController.cancelRequest)
        )
        
        bodyContentField.layer.cornerRadius = 5.0
        bodyContentField.layer.borderWidth = 1.0
        bodyContentField.layer.borderColor = UIColor.superLightGray().cgColor
        
        updatedKindHelp()
        
        // Zip
        self.zipField.addAccessoryView("Done")
        
        // Distance
        self.distanceField.addAccessoryView("Done")
        
        self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
        self.serveFormView.addSubview(self.errorView)
        
        manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: true, delegate: self)
        skillsContainer.addSubview(manageSkillView)
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionViewAttachments.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
   
        if videoUrlResponse != "" {
            tagArray.add(collectionViewArray.count)
            collectionViewArray.append(UIImage(named: "PlayButtonOverlayLarge")!)
            collectionViewAttachments.reloadData()
            videoArray.add(videoUrlResponse)
            videoUrlResponse = ""
            print(videoArray)
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        updateSkillsView()
        
        self.serveScrollView.contentSize = CGSize(width: self.serveScrollView.frame.width, height: self.serveDistanceView.bottomSide + 225.0)
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        skillsSelected = skillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView()
    {
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        skillsController.creatingPos = true
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill])
    {
        // Nothing for now
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == titleField
        {
            bodyContentField.becomeFirstResponder()
            return false
        }
        else if textField == zipField
        {
            zipField.resignFirstResponder()
            return false
        }
        else if textField == distanceField
        {
            distanceField.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == zipField
        {
            let postString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if postString.countPlainCharacters() <= 5
            {
                return true
            }
            else if string.countPlainCharacters() == 0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.errorView.hide()
    }
    
    // MARK: - Protocol UITextViewDelegate -
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == self.bodyContentField
        {
            self.bodyPlaceholderLabel.isHidden = true
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.bodyContentField
        {
            self.bodyPlaceholderLabel.isHidden = textView.text.count > 0
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == self.bodyContentField
        {
            self.bodyPlaceholderLabel.isHidden = textView.text.countPlainCharacters() > 0
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionSelectPhysical(_ sender: AnyObject)
    {
        guard !isPhysicalSelected else
        {
            return
        }
        
        self.isPhysicalSelected = !self.isPhysicalSelected
        updatedKindHelp()
    }
    
    @IBAction func actionSelectVirtual(_ sender: AnyObject)
    {
        guard isPhysicalSelected else
        {
            return
        }
        
        self.isPhysicalSelected = !self.isPhysicalSelected
        updatedKindHelp()
    }
    
    @IBAction func actionSend(_ sender: AnyObject)
    {
        self.view.endEditing(true)
        self.errorView.hide()
        
        if validate()
        {
            var params = getParams()
            
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
            print(params)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                if self.videoArray.count > 0 {
                    
                    var videoInfo  = ""
                    for i in 0 ..< (self.videoArray.count) {
                        if i > 0{
                            videoInfo +=  "," + (self.videoArray[i] as! String)
                        }
                        else{
                            videoInfo +=  self.videoArray[i] as! String
                        }
                    }
                    let info = "\(videoInfo)"
                    params["videos"] = info as AnyObject?
                }
                for (key, value) in params {
                    multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                }
                for i in 0 ..< (self.imageArray.count) {
                    
                    if let imageData = UIImageJPEGRepresentation(self.imageArray[i] , 0.6)
                    {
                        multipartFormData.append(imageData, withName: "image[]", fileName: "image\(i).jpeg", mimeType: "image/png")
                    }
                }
                print(params)
            }, to: GKRouterClient.createServe(), method: .post, headers: nil,
               encodingCompletion: {
                
                encodingResult in
                switch encodingResult
                {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        LoadingView.hideLoadingView(self.navigationController?.view)
                        print(response.result.value!)
                        if response.result.value != nil
                        {
                            self.imageArray.removeAll()
                            if (self.delegate != nil)
                            {
                                self.delegate.createPostDidFinish()
                            }
                            self.showPostFacebook()
                        }
                    }
                    break
                    
                case .failure(let encodingError):
                    LoadingView.hideLoadingView(self.navigationController?.view)
                    print(encodingError)
                    showCommonError({ (type: NoInternetResultType) -> () in
                       self.actionSend(0 as AnyObject)
                    })
                    break
                }
            })
            
//            GKServiceClient.call(GKRouterClient.createServe(), parameters: params, callback: {
//                (response: AnyObject?) -> () in
//
//                LoadingView.hideLoadingView(self.navigationController?.view)
//
//                if let res:AnyObject = response
//                {
//                    let envelope = ResponseSkeleton(res)
//
//                    if envelope.getSuccess()
//                    {
//                        if (self.delegate != nil)
//                        {
//                            self.delegate.createPostDidFinish()
//                        }
//
//                        self.showPostFacebook()
//                    }
//                    else
//                    {
//                        showCommonError({ (type: NoInternetResultType) -> () in
//                            self.actionSend(0 as AnyObject)
//                        })
//                    }
//                }
//                else
//                {
//                    showConnectionError({ (type: NoInternetResultType) -> () in
//                        self.actionSend(0 as AnyObject)
//                    })
//                }
//            })
        }
    }
    
    @IBAction func buttonCameraClicked(_ sender: Any) {
        self.pickFromCamera()
    }
    
    @IBAction func buttonGalleryClicked(_ sender: Any) {
        self.pickFromGallery()
    }
    
    @IBAction func buttonVideoClicked(_ sender: Any) {
        self.requestCameraRollAccessIfNecessary()
        let alertController = UIAlertController(title:nil, message: nil, preferredStyle: .actionSheet)
        picker.navigationBar.tintColor = UIColor.black;
        
        let objcancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(objcancel)
        
        let objCamera = UIAlertAction(title: "Record video", style: .default) { (action:UIAlertAction!) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = false
                self.picker.sourceType = .camera
                self.picker.videoMaximumDuration = 60
                self.picker.delegate = self
                self.picker.mediaTypes = NSArray(objects: kUTTypeMovie) as! [String]
                self.present(self.picker, animated: true, completion: nil)
            }
        }
        alertController.addAction(objCamera)
        
        let objGallery = UIAlertAction(title: "Choose Existing", style: .default) { (action:UIAlertAction!) in
            self.vimeoInitialization()
        }
        alertController.addAction(objGallery)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    // MARK: - UIImagePicker Methods
    
    func pickFromCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func pickFromGallery()
    {
        let imagePicker = UIImagePickerController()
        //  imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = (info[UIImagePickerControllerEditedImage] as? UIImage) {
            imageArray.append(pickedImage)
            collectionViewArray.append(pickedImage)
            collectionViewAttachments.reloadData()
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let pickedVideo = (info[UIImagePickerControllerMediaURL] as? URL)
            self.dismiss(animated: true, completion: nil)
            
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: pickedVideo!)
            }) { saved, error in
                if saved {
                    self.didSelectVideo( self.PHAssetForFileURL(url: pickedVideo!))
                }
            }
        }
    }
    
    private func requestCameraRollAccessIfNecessary()
    {
        PHPhotoLibrary.requestAuthorization { status in
            switch status
            {
            case .authorized:
                print("Camera roll access granted")
            case .restricted:
                print("Unable to present camera roll. Camera roll access restricted.")
            case .denied:
                print("Unable to present camera roll. Camera roll access denied.")
            default:
                // place for .NotDetermined - in this callback status is already determined so should never get here
                break
            }
        }
    }
    
    func PHAssetForFileURL(url: URL) -> VIMPHAsset {
        
        let imageRequestOptions = PHVideoRequestOptions()
        imageRequestOptions.version = .current
        imageRequestOptions.deliveryMode = .fastFormat
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        let fetchResult = PHAsset.fetchAssets(with: .video, options: options).lastObject
        
        var assets = PHAsset()
        assets = fetchResult!
        
        PHImageManager().requestAVAsset(forVideo: fetchResult!, options: nil, resultHandler: { (avurlAsset, audioMix, dict) in
            let newObj = avurlAsset as! AVURLAsset
            print(newObj.url)
            PHPhotoLibrary.shared().performChanges({
                let videoAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: [newObj.url], options: nil)
                PHAssetChangeRequest.deleteAssets(videoAssetToDelete)
            }, completionHandler: {success, error in
                print(success ? "Deleted Successfully!" : error! )
            })
        })
        
        return (VIMPHAsset(phAsset: assets))
    }
    
    
    func didSelectVideo(_ asset: VIMPHAsset)
    {
        let viewController = VideoSettingsViewController(asset: asset)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.backgroundColor = UIColor.white
        self.present(navigationController, animated: true, completion: nil)
        //topMostController().present(viewController, animated: true, completion: nil)
    }
    
    // MARK: - Vimeo Upload
    
    func vimeoInitialization(){
        let cameraRollViewController = CameraRollViewController(nibName: BaseCameraRollViewController.NibName, bundle:Bundle.main)
        
        let cameraNavController = UINavigationController(rootViewController: cameraRollViewController)
        cameraNavController.view.backgroundColor = UIColor.white
        
        self.present(cameraNavController, animated: true, completion: nil)
    }
    
    // MARK: - UICollectionViewDelegate Methods
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return collectionViewArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
        if (collectionViewArray[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
            cell.imageView.contentMode = .center
        }
        else{
            cell.imageView.contentMode = .scaleToFill
        }
        cell.imageView.image = collectionViewArray[indexPath.row]
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionViewArray[indexPath.row]) == (UIImage(named: "PlayButtonOverlayLarge")){
            let privacyViewController = GKWebViewController(nibName: "GKWebViewController", bundle: nil)
            for (index, tag) in (tagArray.enumerated()) {
                if (tag as! Int) == indexPath.row{
                    privacyViewController.url = videoArray.object(at: index) as! String
                }
            }
            self.navigationController?.pushViewController(privacyViewController, animated: true)
        }
        else{
            addImageScrollView(image:  (collectionViewArray[indexPath.row]))
        }
    }
    
    // MARK: - Methods -
    
    @objc func cancelRequest()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func getParams() -> GKDictionary
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["title"] = titleField.text?.encodeEmoji() as AnyObject?
        params["desc"]  = bodyContentField.text.encodeEmoji() as AnyObject?
        params["help_type"] = isPhysicalSelected ? (1 as AnyObject?) : (2 as AnyObject?)
        params["skills"]    = "\((skillsSelected.map{ $0.uid! }))" as AnyObject?
        
        if let indexElement = (skillsSelected.map{ $0.uid }.index(of: 63))
        {
            params["other"] = skillsSelected[indexElement].otherText as AnyObject?
        }
        
        if isPhysicalSelected
        {
            params["zip_code"]  = zipField.text as AnyObject?
        }
        
        let distanceValueOptional: Int? = Int(distanceField.text!)
        
        if let distanceValue = distanceValueOptional, distanceValue > 0
        {
            params["distance"] = distanceValue as AnyObject?
        }
        else
        {
            params["distance"] = 1000 as AnyObject?
        }
        
        return params
    }
    
    func updatedKindHelp()
    {
        self.physicalImageView.image = UIImage(named: self.isPhysicalSelected ? "check-on" : "check-off")
        self.virtualImageView.image = UIImage(named: !self.isPhysicalSelected ? "check-on" : "check-off")
        
        serveDistanceView.isHidden = !isPhysicalSelected
    }
    
    func updateSkillsView()
    {
        manageSkillView.frame = skillsContainer.bounds
        manageSkillView.setupSkills(skillsSelected)
        
        self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    
    func validate() -> Bool
    {
        let baseY = self.errorView.frame.size.height
        
        if self.titleField.text!.count < 5
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.titleField.frame.minX - 8,
                    y: self.titleField.frame.minY - baseY
                ),
                text: "Title should contain at least 5 characters."
            )
            
            return false;
        }
        
        if self.bodyContentField.text!.count < 10
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.bodyContentField.frame.minX - 8,
                    y: self.bodyContentField.frame.minY - baseY
                ),
                text: "Content should contain at least 10 characters."
            )
            
            return false;
        }
        
        if self.skillsSelected.count == 0
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.skillsContainer.frame.minX,
                    y: self.skillsContainer.frame.minY - baseY
                ),
                text: "Please select atleast one skill.")
            
            return false
        }
        
        if isPhysicalSelected && (self.zipField.text?.countPlainCharacters() != 5)
        {
            self.errorView.showInPos(
                CGPoint(
                    x: self.zipField.frame.minX - 8,
                    y: self.serveDistanceView.frame.minY - (baseY * 0.5)
                ),
                text: "Zip code must be of 5 digits")
            
            return false
        }
        
        return true
    }
    
    func showPostFacebook()
    {
        if Static.sharedInstance.isLoginWithFacebook()
        {
            let parentView = self.navigationController?.view;
            let frame =  CGRect(x:0, y:0, width: parentView!.frame.width,
                                height: parentView!.frame.height);
            let postFacebook = GKPostFacebookView(frame: frame)
            postFacebook.delegate = self
            parentView!.addSubview(postFacebook)
            postFacebook.show()
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - GKPostFacebookDelegate
    
    func postFacebookYes(_ postFacebookView: GKPostFacebookView)
    {
        LoadingView.showLoadingView("Sending...",
                                    parentView: self.navigationController?.view,
                                    backColor: true)
        
        FacebookHandler.publishServe(self, content: bodyContentField.text)
        { (result) in
            if (result)
            {
                postFacebookView.hide()
            }
            else
            {
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                showCommonError({ (type: NoInternetResultType) in
                    self.postFacebookYes(postFacebookView)
                })
            }
        }
    }
    
    func postFacebookNo(_ postFacebookView: GKPostFacebookView)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - FBSDKSharingDelegate
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!)
    {
        NSLog("%@", results)
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sharer(@objc sharer: FBSDKSharing!, didFailWithError error: Error!)
    {
        print("%@", error)
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!)
    {
        LoadingView.hideLoadingView(self.navigationController?.view)
        self.dismiss(animated: true, completion: nil)
    }
}

extension GKCreateServeViewController : ShowImageScrollViewDelegate{
    func addImageScrollView(image: UIImage!){
        showImageScrollView = (Bundle.main.loadNibNamed("ShowImageScrollView", owner: self, options: nil)?[0] as? ShowImageScrollView)!
        
        showImageScrollView?.delegate = self
        showImageScrollView?.setImage(image: image)
        showImageScrollView?.alpha = 0
        showImageScrollView?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        (UIApplication.shared.delegate as! AppDelegate).window!.addSubview(showImageScrollView!)
        UIView.animate(withDuration: 0.7, animations: {
            self.showImageScrollView?.alpha = 1
        } ,completion: { (done) in
            
        } )
    }
    
    // MARK: - ShowImageScrollViewDelegate
    func removeShowImageScrollView()  {
        self.showImageScrollView?.alpha = 1
        UIView.animate(withDuration: 0.5, animations: {
            self.showImageScrollView?.alpha = 0
        } ,completion: { (done) in
            self.showImageScrollView?.removeFromSuperview()
            self.showImageScrollView = nil
        } )
    }
}
