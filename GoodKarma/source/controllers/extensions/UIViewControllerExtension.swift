//
//  UIViewControllerExtension.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

public extension UIViewController {
  
  // MARK: - Methods -
  
  func cleanBackButton() {
    
    let barButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    self.navigationItem.backBarButtonItem = barButtonItem
  }
  
  func listenToNotifications(_ notification_name: String) {
    NotificationCenter.default
      .addObserver(
        self,
        selector: #selector(self.receiveFromNotificationStore(_:)), // Selector(GKNotificationStore.notification_selector)
        name: NSNotification.Name(rawValue: notification_name), object: nil
    )
  }
  
  func setMenuBarBadge() {
    
    listenToNotifications(GKNotificationStore.notification_kind_menu)
    
    // Set menu bar button
    self.navigationItem.leftBarButtonItem = BBBadgeBarButtonItem(
      target: self,
      andSelector: #selector(self.showMenu)
    )
    
    // Update Menu Bar
    updateMenuBarButton()
  }
  
  internal func appDelegate() -> AppDelegate {
    
    return UIApplication.shared.delegate as! AppDelegate
    
  }
  
  @objc func showMenu() {

    appDelegate().container.showMenuWithFactor(1.0)
  }
  
  func updateMenuBarButton() {
    
    if self.navigationItem.leftBarButtonItem is BBBadgeBarButtonItem {
      
      let number_notifications = GKNotificationStore.totalNotifications()
      
      //            if self is ALInboxTableViewController {
      //                number_notifications = ALNotificationStore.instance.number_messages
      //            }
      //            else if self is ALNotificationsTableViewController {
      //                number_notifications = ALNotificationStore.instance.number_notifications
      //            }
      (self.navigationItem.leftBarButtonItem as! BBBadgeBarButtonItem).showBadge(number_notifications)
    }
  }
  
  @objc func receiveFromNotificationStore(_ notification: Notification) {
    
    // Controllers that overrite this:
    // - ALMenuViewController
    // - ALMessageDetailViewController
    updateMenuBarButton()
  }
  
}
