//
//  GKNewMessageContactTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/18/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNewMessageContactTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var pictureImageView: WebImageView!
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
            self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
//            self.pictureImageView.layer.borderColor = UIColor.colorToPercent(186, green:196, blue:209).CGColor
//            self.pictureImageView.layer.borderWidth = 1.3
            self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    
    self.selectionStyle = .none
  }
  
  func loadData(_ data: GKDictionary)
  {
    
    self.nameLabel.text = (data["first_name"] as! String).capitalized +  " " + (data["last_name"] as! String).capitalized
    self.pictureImageView.loadImageWithUrl(data["photo_url"] as? String as NSString?)
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKNewMessageContactTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 100.0
  }
    
}
