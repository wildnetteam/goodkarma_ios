////
////  Authentication.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/17/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation
//
//public class Authentication: NSObject
//{
//    // MARK: Initializer
//    
//    public class func save(data: AnyObject) ->Bool
//    {
//        if let user = GKModelUser.parseJSON(data)
//        {
//            let userDefaults = NSUserDefaults.standardUserDefaults()
//            
//            let userDic = [
//                "uid"       : user.getUid(),
//                "first_name": user.getFirstName(),
//                "last_name" : user.getLastName(),
//                "token"     : user.getToken(),
//                "facebook_id": user.getFacebookId()
//            ]
//            
//            userDefaults.setObject(userDic, forKey: "user")
//            userDefaults.synchronize()
//            
//            return true
//        }
//        
//        return false
//    }
//    
//    public class func saveChannel(channelId: String)
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        userDefaults.setObject(channelId, forKey: "channel")
//        userDefaults.synchronize()
//    }
//    
//    public class func getChannel() -> String?
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let channelId = userDefaults.objectForKey("channel") as? String
//        {
//            return channelId
//        }
//        
//        return nil
//    }
//    
//    public class func saveDataFacebook(data:AnyObject)
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let _ = data as? NSDictionary
//        {
//            var arrData = NSMutableArray()
//            
//            if data.valueForKey("friends") != nil
//            {
//                let dicFriends = data.valueForKey("friends") as! NSDictionary
//                
//                if dicFriends.valueForKey("data") != nil
//                {
//                    let arrDataAux = dicFriends.valueForKey("data") as! NSArray
//                    
//                    if arrDataAux.count != 0
//                    {
//                        arrData = dicFriends.valueForKey("data") as! NSMutableArray
//                    }
//                }
//            }
//            
//            var arrDataLikes = NSMutableArray()
//            
//            if data.valueForKey("likes") != nil
//            {
//                let dicLikes = data.valueForKey("likes") as! NSDictionary
//                
//                if dicLikes.valueForKey("data") != nil
//                {
//                    let arrDataAux = dicLikes.valueForKey("data") as! NSArray
//                    
//                    if arrDataAux.count != 0
//                    {
//                        arrDataLikes = dicLikes.valueForKey("data") as! NSMutableArray
//                    }
//                }
//            }
//            
//            var email: NSString! = ""
//            
//            if let mail = data.valueForKey("email") as? NSString
//            {
//                email = mail
//            }
//            
//            let userFacebook = [
//                "uid" : data.valueForKey("id") as! NSString,
//                "email": email,
//                "first_name": data.valueForKey("first_name") as! NSString,
//                "last_name": data.valueForKey("last_name") as! NSString
//                , "friends": arrData
//                , "likes": arrDataLikes
//            ]
//            
//            userDefaults.setObject(userFacebook, forKey: "userFacebook")
//            userDefaults.synchronize()
//        }
//    }
//    
//    public class func isUserLoggedIn() -> Bool
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let _ = userDefaults.objectForKey("user") as? NSDictionary
//        {
//            return true
//        }
//        else
//        {
//            return false
//        }
//    }
//    
//    public class func getUserDictionary() -> NSDictionary?
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let dic = userDefaults.objectForKey("user") as? NSDictionary
//        {
//            return dic
//        }
//        
//        return nil
//    }
//    
//    public class func getDatafacebook() -> NSDictionary?
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        if let dic = userDefaults.objectForKey("userFacebook") as? NSDictionary
//        {
//            return dic
//        }
//        
//        return nil
//    }
//    
//    public class func clearAccount()
//    {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        let channelId = userDefaults.stringForKey("channel")
//      // TODO: Parse
////        let currentInstallation = PFInstallation.currentInstallation()
////        currentInstallation.removeObject(channelId!, forKey: "channels")
////        currentInstallation.saveInBackground()
//      
//        userDefaults.setObject(nil, forKey: "userFacebook")
//        userDefaults.setObject(nil, forKey: "user")
//        userDefaults.setObject(nil, forKey: "channel")
//        userDefaults.synchronize()
//    }
//}
