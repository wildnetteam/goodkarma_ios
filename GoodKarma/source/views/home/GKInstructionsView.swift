//
//  GKInstructionsView.swift
//  ShoutOut
//
//  Created by Eliezer de Armas on 17/12/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit

enum GKInstructionsType: String {
    case OfferHelp
    case SeekHelp
    case Gratitude
    case History
    case AcceptOrCancel
    case FlakeOrSOS
}

class GKInstructionsView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    var typeInstructions: GKInstructionsType!
    var customPosition: CGPoint!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    fileprivate func setup()
    {
        // View
        view = Bundle.main.loadNibNamed("GKInstructionsView", owner: self,
                                                  options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.backgroundColor = UIColor.clear
        self.addSubview(view)
        self.alpha = 0
        self.backgroundColor = UIColor.clear
        
        // Circle View
        self.circleView.layer.cornerRadius = self.circleView.frame.width/2
        self.circleView.backgroundColor = UIColor.white // RGBCOLOR(120, green: 212, blue: 64)
        
        // White View
        self.whiteView.layer.cornerRadius = 3
        
        // Bubble View
        self.bubbleView.layer.anchorPoint = CGPoint(x: 1, y: 0)
        self.bubbleView.frame = CGRectSetPos(self.bubbleView.frame, x: self.bubbleView.frame.width/2 + self.bubbleView.frame.minX,
                                             y: self.bubbleView.frame.minY - self.bubbleView.frame.height/2)
        self.bubbleView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
        self.bubbleView.isHidden = true
        
        // OK Button
        self.okButton.layer.cornerRadius = 4
    }
    
    func show()
    {
        var title: String
        
        switch typeInstructions!
        {
        case GKInstructionsType.OfferHelp:
            title = "Can't find someone to help? Create a \"Serve\" post and openly offer your assistance!"
            
        case GKInstructionsType.SeekHelp:
            title = "Can't find the help you need? Create a request so others can help you out."
            
        case GKInstructionsType.Gratitude:
            title = "Show everyone what you are grateful for!"
            
        case GKInstructionsType.History:
            self.circleView.isHidden = true
            self.arrow.frame = CGRectSetX(self.arrow.frame, x: 50)
            self.bubbleView.layer.anchorPoint = CGPoint(x: 0, y: 0)
            self.bubbleView.frame = CGRectSetPos(self.bubbleView.frame, x: 10,
                                                 y: self.customPosition.y + 37)
            self.contentView.backgroundColor = UIColor.clear
            title = "Tap here to pause or mark this post as complete!"
            
        case GKInstructionsType.AcceptOrCancel:
            self.circleView.isHidden = true
            self.arrow.frame = CGRectSetX(self.arrow.frame, x: 50)
            self.bubbleView.layer.anchorPoint = CGPoint(x: 0, y: 0)
            self.bubbleView.frame = CGRectSetPos(self.bubbleView.frame, x: 10,
                                                 y: 397)
            self.contentView.backgroundColor = UIColor.clear
            title = "Slide left on a participant to Accept or Reject!"
            
        case GKInstructionsType.FlakeOrSOS:
            self.circleView.isHidden = true
            self.arrow.frame = CGRectSetX(self.arrow.frame, x: 50)
            self.bubbleView.layer.anchorPoint = CGPoint(x: 0, y: 0)
            self.bubbleView.frame = CGRectSetPos(self.bubbleView.frame, x: 10,
                                                 y: 397)
            self.contentView.backgroundColor = UIColor.clear
            title = "Slide left on a participant to Flake or Report!"
        }
        
        titleLabel.text = title
        
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                self.alpha = 1
            },
                                   completion:
            { _ in
                self.showBubble()
        })
    }
    
    func showBubble()
    {
        self.bubbleView.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay:0,
                                   options:UIViewAnimationOptions.curveEaseOut,
                                   animations:
            {
                self.bubbleView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            },
                                   completion:
            { _ in
        })
    }
    
    func hide()
    {
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                self.alpha = 0
            },
                                   completion:
            { _ in
                self.removeFromSuperview()
        })
    }
    
    override func draw(_ rect: CGRect)
    {
        if (self.typeInstructions == .History
            || self.typeInstructions == .AcceptOrCancel
            || self.typeInstructions == .FlakeOrSOS)
        {
            let context = UIGraphicsGetCurrentContext()
            
            context?.setFillColor(RGBACOLOR(0, green: 0, blue: 0, alpha: 0.5).cgColor)
            context?.fill(rect)
            
            var rectIntersection : CGRect!
            
            if (self.typeInstructions == .History)
            {
                rectIntersection = CGRect(x: 5, y: self.customPosition.y,
                    width: 140, height: 36).intersection(rect)
            }
            else
            {
                rectIntersection = CGRect(x: 5, y: 300,
                    width: self.frame.width - 10, height: 95).intersection(rect)
            }
            
            if (rectIntersection.intersects(rect))
            {
                context?.addRect(rectIntersection)
                context?.clip()
                context?.clear(rectIntersection)
                context?.setFillColor(UIColor.clear.cgColor)
                context?.fill(rectIntersection)
            }
        }
        else
        {
            super.draw(rect)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func notShowAgainAction(_ sender: AnyObject)
    {
        Static.sharedInstance.saveShowInstructions(typeInstructions)
        
        hide()
    }
    
    @IBAction func okAction(_ sender: AnyObject)
    {
        hide()
    }
    
    @IBAction func closeAction(_ sender: AnyObject)
    {
        hide()
    }
}
