//
//  SOSAddContactsTableViewCell.swift
//  GoodKarma
//
//  Created by Shivani on 03/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
class SOSAddContactsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sendLocationButton: UIButton!
  
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var shortNameButton: UIButton!
    
    @IBOutlet weak var lowerView: UIView!
    var delegate : GKSosContactDelegate!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        shortNameButton.layer.cornerRadius = shortNameButton.frame.size.width/2.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    
    internal class func heightForCell() -> CGFloat {
        
        return 112.0
    }
    
    internal class func cellIdentifier() -> String
    {
        return "SOSAddContactsCell"
    }
    
  
    
    func loadData(data : GKDictionary)
    {
        let phn_no : String!
        let valueExists = data["phn_no"] as? String != nil
        if valueExists
        {
            phn_no = data["phn_no"] as! String
        }
        else
        {
            phn_no = " "
        }
        
        let country_code = data["country_code"] as! String
        self.phoneNumberLabel.text = "\(String(describing: country_code))" + "-" + "\(String(describing: phn_no!))"
        
        var full_name : String!
        if valueExists
        {
            full_name = (data["name"] as! String)
        }
        else
        {
            full_name = " "
        }
        
        
        var last_name : String!
       
        var first_name  = full_name.components(separatedBy: " ")[0]
        first_name =  first_name.capitalized
        let value = full_name.components(separatedBy: " ")
        if value.count > 1
        {
            last_name = value[1]
            last_name = last_name.capitalized
        }
        else
        {
            last_name = "  "
        }
        self.userNameLabel.text = first_name + " " + last_name
        let firstIndex = first_name.index(first_name.startIndex, offsetBy: 0)
        let lastIndex = last_name.index(last_name.startIndex, offsetBy: 0)
        if full_name != " "
        {
            let first_character = String("\(first_name[firstIndex])".uppercased())
            let last_character = String("\(last_name[lastIndex])".uppercased())
            self.shortNameButton.setTitle(first_character + last_character, for: .normal)
        }
        else
        {
            self.shortNameButton.setTitle(" ", for: .normal)
        }
        
    }
    
    @IBAction func actionSendLocation(_ sender: Any)
    {
        if delegate != nil
        {
            let index = (sender as AnyObject).tag - 201
            self.delegate.passSendLocationData(index  :index)
        }
    }
    
//    @IBAction func actionDeleteContact(_ sender: Any)
//    {
////
////        if delegate != nil
////        {
////            let index = (sender as AnyObject).tag - 101
////            self.delegate.passDeleteData(index : index)
////        }
////
//    }
}

