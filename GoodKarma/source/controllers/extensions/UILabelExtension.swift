//
//  UILabelExtension.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 22/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

public extension UILabel {

    func willBeTruncated() -> Bool {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = self.lineBreakMode
        label.font = self.font
        label.text = self.text
        label.attributedText = self.attributedText
        label.sizeToFit()
        if label.frame.height > self.frame.height {
            return true
        }
        return false
    }
}
