////
////  GKModel.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/23/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation

typealias ID = Int

protocol GKModelProtocol {
  
  var uid: ID! { get set }
  
  init()
  //  init(uid: String)
  
  static func readFromDictionary(_ dictionary: GKDictionary) -> Self
  
}

extension GKModelProtocol {
  
  init(uid: ID) {
    
    self.init()
    self.uid = uid
    
  }
  
}
