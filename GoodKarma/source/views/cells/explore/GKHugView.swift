//
//  GKHugView.swift
//  GoodKarma
//
//  Created by Shivani on 11/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

typealias hugServiceResponse = (Bool) -> Void
class GKHugView: UIView
{
    
    @IBOutlet weak var hugButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var virtualHugLabel: UILabel!
    @IBOutlet weak var sendVirtualHugView: UIView!
    @IBOutlet weak var hugView: UIView!
    @IBOutlet weak var appreciateFirstLabel: UILabel!
    
    @IBOutlet weak var virtutalHugimage: UIImageView!
    
    var recipientId : Int!
    var authorId : Int!
    var positivityId : Int!
    var errorMessage : String!
    var postType : Int!

    func setUp()
    {
        self.sendVirtualHugView.isHidden = true
        self.hugView.isHidden = false
        
    }
    @IBAction func hugAction(_ sender: Any)
    {
        self.isHidden = true
    }
    @IBAction func virtualHugAction(_ sender: Any)
    {
        sendHug(hugType : 2)
        {
            (isVirtualHug) in
            if isVirtualHug == true
            {
                self.showVirtualHug()
            }
            else
            {
                
            }
        }
    }
    
    @IBAction func physicalHugAction(_ sender: Any)
    {
        sendHug(hugType : 1)
        {
            (isPhysicalHug) in
            if isPhysicalHug == true
            {
                self.showVirtualHug()
            }
            else
            {
                
            }
        }
    }
    
    func sendHug(hugType : Int , onCompletion: @escaping hugServiceResponse) -> ()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject
        params["authorId"] = self.authorId as AnyObject?
        params["recipientId"] = self.recipientId as AnyObject?
        params["hugType"] = hugType as AnyObject?
        params["positivityId"] = self.positivityId as AnyObject?
        params["post_type"] = self.postType as AnyObject?
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self, backColor: true
        )
        GKServiceClient.call(GKRouterClient.sendHug(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    onCompletion(true)
                    print(response!)
                }
                else
                {
                    self.isHidden = true
                    let error = envelope.getErrors()
                    let errorMessage = error!["object"] as! String
                    let alertController = UIAlertController(title: "Message", message: errorMessage , preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    AppDelegate.appDelegate().window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    onCompletion(false)
                    print(response!)
                }
            }
            else
            {
                onCompletion(false)
                print("error")
            }
        }
        )
        
    }
    
    @objc func showVirtualHug()
    {
        self.hugView.isHidden = true
        self.sendVirtualHugView.isHidden = false
//        let heartGif = UIImage.gifImageWithName("virtual_hug@3x")
//        virtutalHugimage.image = heartGif
    }
    
    
    func handleTap()
    {
        self.isHidden = true
    }
}

