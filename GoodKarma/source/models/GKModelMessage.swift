////
////  GKModelMessage.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/24/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation
//
//enum GKMessageType : Int {
//  case Personal
//  case Another
//}
//
//struct GKModelMessage: GKModelProtocol {
//  
//  var uid: String!
//  var content: String!
//  var sender_uid: String!
//  var message_type: GKMessageType!
//  
//  // MARK: - Methods -
//  
//  // MARK: - Class Methods -
//  
//  static func readFromDictionary(dictionary: GKDictionary) -> GKModelMessage {
//    
//    var message = GKModelMessage()
//    
//    message.uid        = dictionary["_id"] as! String
//    message.content    = dictionary["message"] as! String
//    message.sender_uid = dictionary["sender"] as! String
//    
//    // TODO: Check if this message if from the current user
//    //    if message.sender_uid == GKServiceUser.instance.fetchCredential(ALServiceUser.Credential_Types.Uid) {
//    //
//    //      message.message_type = GKMessageType.Personal
//    //    }
//    //    else {
//    message.message_type = GKMessageType.Another
////    }
//    
//    return message
//  }
//}
//
//
//
//
