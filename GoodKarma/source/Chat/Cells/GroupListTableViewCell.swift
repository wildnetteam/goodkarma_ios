//
//  GroupListTableViewCell.swift
//  SuperMom
//
//  Created by Anuj Jha on 23/09/17.
//  Copyright © 2017 Shatakshi. All rights reserved.
//

import UIKit

class GroupListTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var groupImageLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var participantsNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        groupImageLabel.layer.cornerRadius = groupImageLabel.frame.size.width/2
        groupImageLabel.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
