//
//  GKFriendsViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import AddressBookUI
import MessageUI
import FBSDKShareKit

class GKFriendsViewController: GKViewController, UITableViewDataSource, UITableViewDelegate, ABPeoplePickerNavigationControllerDelegate, GKShowUserProfileDelegate, GKFollowDelegate, MFMessageComposeViewControllerDelegate, FBSDKAppInviteDialogDelegate
{
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var segmentedControl: UISegmentedControl!
  
  @IBOutlet weak var followingTableView: UITableView!
  @IBOutlet weak var followersTableView: UITableView!
  
  @IBOutlet weak var emptyLabel: UILabel!
  
  // MARK: - Properties -
  
  var isFollowersSelected = false
  
  fileprivate var inviteView: GKInviteFriends!
  
  fileprivate var followers: [GKDictionary] = []
  fileprivate var following: [GKDictionary] = []
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Friends"
    
    setMenuBarBadge()
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(GKFriendsViewController.showOptionsInvite))
    
    self.followersTableView.register(
      UINib(nibName: GKFollowerTableViewCell.cellIdentifier() ,bundle: nil),
      forCellReuseIdentifier: GKFollowerTableViewCell.cellIdentifier()
    )
    self.followingTableView.register(
      UINib(nibName: GKFollowingTableViewCell.cellIdentifier() ,bundle: nil),
      forCellReuseIdentifier: GKFollowingTableViewCell.cellIdentifier()
    )
    self.followersTableView.tableFooterView = UIView(frame: CGRect.zero)
    self.followingTableView.tableFooterView = UIView(frame: CGRect.zero)
    
    cleanBackButton()
    
    segmentedControl.selectedSegmentIndex = 1
    updateViews()
  }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        requestFollowings()
    }
  
  // MARK: - Protocols
  // MARK: - Protocol ABPeoplePickerNavigationControllerDelegate -
  
  func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier)
  {
    peoplePicker.dismiss(animated: true, completion: nil)
    
    let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
    let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
    let contact = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
    let nameCFString: CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
    let name: NSString = nameCFString as NSString
    let firstName = name.components(separatedBy: " ")[0]
    
    let userDic = NSMutableDictionary()
    userDic["name"] = name
    userDic["first_name"] = firstName
    userDic["contact"] = contact
    
    showSMS(userDic)
  }
  
  // MARK: - Protocol FBSDKAppInviteDialogDelegate -
  
  func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable: Any]!) {
    
    closeInviteDialog()
    // Show   friends to follow
  }
  
  func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!)
  {
    NSLog("asdf", error.localizedDescription)
  }
  
  // MARK: - Protocol GKShowUserProfileDelegate -
  
  func GKShowProfile(_ userID: Int)
  {
    let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
    profileController.anotherUserID = userID
    
    self.present(
      UINavigationController(rootViewController: profileController), animated: true, completion: nil
    )
  }
  
  // MARK: - Protocol GKFollowDelegate -
    
  func GKFollow(_ userID: Int) {
    
    var params = GKDictionary()
    params["token"]   = Static.sharedInstance.token() as AnyObject?
    params["user_id"] = userID as AnyObject?
    
    LoadingView.showLoadingView("Loading...", parentView: self.navigationController?.view, backColor: true)
    
    GKServiceClient.call(GKRouterClient.follow(), parameters: params) { (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          self.requestData()
        }
      }
    }
  }
  
  func GKUnfollow(_ userID: Int, name: String) {
    
    let alertController = UIAlertController(title: nil, message: "Unfollow \(name)?", preferredStyle: .actionSheet)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
    }
    
    let destroyAction = UIAlertAction(title: "Unfollow", style: .destructive) { (action) in
      self.unfollow(userID)
    }
    
    alertController.addAction(cancelAction)
    alertController.addAction(destroyAction)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Protocol MFMessageComposeViewControllerDelegate -
  
  func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                    didFinishWith result: MessageComposeResult)
  {
    
    switch result.rawValue
    {
    case MessageComposeResult.sent.rawValue :
      
      let text = NSString(format: "Your invitation has been successfully sent!")
      showAlert("Message sent", text: text as String, delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
      
    case MessageComposeResult.cancelled.rawValue :
      NSLog("SMS Canceled")
      
    case MessageComposeResult.failed.rawValue :
      showConnectionError()
      
    default:
      NSLog("")
    }
    
    controller.dismiss(animated: true, completion: nil)
  }
  
  // MARK: - Protocol UITableViewDataSource -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    if tableView == followersTableView {
      
      return self.followers.count
    }
    else {
      
      return self.following.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    if tableView == followersTableView
    {
      let cell = tableView.dequeueReusableCell(
        withIdentifier: GKFollowerTableViewCell.cellIdentifier(), for: indexPath) as! GKFollowerTableViewCell
      
      let data = self.followers[indexPath.row]
      
      cell.delegate = self
      cell.loadData(data, mutual_follow: data["mutual_follow"] as! Bool)
      
      return cell
    }
    else
    {
      let cell = tableView.dequeueReusableCell(
        withIdentifier: GKFollowingTableViewCell.cellIdentifier(), for: indexPath) as! GKFollowingTableViewCell
      
     // let dataUser = self.following[indexPath.row]["id"] as! GKDictionary
        let dataUser = self.following[indexPath.row] 
      cell.delegate = self
      cell.loadData(dataUser)
      
      return cell
    }
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
  {
    if tableView == followersTableView
    {
        let dataUser = self.followers[indexPath.row]
      self.GKShowProfile(dataUser["id"] as! Int)
    }
    else
    {
      //let dataUser = self.following[indexPath.row]["user"] as! GKDictionary
        let dataUser = self.following[indexPath.row] 
        self.GKShowProfile(dataUser["id"] as! Int)
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    if tableView == followersTableView
    {
      return GKFollowerTableViewCell.heightForCell()
    }
    else
    {
      return GKFollowingTableViewCell.heightForCell()
    }
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionSwitchViews(_ sender: UISegmentedControl)
  {
    isFollowersSelected = (sender.selectedSegmentIndex == 0)
    
    requestData()
  }
  
  // MARK: - Invite -
  
  @objc func closeInviteDialog()
  {
    inviteView.close()
  }
  
  func showInviteView()
  {
    self.inviteView = GKInviteFriends(frame: self.view.bounds)
    self.inviteView.skipButton.addTarget(self, action: #selector(GKFriendsViewController.closeInviteDialog), for: .touchUpInside)
    self.inviteView.skipButton.setTitle("Close", for: UIControlState())
    self.inviteView.inviteFacebookButton.addTarget(self, action: #selector(GKFriendsViewController.showDialog), for: .touchUpInside)
    self.inviteView.inviteContactsButton.addTarget(self, action: #selector(GKFriendsViewController.showContacts), for: .touchUpInside)
    self.navigationController!.view.addSubview(self.inviteView)
  }
  
  @objc func showDialog()
  {
    let content = FBSDKAppInviteContent()
    content.appLinkURL = URL(string: "https://fb.me/1734736420084534")
    content.appInvitePreviewImageURL = URL(string: "http://www.goodkarms.com/assets/img/invite-friends.png")
    FBSDKAppInviteDialog.show(with: content, delegate: self)
  }
  
  // MARK: - Methods -

  func requestData()
  {
    if isFollowersSelected
    {
      requestFollowers()
    }
    else
    {
      requestFollowings()
    }
  }
  
  func requestFollowings()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.following(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response
      {
       // let envelope = ResponseSkeleton(res)
        let envelope = ResponseSkeleton(res , string : "following")
        if envelope.getSuccess()
        {
          if let data = envelope.getResource() as? [GKDictionary]
          {
            self.following = data
            self.updateViews()
          }
        }
      }
    })
  }
  
  func requestFollowers()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.followers() , parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        //let envelope = ResponseSkeleton(res)
        let envelope = ResponseSkeleton(res , string: "followers")
        if envelope.getSuccess() {
          
          if let data = envelope.getResource() as? [GKDictionary] {
            
            self.followers = data
            self.updateViews()
          }
        }
      }
    })
  }
  
  @objc func showOptionsInvite()
  {
    self.showInviteView()
  }

  @objc func showContacts()
  {
    
    let c = ABPeoplePickerNavigationController()
    c.peoplePickerDelegate = self
    c.displayedProperties = [
      NSNumber(value: kABPersonPhoneProperty as Int32)
    ]
    
    ABAddressBookRequestAccessWithCompletion(c, { success, error in
        if success {
            self.present(c, animated: true, completion: nil)
        }
    })
  }
  
  func showSMS(_ userInfo: NSDictionary)
  {
    let c = MFMessageComposeViewController()
    c.messageComposeDelegate = self
    c.recipients = [userInfo["contact"] as! String]
    c.body = createBodyText(userInfo)
    
    if (MFMessageComposeViewController.canSendText())
    {
      self.present(c, animated: true, completion: nil)
    }
    else
    {
      showAlert("Could not send SMS", text: "Your device could not send SMS.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
    }
  }
  
  func unfollow(_ userID: Int)
  {
    var params = GKDictionary()
    params["token"]   = Static.sharedInstance.token() as AnyObject?
    params["user_id"] = userID as AnyObject?
    
    LoadingView.showLoadingView("Loading...", parentView: self.navigationController?.view, backColor: true)
    
    GKServiceClient.call(GKRouterClient.unfollow(), parameters: params) { (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
          self.requestData()
        }
      }
    }
  }

  func updateViews()
  {
    if isFollowersSelected
    {
      followersTableView.reloadData()
      
      followingTableView.isHidden  = true
      
      emptyLabel.isHidden          = (followers.count != 0)
      followersTableView.isHidden  = (followers.count == 0)
    }
    else
    {
      followingTableView.reloadData()
      
      followersTableView.isHidden  = true
      
      emptyLabel.isHidden          = (following.count != 0)
      followingTableView.isHidden  = (following.count == 0)
    }
  }
}
