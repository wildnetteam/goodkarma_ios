//
//  GKSkillOtherViewController.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 29/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol GKSkillOtherDelegate: NSObjectProtocol
{
    func otherSaveText(_ skill: GKModelSkill)
}

class GKSkillOtherViewController: GKViewController, UITextFieldDelegate
{
    @IBOutlet weak var textField: UITextField!
    weak var delegate: GKSkillOtherDelegate!
    var skill: GKModelSkill!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Other Skill"
        
        let cancel = UIBarButtonItem(title: "Cancel",
                                     style: .plain,
                                     target: self,
                                     action: #selector(GKSkillOtherViewController.cancel))
        self.navigationItem.leftBarButtonItem = cancel

        let save = UIBarButtonItem(title: "Save",
                                   style: .plain,
                                   target: self,
                                   action: #selector(GKSkillOtherViewController.save))
        save.isEnabled = false
        self.navigationItem.rightBarButtonItem = save
        
        // Observer
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(GKSkillOtherViewController.textFieldDidChange(_:)),
                                       name: NSNotification.Name.UITextFieldTextDidChange,
                                       object: self.textField)
    }
    
    deinit
    {
       NotificationCenter.default.removeObserver(self)
    }
    
    @objc func save()
    {
        if (self.textField.text?.count > 2)
        {
            self.skill.otherText = self.textField.text
            self.view.endEditing(true)
            self.delegate.otherSaveText(self.skill)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func cancel()
    {
       self.view.endEditing(true)
       dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        save()
        
        return false
    }
    
    // MARK: - Observer
    
    @objc func textFieldDidChange(_ notification: Notification)
    {
        if (self.textField.text?.count > 2)
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        else
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
}
