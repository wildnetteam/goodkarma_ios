//
//  GKSkillSubCategoriesViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSkillSubCategoriesViewController: UIViewController,
UITableViewDataSource, UITableViewDelegate, GKSkillOtherDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var backArrowButton: UIButton!
    
    // MARK: - Properties -
    
    var category       : GKDictionary!
    var subcategories  : [GKModelSkill]!
    var skillsSelected : [GKModelSkill]!
    var creatingPos: Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        categoryNameLabel.text = category["name"] as? String
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        reloadTableData()
        
        //    self.navigationItem.rightBarButtonItem?.style = .Done
        //    self.navigationItem.rightBarButtonItem?.action = Selector(self.doneAction())
        //    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(self.doneAction))
        //    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .Plain, target: nil, action: nil)
        
        //    backArrowButton.setImage(UIImage(named: "arrow-l"), forState: .Normal)
        //    backArrowButton.contentMode = .ScaleAspectFit
        
    }
    
    // MARK: - Protocols
    // MARK: - Protocol UITableViewDataSource -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.subcategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let skill = subcategories[indexPath.row]
        let subcategoryID = skill.uid
        var subcategoryName = skill.name
        
        if (skill.uid == 63 && self.creatingPos)
        {
            if let indexElement = (skillsSelected.map{ $0.uid }.index(of: subcategoryID! as Int))
            {
                let selectedSkill = skillsSelected[indexElement]
                
                if (selectedSkill.otherText.count > 0)
                {
                   subcategoryName = selectedSkill.text()
                }
            }
        }
        
        cell.textLabel?.text = subcategoryName
        cell.tintColor = UIColor.baseColor()
        cell.accessoryType = skillsSelected.map{ $0.uid }.contains( subcategoryID! as Int) ? .checkmark : .none
        
        return cell
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let subcategoryID = subcategories[indexPath.row].uid
        
        if let indexElement = (skillsSelected.map{ $0.uid }.index(of: subcategoryID! as Int)) {
            
            skillsSelected.remove(at: indexElement)
        }
        else {
            
            if (self.creatingPos && subcategoryID == 63)
            {
                let skillOtherViewController = GKSkillOtherViewController(nibName: "GKSkillOtherViewController", bundle: nil)
                skillOtherViewController.delegate = self
                skillOtherViewController.skill = subcategories[indexPath.row]
                self.present(UINavigationController(rootViewController: skillOtherViewController), animated: true, completion: nil)
            }
            else
            {
               skillsSelected.append(subcategories[indexPath.row])
            }
        }
        
        reloadTableData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    // MARK: - IBActions -
    
    @IBAction func backAction(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
      //  done()
    }
    
    @IBAction func doneAction(_ sender: AnyObject) {
        
        done()
    }
    
    // MARK: - Methods -
    
    func done() {
        
        //modified
        if let categoriesViewController = (self.presentingViewController as? UINavigationController)?.viewControllers.last as? GKSkillCategoriesViewController
        {
            categoriesViewController.skillsSelected = self.skillsSelected
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            for viewcontroller in self.navigationController!.viewControllers as Array {
                if viewcontroller is  GKSkillCategoriesViewController
                {
                    let vc = viewcontroller as! GKSkillCategoriesViewController
                    vc.skillsSelected = self.skillsSelected
                    self.navigationController?.popToViewController(viewcontroller , animated: true)
                    break
                }
            }
        }
    }
    
    func reloadTableData() {
        
        tableView.reloadData()
    }
    
    // MARK: - GKSkillOtherDelegate -
    
    func otherSaveText(_ skill: GKModelSkill)
    {
        let subcategoryID = skill.uid
        skillsSelected.append(skill)
        
        if let indexElement = (subcategories.map{ $0.uid }.index(of: subcategoryID! as Int))
        {
            subcategories[indexElement] = skill
        }
        
        if let indexElement = (skillsSelected.map{ $0.uid }.index(of: subcategoryID! as Int))
        {
            skillsSelected[indexElement] = skill
        }
        
        self.reloadTableData()
    }
}
