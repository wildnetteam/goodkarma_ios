//
//  GKInboxViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKInboxViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var CreateGroupButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyButton: UIButton!
    @IBOutlet weak var createGroupButton: UIButton!
    // MARK: - Properties -
    
    var recentChatsArray_OneToOne:[RecentChats] = []
    var recentChatsArray_Group:[GroupList] = []
    var isGroupChat: Bool = false
    var participantsNameString = ""
    
    fileprivate var messages: [GKDictionary]! = []
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.CreateGroupButtonHeightConstraint.constant = 0
        self.createGroupButton.isHidden = true
        self.title = "Chat"
        // Loading View
        //    let frameLoadingView = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 130)
        
        //    self.loadingView = LoadingView(frame:frameLoadingView)
        //    self.loadingView.noDataLabel.text = "You don't have messages"
        //    self.loadingView.delegate = self
        //    self.tableView.addSubview(self.loadingView)
        
        // Nib
        //    let classBundle = NSBundle(forClass: MessagesMainCell.self)
        //    let nib = UINib(nibName: "MessagesMainCell", bundle: classBundle)
        //    self.tableView.registerNib(nib, forCellReuseIdentifier: "MessagesMainCell")
        //
        //    self.tableView.registerClass(GKInboxTableViewCell.self, forCellReuseIdentifier: GKInboxTableViewCell.cellIdentifier())
        //FireBaseManager.defaultManager.fetchMyGroups(id: "290")
        FireBaseManager.defaultManager.recentChatsManagerDelegate = self
        FireBaseManager.defaultManager.recentChats = []
        FireBaseManager.defaultManager.fetchRecentChats(id: "\(String(describing: UserDefaults.standard.object(forKey: "id")!))")
        
        self.tableView.register(
            UINib(nibName: GKInboxTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKInboxTableViewCell.cellIdentifier()
        )
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        cleanBackButton()
        setMenuBarBadge()
        
        let composeBarButton = UIBarButtonItem(
            barButtonSystemItem: .compose,
            target: self,
            action: #selector(GKInboxViewController.openContacts)
        )
        composeBarButton.tintColor = UIColor.baseColor()
        composeBarButton.isEnabled = false
        self.navigationItem.rightBarButtonItem = composeBarButton
        
        self.emptyButton.layer.cornerRadius = 5.0
        // Load Data
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        // Request Menu
        //GKNotificationStore.requestMenuCount()
        // Request Data
        //requestMessageList()
    }
    
    // MARK: - Notifications -
    
    override func receiveFromNotificationStore(_ notification: Notification)
    {
        updateMenuBarButton()
        print("TOTAL \(GKNotificationStore.totalNotifications())")
        requestMessageList()
    }
    
    // MARK: - Protocols
    // MARK: - Protocol UITableViewDelegate -
    
    //  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    //  {
    //    return true
    //  }
    //
    //  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    //  {
    //    if editingStyle == UITableViewCellEditingStyle.delete
    //    {
    //      print("delete")
    //      self.deleteConversationAtIndex(indexPath)
    //    }
    //  }
    
    //  func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
    //    .Non
    //  }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isGroupChat{
            return self.recentChatsArray_Group.count
        }else{
            return self.recentChatsArray_OneToOne.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: GKInboxTableViewCell.cellIdentifier(), for: indexPath) as! GKInboxTableViewCell
        
        if isGroupChat{
            cell.nameLabel.text = recentChatsArray_Group[indexPath.row].groupName
            if let url = URL(string: recentChatsArray_Group[indexPath.row].groupImageURL!){
                cell.userImage.sd_setImage(with: url, placeholderImage: UIImage(named: "user-placeholder"))
            }else{
                cell.userImage.image = UIImage(named: "user-placeholder")
            }
            cell.timeLabel.text = recentChatsArray_Group[indexPath.row].time
            cell.messageLabel.text = recentChatsArray_Group[indexPath.row].lastMsg
            cell.unreadCountView.isHidden = true
        }else{
            if recentChatsArray_OneToOne[indexPath.row].senderId != "\(String(describing: UserDefaults.standard.object(forKey: "id")!))"{
                cell.nameLabel.text = recentChatsArray_OneToOne[indexPath.row].senderName
                if let url = URL(string: recentChatsArray_OneToOne[indexPath.row].senderImg!){
                    cell.userImage.sd_setImage(with: url, placeholderImage: UIImage(named: "user-placeholder"))
                }else{
                    cell.userImage.image = UIImage(named: "user-placeholder")
                }
                let unreadDict = recentChatsArray_OneToOne[indexPath.row].unreadCountDict
                //        let keyArray = unreadDict?.allKeys
                //        var nodeStr = ""
                //        var id = ""
                let count = unreadDict?.object(forKey: "\(String(describing: UserDefaults.standard.object(forKey: "id")!))") as! Int
                //        if ((keyArray! as NSArray).object(at: 0) as! String) == recentChatsArray_OneToOne[indexPath.row].senderId{
                //            id = ((keyArray! as NSArray).object(at: 0) as! String)
                //        }else{
                //            id = ((keyArray! as NSArray).object(at: 1) as! String)
                //        }
                //        let num1 = ((keyArray! as NSArray).object(at: 0) as! NSString).doubleValue
                //        let num2 = ((keyArray! as NSArray).object(at: 1) as! NSString).doubleValue
                //        if num1<num2{
                //            nodeStr = "\(Int(num1))-\(Int(num2))"
                //        }else{
                //            nodeStr = "\(Int(num2))-\(Int(num1))"
                //        }
                //        let key = recentChatsArray_OneToOne[indexPath.row].senderId!
                //        var count = unreadDict?.object(forKey:key) as! Int
                //        FireBaseManager.defaultManager.updateUnreadCount(node: nodeStr, id:id , onCompletion: { (cnt) in
                //            print("On Cell - \(cnt)")
                //            count = cnt
                //            if count == 0{
                //                cell.unreadCountView.isHidden = true
                //            }else{
                //                cell.unreadCountView.isHidden = false
                //                cell.unreadCountLabel.text = "\(cnt)"
                //            }
                //        })
                if count == 0{
                    cell.unreadCountView.isHidden = true
                }else{
                    cell.unreadCountView.isHidden = false
                    cell.unreadCountLabel.text = "\(count)"
                }
                
            }else{
                cell.nameLabel.text = recentChatsArray_OneToOne[indexPath.row].recieverName
                if let url = URL(string: recentChatsArray_OneToOne[indexPath.row].recieverImg!){
                    cell.userImage.sd_setImage(with: url, placeholderImage: UIImage(named: "user-placeholder"))
                }else{
                    cell.userImage.image = UIImage(named: "user-placeholder")
                }
                let unreadDict = recentChatsArray_OneToOne[indexPath.row].unreadCountDict
                //        let keyArray = unreadDict?.allKeys
                //        var nodeStr = ""
                //        var id = ""
                
                //        if ((keyArray! as NSArray).object(at: 0) as! String) == recentChatsArray_OneToOne[indexPath.row].senderId{
                //            id = ((keyArray! as NSArray).object(at: 0) as! String)
                //        }else{
                //            id = ((keyArray! as NSArray).object(at: 1) as! String)
                //        }
                //
                //        let num1 = ((keyArray! as NSArray).object(at: 0) as! NSString).doubleValue
                //        let num2 = ((keyArray! as NSArray).object(at: 1) as! NSString).doubleValue
                //        if num1<num2{
                //            nodeStr = "\(Int(num1))-\(Int(num2))"
                //        }else{
                //            nodeStr = "\(Int(num2))-\(Int(num1))"
                //        }
                //        let key = recentChatsArray_OneToOne[indexPath.row].recieverId!
                //        var count = unreadDict?.object(forKey:key) as! Int
                //        FireBaseManager.defaultManager.updateUnreadCount(node: nodeStr, id:id ,onCompletion: { (cnt) in
                //            print("On Cell - \(cnt)")
                //            count = cnt
                //            if count == 0{
                //                cell.unreadCountView.isHidden = true
                //            }else{
                //                cell.unreadCountView.isHidden = false
                //                cell.unreadCountLabel.text = "\(cnt)"
                //            }
                //        })
                let count = unreadDict?.object(forKey: "\(String(describing: UserDefaults.standard.object(forKey: "id")!))") as! Int
                if count == 0{
                    cell.unreadCountView.isHidden = true
                }else{
                    cell.unreadCountView.isHidden = false
                    cell.unreadCountLabel.text = "\(count)"
                }
                
            }
            cell.messageLabel.text = recentChatsArray_OneToOne[indexPath.row].lastMsg
            cell.timeLabel.text = recentChatsArray_OneToOne[indexPath.row].time
            //cell.loadData(self.messages[indexPath.row])
        }
        
        return cell
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let newMessageContact = GKMessagesViewController(nibName:
            "GKChatViewController", bundle: nil)
        if isGroupChat{
            
            if (recentChatsArray_Group[indexPath.row].participants != nil){
                let participantsIds = recentChatsArray_Group[indexPath.row].participants?.allKeys
                print("participantsIds--\(String(describing: participantsIds!))")
                participantsNameString = ""
                for index in 0..<participantsIds!.count{
                    if participantsIds![index] as! String != "\(String(describing: UserDefaults.standard.object(forKey: "id")!))"{
                        let firstName = ((recentChatsArray_Group[indexPath.row].participants)?.object(forKey: participantsIds![index] as! String) as! NSDictionary).object(forKey: "first_name") as! String
                        if participantsNameString != ""{
                            participantsNameString = participantsNameString + ", \(firstName)"
                        }else{
                            participantsNameString = firstName
                        }
                    }
                }
            }
            newMessageContact.groupMembersName = participantsNameString
            newMessageContact.groupName = recentChatsArray_Group[indexPath.row].groupName!
            newMessageContact.groupNode = recentChatsArray_Group[indexPath.row].groupId!
            newMessageContact.groupImage = recentChatsArray_Group[indexPath.row].groupImageURL!
            newMessageContact.isGroupChat = true
        }else{
            if recentChatsArray_OneToOne[indexPath.row].senderId != "\(String(describing: UserDefaults.standard.object(forKey: "id")!))"{
                let anotherUser = GKMessageAnotherUser(uid: Int(recentChatsArray_OneToOne[indexPath.row].senderId!), fullName: recentChatsArray_OneToOne[indexPath.row].senderName)
                newMessageContact.anotherUser = anotherUser
            }else{
                let anotherUser = GKMessageAnotherUser(uid: Int(recentChatsArray_OneToOne[indexPath.row].recieverId!), fullName: recentChatsArray_OneToOne[indexPath.row].recieverName)
                newMessageContact.anotherUser = anotherUser
            }
            newMessageContact.isGroupChat = false
        }
        
        self.navigationController?.pushViewController(newMessageContact, animated:true)
        
        
        
        //    let dataMessage = self.messages[indexPath.row]["another_user"] as! GKDictionary
        //
        //    openConversation(dataMessage["id"] as! Int, fullName: dataMessage["full_name"] as! String)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        return GKInboxTableViewCell.heightForCell()
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionCompose(_ sender: AnyObject)
    {
        openContacts()
    }
    
    @IBAction func createGroupButtonTapped(_ sender: Any) {
        
        print("createGroupButtonTapped")
        let createGroup = CreateGroupViewController(nibName: "CreateGroupViewController", bundle: nil)
        self.navigationController?.pushViewController(createGroup, animated: true)
    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        
        if segmentControl.selectedSegmentIndex == 0{
            isGroupChat = false
            self.CreateGroupButtonHeightConstraint.constant = 0
            self.createGroupButton.isHidden = true
            print("selectedSegmentIndex- \(segmentControl.selectedSegmentIndex)")
            FireBaseManager.defaultManager.fetchRecentChats(id: "\(String(describing: UserDefaults.standard.object(forKey: "id")!))")
        }else{
            isGroupChat = true
            self.CreateGroupButtonHeightConstraint.constant = 30
            self.createGroupButton.isHidden = false
            print("selectedSegmentIndex- \(segmentControl.selectedSegmentIndex)")
            recentChatsArray_OneToOne.removeAll()
            FireBaseManager.defaultManager.fetchMyGroups(id: "\(String(describing: UserDefaults.standard.object(forKey: "id")!))")
            
        }
        self.tableView.reloadData()
        print("recentChatsArray.count-\(recentChatsArray_OneToOne.count)")
    }
    
    // MARK: - Methods -
    
    func checkEmptyState(_ count: Int)
    {
        emptyView.isHidden = (messages.count != 0)
        tableView.isHidden = (messages.count == 0)
    }
    
    @objc func openContacts()
    {
        let newMessageContact = GKNewMessageContactViewController(nibName:
            "GKNewMessageContactViewController", bundle: nil)
        self.navigationController?.pushViewController(newMessageContact, animated:true)
    }
    
    func openConversation(_ userID: Int, fullName: String)
    {
        let messageViewController = GKMessagesViewController(nibName:
            GKMessagesViewController.xibName(), bundle: nil)
        
        let anotherUser = GKMessageAnotherUser(uid: userID, fullName: fullName)
        messageViewController.anotherUser = anotherUser
        //    messageViewController.data = self.messages[indexPath.row]
        self.navigationController?.pushViewController(messageViewController, animated:true)
    }
    
    fileprivate func reloadData()
    {
        //    if (Static.sharedInstance.messageList != nil)
        //    {
        //      self.messages = Static.sharedInstance.messageList!
        //    }
        
        //    self.loadingView.setState(self.messages == nil || self.messages.count == 0 ?
        //      .LoadingViewStateNoData : .LoadingViewStateHide)
        self.tableView.reloadData()
    }
    
    fileprivate func requestMessageList()
    {
        //    if (self.messages == nil || self.messages.count == 0)
        //    {
        //      self.loadingView.setState(.LoadingViewStateLoading)
        //    }
        
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page"] = 1 as AnyObject?
        
        GKServiceClient.call(GKRouterChat.listConversations(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource() as? NSDictionary
                    {
                        self.messages = data["data"] as! [GKDictionary]
                        self.checkEmptyState(self.messages.count)
                        self.reloadData()
                        
                        //            GKNotificationStore.number_messages      = data["message"] as! Int
                        //            GKNotificationStore.number_notifications = data["notification"] as! Int
                    }
                }
            }
        })
    }
    
    //  private func requestMessageListAfterDelete()
    //  {
    //    if (self.messages == nil || self.messages.count == 0)
    //    {
    //      self.loadingView.setState(.LoadingViewStateLoading)
    //    }
    //
    //    let dicUser: NSDictionary = Authentication.getUserDictionary()!
    //    let token = dicUser.objectForKey("token") as? String
    //
    //    var params = [String:AnyObject]()
    //    params["token"] = token
    //    params["page"] = 1
    //
    //    if (Static.sharedInstance.businessId() != nil)
    //    {
    //      params["business_id"] = Static.sharedInstance.businessId()
    //    }
    //    let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
    //
    //    weak var wself: MessagesMainViewController? = self
    //    MessageService().getMessageList(params, callback: {
    //      (response: AnyObject?) -> () in
    //
    //      hud.hide(true)
    //
    //      if (wself != nil)
    //      {
    //        if (response != nil)
    //        {
    //          let jsonDic : NSDictionary = response as! NSDictionary
    //
    //          if (jsonDic["success"]?.boolValue == true)
    //          {
    //            Static.sharedInstance.saveMessageList(jsonDic["object"]! as? NSArray)
    //            wself!.loadData()
    //          }
    //          else
    //          {
    //            if (wself!.messages == nil)
    //            {
    //              wself!.loadingView.setState(.LoadingViewStateError)
    //            }
    //          }
    //        }
    //        else
    //        {
    //          if (wself!.messages == nil)
    //          {
    //            wself!.loadingView.setState(.LoadingViewStateError)
    //          }
    //        }
    //      }
    //    })
    //  }
    
    func deleteConversationAtIndex(_ indexPath: IndexPath)
    {
        
        let dicMessage = self.messages[indexPath.row]
        
        var params          = GKDictionary()
        params["token"]     = Static.sharedInstance.token() as AnyObject?
        
        let another_user = dicMessage["another_user"] as! GKDictionary
        params["recipient"] = another_user["id"] as! Int as AnyObject?
        
        LoadingView.showLoadingView(
            "Deleting...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterChat.deleteConversation(), parameters:params, callback: {
            (response:AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    
                    self.requestMessageList()
                }
                else
                {
                    showCommonError({ (type: NoInternetResultType) -> () in
                        self.deleteConversationAtIndex(indexPath)
                    })
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                    self.deleteConversationAtIndex(indexPath)
                })
            }
        })
    }
    
}

extension GKInboxViewController: FirebaseRecentChatsManagerDelegate{
    func fireBaseManagerDidCompleteFetchingRecentChats() {
        recentChatsArray_OneToOne = FireBaseManager.defaultManager.recentChats
        tableView.reloadData()
        print("recentChatsArray.count-\(recentChatsArray_OneToOne.count)")
    }
    
    func fireBaseManagerDidCompleteFetchingRecentGroupChats() {
        recentChatsArray_Group = FireBaseManager.defaultManager.groupListArray
        tableView.reloadData()
    }
}

