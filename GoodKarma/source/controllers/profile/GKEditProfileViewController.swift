//
//  GKEditProfileViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/14/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class GKEditProfileViewController: GKViewController, GKManageSkillsDelegate, UITextFieldDelegate, UITextViewDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var userImageView: WebImageView!
    @IBOutlet weak var formView: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: Fields
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var collegeMajorField: UITextField!
    @IBOutlet weak var aboutContentField: UITextView!
    
    @IBOutlet weak var skillsContainer: UIView!
    @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var worldTextView: UITextView!
    // MARK: Placeholders
    @IBOutlet weak var placeholderWorld: UILabel!
    
    @IBOutlet weak var aboutPlaceholderLabel    : UILabel!
    
    @IBOutlet weak var contentView: UIView!
    // MARK: - Properties -
    
    var userData: GKDictionary!
    var userPhotoUrl: String? = nil
    var skillsSelected: [GKModelSkill] = []
    
    // Error
    fileprivate var errorView: ErrorView!
    var manageSkillView: GKManageSkillsView!
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Edit Profile"
        
        // Error View
        self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
        self.formView.addSubview(self.errorView)
        
        userImageView.layer.cornerRadius = userImageView.bounds.size.height * 0.5
        
        aboutContentField.layer.cornerRadius = 5.0
        aboutContentField.layer.borderColor = UIColor.hightLightGray().cgColor
        aboutContentField.layer.borderWidth = 0.5
        
        worldTextView.layer.cornerRadius = 5.0
        worldTextView.layer.borderColor = UIColor.hightLightGray().cgColor
        worldTextView.layer.borderWidth = 0.5
        
        manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: true, delegate: self)
        skillsContainer.addSubview(manageSkillView)
        
        //    requestProfile()
        requestGeneralProfile(true, updatePhoto: true) {
            // nothing
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        updateSkillsView()
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: self.saveButton.frame.maxY)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.skillsContainer.frame = CGRectSetHeight(self.skillsContainer.frame, h: manageSkillView.frame.height)
        self.contentView.frame = CGRectSetHeight(self.contentView.frame,
                                                 h: self.skillsContainer.frame.maxY)
        self.formView.frame = self.contentView.frame
        self.saveButton.frame = CGRectSetY(self.saveButton.frame, y: self.contentView.frame.maxY)
        
        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: self.saveButton.frame.maxY)
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        skillsSelected = skillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView()
    {
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill])
    {
        // Nothing for now
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        let baseY = self.errorView.frame.size.height;
        errorView.hideWithoutAnimation()
        
        if textField == firstNameField
        {
            if firstNameField.text?.countPlainCharacters() == 0
            {
                self.errorView.showInPos(
                    CGPoint(x: self.firstNameField.frame.minX - 8,
                        y: self.firstNameField.frame.minY - baseY),
                    text: "Please enter your first name.")
                
                return false
            }
            else
            {
                lastNameField.becomeFirstResponder()
            }
        }
        if textField == lastNameField
        {
            if lastNameField.text?.countPlainCharacters() == 0
            {
                self.errorView.showInPosFromCenter(
                    CGPoint(x: self.lastNameField.frame.minX - 48,
                        y: self.lastNameField.frame.minY - baseY),
                    text: "Please enter your last name.")
                
                return false
            }
            else
            {
                collegeMajorField.becomeFirstResponder()
            }
        }
        if textField == collegeMajorField
        {
            aboutContentField.becomeFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Protocol UITextViewDelegate -
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView == self.aboutContentField
        {
            self.aboutPlaceholderLabel.isHidden = true
        }
        
        if textView == self.worldTextView
        {
            self.placeholderWorld.isHidden = true
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.aboutContentField
        {
            self.aboutPlaceholderLabel.isHidden = textView.text.count > 0
        }
        
        if textView == self.worldTextView
        {
            self.placeholderWorld.isHidden = textView.text.count > 0
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == self.aboutContentField
        {
            self.aboutPlaceholderLabel.isHidden = textView.text.countPlainCharacters() > 0
        }
        
        if textView == self.worldTextView
        {
            self.placeholderWorld.isHidden = textView.text.countPlainCharacters() > 0
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n" && textView == aboutContentField
        {
            aboutContentField.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    // MARK: - IBActions -
    
    //  @IBAction func actionAddPhoto(sender: AnyObject) {
    //
    //  }
    //
    //  @IBAction func actionNext(sender: AnyObject) {
    //
    //  }
    
    @IBAction func actionEditPhoto(_ sender: AnyObject)
    {
        let editPhotoViewController = GKEditPhotoViewController(
            nibName: "GKEditPhotoViewController", bundle: nil)
        editPhotoViewController.photoUrl = self.userPhotoUrl
        self.navigationController?.pushViewController(editPhotoViewController, animated: true)
    }
    
    @IBAction func actionSave(_ sender: AnyObject)
    {
        self.view.endEditing(true)
        self.errorView.hideWithoutAnimation()
        
        if validate()
        {
            LoadingView.showLoadingView("Sending...",
                                        parentView: self.navigationController?.view,
                                        backColor: true)
            var params = GKDictionary()
             var value = ""
            if ((UserDefaults.standard.object(forKey: "latitude") as? String) != nil){
                value = (UserDefaults.standard.object(forKey: "latitude")! as! String)
            }
            params["latitude"] = value as AnyObject?
            var long = ""
            if ((UserDefaults.standard.object(forKey: "longitude") as? String) != nil){
                long = (UserDefaults.standard.object(forKey: "longitude")! as! String)
            }
            params["longitude"] = long as AnyObject
            params["phone_number"] = UserDefaults.standard.object(forKey: "phone_number") as AnyObject
            params["birthdate"] = " " as AnyObject 
            params["token"]       = Static.sharedInstance.token() as AnyObject?
            params["first_name"]  = firstNameField.text as AnyObject?
            params["last_name"]   = lastNameField.text as AnyObject?
            params["college"]     = collegeMajorField.text as AnyObject?
            params["skills"]      = "\((skillsSelected.map{ $0.uid! }))"  as AnyObject
            params["description"] = aboutContentField.text as AnyObject?
            params["save_world"] = worldTextView.text as AnyObject?
            params["isUpdate"] = 1 as AnyObject?
            params["_zip"] = UserDefaults.standard.object(forKey: "zip_code") as AnyObject
            
            print (params)
            GKServiceClient.call(GKRouterClient.registerEditProfile(), parameters: params, callback: {
                (response: AnyObject?) -> () in
                
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res: AnyObject = response {
                    
                    let envelope = ResponseSkeleton(res)
                    
                    if envelope.getSuccess ()
                    {
                        print(res)
                        self.requestGeneralProfile(true, updatePhoto: true) {
                        self.navigationController?.popViewController(animated: true)
                    }
                        
                    }
                    else
                    {
                        showCommonError({ (type: NoInternetResultType) -> () in
                            self.actionSave(0 as AnyObject)
                        })
                    }
                }
                else
                {
                    showConnectionError({ (type: NoInternetResultType) -> () in
                        self.actionSave(0 as AnyObject)
                    })
                }
            })
        }
    }
    
    // MARK: - Methods -
    
    func getParams() -> [String: AnyObject]
    {
        var params = GKDictionary()
        params["token"]       = Static.sharedInstance.token() as AnyObject?
        params["first_name"]  = firstNameField.text as AnyObject?
        params["last_name"]   = lastNameField.text as AnyObject?
        params["college"]     = collegeMajorField.text as AnyObject?
        params["skills"]      = "\(skillsSelected.map{ $0.uid! })" as AnyObject?
        params["description"] = aboutContentField.text as AnyObject?
        params["save_world"] = worldTextView.text as AnyObject?
        
        return params
    }
    
    func loadData()
    {
        firstNameField.text = (userData["first_name"] as? String)?.capitalized
        lastNameField.text  = (userData["last_name"] as? String)?.capitalized
        
        collegeMajorField.text = userData["college"] as? String
        
        //modified
        skillsSelected = GKModelSkill.readFromDictionaryGetProfile(userData["gkarm_skill_users"] as! [GKDictionary])
        aboutContentField.text = userData["description"] as? String
        worldTextView.text = userData["save_world"] as? String
        
        self.aboutPlaceholderLabel.isHidden = aboutContentField.text.count > 0
        self.placeholderWorld.isHidden = worldTextView.text.count > 0
        
        updateSkillsView()
    }
    
    func loadPhoto()
    {
        self.userImageView.loadImageWithUrl(userPhotoUrl as NSString?)
    }
    
    func scrollToPointY(_ pointY: CGFloat)
    {
        if scrollView.contentSize.height - pointY > scrollView.bounds.height
        {
            scrollView.setContentOffset(CGPoint(x: 0.0, y: pointY - 64.0), animated: true)
        }
    }
    
    func updateSkillsView()
    {
        manageSkillView.frame = skillsContainer.bounds
        manageSkillView.setupSkills(skillsSelected)
        
        self.skillsContainer.frame.size.height = manageSkillView.bounds.size.height + 100
        //self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
        self.view.setNeedsLayout()
    }
    
    func validate() -> Bool
    {
        let baseY = self.errorView.frame.size.height;
        
        if (self.firstNameField.text?.countPlainCharacters() < 1)
        {
            self.errorView.showInPos(
                CGPoint(x: self.firstNameField.frame.minX - 8,
                    y: self.firstNameField.frame.minY - baseY),
                text: "Please enter your first name.")
            
            self.scrollToPointY(self.firstNameField.frame.minY - baseY)
            return false
        }
        
        if (self.lastNameField.text?.countPlainCharacters() < 1)
        {
            self.errorView.showInPosFromCenter(
                CGPoint(x: self.lastNameField.frame.minX - 48,
                    y: self.lastNameField.frame.minY - baseY),
                text: "Please enter your last name.")
            
            self.scrollToPointY(self.lastNameField.frame.minY - baseY)
            return false
        }
        print(skillsSelected.count)
        if skillsSelected.count == 0 {
            self.errorView.showInPosFromCenter(
                CGPoint(x: self.skillsContainer.frame.minX,
                    y: self.skillsContainer.frame.minY - baseY),
                text: "Please select atleast one skill.")
            
            self.scrollToPointY(self.skillsContainer.frame.minY - baseY)
            return false
        }
        
        return true
    }
    
    //  func requestPhoto()
    //  {
    //    var params = GKDictionary()
    //    params["token"] = Static.sharedInstance.token()
    //
    //    LoadingView.showLoadingView(
    //      "Loading...",
    //      parentView: self.navigationController?.view,
    //      backColor: true
    //    )
    //
    //    GKServiceClient.call(GKRouterClient.profileEdit(), parameters: params, callback: {
    //      (response: AnyObject?) -> () in
    //
    //      LoadingView.hideLoadingView(self.navigationController?.view)
    //
    //      if let res:AnyObject = response
    //      {
    //        let envelope = ResponseSkeleton(res)
    //
    //        if envelope.getSuccess()
    //        {
    //          if let data = envelope.getResource() as? GKDictionary
    //          {
    //            self.userPhotoUrl = data["photo_url"] as? String
    //            self.loadPhoto()
    //          }
    //        }
    //      }
    //    })
    //  }
    
    func requestGeneralProfile(_ updateData: Bool, updatePhoto: Bool, completion: @escaping () -> ()) {
        
        LoadingView.showLoadingView("Sending...",
                                    parentView: self.navigationController?.view,
                                    backColor: true)
        
        var params = [String: String]()
        params["token"] = Static.sharedInstance.token()
        
        GKServiceClient.call(GKRouterClient.registerProfile(), parameters: self.getParams(), callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res , string : "profile")
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource() as? GKDictionary
                    {
                        let mutable = NSMutableDictionary(dictionary: data)
                        mutable["token"] = Static.sharedInstance.token()
                        
                        Static.sharedInstance.saveUserProfile(mutable)
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "ChangedProfileInSettings"),
                            object: nil)
                        
                        if updateData
                        {
                            self.userData = data
                            self.loadData()
                        }
                        if updatePhoto
                        {
                            self.userPhotoUrl = data["photo_url"] as? String
                            self.loadPhoto()
                        }
                    }
                }
                
                completion()
            }
        })
    }
    
    //  func requestProfile()
    //  {
    //    var params = GKDictionary()
    //    params["token"] = Static.sharedInstance.token()
    //    
    //    LoadingView.showLoadingView(
    //      "Loading...",
    //      parentView: self.navigationController?.view,
    //      backColor: true
    //    )
    //    
    //    GKServiceClient.call(GKRouterClient.profileEdit(), parameters: params, callback: {
    //      (response: AnyObject?) -> () in
    //      
    //      LoadingView.hideLoadingView(self.navigationController?.view)
    //      
    //      if let res:AnyObject = response
    //      {
    //        let envelope = ResponseSkeleton(res)
    //        
    //        if envelope.getSuccess()
    //        {
    //          if let data = envelope.getResource() as? GKDictionary
    //          {
    //            self.userData = data
    //            self.loadData()
    //          }
    //        }
    //      }
    //    })
    //  }
}
