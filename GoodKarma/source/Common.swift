//
//  Common.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit
import AVFoundation
import FBSDKLoginKit

// Screen Size
struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

// Device Type
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom
        == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom
        == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom
        == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom
        == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}

// Google APIKey

    let Google_APIKey = "AIzaSyDqMO0Jmz-IaoFuKbFjfo3NWGCXzzG0_p8"

// Vimeo Auth-token

    let Vimeo_backgroundSessionIdentifier = "com.vimeo.upload"
    let Vimeo_authToken = "68aec93477d82303115a0f72832ccec8"

// MARK - Alert

func secondsToFormatted(_ seconds: Int?, prettyFormat: Bool = true) -> String {
    
    guard var dateDiff = seconds else {
        
        return ""
    }
    let calendar = Calendar.current
    let currentTime = Int(NSDate().timeIntervalSince1970)
    if dateDiff != nil
    {
        dateDiff = currentTime - dateDiff
    }
    
    let numberOfMonths:Int = Int(dateDiff / (60 * 60 * 24 * 30))
    let numberOfDays:Int = Int(dateDiff / (60 * 60 * 24))
    let numberOfHours:Int = Int(dateDiff / (60 * 60))
    let numberOfMinutes:Int = Int(dateDiff / (60))
    let numberOfSeconds:Int = Int(dateDiff)
    
    if numberOfSeconds < 60
    {
        return prettyFormat ? "Just Now": "\(numberOfSeconds)s"
    }
    else
    {
        if numberOfMinutes >= 1 && numberOfMinutes < 60
        {
            return prettyFormat ? "\(numberOfMinutes) min ago": "\(numberOfMinutes)min"
        }
        else
        {
            if (numberOfHours >= 1 && numberOfHours < 24)
            {
                if (numberOfHours == 1)
                {
                    return prettyFormat ? "\(numberOfHours) hour ago": "\(numberOfHours)h"
                }
                else
                {
                    return prettyFormat ? "\(numberOfHours) hours ago": "\(numberOfHours)h"
                }
            }
            else {
                if (numberOfDays >= 1 && numberOfDays < 31)
                {
                    if (numberOfDays == 1)
                    {
                        return prettyFormat ? "\(numberOfDays) day ago": "\(numberOfDays)d"
                    }
                    else
                    {
                        return prettyFormat ? "\(numberOfDays) days ago": "\(numberOfDays)d"
                    }
                }
                else
                {
                    if (numberOfMonths >= 1 && numberOfMonths < 12)
                    {
                        if (numberOfMonths == 1)
                        {
                            return prettyFormat ? "\(numberOfMonths) month ago": "\(numberOfMonths)d"
                        }
                        else
                        {
                            return prettyFormat ? "\(numberOfMonths) months ago": "\(numberOfMonths)d"
                        }
                    }
                }
            }
        }
    }
    return "\(String(describing: Int(numberOfMonths/12)))"
}


func showDate(actualString : String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-mm-dd"
    let actualDate = dateFormatter.date(from: actualString)
    //dayformatter.date(from: myString)
    dateFormatter.dateFormat = "dd"
  
    let actualDay = Int(dateFormatter.string(from: actualDate!))
    if actualDay == nil
    {
        return ""
    }
    let currentDay = Int(dateFormatter.string(from: Date()))
   
    //month
    dateFormatter.dateFormat = "mm"
    let actualMonth = Int(dateFormatter.string(from: actualDate!))
    let currentMonth = Int(dateFormatter.string(from: Date()))
    
    //year
    dateFormatter.dateFormat = "yyyy"
    let actualYear = Int(dateFormatter.string(from: actualDate!))
    let currentYear = Int(dateFormatter.string(from: Date()))
    
    
    if currentYear! - actualYear! == 0
    {
        if currentMonth! - actualMonth! == 0
        {
        // same month
            if currentDay! - actualDay! == 0
            {
                //today
                return "today"
            }
            else if currentDay! - actualDay! == 1
            {
                //yesterday
                return "yesterday"
            }
        }
        else
        {
            let value : String!
            switch(actualMonth)
            {
            case 1?:
                //january
                value = "January"
                break
            case 2?:
                value = "February"
                //feb
                break
            case 3?:
                value = "March"
                //maech
                break
            case 4?:
                value = "April"
                //april
                break
            case 5?:
                value = "May"
                //may
                break
            case 6?:
                value = "June"
                //june
                break
            case 7?:
                value = "July"
                //july
                break
            case 8?:
                value = "Auguat"
                //august
                break
            case 9?:
                value = "September"
                //sep
                break
            case 10?:
                value = "October"
                //oct
                break
            case 11?:
                value = "November"
                //nov
                break
            default:
                value = "December"
                //dec
                break
                
            }
            return ("\(value!)" + " " + "\(String(describing: actualDay!))")
        }
    }
    
        dateFormatter.dateFormat = "yyyy-mm-dd"
       return dateFormatter.string(from: actualDate!)
    
}
func showAlertWithData(_ title: String, text: String, delegate: UIAlertViewDelegate?,
                       cancelButtonTitle: String?, otherButtonTitle: String?, data: AnyObject?)
{
    let alert = UIAlertView(title: title, message: text, delegate: delegate,
                            cancelButtonTitle: cancelButtonTitle)
    
    if (otherButtonTitle != nil)
    {
        alert.addButton(withTitle: otherButtonTitle!)
    }
    
    if (data != nil)
    {
        alert.layer.setValue(data, forKey:"data")
    }
    
    alert.show()
}

func showAlert(_ title: String, text: String)
{
    showAlertWithData(title, text: text, delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil, data: nil);
}

func showAlert(_ title: String, text: String, delegate: UIAlertViewDelegate?,
               cancelButtonTitle: String?, otherButtonTitle: String?)
{
    showAlertWithData(title, text: text, delegate: delegate, cancelButtonTitle: cancelButtonTitle, otherButtonTitle: otherButtonTitle, data: nil);
}

func showConnectionError()
{
    showAlert("Oops!", text: "An error has occurred. Please try again.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
}

func showConnectionError(_ callback: @escaping (NoInternetResultType) ->()) -> ()
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    let noInternetView =
        NoInternetView(frame: CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height))
    
    let v: UIView? = window.viewWithTag(kTagNoInternetView)
    
    if (v == nil)
    {
        window.addSubview(noInternetView)
        noInternetView.show({ (result: NoInternetResultType) -> () in
            callback(result)
        })
    }
}

func showCommonError(_ callback: @escaping (NoInternetResultType) ->()) -> ()
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    let noInternetView =
        NoInternetView(frame: CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height))
    noInternetView.titleLabel.text = "Oops!"
    noInternetView.textLabel.text = "An error has occurred. \nPlease try again"
    noInternetView.tryAgainButton.setTitle("Try Again", for: UIControlState())
    
    let v: UIView? = window.viewWithTag(kTagNoInternetView)
    
    if (v == nil)
    {
        window.addSubview(noInternetView)
        noInternetView.show({ (result: NoInternetResultType) -> () in
            callback(result)
        })
    }
}

func showCommonErrorWithoutSignOut(_ callback: @escaping (NoInternetResultType) ->()) -> ()
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    let noInternetView =
        NoInternetView(frame: CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height))
    noInternetView.titleLabel.text = "Oops!"
    noInternetView.textLabel.text = "An error has occurred. \nPlease try again"
    noInternetView.tryAgainButton.setTitle("Try Again", for: UIControlState())
    noInternetView.signOutView.isHidden = true
    
    let v: UIView? = window.viewWithTag(kTagNoInternetView)
    
    if (v == nil)
    {
        window.addSubview(noInternetView)
        noInternetView.show({ (result: NoInternetResultType) -> () in
            callback(result)
        })
    }
}

func showMessageError(_ title: String, text: String,
                      callback: @escaping (NoInternetResultType) ->()) -> ()
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    let noInternetView =
        NoInternetView(frame: CGRect(x: 0, y: 0, width: window.frame.width,
            height: window.frame.height), isMessage: true)
    noInternetView.titleLabel.text = title
    noInternetView.textLabel.text = text
    noInternetView.tryAgainButton.setTitle("Ok", for: UIControlState())
    
    let v: UIView? = window.viewWithTag(kTagNoInternetView)
    
    if (v == nil)
    {
        window.addSubview(noInternetView)
        noInternetView.show({ (result: NoInternetResultType) -> () in
            callback(result)
        })
    }
}

func showMessageError(_ title: String, text: String, okButtonText: String,
                      logoutButtonText: String, callback: @escaping (NoInternetResultType) ->()) -> ()
{
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    let noInternetView =
        NoInternetView(frame: CGRect(x: 0, y: 0, width: window.frame.width,
            height: window.frame.height), isMessage: false)
    noInternetView.titleLabel.text = title
    noInternetView.textLabel.text = text
    noInternetView.tryAgainButton.setTitle(okButtonText, for: UIControlState())
    noInternetView.setSignOutText(logoutButtonText)
    
    let v: UIView? = window.viewWithTag(kTagNoInternetView)
    
    if (v == nil)
    {
        window.addSubview(noInternetView)
        noInternetView.show({ (result: NoInternetResultType) -> () in
            callback(result)
        })
    }
}

func showMessageInvalidToken()
{
    showMessageError("Invalid Session", text: "Session has expired, \nplease log-in.") { (type: NoInternetResultType) -> () in
        logout(true)
    }
}

// MARK: - CGRect

func CGRectSetPos(_ r: CGRect, x: CGFloat, y: CGFloat) -> CGRect
{
    return CGRect(x: x, y: y, width: r.size.width, height: r.size.height)
}

func CGRectSetX(_ r: CGRect, x: CGFloat) -> CGRect
{
    return CGRect(x: x, y: r.origin.y, width: r.size.width, height: r.size.height)
}

func CGRectSetY(_ r: CGRect, y: CGFloat) -> CGRect
{
    return CGRect(x: r.origin.x, y: y, width: r.size.width, height: r.size.height)
}

func CGRectSetSize(_ r: CGRect, w: CGFloat, h: CGFloat) -> CGRect
{
    return CGRect(x: r.origin.x, y: r.origin.y, width: w, height: h)
}

func CGRectSetWidth(_ r: CGRect, w: CGFloat) -> CGRect
{
    return CGRect(x: r.origin.x, y: r.origin.y, width: w, height: r.size.height)
}

func CGRectSetHeight(_ r: CGRect, h: CGFloat) -> CGRect
{
    return CGRect(x: r.origin.x, y: r.origin.y, width: r.size.width, height: h)
}

// MARK: - Color

func RGBCOLOR(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor
{
    return UIColor(red:red/255.0, green:green/255.0, blue:blue/255.0, alpha: 1.0)
}

func RGBACOLOR(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor
{
    return UIColor(red:red/255.0, green:green/255.0, blue:blue/255.0, alpha: alpha)
}

// MARK: - Delay

func delay(_ delay:Double, closure:@escaping ()->())
{
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

// MARK: - Auth

func logout(_ force: Bool)
{
    logoutDone(force)
}

func logoutDone(_ force: Bool)
{
    UIApplication.shared.applicationIconBadgeNumber = 0
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let controller = appDelegate.window?.rootViewController
    
    if force {
        FBSDKLoginManager().logOut()
        Static.sharedInstance.saveLogin(false)
        UserDefaults.standard.removeObject(forKey: "isFacebookId")
        UserDefaults.standard.removeObject(forKey: "id")

        (UIApplication.shared.delegate as! AppDelegate)
            .showLogin(controller!)
    }
    else {
        var params = [String: String]()
        params["token"] = Static.sharedInstance.token()
        
        LoadingView.showLoadingView("Sending...", parentView: controller?.view, backColor: true)
        
        GKServiceClient.call(GKRouterAuth.logout(), parameters:params as GKDictionary, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(controller?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    FBSDKLoginManager().logOut()
                    Static.sharedInstance.saveLogin(false)
                    UserDefaults.standard.removeObject(forKey: "isFacebookId")
                    UserDefaults.standard.removeObject(forKey: "id")

                    appDelegate.showLogin(controller!)
                }
                else
                {
                    showCommonError({ (type) -> () in
                        logout(true)
                    })
                }
            }
            else
            {
                showConnectionError({ (type) -> () in
                    logout(true)
                })
            }
        })
    }
}

// MARK: - Help Difficluty -

func difficultyToString(_ difficulty: Int) -> String {
    
    switch difficulty {
    case 1: return "Very Easy"
    case 2: return "Easy"
    case 3: return "Average"
    case 4: return "Hard"
    case 5: return "Very Hard"
    default: return ""
    }
}

// MARK: -wiserText

//func wiserText(editorView : RichEditorView , controller : UIViewController)
//{
//    var toolbar: RichEditorToolbar = {
//        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
//        toolbar.options = RichEditorDefaultOption.all
//        return toolbar
//    }()
//    editorView.delegate = controller
//    editorView.inputAccessoryView = toolbar
//
//    toolbar.delegate = controller
//    toolbar.editor = editorView
//
//    // We will create a custom action that clears all the input text when it is pressed
//    let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
//        toolbar.editor?.html = ""
//    }
//
//    var options = toolbar.options
//    options.append(item)
//    toolbar.options = options
//}

// MARK: - Invite

func createBodyText(_ userInfo: NSDictionary) -> String
{
    let user = Static.sharedInstance.userProfile
    
    // FIXME: change URL to ios App
    let url = "https://itunes.apple.com/us/app/goodkarms/id1197557400"
    
    let bodyText = NSMutableString()
    bodyText.append(user?["first_name"] as! String)
    bodyText.append(" has invited you to download the GoodKarms app")
    bodyText.append(" from the AppStore. Please follow this link: ")
    bodyText.append(url)
    
    return bodyText as String
}

// MARK: - String

func shortName(_ fullName: String) -> String
{
    let array = (fullName as NSString).components(separatedBy: " ");
    
    if (array.count > 1)
    {
        return array[0].capitalized
            + " "
            + (array[1] as NSString).substring(to: 1).capitalized
            + "."
    }
    return fullName;
}

func firstName(_ fullName: String) -> String
{
    let array = (fullName as NSString).components(separatedBy: " ");
    
    if (array.count > 0)
    {
        return array[0].capitalized
    }
    
    return fullName;
}

func stringHeight(_ text: String, width: Int, font: UIFont) -> Int
{
    let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width,
        height: 9999))
    label.font = font
    label.numberOfLines = 0
    label.text = text
    label.sizeToFit()
    
    return Int(label.frame.height)
}

// MARK: - Views

func enabledView(_ view: UIView!, enabled: Bool)
{
    view.isUserInteractionEnabled = enabled
    view.alpha = enabled ? 1 : 0.3
}

// MARK: - Get Thumbail / Preview image from Server Video Url

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    
    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1,2) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }
    
    return nil
}

