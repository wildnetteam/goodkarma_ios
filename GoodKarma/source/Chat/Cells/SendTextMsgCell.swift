//
//  SendTextMsgCell.swift
//  SuperMom
//
//  Created by Anuj Jha on 19/09/17.
//  Copyright © 2017 Shatakshi. All rights reserved.
//

import UIKit
protocol sendingSpamDelegate {
    
    func markSpam(tag: Int)
}

class SendTextMsgCell: UITableViewCell {

    @IBOutlet weak var tickImageVIewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var senderMessageLabel: UILabel!
    @IBOutlet weak var tickMsgImageView: UIImageView!
    @IBOutlet weak var msgView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    var delegate: sendingSpamDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        msgView.layer.cornerRadius = 10
        msgView.layer.masksToBounds = true
        senderMessageLabel.sizeToFit()
        msgView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)//UIColor(red:0.96, green:0.71, blue:0, alpha:1)
        cornerView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)//UIColor(red:0.96, green:0.71, blue:0, alpha:1)
    }
    @IBAction func spamButtonTapped(_ sender: UIButton) {
        if delegate != nil{
            delegate?.markSpam(tag: sender.tag)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
