//
//  GKExplorePositivityTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/22/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKExplorePositivityTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var pictureImageView : WebImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var originalRequestLabel: UILabel!
    @IBOutlet weak var likeButton : UIButton!
    @IBOutlet weak var likeCountLabel : UILabel!
    
    @IBOutlet weak var collectionViewPost: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!

    // MARK: - Properties -
    
    var data: GKDictionary!
    var delegate: (GKShowUserProfileDelegate & GKLikePostDelegate & GKShareDelegate)?
    var popTip: AMPopTip!
    
    static var isImages = Bool()

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
        self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
        self.pictureImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
        self.pictureImageView.isUserInteractionEnabled = true
        
        // Check Button
        //        self.checkButton.hidden = true
        
        self.selectionStyle = .none
        
        // PopTip
        self.popTip = AMPopTip()
        self.popTip.popoverColor = UIColor.black
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionViewPost.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
    }
    
    // MARK: - IBActions -
    
    @IBAction func buttonShareClicked(_ sender: Any) {
        if delegate != nil
        {
            delegate?.GKSharePost(share_text: self.titleLabel.attributedText!)
        }
    }
    
    @IBAction func sendLike(_ sender: AnyObject) {
        
        let postID = self.data["id"] as! Int
        
        self.delegate?.GKSendLike(5, postID: postID)
    }
    
    @IBAction func tooltipAction(_ sender: AnyObject)
    {
        popTip.showText("Positivity", direction: .left, maxWidth: 200, in: self,
                        fromFrame: (sender as! UIView).frame, duration: 2)
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKExplorePositivityTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat {
        
        if isImages {
            return 320.0
        }
        return 320.0
    }
    
    // MARK: - UICollectionView Methods
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewPost.delegate = dataSourceDelegate
        collectionViewPost.dataSource = dataSourceDelegate
        collectionViewPost.tag = row
        collectionViewPost.reloadData()
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        self.popTip.hideForced(true)
        
        let author            = data["author"]    as! GKDictionary
        let requestPositivity = data["request_positivity"] as! NSDictionary
        let recipient         = requestPositivity["author"] as! GKDictionary
        
        self.originalRequestLabel.text = shortName(requestPositivity.value(forKeyPath: "author.full_name") as! String) + ": " + (requestPositivity["desc"] as! String).decodeEmoji()
        
        let authorNameAttrString = NSAttributedString(
            string: (author["first_name"] as! String).capitalized +  " " + (author["last_name"] as! String).capitalized,
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.black
            ]
        )
        let sendToAttrString = NSAttributedString(
            string: " wrote ",
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Light, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.darkGray
            ]
        )
        let recipientNameAttrString = NSAttributedString(
            string: "\((recipient["first_name"] as! String).capitalized +  " " + (recipient["last_name"] as! String)): ".capitalized,
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.black
            ]
        )
        let positivityAttrString = NSAttributedString(
            string: (data["desc"] as! String).decodeEmoji(),
            attributes: [
                NSAttributedStringKey.font: UIFont.fontRoboto(.Light, size: 14),
                NSAttributedStringKey.foregroundColor: UIColor.darkGray
            ]
        )
        
        let attrString = NSMutableAttributedString()
        attrString.append(authorNameAttrString)
        attrString.append(sendToAttrString)
        attrString.append(recipientNameAttrString)
        attrString.append(positivityAttrString)
        
        self.pictureImageView.loadImageWithUrl(author["photo_url"] as? String as NSString?)
        self.titleLabel.attributedText = attrString
        self.timestampLabel.text = secondsToFormatted(data["date_created"] as? Int)
        
        // Like
        self.updateLike(data["like"] as! Bool, likeCount: data["like_count"] as! Int)
    }
    
    @objc func tapImageView()
    {
        let authorData = self.data["author"] as! GKDictionary
        self.delegate?.GKShowProfile(authorData["id"] as! Int)
    }
    
    func updateLike(_ isLike: Bool, likeCount: Int)
    {
        likeCountLabel.text = "\(likeCount)"
        
        if isLike
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-on"), for: UIControlState())
            likeCountLabel.textColor = UIColor.white
        }
        else
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-off"), for: UIControlState())
            likeCountLabel.textColor = UIColor.red
        }
    }
}
