//
//  CallingCode.h
//  GoodKarma
//
//  Created by Paul Aguilar on 6/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CallingCode : NSObject
{
    NSDictionary *codes;
}

- (NSString *)getCountryCallingCode:(NSString *)countryCode;
- (NSString *)getCountryCallingCodeAtIndex:(NSInteger)index;
- (NSString *)coutryCodeAtIndex:(NSInteger)index;
- (NSArray *)allCodes;
- (NSDictionary *)dataAtCode:(NSString *)countryCode;

@end
