//
//  RecievePhotoMsgCell.swift
//  SuperMom
//
//  Created by Anuj Jha on 22/09/17.
//  Copyright © 2017 Shatakshi. All rights reserved.
//

import UIKit

protocol zoomRecievedImageDelegate {
    func zoomImage(image:UIImage!)
}

class RecievePhotoMsgCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var innerBGView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var zoomImageButton: UIButton!
    @IBOutlet weak var recievePhotoMsgImageView: UIImageView!
    @IBOutlet weak var recieveMsgBackgroundView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    var delegate: zoomImageDelegate?
    //var deleteImageDelegate: deleteMsgDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
//        userImageView.layer.masksToBounds = true
        innerBGView.layer.cornerRadius = 15
        //recieveMsgBackgroundView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        cornerView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
        innerBGView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
        
        //recieveMsgBackgroundView.backgroundColor = UIColor.gray
    }
    
    @IBAction func zoomImageButtonTapped(_ sender: Any) {
        if delegate != nil{
            delegate?.zoomImage(image: recievePhotoMsgImageView.image)
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func deleteMessageActionTapped(sender:Any){
//        if deleteImageDelegate != nil{
//            deleteImageDelegate?.deleteMsg()
//        }
        
    }
}

