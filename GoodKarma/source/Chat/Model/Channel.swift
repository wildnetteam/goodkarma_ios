//
//  Channel.swift
//  GoodKarma
//
//  Created by Anuj Jha on 03/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

class ClassContacts {
    
    var childFirstName: String?
    var childLastName: String?
    var momFirstName: String?
    var momLastName: String?
    var profileImg: String?
    var moderator: Bool?
    var mobileNo: NSInteger?
    
    init(childFirstName: String?, childLastName: String?, momFirstName: String?, momLastName: String?, profileImg: String?, moderator: Bool?, mobileNo: NSInteger?) {
        
        self.childFirstName = childFirstName
        self.childLastName = childLastName
        self.momFirstName = momFirstName
        self.momLastName = momLastName
        self.profileImg = profileImg
        self.moderator = moderator
        self.mobileNo = mobileNo
    }
}

class BusContacts {
    
    var childFirstName: String?
    var childLastName: String?
    var momFirstName: String?
    var momLastName: String?
    var profileImg: String?
    var moderator: Bool?
    var mobileNo: NSInteger?
    
    init(childFirstName: String?, childLastName: String?, momFirstName: String?, momLastName: String?, profileImg: String?, moderator: Bool?, mobileNo: NSInteger?) {
        
        self.childFirstName = childFirstName
        self.childLastName = childLastName
        self.momFirstName = momFirstName
        self.momLastName = momLastName
        self.profileImg = profileImg
        self.moderator = moderator
        self.mobileNo = mobileNo
    }
}

class GroupId{
    var groupId: String?
    init(groupId: String) {
        self.groupId = groupId
    }
}

class Message {
    
    var text: String?
    var senderId: String?
    var createdAt: String?
    var type: String?
    //var senderImg: String?
    var senderName: String?
    var objectId: String?
    var picture: String?
    var audioURL: String?
    var audio_Duration: Int?
    //var spamCount: NSInteger?
    var status: String?
    //var spamMarkedBy: NSMutableArray?
    
    init(text: String?, senderId: String?, createdAt: String?, type: String?, senderName: String?, objectId: String?, picture: String?, status: String?, audioURL: String?, audio_Duration: Int?) {
        
        self.text = text
        self.senderId = senderId
        self.createdAt = createdAt
        self.type = type
        //self.senderImg = senderImg
        self.senderName = senderName
        self.objectId = objectId
        self.picture = picture
        //self.deletedByModerator = deletedByModerator
        //self.markedSpam = markedSpam
        //self.spamCount = spamCount
        self.status = status
        self.audioURL = audioURL
        self.audio_Duration = audio_Duration
        //self.spamMarkedBy = spamMarkedBy
    }
    
}

class RecentChats{
    
    //var myId: String?
    var msgType: String?
    var lastMsg: String?
    var time: String?
    var senderId: String?
    var senderImg: String?
    var senderName: String?
    var recieverId: String?
    var recieverImg: String?
    var recieverName: String?
    var node: String?
    var unreadCountDict: NSMutableDictionary?
    
    init( msgType: String?, lastMsg: String?, time: String?, senderId: String?, senderImg: String?, senderName: String?, recieverId: String?, recieverImg: String?, recieverName: String?, node: String?, unreadCountDict: NSMutableDictionary?) {
        
        //self.myId = myId
        self.msgType = msgType
        self.lastMsg = lastMsg
        self.time = time
        self.senderId = senderId
        self.senderImg = senderImg
        self.senderName = senderName
        self.recieverId = recieverId
        self.recieverImg = recieverImg
        self.recieverName = recieverName
        self.node = node
        self.unreadCountDict = unreadCountDict
        
    }
}

class GroupList{
    var groupName: String?
    var groupId: String?
    var groupImageURL: String?
    var lastMsg: String?
    var time: String?
    var participants: NSDictionary?
    
    init(groupName: String, groupId: String, groupImageURL: String, lastMsg: String, time: String, participants: NSDictionary) {
        self.groupName = groupName
        self.groupId = groupId
        self.groupImageURL = groupImageURL
        self.lastMsg = lastMsg
        self.time = time
        self.participants = participants
    }
}



