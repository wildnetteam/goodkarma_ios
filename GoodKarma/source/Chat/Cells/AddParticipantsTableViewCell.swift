//
//  AddParticipantsTableViewCell.swift
//  GoodKarma
//
//  Created by Anuj Jha on 01/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class AddParticipantsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
