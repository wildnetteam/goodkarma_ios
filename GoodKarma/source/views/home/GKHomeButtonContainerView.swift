//
//  GKHomeButtonContainerView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/9/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

enum GKHomeState: String {
  case Positivity
  case Gratitude
  case HelpRequest
  case Serve
    case SOS
}

class GKHomeButtonContainerView: UIView {
  
  // MARK: - IBOutlets -
  
  @IBOutlet var view: UIView!
  
  @IBOutlet var positiveButton  : UIButton!
  @IBOutlet var gratitudeButton : UIButton!
  @IBOutlet var requestButton   : UIButton!
  @IBOutlet var serveButton     : UIButton!

    @IBOutlet weak var sosbutton: UIButton!
    
    
    @IBOutlet weak var sosImageView: UIImageView!
    @IBOutlet var positiveImageView  : UIImageView!
  @IBOutlet var gratitudeImageView : UIImageView!
  @IBOutlet var requestImageView   : UIImageView!
  @IBOutlet var serveImageView     : UIImageView!
  
  // MARK: - Properties -
  
//  var buttons: [UIButton] = []
  var delegate: GKHomeButtonContainerDelegate?
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionSectionSelected(_ sender: UIButton) {
    
    var state: GKHomeState
    
    switch sender.tag {
    case 1:  state = .Positivity
    case 2:  state = .Gratitude
    case 3:  state = .HelpRequest
    case 4:  state = .Serve
    case 5 : state = .SOS
    default: state = .Positivity
    }
    
    updateButonAndImages(state)
    self.delegate?.GKDidSelectButtonSection(state)
  }
  
  // MARK: - Setup -
  
  fileprivate func setup() {
    
    view = Bundle.main.loadNibNamed(
      "GKHomeButtonContainerView", owner: self, options: nil)?[0] as! UIView
    
    view.frame = self.bounds
    view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    self.addSubview(view)
    
    updateButonAndImages(nil)
  }
  
  func cleanSelections() {
    
    positiveButton.setTitleColor(UIColor.gray, for: UIControlState())
    gratitudeButton.setTitleColor(UIColor.gray, for: UIControlState())
    requestButton.setTitleColor(UIColor.gray, for: UIControlState())
    serveButton.setTitleColor(UIColor.gray, for: UIControlState())
    sosbutton.setTitleColor(UIColor.gray, for: UIControlState())
    
    sosImageView.image = UIImage(named : "black_Sos")
    positiveImageView.image  = UIImage(named: "home-positive-off")
    gratitudeImageView.image = UIImage(named: "home-gratitude-off")
    requestImageView.image   = UIImage(named: "home-request-off")
    serveImageView.image     = UIImage(named: "home-serve-off")
    
  }
  
  func updateButonAndImages(_ state: GKHomeState? = nil) {
    
    cleanSelections()
    
    guard let homeState = state else {
      
      return
    }
    
    switch homeState {
      case .Positivity  :
//        positiveButton.setTitleColor(UIColor.baseColor(), forState: .Normal)
        positiveImageView.image = UIImage(named: "home-positive-on")
      
      case .Gratitude   :
//        gratitudeButton.setTitleColor(UIColor.baseColor(), forState: .Normal)
        gratitudeImageView.image = UIImage(named: "home-gratitude-on")
      
      case .HelpRequest :
//        requestButton.setTitleColor(UIColor.baseColor(), forState: .Normal)
        requestImageView.image = UIImage(named: "home-request-on")
      
      case .Serve       :
//        serveButton.setTitleColor(UIColor.baseColor(), forState: .Normal)
        serveImageView.image = UIImage(named: "home-serve-on")
    
      case .SOS :
        sosImageView.image = UIImage(named :"blue_Sos")
      //  serveImageView.image = UIImage(named: "home-serve-on")
    }
  }
}
