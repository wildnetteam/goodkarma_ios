//
//  GKFollowingTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/21/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKFollowingTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var pictureImageView : WebImageView!
  @IBOutlet weak var nameLabel        : UILabel!
  @IBOutlet weak var followButton     : UIButton!
  
  // MARK: - Properties -
  
  var dataUser: GKDictionary!
  var delegate: GKFollowDelegate?
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
    self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
    self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    
    self.selectionStyle = .none
    // Follow button
    self.followButton.layer.cornerRadius = 5.0
//    self.checkImage.contentMode = .ScaleAspectFit
  }
  
  func loadData(_ dataUser: GKDictionary)
  {
    self.dataUser = dataUser
    
    self.nameLabel.text = "\(dataUser["first_name"] as! String)".capitalized + "\(dataUser["last_name"] as! String)".capitalized
    self.pictureImageView.loadImageWithUrl(dataUser["photo_url"] as? String as NSString?)
  
    }
  
  // MARK: - IBActions -
  
  @IBAction func followAction(_ sender: AnyObject)
  {
    
//    let mutual_follow = data["mutual_follow"] as! Bool
    
//    if mutual_follow {
    
    self.delegate?.GKUnfollow(self.dataUser["id"] as! Int, name: (self.dataUser["first_name"] as! String) + (self.dataUser["last_name"] as! String))
//    }
//    else {
//      
//      self.delegate?.GKFollow(follower["id"] as! Int)
//    }
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKFollowingTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 85.0
  }
  
}
