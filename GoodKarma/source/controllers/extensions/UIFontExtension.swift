//
//  UIFontExtension.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

public extension UIFont {
  
  enum `Type`: String {
    
    case Thin   = "Roboto-Thin",
    Light       = "Roboto-Light",
    Regular     = "Roboto-Regular",
    Medium      = "Roboto-Medium",
    Bold        = "Roboto-Bold"
    
  }
  
  // MARK: - Methods -
  
  // MARK: - Class Methods -
  
  /**
   This uses 'Roboto' family font with 5 types :
   - Thin
   - Light
   - Regular
   - Medium
   - Bold
   
   - parameter type: The type of font: UIFont.Type
   - parameter size: The size
   */
  class func fontRoboto(_ type: Type, size: CGFloat) -> UIFont {
    
    let font = UIFont(name: type.rawValue, size: size)
    return font!
    
  }
  
}

