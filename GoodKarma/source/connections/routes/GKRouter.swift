//
//  Router.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

typealias Router = GKRouter

enum GKRouterEnvironment: Int
{
  case development
  case staging
  case production
}

protocol ResourceRouter
{
  static func endpoint(_ path: String) -> String
}

extension ResourceRouter
{
  static func endpoint(_ path: String) -> String // , needs_token: Bool = true
  {
    let base_endpoint = "\(GKRouter.host_api)/\(path)"
    
//    guard needs_token else {
//      
//      return base_endpoint
//    }
//    
//    guard let token = GKModelUser.access_token else {
//      
//      return base_endpoint
//    }
    
//    return base_endpoint + "?access_token=\(token)"
    return base_endpoint
  }
}

struct GKRouter
{
  // MARK: - Properties -
  
  // First controller `Login` checks if there is an endpoint
  
  fileprivate static var host_api: String
  {
    switch GKRouter.environment()
    {
    //      // FIXME: Not SSL for now
    case .development:
        //      return "http://0.0.0.0:8000/api"
        // Paul
        //      return  "http://10.130.30.72:8000/api"
        // Andres
        return "http://10.130.30.67:8000/api"
    case .staging:
        // EC2
       //  return "http://52.9.25.186/api"   //live_old_url
        // return "http://115.112.128.141:8001/api"  // new url
//        return "http://180.151.103.85:3013/api"  // new local url
//        return "http://180.151.103.85:3015/api"  // phase2 local url
        return "http://54.215.206.30:3013/api"  // phase2 AWS url//
//        return "http://34.201.73.247:3015/api/"  // phase2 local wildnet url
        
    case .production:
        // EC2
        return "http://"
    }
  }
  

  // MARK: - Methods -
  
  static func environment() -> GKRouterEnvironment
  {
    // FIXME: Should change to staging enviroment
    return GKRouterEnvironment.staging
  }
}
