//
//  NSData+AES256.h
//  Hawkr
//
//  Created by Eliezer de Armas on 15/07/15.
//  Copyright (c) 2015 TeclaLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
