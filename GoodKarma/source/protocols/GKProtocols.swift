//
//  GKProtocol.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/29/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

protocol GKHistoryParticipantDelegate
{
    func requestOfferedListData() -> GKDictionary
}
protocol GKSendThankYouDelegate
{
    func sendThankYou()
}
protocol deleteSosData
{
    func delete(index : Int)
}
protocol GKSosContactDelegate
{
    func passDeleteData(index : Int)
    func passSendLocationData(index : Int)
    
}
protocol GKShowSosContact {
    func showSosData(info : [GKDictionary])
}
protocol GKSosAddContactDelegate
{
    func passDataToBeAdded(info : GKDictionary)
}

//modified
// MARK: - Cancel Job -
protocol updateStatusDelegate
{
    func updateStatus(reason : String)
}

// MARK: - Profile -
protocol GKShowUserProfileDelegate {
    
    func GKShowProfile(_ userID: Int)
}

// MARK: - Block -
protocol GKUnblockDelegate {
    
    func GKUnblock(_ userID: Int)
}

// MARK: - Message -
protocol GKStartMessageDelegate {
    
    func GKSendMessage(_ userID: Int, fullName: String)
}

// MARK: - Like -
protocol GKLikePostDelegate {
    
    func GKSendLike(_ typePost: Int, postID: Int)
}

// MARK: - Hug -
//protocol GKHugDelegate {
//
//    func GKHugPass()
//}

// MARK: - Help Request -
protocol GKHelpRequestParticipateDelegate {
    
    func GKParticipateInHelpRequest(_ id: GKDictionary)
}

// MARK: - Positivity -
protocol GKRespondPositiveRequestDelegate {
    
    func GKRespondPositiveRequest(_ requestPositivityID: Int)
}

// MARK: - Hug -
protocol GKHugDelegate {
    
    func GKPassHug(full_name : String , recipientId : Int , authorId :  Int , positivityId : Int , post_type : Int)
}

// MARK: - Share -
protocol GKShareDelegate {
    
    func GKSharePost(share_text : NSAttributedString)
}

// MARK: - Serve -
protocol GKServeParticipateDelegate {
    
    func GKParticipateInServe(_ serveData: GKDictionary)
}

// MARK: - Follow -
protocol GKFollowDelegate {
    
    func GKFollow(_ userID: Int)
    func GKUnfollow(_ userID: Int, name: String)
}

// MARK: - Friend -
protocol GKFriendSocialDelegate {
    
    func GKFriendSocialSkip()
}

// MARK: - History -

protocol GKHistoryPostStateDelegate {
    
    func GKHandlePostState(_ postData: GKDictionary)
    func GKHandleParticipationState(_ postData: GKDictionary)
}

protocol GKTestimonialDelegate {
    
    func GKLeaveTestimonial(_ clientData: GKDictionary)
}

// MARK: - Home -
protocol GKHomeButtonContainerDelegate {
    
    func GKDidSelectButtonSection(_ section: GKHomeState)
}

protocol GKHomeFilterDelegate {
    
    func GKFilterUpdateParams(_ filterParams: GKDictionary)
}

// MARK: - Main Feed -
@objc protocol GKMainFeedDelegate
{
    
    func GKRequestData(_ completion: @escaping ([GKDictionary]) -> ())
    //new
    @objc optional func passRequest(request : String)
    @objc optional func GKSendMessage(_ userID: Int, fullName: String)
}

// MARK: - Skills -
protocol GKManageSkillsDelegate {
    
    func GKShowAddSkillsView()
    func GKRemoveSkill(_ skill: GKModelSkill)
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill])
}

