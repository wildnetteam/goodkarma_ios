////
////  GKStore.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/20/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import UIKit
//
//
//class GKStore
//{
//  var friendsList: NSArray? = nil
//  var businessShareList: NSArray? = nil
//  var usersProfile: NSMutableDictionary? = nil
//  var messageList: NSArray? = nil
//  var messages: NSMutableDictionary? = nil
//  var relationship: NSDictionary? = nil
//  var menuCounter: NSMutableDictionary? = nil
//  
//  class var sharedInstance: GKStore
//  {
//    struct GKStoreStruct
//    {
//      static let instance: GKStore = GKStore()
//    }
//    
//    return GKStoreStruct.instance
//  }
//  
//  func loadInitial()
//  {
//    Cache.createCacheDirectory()
//    
//    // Friends List
//    self.friendsList = Cache.loadArrayWithName("friendsList")
//    
//    // Business Share List
//    self.businessShareList = Cache.loadArrayWithName("businessShareList")
//    
//    // Users Profile
//    var usersProfile = Cache.loadDictionaryWithName("usersProfile")
//    
//    if (usersProfile == nil)
//    {
//      usersProfile = NSMutableDictionary()
//    }
//    
//    self.usersProfile = usersProfile!.mutableCopy() as? NSMutableDictionary
//    
//    // Message List
//    self.messageList = Cache.loadArrayWithName("messageList")
//    
//    if (self.messageList == nil)
//    {
//      self.messageList = NSArray()
//    }
//    
//    // Messages
//    var messages = Cache.loadDictionaryWithName("messages")
//    
//    if (messages == nil)
//    {
//      messages = NSMutableDictionary()
//    }
//    
//    self.messages = messages!.mutableCopy() as? NSMutableDictionary
//    
//    // Relationship
//    self.relationship = createRelationship()
//    
//    // Menu Counter
//    var menuCounter = Cache.loadDictionaryWithName("MenuCounter")
//    
//    if (menuCounter == nil)
//    {
//      menuCounter = createMenuCounter()
//    }
//    
//    self.menuCounter = menuCounter?.mutableCopy() as? NSMutableDictionary
//  }
//  
//  func clearCache()
//  {
//    self.friendsList = NSArray()
//    saveFriendsList(self.friendsList);
//    
//    self.businessShareList = NSArray()
//    saveFriendsList(self.businessShareList);
//    
//    self.usersProfile = NSMutableDictionary()
//    saveUsersProfile()
//    
//    self.messageList = NSArray()
//    saveMessageList(self.messageList);
//    
//    self.messages = NSMutableDictionary()
//    saveMessages()
//    
//    self.menuCounter = createMenuCounter().mutableCopy() as? NSMutableDictionary
//    saveMenuCounter()
//  }
//  
//  // MARK: - Friends List
//  
//  func saveFriendsList(friendsList: NSArray?)
//  {
//    self.friendsList = friendsList
//    Cache.saveWithName("friendsList", collection:self.friendsList)
//  }
//  
//  // MARK: - Friends List
//  
//  func saveBusinessShareList(businessShareList: NSArray?)
//  {
//    self.businessShareList = businessShareList
//    Cache.saveWithName("businessShareList", collection:self.friendsList)
//  }
//  
//  // MARK: - Users Profile
//  
//  func saveUsersProfile()
//  {
//    Cache.saveWithName("usersProfile", collection:self.usersProfile)
//  }
//  
//  func addUserProfile(userProfile: NSDictionary?, facebookId: String)
//  {
//    self.usersProfile?[facebookId] = userProfile
//    saveUsersProfile()
//  }
//  
//  func userProfile(facebookId: String) -> NSDictionary?
//  {
//    let profile: AnyObject? = self.usersProfile![facebookId]
//    
//    if (profile != nil)
//    {
//      return profile as? NSDictionary
//    }
//    
//    return nil
//  }
//  
//  func businessId() -> String!
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    if userDefaults.valueForKey("VETTING_AS") != nil
//    {
//      let vettingAsPreference: String = userDefaults.valueForKey("VETTING_AS") as! String
//      
//      if vettingAsPreference == "BUSINESS"
//      {
//        let businessAuthorId: String = userDefaults.valueForKey("BUSINESS_AUTHOR_ID") as! String
//        
//        return businessAuthorId
//        
//      }
//    }
//    
//    return nil
//  }
//  
//  func businessFBId() -> String!
//  {
//    if (isBusiness())
//    {
//      let userDefaults = NSUserDefaults.standardUserDefaults()
//      return userDefaults.valueForKey("BUSINESS_FB_ID") as! String
//    }
//    
//    return nil
//  }
//  
//  func isBusiness() -> Bool
//  {
//    return businessId() != nil
//  }
//  
//  func businessData() -> NSDictionary!
//  {
//    if (isBusiness())
//    {
//      let userDefaults = NSUserDefaults.standardUserDefaults()
//      
//      if let businessData = userDefaults.valueForKey("BUSINESS_DATA") as? NSDictionary
//      {
//        return businessData
//      }
//    }
//    
//    return nil
//  }
//  
//  func profileName(profileData: NSDictionary) -> String
//  {
//    if (isBusiness(profileData))
//    {
//      return profileData["name"] as! String
//    }
//    
//    return profileData["first_name"] as! String
//  }
//  
//  func profileNameShort(profileData: NSDictionary) -> String
//  {
//    if (isBusiness(profileData))
//    {
//      return profileData["name"] as! String
//    }
//    
//    let firstName = profileName(profileData)
//    let lastName = profileData["last_name"] as! String
//    let firstLetter = lastName.substringToIndex(lastName.startIndex.successor())
//      .uppercaseString
//    
//    return NSString(format: "%@ %@.", firstName, firstLetter) as String
//  }
//  
//  func shortName(fullName: String) -> String
//  {
//    var name: NSString = fullName as NSString
//    let components = name.componentsSeparatedByString(" ")
//    
//    if (components.count > 1)
//    {
//      let lastName = components[1] as NSString
//      name = NSString(format: "%@ %@.", components[0],
//                      lastName.substringToIndex(1).uppercaseString)
//    }
//    
//    return name as String
//  }
//  
//  func isBusiness(profileData: NSDictionary) -> Bool
//  {
//    if ((profileData["model"] as! NSNumber).boolValue)
//    {
//      return true
//    }
//    
//    return false
//  }
//  
////  func isMe(profileData: NSDictionary) -> Bool
////  {
////    var userId: String!
////    
////    if (isBusiness())
////    {
////      userId = businessId()
////    }
////    else
////    {
////      let user: NSDictionary = Authentication.getUserDictionary()!
////      userId = user["uid"] as! String
////    }
////    
////    if let profileId = profileData["id"] as? NSNumber
////    {
////      return profileId.integerValue == Int(userId)!
////    }
////    
////    return false
////  }
//  
//  // MARK: - Message List
//  
//  func saveMessageList(messageList: NSArray?)
//  {
//    self.messageList = messageList
//    Cache.saveWithName("messageList", collection:self.messageList)
//  }
//  
//  // MARK: - Messages
//  
//  func saveMessages()
//  {
//    Cache.saveWithName("messages", collection:self.messages)
//  }
//  
//  func addMessages(messages: NSArray?, conversationId: String)
//  {
//    self.messages?[conversationId] = messages
//    saveMessages()
//  }
//  
//  func getMessages(conversationId: String) -> NSArray?
//  {
//    let messages: AnyObject? = self.messages![conversationId]
//    
//    if (messages != nil)
//    {
//      return messages as? NSArray
//    }
//    
//    return nil
//  }
//  
//  func searchConversationInCache(facebookId: String) -> NSDictionary!
//  {
//    let messages = self.messageList!
//    
//    for message in messages
//    {
//      let anotherUser = message["another_user"] as! NSDictionary
//      let anotherFBId = anotherUser["facebook_id"] as! String
//      
//      if (anotherFBId == facebookId)
//      {
//        return message as! NSDictionary
//      }
//    }
//    
//    return nil
//  }
//  
//  // MARK: - Relationship
//  
//  func createRelationship() -> NSDictionary
//  {
//    let data = NSMutableDictionary()
//    data[0] = "Unknown"
//    data[1] = "Single"
//    data[2] = "In a Relationship"
//    data[3] = "Engaged"
//    data[4] = "Married"
//    data[5] = "It's is complicated"
//    data[6] = "In an Open Relationship"
//    data[7] = "Widowed"
//    data[8] = "Separated"
//    data[9] = "Divorced"
//    data[10] = "In a Civil Union"
//    data[11] = "In a Domestic Partership"
//    
//    return data
//  }
//  
//  func relationshipValues() -> NSArray!
//  {
//    return self.relationship?.allValues
//  }
//  
//  func relationshipIndex(v: String) -> Int
//  {
//    return self.relationship?.allKeysForObject(v)[0] as! Int
//  }
//  
//  // MARK: - Menu Counter
//  
////  func requestMenuCount(callback: () -> ())
////  {
////    if (Authentication.isUserLoggedIn())
////    {
////      let dicUser: NSDictionary = Authentication.getUserDictionary()!
////      let dicFacebook: NSDictionary = Authentication.getDatafacebook()!
////      let token = dicUser.objectForKey("token") as? String
////      
////      var params = [String:AnyObject]()
////      params["token"] = token
////      params["facebook_friends_id_list"] = dicFacebook.valueForKeyPath("friends.id")
////      
////      if (GKStore.sharedInstance.isBusiness())
////      {
////        params["business_id"] = GKStore.sharedInstance.businessId()
////      }
////      
//////      GKServiceUser().menuCount(params, callback: {
//////        (response:AnyObject?) -> () in
//////        
//////        if (response != nil)
//////        {
//////          let jsonDic : NSDictionary = response as! NSDictionary
//////          
//////          if (jsonDic["success"]?.boolValue == true)
//////          {
//////            let obj = jsonDic["object"] as! NSDictionary
//////            GKStore.sharedInstance.menuCounterUpdate(obj)
//////            
//////            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//////            appDelegate.updateBadgeCount()
//////            
//////            callback()
//////          }
//////        }
//////      })
////    }
////  }
//  
//  func createMenuCounter() -> NSDictionary
//  {
//    let menuCounter = NSMutableDictionary()
//    menuCounter["shouts_unread_feed"] = 0
//    menuCounter["messages"] = 0
//    menuCounter["shouts_received"] = 0
//    menuCounter["shouts_unread_events"] = 0
//    
//    return menuCounter
//  }
//  
//  func saveMenuCounter()
//  {
//    Cache.saveWithName("menuCounter", collection:self.menuCounter)
//  }
//  
//  func menuCounterRemove(key: String?)
//  {
//    menuCounterUpdateAtKey(key, counter: 0)
//  }
//  
//  func menuCounterUpdateAtKey(key: String?, counter: Int)
//  {
//    self.menuCounter?[key!] = counter
//    saveMenuCounter()
//  }
//  
//  func menuCounterUpdate(data: NSDictionary)
//  {
//    var value = (data["shouts_unread_feed"] as! NSNumber).integerValue
//    menuCounterUpdateAtKey("shouts_unread_feed", counter: value)
//    
//    value = (data["messages"] as! NSNumber).integerValue
//    menuCounterUpdateAtKey("messages", counter: value)
//    
//    value = (data["shouts_received"] as! NSNumber).integerValue
//    menuCounterUpdateAtKey("shouts_received", counter: value)
//    
//    value = (data["shouts_unread_events"] as! NSNumber).integerValue
//    menuCounterUpdateAtKey("shouts_unread_events", counter: value)
//  }
//  
//  // MARK: Instructions
//  
//  func canShowAccessInstructionsVet() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("ACCESS_INSTRUCTIONS_VET")
//  }
//  
//  func saveShowAccessInstructionsVet()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "ACCESS_INSTRUCTIONS_VET")
//    userDefaults.synchronize()
//  }
//  
//  func canShowAccessInstructionsGetVetted() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("ACCESS_INSTRUCTIONS_GET_VETTED")
//  }
//  
//  func saveShowAccessInstructionsGetVetted()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "ACCESS_INSTRUCTIONS_GET_VETTED")
//    userDefaults.synchronize()
//  }
//  
//  func canShowContactsInstructionsVet() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("CONTACTS_INSTRUCTIONS_VET")
//  }
//  
//  func saveShowContactsInstructionsVet()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "CONTACTS_INSTRUCTIONS_VET")
//    userDefaults.synchronize()
//  }
//  
//  func canShowContactsInstructionsGetVetted() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("CONTACTS_INSTRUCTIONS_GET_VETTED")
//  }
//  
//  func saveShowContactsInstructionsGetVetted()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "CONTACTS_INSTRUCTIONS_GET_VETTED")
//    userDefaults.synchronize()
//  }
//  
//  func canShowProfileInstructions() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("PROFILE_INSTRUCTIONS")
//  }
//  
//  func saveProfileInstructions()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "PROFILE_INSTRUCTIONS")
//    userDefaults.synchronize()
//  }
//  
//  func canShowMessageInstructions() -> Bool
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    
//    return !userDefaults.boolForKey("MESSAGE_INSTRUCTIONS")
//  }
//  
//  func saveMessageInstructions()
//  {
//    let userDefaults = NSUserDefaults.standardUserDefaults()
//    userDefaults.setBool(true, forKey: "MESSAGE_INSTRUCTIONS")
//    userDefaults.synchronize()
//  }
//}
