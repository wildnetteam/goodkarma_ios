//
//  GKNotificationTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/17/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNotificationTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var pictureImageView : WebImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var timestampLabel: UILabel!
  
  // MARK: - Properties -
  
  var data: GKDictionary!
  var delegate: GKShowUserProfileDelegate?
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
    self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
    self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    self.pictureImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
    self.pictureImageView.isUserInteractionEnabled = true
    
    self.selectionStyle = .none
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKNotificationTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 90.0
  }
  
  // MARK: - Methods -
  
  func loadData(_ data: GKDictionary)
  {
    self.data = data
    
    //let author = data["author"] as! GKDictionary
    
    let nameAttrString = NSAttributedString(
      string: "\((data["first_name"] as! String).capitalized +  " " + (data["last_name"] as! String).capitalized): ",
      attributes: [
        NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
        NSAttributedStringKey.foregroundColor: UIColor.black
      ]
    )
    let gratitudeAttrString = NSAttributedString(
      string: data["details"] as! String,
      attributes: [
        NSAttributedStringKey.font: UIFont.fontRoboto(.Light, size: 14),
        NSAttributedStringKey.foregroundColor: UIColor.darkGray
      ]
    )
    
    let attrString = NSMutableAttributedString()
    attrString.append(nameAttrString)
    attrString.append(gratitudeAttrString)
    
    self.titleLabel.attributedText = attrString
    
    self.pictureImageView.loadImageWithUrl(data["photo_url"] as? String as NSString?)
    self.timestampLabel.text = secondsToFormatted(data["createdAt"] as? Int)
  }
  
  @objc func tapImageView()
  {
   // let authorData = self.data["author"] as! GKDictionary
    self.delegate?.GKShowProfile(self.data["userId"] as! Int)
  }
}
