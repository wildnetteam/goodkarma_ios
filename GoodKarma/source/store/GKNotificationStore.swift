//
//  GKNotificationStore.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKNotificationStore {
  
  // MARK: - Properties -
  
  static let notification_kind_menu    = "GKNotificationStoreNotification"
  static let notification_kind_message = "GKNotificationStoreNotificationMessage"
  static let notification_selector     = "receiveFromNotificationStore:"
  
//  static var device_token: String? {
//    
//    didSet {
//      
////      GKNotificationStore.sendDeviceToken()
//    }
//  }
  
  // Set this to notify to all controllers
  // They should display the red circle in the menu bar button
  // Special cases are when the user is in:
  // - notificactions
  // - inbox
  // - message detail
  
  static var new_message_user_id = 0 {
    didSet {
      GKNotificationStore.notifyMessage()
    }
  }
  
  static var number_messages = 0 {
    didSet {
      GKNotificationStore.notifyUpdates()
    }
  }
  static var number_notifications = 0 {
    didSet {
      GKNotificationStore.notifyUpdates()
    }
  }
  
  // MARK: - Methods -
  
  static func clearAll() {
    
    GKNotificationStore.number_messages = 0
    GKNotificationStore.number_notifications = 0
  }
  
//  static func handleRemoteNotification(user_info: [NSObject : AnyObject]) {
//    print(user_info)
//    
//    if let kind = user_info["kind"] as? String {
//      
//      GKNotificationStore.number_messages      = user_info["b_conversations"] as! Int
//      GKNotificationStore.number_notifications = user_info["b_notifications"] as! Int
//      
//      if kind == "message" {
//        
//        // If message detail listen
//        NSNotificationCenter.defaultCenter().postNotificationName(GKNotificationStore.notification_kind_message, object: nil, userInfo: user_info)
//      }
//    }
//  }
  
  static func notifyUpdates() {
    //        if self.totalNotifications() == 0 {
    //            UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    //        }
    UIApplication.shared.applicationIconBadgeNumber = GKNotificationStore.totalNotifications()
    
    NotificationCenter.default.post(name: Notification.Name(rawValue: GKNotificationStore.notification_kind_menu), object: nil, userInfo: nil)
  }
  
  static func notifyMessage() {
    
    NotificationCenter.default.post(name: Notification.Name(rawValue: GKNotificationStore.notification_kind_message), object: nil, userInfo: nil)
  }
  
//  static func sendDeviceToken() {
//    if let token = GKNotificationStore.device_token {
//      
//      // todo: send token
//      
//      GKServiceAuth.sendTokenDevice(token, callback: { (success) -> () in
//        if success {
//          print("token device updated")
//        }
//        else {
//          print("token device error")
//        }
//      })
//    }
//  }
  
//  static func setupRemoteNotifications() {
//    let types: UIUserNotificationType =  [UIUserNotificationType.Alert, .Badge, .Sound]
//    let settings = UIUserNotificationSettings(forTypes: types, categories: nil)
//    
//    let application = UIApplication.sharedApplication()
//    application.registerUserNotificationSettings(settings)
//    application.registerForRemoteNotifications()
//  }
  
  static func totalNotifications() -> Int
  {
    return GKNotificationStore.number_messages + GKNotificationStore.number_notifications
  }
  
  static func requestMenuCount()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    GKServiceClient.call(GKRouterClient.menuCounter(), parameters: params) { (response: AnyObject?) -> () in
      
      //        LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res: AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
          if let data = envelope.getResource() as? NSDictionary
          { 
            GKNotificationStore.number_messages      = data["message"] as! Int
            GKNotificationStore.number_notifications = data["notification"] as! Int
            
//            self.updateCounts()
          }
        }
      }
    }
  }
  
}
