//
//  GKCompanyAddPeopleViewController.swift
//  GoodKarma
//
//  Created by Shatakshi on 07/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import SearchTextField


class GKCompanyAddPeopleViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var textFieldSearch: SearchTextField!
    @IBOutlet weak var tablePeople: UITableView!
    @IBOutlet weak var buttonSave: UIButton!
    
    var companyId = String()
    var referenceCode = String()
    var arrSearchEmployeeList : NSArray = []
    var arrGroup : NSMutableArray = []
    var arrNames : NSMutableArray = []
    var dictMyInfo = [String:Any]()
    var isInMenu = false
    var parentObject: Any!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isInMenu
        {
            self.title = "Company"
            setMenuBarBadge()
            
        }
 
        let nib = UINib(nibName: "GKCompanyAddPeopleTableViewCell", bundle: nil)
        self.tablePeople.register(nib, forCellReuseIdentifier: "GKCompanyAddPeopleTableViewCell")
        tablePeople.tableFooterView = UIView()
        
        dictMyInfo["employeeId"] = Static.sharedInstance.userId()
        dictMyInfo["isGroupAdmin"] = "1"
        dictMyInfo["title"] = "Creator"
        arrGroup.add(dictMyInfo)
        print(arrGroup)

        let user = Static.sharedInstance.userProfile
        let lastName = user?.object(forKey: "last_name") as! String
        let firstName = user?.object(forKey: "first_name") as! String
        arrNames.add("\(firstName)" + " " + "\(lastName)")

        configureCustomSearchTextField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 2 - Configure a custom search text view
    fileprivate func configureCustomSearchTextField() {
        // Set theme - Default: light
        textFieldSearch.theme = SearchTextFieldTheme.lightTheme()
        
        // Modify current theme properties
        textFieldSearch.theme.font = UIFont(name: "Roboto-Regular", size: 14)!
        textFieldSearch.theme.bgColor = UIColor.white.withAlphaComponent(1.0)
        textFieldSearch.theme.borderColor = UIColor.lightGray.withAlphaComponent(0.5)
        textFieldSearch.theme.separatorColor = UIColor.lightGray.withAlphaComponent(0.5)
        textFieldSearch.theme.cellHeight = 44
        textFieldSearch.theme.placeholderColor = UIColor.lightGray
        textFieldSearch.theme.fontColor = UIColor.darkGray
        
        // Max results list height - Default: No limit
        textFieldSearch.maxResultsListHeight = 300
        
        // Set specific comparision options - Default: .caseInsensitive
        textFieldSearch.comparisonOptions = [.caseInsensitive]
        
        // You can force the results list to support RTL languages - Default: false
        textFieldSearch.forceRightToLeft = false
        
        textFieldSearch.highlightAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):UIFont(name: "Roboto-Regular", size: 14)!]
        
        // Handle item selection - Default behaviour: item title set to the text field
        textFieldSearch.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            self.dictMyInfo["employeeId"] = (self.arrSearchEmployeeList[itemPosition] as! NSDictionary).value(forKey: "id")
            self.dictMyInfo["isGroupAdmin"] = "0"
            self.dictMyInfo["title"] = "ios developer"
            self.arrGroup.add(self.dictMyInfo)
            self.arrNames.add(item.title)
            
            // Do whatever you want with the picked item
            self.textFieldSearch.text = item.title
            self.tablePeople.reloadData()
        }
        
        // Update data source when the user stops typing
        textFieldSearch.userStoppedTypingHandler = {
            if let criteria = self.textFieldSearch.text {
                if criteria.count > 1 {
                    // Show loading indicator
                    self.textFieldSearch.showLoadingIndicator()
                    
                    self.searchEmployee(searchString:criteria) { results in
   
                        // Set new items to filter
                        self.textFieldSearch.filterItems(results)
                        
                        // Stop loading indicator
                        self.textFieldSearch.stopLoadingIndicator()
                    }
                }
            }
        }
    }
    
    //MARK: BUTTON ACTIONS
    
    @IBAction func buttonSaveClicked(_ sender: Any) {
        addEmployees()
    }
    
    @objc func groupAdminClicked(_ sender: UIButton)
    {
        (sender as UIButton).backgroundColor = UIColor.baseColor()
        (sender as UIButton).setTitleColor(.white, for: .normal)
    }
    
    // MARK: - UITableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GKCompanyAddPeopleTableViewCell", for: indexPath) as! GKCompanyAddPeopleTableViewCell
        
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.height / 2
        
        cell.buttonGroupadmin.layer.borderWidth = 1.0
        cell.buttonGroupadmin.layer.borderColor = UIColor.baseColor().cgColor
        cell.buttonGroupadmin.layer.cornerRadius = 4.0
        
        cell.labelName.text = (arrNames[indexPath.row] as! String).capitalized
        cell.labelRole.text = (((arrGroup[indexPath.row] as! NSDictionary).value(forKey: "title")) as! String).capitalized

        cell.buttonGroupadmin.tag = indexPath.row
        cell.buttonGroupadmin.addTarget(self, action: #selector(GKCompanyAddPeopleViewController.groupAdminClicked(_:)), for:UIControlEvents.touchUpInside)
        
        return cell
    }

    //MARK:------ WEBServices
    
    func searchEmployee(searchString:String, callback: @escaping ((_ results: [SearchTextFieldItem]) -> Void)) {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["name"] = (searchString.replacingOccurrences(of: " ", with: "")) as AnyObject
        
        GKServiceClient.call(GKRouterClient.searchEmployee(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
                if let res:AnyObject = response  {
                    let envelope = ResponseSkeleton(res)
                    
                    if (envelope.getStatusCode()  as NSNumber == 200) {
                        
                        let data = envelope.getResource() as? NSArray
                        print(data!)
                        
                        var results = [SearchTextFieldItem]()
                        self.arrSearchEmployeeList = data!
                        
                        for result in data! {
                            let dict = (result as! NSDictionary)
                            results.append(SearchTextFieldItem(title: (dict["first_name"] as! String) + " " + (dict["last_name"] as! String), subtitle: dict["email"] as? String, image: UIImage(named: "acronym_icon")))
                        }
                        
                        DispatchQueue.main.async {
                            callback(results)
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            callback([])
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        callback([])
                    }
                }
            })
        }
    
    func addEmployees() {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["companyId"] = companyId as AnyObject
        params["employees"] = arrGroup as AnyObject

        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.addEmployee(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response  {
                let envelope = ResponseSkeleton(res)
                
                if (envelope.getStatusCode()  as NSNumber == 200) {
                    
                    let data = envelope.getResource() as? NSArray
                    print(data!)
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
                else {
 
                }
            }
            else{
 
            }
        })
    }
    
}
