//
//  GKRouterClient.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/27/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKRouterAuth: ResourceRouter {
  
  // Recovery
  
  static func passwordRecovery() -> String {
    return endpoint ("password/recovery/")
  }
  
  static func changePassword() -> String {
    return endpoint("password/reset/")
  }
  
  // Email login
  static func signIn()->String {
    return endpoint("auth/login/")
  }
  
  static func logout()->String {
    return endpoint("auth/logout/")
  }
  
// Services
//  static func sendLocation() -> String {
//    return endpoint("service/get-address/")
//  }
  
  // Facebook login
  static func signWithFacebook() -> String {
    return endpoint("auth/social/")
  }
  
  // Push notifications
    static func deviceToken() -> String {
        return endpoint ("karm/notification/device/")
    }
 
  // Verify phone
  static func validatePhone() -> String {
    return endpoint ("karm/validate/phone")
  }
  
    static func resendOtp() -> String {
        return endpoint ("resend/otp")
    }
    
    // Zipcode
    static func getZipcode() -> String {
        return endpoint ("karm/get/zipcode/")
    }
  
}
