//
//  GKHistoryOfferedTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/18/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKHistoryOfferedTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    //  @IBOutlet weak var containerView : UIView!
    
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var statusArrowImageView: UIImageView!
    @IBOutlet weak var userImageView : WebImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var postTypeLabel : UILabel!
    @IBOutlet weak var detailLabel : UILabel!
    @IBOutlet weak var timestampLabel : UILabel!
    @IBOutlet weak var postTypeImageView : UIImageView!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var timeImageView: UIImageView!
    
    // MARK: - Properties -
    
    var data: GKDictionary!
    var delegate: (GKShowUserProfileDelegate & GKHistoryPostStateDelegate)?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        self.userImageView.layer.cornerRadius = userImageView.bounds.height * 0.5
        self.userImageView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(self.tapImageView))
        )
        self.userImageView.isUserInteractionEnabled = true
        
        self.selectionStyle = .none
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionShowStatus(_ sender: AnyObject)
    {
        delegate?.GKHandleParticipationState(self.data)
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKHistoryOfferedTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat {
        
        return 144.0
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        
        var author: GKDictionary!
        
       // let type = data["post_type"] as! Int
        var desc: String!
        let postType = "Help Request"
        postTypeImageView.image = UIImage(named: "home-positive-on")
        self.userNameLabel.text = (data["first_name"] as? String)!.capitalized + " " + (data["last_name"] as? String)!.capitalized
        desc = self.data["desc"] as! String
        // Serve
//        if type == 1
//        {
//            print(self.data)
//            postType = "Serve"
//            author = self.data["author"] as! GKDictionary
//            desc = self.data["desc"] as! String
//
//            self.userNameLabel.text = (author["first_name"] as? String)!.capitalized + " " + (author["last_name"] as? String)!.capitalized
//
//            postTypeImageView.image = UIImage(named: "home-serve-on")
//        }
            // Request Help
//        else if type == 3
//        {
//            postType = "Help Request"
//            author = self.data["author"] as! GKDictionary
//            desc = self.data["desc"] as! String
//            self.userNameLabel.text = (author["first_name"] as? String)!.capitalized + " " + (author["last_name"] as? String)!.capitalized
//            postTypeImageView.image = UIImage(named: "home-request-on")
//        }
//        else if type == 2
//        {
//            postType = "Help Request"
//            author = self.data["author"] as! GKDictionary
//            desc = self.data["desc"] as! String
//            self.userNameLabel.text = (author["first_name"] as? String)!.capitalized + " " + (author["last_name"] as? String)!.capitalized
//            postTypeImageView.image = UIImage(named: "home-request-on")
//        }
//
//        else if type == 4
//        {
//            postType = "Positivity"
//            author = self.data["author"] as! GKDictionary
//            desc = self.data["desc"] as! String
//            self.userNameLabel.text = (author["first_name"] as? String)!.capitalized +  " " + (author["last_name"] as? String)!.capitalized
//            postTypeImageView.image = UIImage(named: "home-positive-on")
//        }
            // Positivity
//        else if type == 5
//        {
//            postType = "Positivity"
//            author = self.data["author"] as! GKDictionary
//            desc = self.data["desc"] as! String
//            self.userNameLabel.text = (author["first_name"] as? String)!.capitalized +  "  " + (author["last_name"] as? String)!.capitalized
//            postTypeImageView.image = UIImage(named: "home-positive-on")
//        }
        
        
        let status = self.data["status"] as! Int
        statusButton.isHidden = false
        
        // Status
        if (status == 4)
        {
            statusButton.isUserInteractionEnabled = false
            statusButton.setTitle("Finished", for: UIControlState())
            statusArrowImageView.isHidden = true
        }
        else if (status == 2)
        {
            statusButton.setTitle("Accepted", for: UIControlState())
            statusButton.isUserInteractionEnabled = false
            statusArrowImageView.isHidden = false
        }
        else if (status == 3)
        {
            statusButton.setTitle("Flaked", for: UIControlState())
            statusButton.isUserInteractionEnabled = false
            statusArrowImageView.isHidden = true
        }
        else if (status == 5)
        {
            statusButton.setTitle("Report", for: UIControlState())
            statusButton.isUserInteractionEnabled = false
            statusArrowImageView.isHidden = true
        }
        else
        {
            statusButton.setTitle("Created", for: UIControlState())
            statusButton.isUserInteractionEnabled = false
            statusArrowImageView.isHidden = true
        }
        
        
        self.userImageView.loadImageWithUrl(data["photo_url"] as? String as NSString?)
        
        // Serve
//        if type == 1
//        {
//            postType = "Serve"
//        }
//            // Request Help
//        else if type == 3
//        {
//            postType = "Help Request"
//        }
//            // Positivity
//        else if type == 5
//        {
//            postType = "Positivity"
//            statusButton.isHidden = true
//            statusArrowImageView.isHidden = true
//        }
//
//        if ((type == 1 || type == 3) && status == 4)
//        {
//            arrowImageView.isHidden = false
//        }
//        else
//        {
//            arrowImageView.isHidden = true
//        }
//
        self.detailLabel.text    = desc.decodeEmoji()
        self.postTypeLabel.text  = postType
        self.timestampLabel.text = secondsToFormatted(data["createdAt"] as? Int)
        
        // Disable actions
        statusArrowImageView.isHidden = true
        statusButton.isUserInteractionEnabled = false
    }
    
    @objc func tapImageView()
    {
        var author: GKDictionary!
        if data != nil
        {
//            let type = data["post_type"] as! Int
//
//            // Serve
//            if type == 1
//            {
//                author = self.data["author"] as! GKDictionary
//            }
//                // Request Help
//            else if type == 3
//            {
//                author = self.data["author"] as! GKDictionary
//            }
//                // Positivity
//            else if type == 5
//            {
//                author = self.data["author"] as! GKDictionary
//            }
//            else if type == 4
//            {
//                author = self.data["author"] as! GKDictionary
//            }
            
            self.delegate?.GKShowProfile(data["user_id"] as! Int)
        }
        
    }
}

