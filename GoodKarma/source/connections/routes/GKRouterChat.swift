//
//  GKRouterChat.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/27/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKRouterChat: ResourceRouter {
  
  // Conversation
  static func listConversations()->String {
    return endpoint("chat/conversation/list/")
  }
  
  static func detailMessage()->String {
    return endpoint("chat/conversation/detail/")
  }
  
  static func createConversation()->String {
    return endpoint("chat/conversation/create/")
  }
  
  static func deleteConversation()->String {
    return endpoint("chat/conversation/delete/")
  }
  
  // Messages
  
//  static func sendMessage()-> String{
//    return endpoint("chat/messages/send/")
//  }

}