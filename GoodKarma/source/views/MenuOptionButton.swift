//
//  MenuOptionButton.swift
//  ShoutOut
//
//  Created by Cesar Velasquez on 8/06/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit

class MenuOptionButton: UIButton {
  
  var notification  : UILabel!
  var leftImageView : UIImageView!
  //    var imageArrow: UIImageView!
  
  init(frame: CGRect, index: Int) {
    
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.clear
    self.setTitleColor(UIColor.white, for: UIControlState())
    
    // Add bordere
    //      let border = UIView(frame: CGRectMake(PGCView.Metrics.pad, frame.height - 1,
    //        frame.width  - PGCView.Metrics.pad, 1.0))
    //      border.backgroundColor = UIColor.superLightGray()
    //        self.addSubview(border)
    
    setup()
    
    leftImageView.image = UIImage(named: "menu-option-\(index)")
  }
  
  required init?(coder aDecoder: NSCoder) {
    
    super.init(coder: aDecoder)!
  }
  
  // MARK: - Setup -
  
  func setupTitle(_ title: String) {
    self.contentVerticalAlignment = .center
    self.contentHorizontalAlignment = .left
    self.titleLabel?.backgroundColor = UIColor.clear
    self.titleLabel?.contentMode = UIViewContentMode.left
    self.titleLabel?.numberOfLines = 0
    self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
    self.titleLabel?.textAlignment = .left
    self.setTitle(title, for: UIControlState())
  }
  
  func setup() {
    self.contentVerticalAlignment = .center
    self.contentHorizontalAlignment = .left
    
    self.titleLabel?.backgroundColor = UIColor.clear
    self.titleLabel?.contentMode = UIViewContentMode.left
    //        self.titleLabel?.font = UIFont.fontRegularMediumLarge()
    self.titleLabel?.numberOfLines = 0
    self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
    self.titleLabel?.textAlignment = .left
    
    // Left Image
    let sizeIcon: CGFloat = 22.0
    
    leftImageView = UIImageView()
    leftImageView.frame = CGRect(
      x: PGCView.Metrics.pad,
      y: (self.bounds.size.height - sizeIcon) * 0.5,
      width: sizeIcon,
      height: sizeIcon
    )
//    leftImageView.image = UIImage(named: "arrow-menu")
    self.addSubview(leftImageView)
    
    self.titleEdgeInsets = UIEdgeInsetsMake(
      0.0,
      leftImageView.rightSide + PGCView.Metrics.pad,
      0.0,
      0.0
    )
    
    // Add notification
    let notifiWidth:CGFloat = 20.0
    
    notification = UILabel(
      frame: CGRect(
        x: self.bounds.size.width - 40.0,
        y: (self.bounds.size.height - notifiWidth) * 0.5,
        width: notifiWidth,
        height: notifiWidth
      )
    )
    
    // Notification
    notification.backgroundColor = UIColor.red
    notification.font = UIFont.fontRoboto(.Light, size: 12)
    notification.clipsToBounds = true
    notification.layer.cornerRadius = notifiWidth * 0.5
    notification.tag = 111
    notification.textAlignment = .center
    notification.textColor = UIColor.white
    notification.isHidden = true
    self.addSubview(notification)
    
    // ArrowRight
    //        let sizeArrow:CGFloat = 15.0
    //        imageArrow = UIImageView()
    //        imageArrow.frame = CGRectMake(
    //            (self.frame.width - sizeArrow - PGCView.Metrics.pad) ,
    //            (self.frame.height - sizeArrow) * 0.5, sizeArrow,
    //            sizeArrow)
    //        imageArrow.image = UIImage(named: "arrow-menu")
    //        self.addSubview(imageArrow)
  }
  
//  func addNotification(number: Int) {
//    if number > 0
//    {
//      notification.hidden = false
//      notification.text = "\(number)"
//    }
//    else
//    {
//      notification.hidden = true
//    }
//  }
  
  func updateCount(_ count: Int) {
    
    if count > 0 {
      
      self.notification.isHidden = false
      self.notification.text = "\(count)"
    }
    else {
      
      self.notification.isHidden = true
    }
  }
  
}
