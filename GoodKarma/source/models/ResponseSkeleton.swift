//
//  ResponseSkeleton.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/17/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation
import SwiftyJSON
import FBSDKLoginKit

open class ResponseSkeleton: NSObject
{
  fileprivate var resource: AnyObject!
  fileprivate var success: Bool!
  fileprivate var errors: AnyObject!
  fileprivate var statusCode:AnyObject!
  
  // MARK: Initializer
 
    public init(_ data: AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let controller = appDelegate.window?.rootViewController
        
        self.statusCode = data.object(forKey: "status_code") as AnyObject
        if statusCode as! Int == 200
        {
            self.success = true
            self.resource = data.object(forKey: "object") as AnyObject
        }
        else if statusCode as! Int == 401
        {
            self.success = false
            showMessageInvalidToken()
            FBSDKLoginManager().logOut()
            Static.sharedInstance.saveLogin(false)
            UserDefaults.standard.removeObject(forKey: "isFacebookId")
            UserDefaults.standard.removeObject(forKey: "id")
            
            appDelegate.showLogin(controller!)
        }
        else
        {
            self.success = false
            let value : GKDictionary!
            let valueExists = data["err"] as? GKDictionary != nil
            if valueExists{
                value = data["err"]  as! GKDictionary
            }
            else
            {
                value = data["error"] as! GKDictionary
                self.errors = value as AnyObject
            }
            self.resource = value["object"]  as AnyObject
        }
    
    }
   
    
    public init(_ data: AnyObject , string : String)
    {
        if string == "Block" ||  string == "testimonials"
        {
                self.statusCode = data.object(forKey: "status_code") as AnyObject
               if statusCode as! Int == 200
               {
                self.success = true
                self.resource = data.object(forKey: "object") as AnyObject
            }
            else
               {
                self.success = false
            }
                
        }
        else if string == "followers"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                 self.success = true
                 self.resource = data.object(forKey: "object") as AnyObject
            }
           else
            {
                let err = data.object(forKey: "err") as AnyObject
                self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
            }
           
        }
      else if string == "following"
        {
            
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data.object(forKey: "object") as AnyObject
            }
            else
            {
                let err = data.object(forKey: "err") as AnyObject
                self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
            }
            
        }
        else if string == "gratitude_privacy"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data.object(forKey: "object") as AnyObject
            }
            else
            {
                let err = data.object(forKey: "err") as AnyObject
                self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
            }
        }
        else if string == "due_settings"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data
            }
            else
            {
                let err = data.object(forKey: "err") as AnyObject
                self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
            }
        }
        
        else if string == "explore_filters_settings"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data
            }
            else
            {
                //let err = data.object(forKey: "err") as AnyObject
                //self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
            }
        }
        else if string == "request_history_data"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data.object(forKey: "object") as AnyObject
            }
            else
            {
               // let err = data.object(forKey: "err") as AnyObject
               // self.errors = err.object(forKey: "object") as AnyObject
                self.success = false
                // self.resource = data.object(forKey: "object") as AnyObject
            }
        }
        else if string == "profile"
        {
            
                self.statusCode = data.object(forKey: "status_code") as AnyObject
                if statusCode as! Int == 200
                {
                    self.success = true
                    self.resource = data.object(forKey: "object") as AnyObject
                }
                else
                {
                    self.success = false
                    let error : GKDictionary!
                    let valueExists = data["err"] as? GKDictionary != nil
                    if valueExists{
                        error = data["err"]  as! GKDictionary
                    }
                    else
                    {
                        error = data["error"] as! GKDictionary
                    }
                    self.resource = error["object"] as AnyObject
                }
        }
        
        else if string == "login"
        {
            self.statusCode = data.object(forKey: "status_code") as AnyObject
            if self.statusCode as! Int  == 200
            {
                self.success = true
                self.resource = data.object(forKey: "object") as AnyObject
            }
            else
            {
                 let err = data.object(forKey: "error") as AnyObject
                 self.resource = err.object(forKey: "object") as AnyObject
                 self.success = false
                 self.errors = "" as AnyObject
               // self.resource = data.object(forKey: "object") as AnyObject
            }
        }
    }
    
  open func getResource() -> AnyObject
  {
    return resource
  }
  
  open func getSuccess() -> Bool
  {
    return success
  }
  
  open func getErrors() -> [NSString: AnyObject]?
  {
    if let listErrors = errors as? [NSString: AnyObject]
    {
      if listErrors.count > 0
      {
        return listErrors
      }
    }
    
    return nil
  }
    
    open func getStatusCode() -> Int
    {
        if let statusCode = self.statusCode as? Int
        {
            return statusCode
        }
        
        return -1
    }
}
