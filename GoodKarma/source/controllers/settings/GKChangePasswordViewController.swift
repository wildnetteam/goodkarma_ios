//
//  GKChangePasswordViewController.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 23/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import FBSDKLoginKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class GKChangePasswordViewController: GKViewController, UITextFieldDelegate
{
    @IBOutlet weak var currentTextField: UITextField!
    @IBOutlet weak var newTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Change Password"
        
        let send = UIBarButtonItem(title: "Send",
                                   style: .plain,
                                   target: self,
                                   action: #selector(GKChangePasswordViewController.send))
        send.isEnabled = false
        self.navigationItem.rightBarButtonItem = send
        
        // Observer
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(GKChangePasswordViewController.textFieldDidChange(_:)),
                                       name: NSNotification.Name.UITextFieldTextDidChange,
                                       object: self.currentTextField)
        notificationCenter.addObserver(self,
                                       selector: #selector(GKChangePasswordViewController.textFieldDidChange(_:)),
                                       name: NSNotification.Name.UITextFieldTextDidChange,
                                       object: self.newTextField)
        notificationCenter.addObserver(self,
                                       selector: #selector(GKChangePasswordViewController.textFieldDidChange(_:)),
                                       name: NSNotification.Name.UITextFieldTextDidChange,
                                       object: self.confirmTextField)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func send()
    {
        if (canSend())
        {
            self.view.endEditing(true)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let controller = appDelegate.window?.rootViewController
            
            var params = GKDictionary()
            params["token"] = Static.sharedInstance.token() as AnyObject?
            params["old"] = self.currentTextField.text as AnyObject?
            params["new_1"] = self.newTextField.text as AnyObject?
            params["new_2"] = self.confirmTextField.text as AnyObject?
            
            LoadingView.showLoadingView("Sending...",
                                        parentView: self.navigationController?.view,
                                        backColor: true)
            
            GKServiceClient.call(GKRouterClient.resetPassword(), parameters: params,
                                 callback:
                {(response: AnyObject?) -> () in
                    
                    if let res:AnyObject = response
                    {
                        let envelope = ResponseSkeleton(res)
                        
                        if envelope.getSuccess()
                        {
                            LoadingView.hideWithSuccess(self.navigationController?.view,
                                text: "Success!")
                            FBSDKLoginManager().logOut()
                            Static.sharedInstance.saveLogin(false)
                            UserDefaults.standard.removeObject(forKey: "isFacebookId")
                            UserDefaults.standard.removeObject(forKey: "id")
                            
                            appDelegate.showLogin(controller!)
                        }
                        else
                        {
                            LoadingView.hideLoadingView(self.navigationController?.view)
                            showConnectionError()
                        }
                    }
                    else
                    {
                        LoadingView.hideLoadingView(self.navigationController?.view)
                        showConnectionError()
                    }
            })
        }
    }
    
    func canSend() -> Bool
    {
        let currentText = self.currentTextField.text
        let newText = self.newTextField.text
        let confirmText = self.confirmTextField.text;
        
        if (currentText?.count >= 5
            && newText == confirmText
            && newText?.count >= 6
            && currentText != newText)
        {
            return true
        }
        
        return false
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if textField == currentTextField
        {
            self.newTextField.becomeFirstResponder()
        }
        else if textField == newTextField
        {
            self.confirmTextField.becomeFirstResponder()
        }
        else
        {
            self.confirmTextField.resignFirstResponder()
            send()

        }
//        if (NSObject.isEqual(self.currentTextField))
//        {
//            self.newTextField.becomeFirstResponder()
//        }
//        else if (NSObject.isEqual(self.newTextField))
//        {
//            self.confirmTextField.becomeFirstResponder()
//        }
//        else if (NSObject.isEqual(self.confirmTextField))
//        {
//            send()
//        }
//
        return false
    }
    
    // MARK: - Observer
    
    @objc func textFieldDidChange(_ notification: Notification)
    {
        if (canSend())
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        else
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
}
