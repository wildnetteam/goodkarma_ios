//
//  GKThankYouViewController.swift
//  GoodKarma
//
//  Created by Shivani on 05/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import RichEditorView

class GKThankYouViewController: UIViewController , UIImagePickerControllerDelegate ,UINavigationControllerDelegate, UITextViewDelegate
{
    @IBOutlet var editorView: RichEditorView!
    @IBOutlet weak var thankYouTexView: UITextView!
    @IBOutlet weak var buttonSendThankYou: UIButton!
    @IBOutlet weak var viewMedia: UIView!
    
    var parentObject: Any!
    
    var isTestimonial = false
    var delegate : GKSendThankYouDelegate!
    
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        if isTestimonial{
            self.title = "Add custom message"
            buttonSendThankYou.setTitle("Add", for: .normal)
            viewMedia.isHidden = true
        }

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(GKThankYouViewController.cancel)
        )
        editorView.delegate = self
        editorView.inputAccessoryView = toolbar
        
        toolbar.delegate = self
        toolbar.editor = editorView
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        var options = toolbar.options
        options.append(item)
        toolbar.options = options
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func cancel() {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionVideo(_ sender: Any)
    {
        
    }
    
    @IBAction func actionCamera(_ sender: Any)
    {
        self.pickFromCamera()
    }
    

    @IBAction func actionGallery(_ sender: Any)
    {
        self.pickFromGallery()
    }
    
    @IBAction func actionSendThankYou(_ sender: Any)
    {
        if isTestimonial{
            (parentObject as! GKAddTestimonialViewController).customMessage = editorView.text
            cancel()
        }
        else{
            let message  = String()
            if thankYouTexView.text.count == 0
            {
                let alertController = UIAlertController(title: "Message", message: message , preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                self.present(alertController, animated: false, completion: nil)
            }
            else
            {
                if delegate != nil
                {
                    delegate.sendThankYou()
                }
            }
        }
    }
    
    func pickFromCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func pickFromGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
}

extension GKThankYouViewController: RichEditorDelegate {

    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        print(content)
    }
}

extension GKThankYouViewController: RichEditorToolbarDelegate {
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if toolbar.editor?.hasRangeSelection == true {
            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
        }
    }
    
}

