//
//  MenuProfile.swift
//  ShoutOut
//
//  Created by Cesar Velasquez on 8/06/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit

class MenuProfile: PGCView
{
  var imageView: WebImageView!
  var nameLabel: UILabel!
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.clear
    
    setup(frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func setup(_ frame: CGRect)
  {
    
    let widthImage:CGFloat = 50
    
    imageView = WebImageView(
      frame: CGRect(
        x: Metrics.pad,
        y: (frame.height - widthImage) * 0.5,
        width: widthImage,
        height: widthImage
      )
    )
    imageView.backgroundColor = UIColor.white
    imageView.contentMode = UIViewContentMode.scaleAspectFill
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = imageView.frame.width * 0.5
//    imageView.image = UIImage(named: "user-placeholder")
    self.addSubview(imageView)
    
    let originXLabel = imageView.rightSide + Metrics.pad
    
    nameLabel = UILabel()
    nameLabel.frame = CGRect(
      x: originXLabel,
      y: 0.0,
      width: self.bounds.size.width - (originXLabel + Metrics.pad),
      height: self.bounds.size.height
    )
    nameLabel.textAlignment = NSTextAlignment.left
    nameLabel.textColor = UIColor.superLightGray()
    nameLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
    nameLabel.font = UIFont.fontRoboto(.Regular, size: 16)
    nameLabel.text = "User Name"
    
    self.addSubview(nameLabel)
    
  }
}

