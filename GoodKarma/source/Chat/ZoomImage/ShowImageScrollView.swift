//
//  ShowImageScrollView.swift
//  Bizzalley
//
//  Created by Anshul on 22/03/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit

protocol ShowImageScrollViewDelegate {
    
    func removeShowImageScrollView()
}

class ShowImageScrollView: UIView , UIScrollViewDelegate{

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var closeBtn:UIButton!
    
    var delegate:ShowImageScrollViewDelegate?
    var image:UIImage?
    
    
    //MARK:- Intializer
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setImage(image:UIImage){
        
        closeBtn.layer.cornerRadius = closeBtn.frame.size.width/2
        closeBtn.layer.masksToBounds = true
        scrollView.contentSize = imageView.frame.size
        scrollView.clipsToBounds = false
       scrollView.delegate = self
        
       
        scrollView.minimumZoomScale = 1.0;
        
        scrollView.maximumZoomScale = 6.0
        //scrollView.zoomScale = 1;
        
        
        imageView.image = image
    }

    @IBAction func closeBtnTapped(_ sender: UIButton) {
        if delegate != nil {
            delegate?.removeShowImageScrollView()
        }
    }
    public func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imageView
    }
}
