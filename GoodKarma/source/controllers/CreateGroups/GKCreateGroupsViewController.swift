//
//  GKCreateGroupsViewController.swift
//  GoodKarma
//
//  Created by Shivani on 06/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKCreateGroupsViewController: UIViewController {

    @IBOutlet weak var inProgressView: UIView!
    var isInMenu = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        let view =  Bundle.main.loadNibNamed("GKCreateGroupView", owner: self, options: nil)?[0] as? GKCreateGroupView
//        view?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        self.view.addSubview(view!)
//     //   self.view.addSubView(view)
        if isInMenu
        {
            self.title = "Create Groups"
            setMenuBarBadge()
        }
        else
        {
            self.title = "Posts"
        }
       inProgressView.isHidden = true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    


}
