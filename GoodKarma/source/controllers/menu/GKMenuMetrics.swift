//
//  GKMenuMetrics.swift
//  ShoutOut
//
//  Created by Cesar Velasquez on 8/06/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//


import UIKit

struct GKMenuMetrics {
  
  let edgeSpeed: CGFloat = 800.0
  let padding:   CGFloat =  20.0
  let k11:       CGFloat =   0.8
  let k22:       CGFloat =   0.8  // Scale, max scale = 1 - k22
  let k41:       CGFloat =   0.85 // Translation x, factor
  
}