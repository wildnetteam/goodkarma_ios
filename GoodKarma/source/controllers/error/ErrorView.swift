//
//  ErrorView.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

class ErrorView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup()
    {
        view = NSBundle.mainBundle().loadNibNamed("ErrorView", owner: self,
            options: nil)[0] as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        self.addSubview(view)
        
        self.backgroundColor = UIColor.clearColor()
        self.hidden = true
    }
    
    private func setAnchorPoint(anchorPoint: CGPoint)
    {
        let oldOrigin = self.frame.origin
        self.layer.anchorPoint = anchorPoint
        let newOrigin = self.frame.origin
        
        var transition : CGPoint = CGPointMake(0, 0)
        transition.x = newOrigin.x - oldOrigin.x
        transition.y = newOrigin.y - oldOrigin.y
        
        self.center = CGPointMake(self.center.x - transition.x,
            self.center.y - transition.y)
    }
    
    func showInPos(point: CGPoint, text: NSString)
    {
        if (self.hidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPointMake(0.2, 1))
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
            self.hidden = false
            
            UIView.animateWithDuration(0.3, delay:0,
                options:UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func showInPosFromCenter(point: CGPoint, text: NSString)
    {
        if (self.hidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view-center")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPointMake(0.5, 1))
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
            self.hidden = false
            
            UIView.animateWithDuration(0.3, delay:0,
                options:UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func showInPosFromRight(point: CGPoint, text: NSString)
    {
        if (self.hidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view-right")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPointMake(0.8, 1))
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
            self.hidden = false
            
            UIView.animateWithDuration(0.3, delay:0,
                options:UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func hide()
    {
        self.endEditing(true)
        
        if (!self.hidden)
        {
            UIView.animateWithDuration(0.3, delay:0,
                options:UIViewAnimationOptions.CurveEaseInOut,
                animations:
                {
                    self.alpha = 0
                },
                completion:
                { _ in
                    self.hidden = true
                    self.alpha = 1
            })
        }
    }
  
  
  func hideWithoutAnimation()
  {
    self.endEditing(true)
    
    if (!self.hidden)
    {
      self.hidden = true
    }
  }
}
