//
//  UIColorExtension.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

public extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
  
  class func colorToPercent(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
    return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
  }
  
  class func colorToPercent(_ red: CGFloat, green: CGFloat, blue: CGFloat,
                            alpha: CGFloat) -> UIColor {
    return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
  }
//  
//  class func orange() -> UIColor {
//    return colorToPercent(248, green: 99, blue: 34)
//  }
//  
//  class func orangeSend() -> UIColor {
//    
//    return colorToPercent(255, green: 78, blue: 43)
//    
//  }
//  class func greenForHeader() -> UIColor {
//    
//    return colorToPercent(120.0, green: 170.0, blue: 64.0)
//    //Old
//    //return colorToPercent(120.0, green: 212.0, blue: 64.0)
//    
//  }
//  
//  class func orangeRequest() -> UIColor {
//    
//    return colorToPercent(255, green: 101, blue: 44)
//    
//  }
//  
//  class func grayBorder()->UIColor {
//    return colorToPercent(239, green: 238, blue: 242)
//  }
//  
//  class func blueText()->UIColor {
//    return colorToPercent(69, green: 147, blue: 207)
//  }
  
  class func labelTintColor() -> UIColor{
    
    return UIColor.baseColor()
  }
  
  class func baseColor() -> UIColor {
    
    return colorToPercent(0, green: 168, blue: 243)
  }
  
  class func lightBlue() -> UIColor {
    
    return colorToPercent(213, green: 224, blue: 237)
  }
  
  class func blueOpaque() -> UIColor {
    
    return colorToPercent(100, green: 143, blue: 191)
  }
  
  class func greenOpaque() -> UIColor {
    
    return colorToPercent(0, green: 153, blue: 0)
  }
  
  public class func ultraLightGray() -> UIColor {
    
    return UIColor(white: 0.97, alpha: 1.0)
  }
  
  public class func megaLightGray() -> UIColor {
    
    return UIColor(white: 0.95, alpha: 1.0)
  }
  
  public class func superLightGray() -> UIColor {
    
    return UIColor(white: 0.9, alpha: 1.0)
  }
  
  public class func hightLightGray() -> UIColor {
    
    return UIColor(white: 0.8, alpha: 1.0)
  }
  
  // MARK: - BarColor -
  
//  public class func customBlueColor() -> UIColor {
//    
//    return UIColor.colorToPercent(49.0, green: 111.0, blue: 181.0)
//  }
  
}
