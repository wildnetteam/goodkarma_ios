//
//  GKForgotPasswordViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/31/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKForgotPasswordViewController: UIViewController, UITextFieldDelegate {
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var contentView: UIView!
  
  var errorView: ErrorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    cleanBackButton()
    
    self.title = "Forgot Password"
    
    self.automaticallyAdjustsScrollViewInsets = false
    
    self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
    self.contentView.addSubview(self.errorView)
  }
  
  @IBAction func actionSend(_ sender: AnyObject) {
    
    send()
  }
  // MARK: - UITextFieldDelegate
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    send()
    
    return false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    self.errorView.hide()
  }
  
  // MARK: - Methods -
  
  func validate() -> Bool {
    
    let baseY = self.errorView.frame.size.height;
    
    if (!self.emailTextField.text!.isEmailValid())
    {
      self.errorView.showInPos(CGPoint(x: self.emailTextField.frame.minX - 8,
        y: self.emailTextField.frame.minY - baseY),
                               text: "Need to set a valid email.")
      
      return false;
    }
    
    return true
  }
  
  func send()
  {
    self.view.endEditing(true)
    self.errorView.hide()
    
    if (validate()) {
      LoadingView.showLoadingView("Sending...",
                                  parentView: self.navigationController?.view, backColor: true)
      
      var params = [String:AnyObject]()
      params["email"] = self.emailTextField.text as AnyObject?
      
      GKServiceClient.call(GKRouterAuth.passwordRecovery(), parameters:params, callback: {
        (response:AnyObject?) -> () in
        
        LoadingView.hideLoadingView(self.navigationController?.view)
        
        if let res:AnyObject = response {
          
          let envelope = ResponseSkeleton(res)
          
          if let _ = envelope.getErrors() {
            
            showCommonError({ (NoInternetResultType) -> () in
            })
          }
          else {
            
            if envelope.getSuccess() {
              
              Static.sharedInstance.savePassword("")
              showAlert("Password Reset", text: "A new password has been sent to your registered email", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
              self.navigationController?.popViewController(animated: true)
            }
            else {
              
              showAlert("Message", text: "User not found", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
            }
          }
        }
      })
    }
  }
}
