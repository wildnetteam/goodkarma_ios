//
//  GKEditPhotoViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/20/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKEditPhotoViewController: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var imageView: WebImageView!
  @IBOutlet weak var addPhotoButton: UIButton!
  @IBOutlet weak var contentView: UIView!
  
  // MARK: - Properties -
  
  var imagePicker = UIImagePickerController()
  var fromFacebook = false
  var photoUrl: String? = nil
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
//    cleanBackButton()
    
    self.title = "Edit Photo"
    
    // Content Photo
    self.imageView.layer.cornerRadius = self.imageView.bounds.width * 0.5
    self.addPhotoButton.layer.cornerRadius = self.addPhotoButton.bounds.width * 0.5
    
    self.imageView.loadImageWithUrl(photoUrl as NSString?)
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UIActionSheetDelegate -
  
  func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    
    if (buttonIndex <= 1) {
      
      imagePicker.allowsEditing = false
      imagePicker.sourceType = buttonIndex == 0 ? .camera : .photoLibrary
      imagePicker.delegate = self;
      imagePicker.allowsEditing = true;
      
      present(imagePicker, animated: true, completion: nil)
      UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: false)
    }
  }
  
  // MARK: - Protocol UIImagePickerControllerDelegate -
  
  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [String : Any])
  {
    let image: UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
    
    self.imageView.image = image
    
    self.dismiss(animated: true, completion: nil)
    UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
  {
    self.dismiss(animated: true, completion: nil)
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionSave(_ sender: AnyObject)
  {
    if validate()
    {
      var params = [String: String]()
      params["token"]     = Static.sharedInstance.token()
      
      LoadingView.showLoadingView(
        "Sending photo...",
        parentView: self.navigationController?.view,
        backColor: true
      )
      
      let imageData = UIImageJPEGRepresentation(self.imageView.image!, 0.5)
      
      GKServiceClient.uploadWithAlamofire(params, imageData: imageData!, callback: { (response: AnyObject?) in
        
        LoadingView.hideLoadingView(self.navigationController?.view)
        
        if let res:AnyObject = response
        {
          let envelope = ResponseSkeleton(res)
          
          if envelope.getSuccess()
          {
            self.next()
          }
          else
          {
            showCommonError({ (type: NoInternetResultType) -> () in
              self.actionSave(0 as AnyObject)
            })
          }
        }
        else
        {
          showConnectionError({ (type: NoInternetResultType) -> () in
            self.actionSave(0 as AnyObject)
          })
        }
      })
    }
  }
  
  @IBAction func actionAddPhoto(_ sender: AnyObject)
  {
    let actionSheet =  UIActionSheet(
      title: nil,
      delegate: self,
      cancelButtonTitle: nil,
      destructiveButtonTitle: nil
    )
    actionSheet.addButton(withTitle: "Take Photo")
    actionSheet.addButton(withTitle: "Camera Roll")
    actionSheet.addButton(withTitle: "Cancel")
    actionSheet.cancelButtonIndex = 2
    actionSheet.actionSheetStyle = UIActionSheetStyle.blackTranslucent
    actionSheet.show(in: self.view)
  }
  
  // MARK: - Methods -
  
  func next()
  {
    self.navigationController?.popViewController(animated: true)
  }
  
  func validate() -> Bool
  {
    if imageView.image == nil
    {
      showMessageError("Alert", text: "Add a profile photo so others can see you smile!", callback: { (type: NoInternetResultType) in
        
      })
      
      return false;
    }
    
    return true
  }
}
