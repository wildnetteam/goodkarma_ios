//
//  RecieveAudioTableViewCell.swift
//  Bizzalley
//
//  Created by Anuj Jha on 13/04/17.
//  Copyright © 2017 Anuj Jha. All rights reserved.
//

import UIKit
import AVFoundation


protocol playAudioDelegate {
    func playAudio(_ tag: Int)
}

class RecieveAudioTableViewCell: UITableViewCell {

    @IBOutlet weak var bgViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userNameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
     @IBOutlet weak var loaderView: InstagramActivityIndicator!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var audioDurationLabel: UILabel!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    var delegate: playAudioDelegate?
    //var deleteImageDelegate: deleteMsgDelegate?
    var infoDict:Message?
    var timer:Timer?
    var seconds:Int = 0
    var player:AVAudioPlayer?
    
    var downloadTask:URLSessionDownloadTask?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.progressView.progress = 0.0
        innerView.layer.cornerRadius = 15
        //recieveMsgBackgroundView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        cornerView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
        innerView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
    }
    
    func setLoaderUI(){
        loaderView.lineWidth = 5
        //loaderView.strokeColor = UIColor.blue
        loaderView.numSegments = 15
        loaderView.rotationDuration = 10;
        loaderView.animationDuration = 1.0;
        loaderView.startAnimating()
    }

    @IBAction func playAudioButtonTapped(_ sender: UIButton) {
        if delegate != nil{
            delegate?.playAudio(sender.tag)
        }
        DispatchQueue.main.async {
            self.playButton.isSelected = false
        }
        
        if player != nil && (player?.isPlaying)! {
            
            if self.timer != nil {
                self.timer?.invalidate()
            }
            if downloadTask != nil {
                downloadTask?.cancel()
            }
            player?.stop()
            self.progressView.progress = 0.0
            
        } else {
            downloadAndPlayAudioFromURL()
        }
    }
    
    func downloadAndPlayAudioFromURL(){
         setLoaderUI()
        let urlStr  = infoDict?.audioURL
        let url = NSURL(string: urlStr!)
        //print("the url = \(url!)")
        weak var weakSelf = self
        
        player?.stop()
        downloadTask?.cancel()
        //self.timer?.invalidate()
        //self.progressView.progress = 0.0
        
        downloadTask = URLSession.shared.downloadTask(with: url! as URL, completionHandler: { (URL, response, error) -> Void in
            
            if error == nil {
                weakSelf!.play(url: URL! as NSURL)
            }
            
        })
        
        downloadTask?.resume()
    }
    
    func moveSlider(){
        DispatchQueue.main.async {
            self.loaderView.stopAnimating()
            self.progressView.progress = 0.0
            if #available(iOS 10.0, *) {
                self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timer) in
                    self.progressView.progress += Float(1.0) / Float(self.seconds * 10)
                    if self.progressView.progress == 1.0 {
                        DispatchQueue.main.async {
                        timer.invalidate()
                         self.progressView.progress = 0.0
                        self.playButton.isSelected = false
                        }
                    }
                })
            } else {
                // Fallback on earlier versions
            }  
        }
        

    }
    
    func play(url:NSURL) {
        //print("playing \(url)")
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
        } catch {
        }
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url as URL)
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
            DispatchQueue.main.async {
                self.playButton.isSelected = true

            }
                        moveSlider()
        } catch _ as NSError {
            //self.player = nil
            //print(error.localizedDescription)
        } catch {
            //print("AVAudioPlayer init failed")
        }
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func deleteMessageActionTapped(sender:Any){
//        if deleteImageDelegate != nil{
//            deleteImageDelegate?.deleteMsg()
//        }
        
    }
    
}
