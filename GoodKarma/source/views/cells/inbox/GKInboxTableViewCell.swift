//
//  GKInboxTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/14/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKInboxTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
  @IBOutlet weak var unreadCountLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var unreadCountView: UIView!
    @IBOutlet weak var cellContentView: UIView!
  //    @IBOutlet weak var elapsedLabel: UILabel!
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
    //        self.pictureImageView.layer.cornerRadius = CGRectGetWidth(self.pictureImageView.frame)/2
    //        self.pictureImageView.layer.borderColor = UIColor.colorToPercent(186, green:196, blue:209).CGColor
    //        self.pictureImageView.layer.borderWidth = 1.3
    //        self.pictureImageView.activityIndicator.transform =
    //            CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7)
    
    // State
    self.userImage.layer.cornerRadius = self.userImage.frame.width * 0.5
    self.unreadCountView.layer.cornerRadius = self.unreadCountView.frame.width * 0.5
    unreadCountView.clipsToBounds = true
    unreadCountView.layer.masksToBounds = true
    self.selectionStyle = .none
  }
  
  func loadData(_ data: GKDictionary)
  {
    // self.elapsedLabel.text = Common.vetSimpleTime(date)
    
    self.nameLabel.text = data["another_user"]!["full_name"] as? String
    self.messageLabel.text = (data["detail"] as? String)?.decodeEmoji()
    setIsNewMessage(!(data["viewed"] as! Bool))
  }
  
  fileprivate func setIsNewMessage(_ isNewMessage: Bool)
  {
    if (isNewMessage)
    {
      //self.stateView.isHidden = false
      self.messageLabel.font = UIFont.fontRoboto(.Medium, size: 13)
      self.messageLabel.textColor = UIColor.black
    }
    else
    {
      //self.stateView.isHidden = true
      self.messageLabel.font = UIFont.fontRoboto(.Regular, size: 13)
      self.messageLabel.textColor = UIColor.gray
    }
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKInboxTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 100.0
  }
  
}
