//
//  GKViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/20/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKViewController: UIViewController
{

//  var screen: CGRect!
//  var visibleFrame: CGRect!
  
  //Bar Buttons
//  var menuButtonItem: UIBarButtonItem!
  
  //Remove notifications
  
  deinit{
    
    NotificationCenter.default.removeObserver(self)
  }
  
  override func loadView()
  {
    super.loadView()
    
//    screen = UIScreen.mainScreen().bounds
//    visibleFrame = CGRectMake(
//      0.0, 20.0 + 44.0,
//      screen.size.width,
//      screen.size.height - (20.0 + 40.0))
//    view = UIView(frame: screen)
//    self.view.backgroundColor = UIColor.whiteColor()
    
    //Navigation Controller
    
    self.navigationController?.navigationBar.barTintColor = UIColor.white
    self.navigationController?.navigationBar.tintColor = UIColor.gray
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.black]
    
    //Back Button
    cleanBackButton()
    
//    let menuBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//    menuBtn.setImage(UIImage(named: "menu-ico"), forState: UIControlState.Normal)
//    menuBtn.addTarget(self, action: #selector(GKViewController.menu), forControlEvents: UIControlEvents.TouchUpInside)
//    menuButtonItem = UIBarButtonItem(customView: menuBtn)
  }
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
  }
  
  //MARK: - Methods -
  
  //MARK: - Bar Buttons -
//  func enableMenuButton(){
//    self.navigationItem.leftBarButtonItem = menuButtonItem;
//  }

}
