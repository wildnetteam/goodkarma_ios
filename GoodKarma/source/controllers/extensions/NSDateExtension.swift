//
//  UIDateExtension.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

extension Date {
  
  // Convert UTC (or GMT) to local time
  func toLocalTime() -> Date
  {
    let timezone: TimeZone = TimeZone.autoupdatingCurrent
    let seconds: TimeInterval = TimeInterval(timezone.secondsFromGMT(for: self))
    return Date(timeInterval: seconds, since: self)
  }
  
  func toGlobalTime() -> Date
  {
    let timezone: TimeZone = TimeZone.autoupdatingCurrent
    let seconds: TimeInterval = -TimeInterval(timezone.secondsFromGMT(for: self))
    return Date(timeInterval: seconds, since: self)
  }
  
  static func parseDateWhitFormat(_ date_str: String) -> String? {
    // Set date with user key
    let date_formatter = DateFormatter()
    date_formatter.locale = Locale(identifier: "en_US_POSIX")
    date_formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    var date_formatted: String?
    
    if let date = date_formatter.date(from: date_str) {
      // TODO: update dates
//      if NSDate().daysFrom(date) >= 6 {
        date_formatter.dateFormat = "MM/dd/yyyy"
//      }
//      else if NSDate().daysFrom(date) >= 2 {
//        date_formatter.dateFormat = "eeee"
//      }
//      else if NSDate().daysFrom(date) >= 1 {
//        return "yesterday"
//      }
//      else {
//        date_formatter.dateFormat = "hh:mm a"
//      }
      date_formatted = date_formatter.string(from: date)
    }
    return date_formatted
  }
  
  static func stringToDate(_ date_str: String) -> Date {
    let date_formatter = DateFormatter()
    date_formatter.locale = Locale(identifier: "en_US_POSIX")
    date_formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    return date_formatter.date(from: date_str)!
  }
    
    func differenceInDaysWithDate(_ date: Date) -> Int {
        let calendar: Calendar = Calendar.current
        
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: date)
        
        let components = (calendar as NSCalendar).components(.day, from: date1, to: date2, options: [])
        return components.day!
    }
  
}
