//
//  AudioManager.swift
//  SnatchApp
//
//  Created by Uday Pratap Singh on 30/05/16.
//  Copyright © 2016 Snatch. All rights reserved.
//

import UIKit
import AVFoundation

class AudioManager: NSObject, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    var engine : AVAudioEngine?
    var pitchPlayer : AVAudioPlayerNode?
    var pitch : AVAudioUnitTimePitch?
    var recordingSession =  AVAudioSession()
    var audioRecorder: AVAudioRecorder!
    var player:AVAudioPlayer?
    var buffer: AVAudioPCMBuffer!
    static let audioInstance = AudioManager()
    
    let settings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 12000.0,
        AVNumberOfChannelsKey: 1 as NSNumber,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ] as [String : Any]
    
    
    class var sharedInstance: AudioManager  {
        return audioInstance
    }
    
    
    func recordingSetup(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] (allowed: Bool) -> Void in
                DispatchQueue.main.async {
                    if allowed {
                        do {
                            
                            self.audioRecorder = try AVAudioRecorder(url: self.directoryURL()!, settings: self.settings)
                            self.audioRecorder.delegate = self
                            self.audioRecorder.prepareToRecord()
                            self.audioRecorder.record()
                            
                        } catch {
                            
                        }
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }
    
    func askingPermission()
    {
    AVAudioSession.sharedInstance().requestRecordPermission { (accept) in
        
        }
    
    }
    
    func finishRecording(success: Bool) -> Bool {
        audioRecorder.stop()
        if success {
            return true
        } else {
            return false
        }
    }
    
    
    func directoryURL() -> URL? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        // print("audio file path = \(path)")
        let url = URL(fileURLWithPath: path)
        let audioURL = url.appendingPathComponent("recording.3gp")
        return audioURL
    }
    
    func playAudio(_ directoryUrl:URL)
    {
        
        let task = URLSession.shared.dataTask(with: directoryUrl) {(data, response, error) in
            
            //print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue))
            
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                self.player = try AVAudioPlayer(data: data!)
                self.player?.delegate = self
                self.player?.prepareToPlay()
                self.player!.play()
            }
            catch {
                //print("Something bad happened. Try catching specific errors to narrow things down")
            }
        }
        
        task.resume()
        
        
        
    }
    
    func stopAudio(){
        if player != nil {
            player!.stop()
            player!.currentTime = 0
        }
    }
    
    func removeOldFileIfExist() {
        /*let path = CommonUtilities.getDocumentsDirectory()
        let fileName = "recording.3gp"
        let filePath = NSString(format:"%@/%@", path, fileName) as String
        if FileManager.default.fileExists(atPath: filePath) {
            do {
                try FileManager.default.removeItem(atPath: filePath)
                // print("old image has been removed")
            } catch {
                // print("an error during a removing")
            }
        }*/
    }
    
    func pichChanged(_ directoryUrl:URL,pitchValue:Float,volumeValue:Float, messageId: String){
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        } catch {
            
        }
        engine = AVAudioEngine()
        pitch = AVAudioUnitTimePitch()
        pitchPlayer = AVAudioPlayerNode()
        
        let file = try? AVAudioFile(forReading:directoryUrl)
        buffer = AVAudioPCMBuffer(pcmFormat: file!.processingFormat, frameCapacity: AVAudioFrameCount(file!.length))
        do {
            try file!.read(into: buffer)
        } catch _ {
        }
        
        // print(pitchValue)
        pitch?.pitch = (pitchValue * 100) - 600
        pitch?.rate = 0.4 + (0.1 * pitchValue)
        engine!.attach(pitchPlayer!)
        engine!.attach(pitch!)
        engine!.connect(pitchPlayer!, to: pitch!, format: buffer.format)
        engine!.connect(pitch!, to: engine!.mainMixerNode, format: buffer.format)
        engine!.prepare()
        do {
            try engine!.start()
        } catch _ {
        }
        pitchPlayer!.scheduleFile(file!, at: nil, completionHandler: {
            let userInfo = ["messageId": messageId]
           // NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAudioButtonColour), object: nil, userInfo: userInfo)
           // NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeGroupAudioButtonColour), object: nil, userInfo: userInfo)
        })
        
        pitchPlayer!.play()
        
    }
    func stopAudio(_ directoryUrl:URL,pitchValue:Float,volumeValue:Float, messageId: String){
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        } catch {
            
        }
        engine = AVAudioEngine()
        pitch = AVAudioUnitTimePitch()
        pitchPlayer = AVAudioPlayerNode()
        
        let file = try? AVAudioFile(forReading:directoryUrl)
        buffer = AVAudioPCMBuffer(pcmFormat: file!.processingFormat, frameCapacity: AVAudioFrameCount(file!.length))
        do {
            try file!.read(into: buffer)
        } catch _ {
        }
        
        // print(pitchValue)
        pitch?.pitch = (pitchValue * 100) - 600
        pitch?.rate = 0.4 + (0.1 * pitchValue)
        engine!.attach(pitchPlayer!)
        engine!.attach(pitch!)
        engine!.connect(pitchPlayer!, to: pitch!, format: buffer.format)
        engine!.connect(pitch!, to: engine!.mainMixerNode, format: buffer.format)
        engine!.prepare()
        do {
            try engine!.start()
        } catch _ {
        }
        pitchPlayer!.scheduleFile(file!, at: nil, completionHandler: {
            let userInfo = ["messageId": messageId]
           // NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAudioButtonColour), object: nil, userInfo: userInfo)
             //NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeGroupAudioButtonColour), object: nil, userInfo: userInfo)
        })
        
        pitchPlayer!.stop()
        
    }
    
    func stopAndDeattacheNodes() {
        if pitchPlayer != nil {
            pitchPlayer!.stop()
            pitchPlayer!.reset()
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        //NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAudioButtonImage), object: nil)
    }
    
    //func audioPlayerEndInterruption(_ player: AVAudioPlayer) {
        //        NSNotificationCenter.defaultCenter().postNotificationName(ChangeAudioButtonColour, object: nil)
        //        AlertManager.showAlertWithTitle(ALERT_TITLE, message: "Error Playing")
    //}
}
