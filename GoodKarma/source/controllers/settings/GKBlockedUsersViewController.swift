//
//  GKBlockedUsersViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 10/6/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKBlockedUsersViewController: GKViewController, GKUnblockDelegate, UITableViewDataSource, UITableViewDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var emptyLabel: UILabel!
  
  // MARK: - Properties -
  
  var blockedUsers: [GKDictionary] = []
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Blocked Users"
    
    self.tableView.register(
      UINib(nibName: GKBlockedUserTableViewCell.cellIdentifier(),bundle: nil),
      forCellReuseIdentifier: GKBlockedUserTableViewCell.cellIdentifier()
    )
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    
    // Load Data
    reloadData()
    
    requestUsersList()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol GKUnblockDelegate -

  func GKUnblock(_ userID: Int)
  {
    print("UNBLOCk")
    
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["recipient"] = userID as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.profileBlock(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
        print(response!)

      if let res:AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
          self.requestUsersList()
        }
      }
    })
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return self.blockedUsers.count
  }
  
    func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(
      withIdentifier: GKBlockedUserTableViewCell.cellIdentifier(), for: indexPath
      ) as! GKBlockedUserTableViewCell
    
    cell.delegate = self
    cell.loadData(self.blockedUsers[indexPath.row]["gkarm_user"] as! GKDictionary)
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
//  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
//  {
//    let detail = self.notifications[indexPath.row]["detail"] as! String
//    
//    GKMoreDetailView.show(detail, buttonTitle: "View post")
//    {
//      let notification = self.notifications[indexPath.row]
//      let dataPost = notification["post"] as! GKDictionary
//      let typePost = notification["post_type"] as! Int
//      
//      self.appDelegate().showHistoryRequestedPost(dataPost, typePost: typePost)
//    }
//  }
  
    func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return GKBlockedUserTableViewCell.heightForCell()
  }
  
  // MARK: - Methods -
  
  func checkEmptyState(_ count: Int)
  {
    emptyLabel.isHidden = (count != 0)
    tableView.isHidden  = (count == 0)
  }
  
  func reloadData()
  {
    self.tableView.reloadData()
  }
  
  func requestUsersList()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    GKServiceClient.call(GKRouterClient.settingsBlockedUser(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      if let res:AnyObject = response
      {
      //  let envelope = ResponseSkeleton(res)
        let envelope = ResponseSkeleton(res , string : "Block")
        if envelope.getSuccess()
        {
          self.blockedUsers = envelope.getResource() as! [GKDictionary]

          self.checkEmptyState(self.blockedUsers.count)
          self.reloadData()
        }
      }
    })
  }
}
