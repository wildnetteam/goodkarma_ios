//
//  GKNotificationPreferencesViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/9/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNotificationPreferencesViewController: GKViewController, UITableViewDataSource, UITableViewDelegate
{
  // MARK: - IBOutlets -
  
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Properties -
  
  // Title, Value, Parameter name
  var notificationsOptions: [(String, Bool, String)] = []
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = "Notifications"
    
    let cancelBarButton = UIBarButtonItem(
      title: "Cancel", style: .plain, target: self, action: #selector(self.cancel)
    )
    //    cancelBarButton.tintColor = UIColor.baseColor()
    self.navigationItem.leftBarButtonItem = cancelBarButton
    
    let saveBarButton = UIBarButtonItem(
      title: "Save", style: .plain, target: self, action: #selector(self.save)
    )
    //    saveBarButton.tintColor = UIColor.baseColor()
    self.navigationItem.rightBarButtonItem = saveBarButton
    
    self.tableView.register(
      UINib(nibName: GKNotificationPreferenceTableViewCell.cellIdentifier(),bundle: nil),
      forCellReuseIdentifier: GKNotificationPreferenceTableViewCell.cellIdentifier()
    )
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    //    self.tableView.separatorStyle = .None
    
    loadNotificationsOptions()
    requestData()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return notificationsOptions.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(withIdentifier: GKNotificationPreferenceTableViewCell.cellIdentifier(), for: indexPath) as! GKNotificationPreferenceTableViewCell
    
    cell.descLabel.text = (self.notificationsOptions[indexPath.row]).0
    cell.loadSwitch((self.notificationsOptions[indexPath.row]).1)
    
    cell.cellSwitch.tag = indexPath.row
    cell.cellSwitch.addTarget(self, action: #selector(self.changeSwitchValue(_:)) , for: .valueChanged)
    
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return GKNotificationPreferenceTableViewCell.heightForCell()
  }
  
  // MARK: - Methods -
  
  @objc func changeSwitchValue(_ switchControl: UISwitch)
  {
    if switchControl.tag == 0
    {
      for (index, _) in self.notificationsOptions.enumerated()
      {
        self.notificationsOptions[index].1 = switchControl.isOn
      }
    }
    else
    {
      // Check if "first on" or "last off"
      var allOff = true
      
      for (index, _) in self.notificationsOptions.enumerated()
      {
        if !(index == 0 || index == switchControl.tag)
        {
          allOff = (allOff && !self.notificationsOptions[index].1)
        }
      }
      
      if allOff
      {
        // Update first switch control "allow notifications"
        self.notificationsOptions[0].1 = switchControl.isOn
      }
      
      self.notificationsOptions[switchControl.tag].1 = switchControl.isOn
    }
    
    self.tableView.reloadData()
  }
  
  @objc func cancel()
  {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
  
  func requestData()
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.settingsNotifications(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response
      {
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess()
        {
          if let data = envelope.getResource() as? GKDictionary {
            
            for (index, notificationOption) in self.notificationsOptions.enumerated()
            {
                let dataValue: Bool! = data[notificationOption.2] as? Bool
                
                if (dataValue != nil)
                {
                    self.notificationsOptions[index].1 = dataValue
                }
            }
            
            self.tableView.reloadData()
          }
        }
      }
    })
  }
  
  @objc func save()
  {
    var params = GKDictionary()
    params["token"]             = Static.sharedInstance.token() as AnyObject?
    
    for preference in notificationsOptions
    {
        let value : Int!
     if preference.1
     {
        value = 1
        }
        else
     {
        value = 0
        }
        
        
      params[preference.2] = value as AnyObject?
    }
    
    LoadingView.showLoadingView(
      "Loading...",
      parentView: self.navigationController?.view, backColor: true
    )
    
    GKServiceClient.call(GKRouterClient.settingsNotifications(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          self.navigationController?.dismiss(animated: true, completion: nil)
        }
      }
    })
  }
  
  func loadNotificationsOptions()
  {
    notificationsOptions = [
      ("Allow notifications",        false, "allow_notification"),
      ("New Messages",               false, "new_message"),
      ("New Connections",            false, "new_connection"),
      ("Facebook friend has joined", false, "friend_joined"),
      ("Due date reminder",          false, "message_due_date"),
      ("Reminder to close post",     false, "reminder_end_project"),
      ("Likes on my posts",          false, "allow_like_notification")
    ]
  }
}
