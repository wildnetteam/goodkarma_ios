//
//  GKWalkthroughView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/20/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKWalkthroughView: UIView {
    
    @IBOutlet weak var backgroundImageView : UIImageView!
    @IBOutlet weak var frontImageView      : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var descLabel  : UILabel!
    @IBOutlet weak var descTopConstraint: NSLayoutConstraint!
    
    func setupTitle(_ title: String, desc: String, index: Int) {
        
        titleLabel.text = title
        descLabel.text = desc
        
        backgroundImageView.image = GKWalkthroughView.getBackgroundImage(index)
        frontImageView.image = GKWalkthroughView.getFrontImage(index)
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            descTopConstraint.constant = 4
            descLabel.font = UIFont(name: "Roboto", size: 12)
        }
    }
    
    static func getBackgroundImage(_ index: Int) -> UIImage? {
        
        return UIImage(named: "intro-\(index)-background")
    }
    
    static func getFrontImage(_ index: Int) -> UIImage? {
        
        return UIImage(named: "intro-\(index)-phone")
    }
    
    
}
