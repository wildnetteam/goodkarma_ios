//
//  GKMyJobTableViewCell.swift
//  GoodKarma
//
//  Created by Shivani on 10/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyJobTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
   
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIButton!

    @IBOutlet weak var profileImageWebView: WebImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        
        profileImageWebView.layer.cornerRadius = profileImageWebView.frame.size.width / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

       
    }
    internal class func heightForCell() -> CGFloat {
        
        return 149.0
    }
    
    func loadData(_ data: AnyObject)
    {
        let info : String!
        let value : GKDictionary!
        let valueExists = data["job_assigned_by"] as? GKDictionary != nil
        if valueExists
        {
            value = data["job_assigned_by"]  as! GKDictionary
            info  = " has assigned you a new job"
            
        }
        else
        {
            value = data["job_assigned_to"] as! GKDictionary
            info = " has assigned a job by you"
        }

        //username
        let first_name = (value["first_name"] as! String).capitalized + " "
        
        let last_name = (value["last_name"] as! String).capitalized
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.darkGray ]
        let myAttrString = NSAttributedString(string: info, attributes: myAttribute)
        
        
        let first_nameAtrributedString = NSAttributedString(string: first_name, attributes: nil)
        
       
        let last_nameAttributedString = NSAttributedString(string: last_name, attributes: nil)
        
        let valueMessage = NSMutableAttributedString()
        valueMessage.append(first_nameAtrributedString)
        valueMessage.append(last_nameAttributedString)
        valueMessage.append(myAttrString)
        userNameLabel.attributedText = valueMessage
     //   let tempString  = attributedFirstName.app
        
        self.timeLabel.text = secondsToFormatted(data["createdAt"] as? Int)
        
        //profile image
        self.profileImageWebView.loadImageWithUrl(value["photo_url"] as? String as NSString?)
        
      
        //title
        self.helpLabel.text = data.object(forKey: "title") as? String
        
        
        //date
        
        let dateString = showDate(actualString: (data["need_by"] as? String)!)
        self.dateLabel.text = dateString
  
    }
}
