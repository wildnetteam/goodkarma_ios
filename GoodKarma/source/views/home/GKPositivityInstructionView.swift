//
//  GKPositivityInstructionView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 10/19/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKPositivityInstructionView: UIView {
  
  @IBOutlet var view: UIView!
  @IBOutlet weak var circleView: UIView!
  @IBOutlet weak var whiteView: UIView!
  @IBOutlet weak var bubbleView: UIView!
  @IBOutlet weak var okButton: UIButton!
  
  var typeInstructions: GKInstructionsType!
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)!
    self.setup()
  }
  
  fileprivate func setup()
  {
    // View
    view = Bundle.main.loadNibNamed("GKPositivityInstructionView", owner: self,
                                              options: nil)?[0] as! UIView
    view.frame = self.bounds
    view.backgroundColor = UIColor.clear
    
    self.addSubview(view)
    self.alpha = 0
    self.backgroundColor = UIColor.clear
    
    // Circle View
    self.circleView.layer.cornerRadius = self.circleView.frame.height / 2
    self.circleView.backgroundColor = UIColor.white
    
    // White View
    self.whiteView.layer.cornerRadius = 3
    
    // Bubble View
    self.bubbleView.layer.anchorPoint = CGPoint(x: 0, y: 0)
    self.bubbleView.frame = CGRectSetPos(
      self.bubbleView.frame,
      x: self.bubbleView.frame.minX - self.bubbleView.frame.width/2 ,
      y: self.bubbleView.frame.minY - self.bubbleView.frame.height/2
    )
    self.bubbleView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
    self.bubbleView.isHidden = true
    
    // OK Button
    self.okButton.layer.cornerRadius = 4
  }
  
  func show()
  {
    UIView.animate(withDuration: 0.5, delay:0,
                               options:UIViewAnimationOptions(),
                               animations:
      {
        self.alpha = 1
      },
                               completion:
      { _ in
        self.showBubble()
    })
  }
  
  func showBubble()
  {
    self.bubbleView.isHidden = false
    
    UIView.animate(withDuration: 0.3, delay:0,
                               options:UIViewAnimationOptions.curveEaseOut,
                               animations:
      {
        self.bubbleView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
      },
                               completion:
      { _ in
    })
  }
  
  func hide()
  {
    UIView.animate(withDuration: 0.5, delay:0,
                               options:UIViewAnimationOptions(),
                               animations:
      {
        self.alpha = 0
      },
                               completion:
      { _ in
        self.removeFromSuperview()
    })
  }
  
  // MARK: - Actions
  
  @IBAction func notShowAgainAction(_ sender: AnyObject)
  {
    Static.sharedInstance.saveShowPositivityInstructions()
    
    hide()
  }
  
  @IBAction func okAction(_ sender: AnyObject)
  {
    hide()
  }
  
  @IBAction func closeAction(_ sender: AnyObject)
  {
    hide()
  }

}
