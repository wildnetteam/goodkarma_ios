//
//  ErrorView.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

class ErrorView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    fileprivate func setup()
    {
        view = Bundle.main.loadNibNamed("ErrorView", owner: self,
            options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(view)
        
        self.backgroundColor = UIColor.clear
        self.isHidden = true
    }
    
    fileprivate func setAnchorPoint(_ anchorPoint: CGPoint)
    {
        let oldOrigin = self.frame.origin
        self.layer.anchorPoint = anchorPoint
        let newOrigin = self.frame.origin
        
        var transition : CGPoint = CGPoint(x: 0, y: 0)
        transition.x = newOrigin.x - oldOrigin.x
        transition.y = newOrigin.y - oldOrigin.y
        
        self.center = CGPoint(x: self.center.x - transition.x,
            y: self.center.y - transition.y)
    }
    
    func showInPos(_ point: CGPoint, text: NSString)
    {
        if (self.isHidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPoint(x: 0.2, y: 1))
            self.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.isHidden = false
            
            UIView.animate(withDuration: 0.3, delay:0,
                options:UIViewAnimationOptions(),
                animations:
                {
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func showInPosFromCenter(_ point: CGPoint, text: NSString)
    {
        if (self.isHidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view-center")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPoint(x: 0.5, y: 1))
            self.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.isHidden = false
            
            UIView.animate(withDuration: 0.3, delay:0,
                options:UIViewAnimationOptions(),
                animations:
                {
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func showInPosFromRight(_ point: CGPoint, text: NSString)
    {
        if (self.isHidden)
        {
            self.imageView.image = UIImage(named:"bg-error-view-right")
            self.textLabel.text = text as String
            self.frame = CGRectSetPos(self.frame, x: point.x, y: point.y)
            
            setAnchorPoint(CGPoint(x: 0.8, y: 1))
            self.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.isHidden = false
            
            UIView.animate(withDuration: 0.3, delay:0,
                options:UIViewAnimationOptions(),
                animations:
                {
                    self.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                },
                completion:
                { _ in
            })
        }
    }
    
    func hide()
    {
        self.endEditing(true)
        
        if (!self.isHidden)
        {
            UIView.animate(withDuration: 0.3, delay:0,
                options:UIViewAnimationOptions(),
                animations:
                {
                    self.alpha = 0
                },
                completion:
                { _ in
                    self.isHidden = true
                    self.alpha = 1
            })
        }
    }
  
  
  func hideWithoutAnimation()
  {
    self.endEditing(true)
    
    if (!self.isHidden)
    {
      self.isHidden = true
    }
  }
}
