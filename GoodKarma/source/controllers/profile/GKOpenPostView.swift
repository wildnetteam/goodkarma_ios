//
//  GKOpenPostView.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 8/2/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

protocol OpenPostViewDelegate: NSObjectProtocol
{
    func openPostViewDidTap()
}

class GKOpenPostView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var button: UIButton!
    weak var delegate: OpenPostViewDelegate!
    var visible = false
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    fileprivate func setup()
    {
        // View
        view = Bundle.main.loadNibNamed("GKOpenPostView", owner: self,
                                                  options: nil)?[0] as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        self.backgroundColor = UIColor.clear
        self.alpha = 0
    }
    
    func show(_ title: String)
    {
        self.button.setTitle(title, for: UIControlState())
        
        if (!self.visible)
        {
            self.frame = CGRectSetY(self.frame, y: 0)
            self.alpha = 1
            
            UIView.animate(withDuration: 0.5, delay:0,
                                       options:UIViewAnimationOptions(),
                                       animations:
                {
                    self.frame = CGRectSetY(self.frame, y: 64)
                },
                                       completion:
                { _ in
                    self.visible = true
            })
        }
    }
    
    func hide()
    {
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                self.alpha = 0
            },
                                   completion:
            { _ in
                self.frame = CGRectSetY(self.frame, y: 0)
                self.visible = false
        })
    }
    
    // MARK: - Actions
    
    @IBAction func buttonAction(_ sender: AnyObject)
    {
        self.delegate.openPostViewDidTap()
    }
}
