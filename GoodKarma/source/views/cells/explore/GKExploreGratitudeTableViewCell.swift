//
//  GKExploreGratitudeTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/22/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import AFNetworking

class GKExploreGratitudeTableViewCell: UITableViewCell, NIAttributedLabelDelegate {
  
  // MARK: - IBOutlets -
  
    @IBOutlet weak var hugButton: UIButton!
    @IBOutlet weak var pictureImageView : WebImageView!
    @IBOutlet weak var titleLabel: NIAttributedLabel!
    @IBOutlet weak var timestampLabel: UILabel!
  
    @IBOutlet weak var likeButton : UIButton!
    @IBOutlet weak var likeCountLabel : UILabel!
    @IBOutlet weak var messageButton: UIButton!
    
    @IBOutlet weak var buttonShare: UIButton!
    
    @IBOutlet weak var collectionViewPost: UICollectionView!
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lowerViewTopConstarint: NSLayoutConstraint!
    
    static var isImages = Bool()
    
    // MARK: - Properties -
  
    var data: GKDictionary!
    var delegate: (GKShowUserProfileDelegate & GKLikePostDelegate & GKStartMessageDelegate & GKHugDelegate & GKShareDelegate)?
    var popTip: AMPopTip!
    
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
    self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
    self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    self.pictureImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
    self.pictureImageView.isUserInteractionEnabled = true
    
    self.selectionStyle = .none
    
    // Title Label
    self.titleLabel.delegate = self
    self.titleLabel.isUserInteractionEnabled = true
    
    // PopTip
    self.popTip = AMPopTip()
    self.popTip.popoverColor = UIColor.black
    
    let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
    collectionViewPost.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
  }
  
  // MARK: - IBActions -
  
  @IBAction func sendMessage(_ sender: AnyObject) {
    
    let authorData = data["author"] as! GKDictionary
    
    let userID = authorData["id"] as! Int
    let userName = "\(authorData["first_name"] as! String)".capitalized + "\(authorData["last_name"] as! String)".capitalized
    
    delegate?.GKSendMessage(userID, fullName: userName)
  }
  
  @IBAction func sendLike(_ sender: AnyObject) {
    
    let postID = self.data["id"] as! Int
    
    self.delegate?.GKSendLike(4, postID: postID)
  }
    
    @IBAction func tooltipAction(_ sender: AnyObject)
    {
        popTip.showText("Gratitude", direction: .left, maxWidth: 200, in: self,
                        fromFrame: (sender as! UIView).frame, duration: 2)
    }
    
    @IBAction func buttonShareClicked(_ sender: Any) {
        if delegate != nil
        {
            delegate?.GKSharePost(share_text: self.titleLabel.attributedText!)
        }
    }
    
    @IBAction func hugAction(_ sender: UIButton)
    {
        let author = data["author"] as AnyObject
        let first_name = author["first_name"] as! String
        let last_name = author["last_name"] as! String
        let full_name = "\(first_name)".capitalized + " " + "\(last_name)".capitalized
        let authorData = data["author"] as! GKDictionary
        let userID = authorData["id"] as! Int
        let postID = self.data["id"] as! Int
        let postType = data["post_type"]
        if delegate != nil
        {
            delegate?.GKPassHug(full_name : full_name , recipientId : userID , authorId :  Static.sharedInstance.userId() , positivityId : postID , post_type : postType as! Int)
        }
    }
    
    // MARK: - UICollectionView Methods
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewPost.delegate = dataSourceDelegate
        collectionViewPost.dataSource = dataSourceDelegate
        collectionViewPost.tag = row
        collectionViewPost.reloadData()
    }
    
    
//    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
//    {
//        return imageArray.count
//    }
//
//    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//    {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
//        cell.imageView.loadImageWithUrl( imageArray[indexPath.row] as? String as NSString?)
//        return cell
//    }
//
//    public func numberOfSections(in collectionView: UICollectionView) -> Int
//    {
//        return 1
//    }
    
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKExploreGratitudeTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    if isImages {
        return 320.0
    }
    return 320.0
  }
  
  // MARK: - Methods -
  
  func loadData(_ data: GKDictionary)
  {
    self.data = data
    print(self.data)
    self.popTip.hideForced(true)
    
    let author = data["author"] as! GKDictionary
    let fullName = "\(author["first_name"] as! String) ".capitalized + "\(author["last_name"] as! String): ".capitalized
    
    let nameAttrString = NSAttributedString(
      string: fullName,
      attributes: [
        NSAttributedStringKey.font: UIFont.fontRoboto(.Medium, size: 14),
        NSAttributedStringKey.foregroundColor: UIColor.black
      ]
    )
    let gratitudeAttrString = NSAttributedString(
      string: (data["desc"] as! String).decodeEmoji(),
      attributes: [
        NSAttributedStringKey.font: UIFont.fontRoboto(.Light, size: 14),
        NSAttributedStringKey.foregroundColor: UIColor.darkGray
      ]
    )
    
    let attrString = NSMutableAttributedString()
    attrString.append(nameAttrString)
    attrString.append(gratitudeAttrString)
    
    self.pictureImageView.loadImageWithUrl(author["photo_url"] as? String as NSString?)
    self.titleLabel.attributedText = attrString
    self.titleLabel.verticalTextAlignment = NIVerticalTextAlignment(1)
    self.titleLabel.addLink(URL(string:"0"),
                            range:NSMakeRange(0, fullName.count))
    self.titleLabel.linkColor = UIColor.black
    self.timestampLabel.text = secondsToFormatted(data["date_created"] as? Int)
    
    // Like
    self.updateLike(self.data["like_status"] as! Bool, likeCount: self.data["like_count"] as! Int)
    
    // Disable buttons
    let isMePost = Static.sharedInstance.isMe(author as NSDictionary)
    enabledView(self.messageButton, enabled: !isMePost)
     enabledView(self.hugButton, enabled: !isMePost)
  }
  
  @objc func tapImageView()
  {
    let authorData = self.data["author"] as! GKDictionary
    self.delegate?.GKShowProfile(authorData["id"] as! Int)
  }
  
  func updateLike(_ isLike: Bool, likeCount: Int)
  {
    likeCountLabel.text = "\(likeCount)"
    
    if isLike
    {
      likeButton.setImage(UIImage(named: "explore-bar-like-on"), for: UIControlState())
      likeCountLabel.textColor = UIColor.white
    }
    else
    {
      likeButton.setImage(UIImage(named: "explore-bar-like-off"), for: UIControlState())
      likeCountLabel.textColor = UIColor.red
    }
  }
    
    // MARK: - NIAttributedLabelDelegate -
    
    func attributedLabel(_ attributedLabel: NIAttributedLabel!, didSelect result: NSTextCheckingResult!, at point: CGPoint)
    {
        tapImageView()
    }
}
