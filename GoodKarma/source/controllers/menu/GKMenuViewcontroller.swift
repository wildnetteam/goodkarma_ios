//
//  GKMenuViewcontroller.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/20/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import Foundation
import AddressBookUI
import MessageUI
import FBSDKShareKit

// FIXME:varmparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


//enum GKMenuOptions: Int {
//  case Home
//  case Explore
//  case Messages
//  case Profile
//  case Friends
//  case Testimonials
//  case History
//  case Notifications
//  case Settings
//  case Log-out
//}

class GKMenuViewcontroller: GKViewController, ABPeoplePickerNavigationControllerDelegate, FBSDKAppInviteDialogDelegate, GKFriendSocialDelegate, MFMessageComposeViewControllerDelegate, UIGestureRecognizerDelegate {
    /*!
     @abstract Sent to the delegate when the app invite encounters an error.
     @param appInviteDialog The FBSDKAppInviteDialog that completed.
     @param error The error.
     */
    public func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print(error)
    }

    
    // MARK: - Properties -
    
    fileprivate var inviteView: GKInviteFriends!
    fileprivate var followFriends: GKFollowSocialFriendsView!
    fileprivate var menuIsVisible:Bool!
    fileprivate var metrics = GKMenuMetrics()
    
    // MARK: - Buttons -
    
    fileprivate var buttonArray = [MenuOptionButton]()
    fileprivate var menuButtonsContainer: UIScrollView!
    
    // MARK:  - Profile
    fileprivate var profileView: MenuProfile!
    
    //MARK:   - Navigations
    fileprivate var homeNavigation: UINavigationController!
    fileprivate var profileNavigation: UINavigationController!
    fileprivate var friendsNavigation: UINavigationController!
    fileprivate var messagesNavigation: UINavigationController!
    fileprivate var settingsNavigation: UINavigationController!
    fileprivate var aboutNavigation: UINavigationController!
    
    // MARK: - Menu Effect -
    
    fileprivate var menuWidth: CGFloat!
    fileprivate var panGesture: UIPanGestureRecognizer!
    
    // This disabled to touch on navigation view
    fileprivate var navigationOverlayView: UIView!
    fileprivate var viewAboveNavigationView : PGCView!
    
    // Values
    // When velocity is greater than this
    // do another behavior
    fileprivate var lastPoint = CGPoint.zero
    fileprivate var token: CGFloat!
    
    fileprivate var navigationView: PGCView!
    
    internal var currentDetailViewController: UIViewController!
    internal var currentController: String!
    
    // MARK: - Override UIViewController -
    
    override func loadView() {
        
        super.loadView()
        
        // Badge Count
        //    self.delegate().updateBadgeCount()
        //    appDelegate().updateBadgeCount()
        
        self.view.backgroundColor = UIColor.baseColor()
        
        let screen = PGCView.Metrics.screen
        
        menuWidth = screen.size.width * metrics.k41
        
        //    var originYMenuButton:CGFloat = 4.0 * metrics.padding
        
        // Gesture for swipe left menu
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(GKMenuViewcontroller.drag(_:)))
        panGesture.cancelsTouchesInView   = true
        panGesture.delegate               = self
        panGesture.maximumNumberOfTouches = 1
        view.addGestureRecognizer(panGesture)
        
        //Menu Options
        let buttonTitles = GKStoreMenu.menuTitles()
        
        menuButtonsContainer = UIScrollView(
            frame: CGRect(
                x: 0.0,
                y: 0.0,
                width: menuWidth,
                height: screen.size.height
            )
        )
        menuButtonsContainer.showsVerticalScrollIndicator   = false
        menuButtonsContainer.backgroundColor = UIColor.baseColor()
        view.addSubview(menuButtonsContainer)
        
        // Profile
        profileView = MenuProfile(
            frame: CGRect(
                x: 0.0, y: 30.0, width: menuButtonsContainer.bounds.size.width, height: 60.0
            )
        )
        menuButtonsContainer.addSubview(profileView)
        
        // Buttons
        let heightMenuButton: CGFloat = 50.0
        let widthMenuButton   = menuButtonsContainer.bounds.size.width
        var originYMenuButton = profileView.bottomSide
        
        for (index, title) in buttonTitles.enumerated() {
            //Logout
            if index != buttonTitles.count {
                let but = MenuOptionButton(
                    frame: CGRect(
                        x: 0.0,
                        y: originYMenuButton,
                        width: widthMenuButton,
                        height: heightMenuButton
                    ), index: index + 1
                )
                
                but.isEnabled = true
                but.tag = index + 1
                
                //ACTION MENU
                but.addTarget(self,
                              action: #selector(GKMenuViewcontroller.actionMenu(_:)),
                              for: UIControlEvents.touchUpInside)
                but.setTitle(title, for: UIControlState())
                
                // Hold all the menu buttons
                buttonArray.append(but)
                originYMenuButton += heightMenuButton
                
                menuButtonsContainer.addSubview(but)
            }
        }
        
        let contentHeight = buttonArray.last!.bottomSide
        menuButtonsContainer.contentSize = CGSize(width: menuWidth, height: contentHeight)
        
        // Navigation view
        // Here is where is going to put all the navigations
        navigationView = PGCView()
        navigationView.frame = screen
        view.addSubview(navigationView)
        
        navigationOverlayView = PGCView()
        navigationOverlayView.frame = screen
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(GKMenuViewcontroller.tappedOverlayView))
        tapGesture.cancelsTouchesInView = false
        navigationOverlayView.addGestureRecognizer(tapGesture)
        // First time is showing Home Navigation
        menuIsVisible = false
        
        let layer2 = navigationView.layer
        var rotationAndPerspectiveTransform = CATransform3DIdentity
        rotationAndPerspectiveTransform.m41 = self.menuWidth
        layer2.transform = rotationAndPerspectiveTransform
        
        showHome(false, recovery: false)
        
        listenToNotifications(GKNotificationStore.notification_kind_menu)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(GKMenuViewcontroller.changedProfileInSettingsObserver),
            name: NSNotification.Name(rawValue: "ChangedProfileInSettings"),
            object: nil
        )
    }
    
    // MARK: - Notifications -
    
    override func receiveFromNotificationStore(_ notification: Notification) {
        
        updateCounts()
    }
    
    // MARK: - Protocols
    // MARK: - Protocol ABPeoplePickerNavigationControllerDelegate -
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier)
    {
        peoplePicker.dismiss(animated: true, completion: nil)
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        let contact = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
        let nameCFString: CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
        let name: NSString = nameCFString as NSString
        let firstName = name.components(separatedBy: " ")[0]
        
        let userDic = NSMutableDictionary()
        userDic["name"] = name
        userDic["first_name"] = firstName
        userDic["contact"] = contact
        
        showSMS(userDic)
    }
    
    // MARK: - Protocol GKFriendSocialDelegate -
    
    func GKFriendSocialSkip() {
        
        self.closeFollowFriendsDialog()
    }
    
    // MARK: - Protocol MFMessageComposeViewControllerDelegate -
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController,
                                      didFinishWith result: MessageComposeResult)
    {
        //    let userInfo: NSDictionary! = controller.view.layer.valueForKey("data") as? NSDictionary
        
        switch result.rawValue
        {
        case MessageComposeResult.sent.rawValue :
            
            let text = NSString(format: "Your invitation has been successfully sent!")
            showAlert("Message sent", text: text as String, delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
            
            self.closeInviteDialog()
            
        case MessageComposeResult.cancelled.rawValue :
            NSLog("SMS Canceled")
            
        case MessageComposeResult.failed.rawValue :
            showConnectionError()
            
        default:
            NSLog("")
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Protocols FBSDKAppInviteDialogDelegate -
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable: Any]!) {
        
        closeInviteDialog()
    }
    
    func appInviteDialogg(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        //    closeInviteDialog()
    }
    
    // MARK: - Menu -
    
    
    @objc func tappedOverlayView() {
        if menuIsVisible! {
            hideMenuWithFactor(1.0)
        }
    }
    
    func presentDetailViewController(_ viewController: UIViewController) {
        if currentDetailViewController != nil {
            self.removeCurrentDetailViewController()
            currentDetailViewController = nil
        }
        
        addChildViewController(viewController)
        
        viewController.view.frame = self.frameForDetailViewController()
        
        navigationView.addSubview(viewController.view)
        //add view gray
        viewAboveNavigationView = PGCView()
        viewAboveNavigationView.frame = CGRect(x: 0, y: 0,
                                                   width: PGCView.Metrics.screen.width,
                                                   height: PGCView.Metrics.screen.height)
        viewAboveNavigationView.backgroundColor = UIColor.gray
        viewAboveNavigationView.alpha = 0.3
        navigationView.addSubview(viewAboveNavigationView)
        viewAboveNavigationView.isUserInteractionEnabled = false
        
        
        
        currentDetailViewController = viewController
        
        viewController.didMove(toParentViewController: self)
    }
    
    func frameForDetailViewController() -> CGRect {
        return navigationView.bounds
    }
    
    func removeCurrentDetailViewController() {
        // Call the willMoveToParentViewController with nil
        // This is the last method where your detailViewController
        // can perform some operations before neing removed
        currentDetailViewController.willMove(toParentViewController: nil)
        
        // Remove the DetailViewController's view from the Container
        currentDetailViewController.view.removeFromSuperview()
        
        // Update the hierarchy
        // Automatically the method didMoveToParentViewController:
        // will be called on the detailViewController)
        currentDetailViewController.removeFromParentViewController()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        /*if(self.currentController == "MessagesMainViewController")
         {
         println(gestureRecognizer.description)
         println(otherGestureRecognizer.description)
         
         return true
         }
         else
         {
         return false
         }*/
        
        if(self.currentController == "MessagesMainViewController")
        {
            let string = otherGestureRecognizer.description
            if ((string).range(of: "handleSwipeBeginning") != nil)
            {
                return true
            }
            else
            {
                return false
            }
            
            /*if (otherGestureRecognizer.view!.isKindOfClass(UITableView) == true)
             {
             return true
             }
             else
             {
             return false
             }*/
            
        }
        else
        {
            return false
        }
    }
    
    @objc func drag(_ gesture: UIPanGestureRecognizer) {
        let point = gesture.translation(in: self.view)
        
        if gesture.state == .began {
            
            if menuIsVisible! {
                token = menuWidth
            }
            else {
                token = 0.0
            }
            
            lastPoint.x = point.x
        }
        else if gesture.state == .changed {
            
            let currentPoint = point.x
            let moveX: CGFloat = lastPoint.x - currentPoint
            let percent = (menuWidth - token) / menuWidth
            
            // Top Profile
            var profileFrame = profileView.frame
            profileFrame.origin.x = -(profileFrame.size.width * percent)
            profileView.frame = profileFrame
            
            // Navigation View
            let factor = (1 - percent)
            var rotationAndPerspectiveTransform = CATransform3DIdentity
            rotationAndPerspectiveTransform.m41 = self.menuWidth * factor
            navigationView.layer.transform = rotationAndPerspectiveTransform
            
            // Button Container
            var rect   = self.menuButtonsContainer.frame
            rect.origin.x = -(menuWidth * percent)
            self.menuButtonsContainer.frame  = rect
            
            // Status Bar
            //      self.updateStatusBar(originX: menuWidth * factor)
            
            lastPoint.x = currentPoint
            
            token = token - moveX
            
            if token > menuWidth {
                token = menuWidth
            }
            else if token < 0.0 {
                token = 0.0
            }
            
        }
        else if gesture.state == .ended {
            
            let velocity = gesture.velocity(in: self.view)
            
            // Apply velocity
            if abs(velocity.x) > metrics.edgeSpeed {
                if velocity.x < 0 {
                    hideMenuWithFactor(abs(velocity.x) / metrics.edgeSpeed)
                }
                else {
                    showMenuWithFactor(abs(velocity.x) / metrics.edgeSpeed)
                }
            }
            else {
                // Default behavior when drag gesture is no longer present
                if menuButtonsContainer.frame.origin.x < -(menuWidth * 0.5) {
                    hideMenuWithFactor(1.0)
                }
                else {
                    showMenuWithFactor(1.0)
                }
            }
        }
        
    }
    
    func showMenuWithFactor(_ factor: CGFloat) {
        
        self.view.endEditing(true)
        //        updateCounts()
        //    requestMenuCount()
        
        var duration = 0.3
        
        if factor > 0 {
            duration /= Double(factor)
        }
        
        // Animate the detail view
        UIView.animate(withDuration: duration, animations: { () -> Void in
            
            // Top Profile
            var profileFrame = self.profileView.frame
            profileFrame.origin.x = 0.0
            self.profileView.frame = profileFrame
            
            // Button container
            var rect = self.menuButtonsContainer.frame
            rect.origin.x = 0.0
            self.menuButtonsContainer.frame  = rect
            
            // Navigation View
            let layer = self.navigationView.layer
            var rotationAndPerspectiveTransform = CATransform3DIdentity
            rotationAndPerspectiveTransform.m41 = self.menuWidth
            layer.transform = rotationAndPerspectiveTransform
            
            // Status Bar
            //      self.updateStatusBar(originX: self.menuWidth)
            
        }, completion: { (finished: Bool) -> Void in
            
            // Add detail view overlay to the detail view
            self.navigationView.addSubview(self.navigationOverlayView)
            self.menuIsVisible = true
            self.viewAboveNavigationView.isHidden = false
            self.setNeedsStatusBarAppearanceUpdate()
            
            self.view.addGestureRecognizer(self.panGesture)
        }) 
    }
    
    
    //    func updateStatusBar(originX originX: CGFloat) {
    //        if let winwin = UIApplication.sharedApplication().valueForKey("statusBarWindow") as? UIWindow {
    //            var frameWindow =  winwin.frame
    //            frameWindow.origin.x = originX
    //            winwin.frame = frameWindow
    //        }
    //    }
    
    func hideMenuWithFactor(_ factor: CGFloat) {
        hideMenuWithFactor(factor, completion: nil)
    }
    
    func hideMenuWithFactor(_ factor: CGFloat, completion: (() -> Void)?) {
        var durations: CGFloat = 0.3
        
        if factor > 0 {
            durations /= factor
        }
        
        // Animate the detail view
        UIView.animate(
            withDuration: Double(durations), delay: 0.0, options: UIViewAnimationOptions(), animations: {
                
                // Top Profile
                var profileFrame = self.profileView.frame
                profileFrame.origin.x = -(profileFrame.size.width)
                self.profileView.frame = profileFrame
                
                // Button container
                var rect = self.menuButtonsContainer.frame
                rect.origin.x = -1 * self.menuWidth
                self.menuButtonsContainer.frame  = rect
                
                // Navigation View
                let layer = self.navigationView.layer
                let rotationAndPerspectiveTransform = CATransform3DIdentity
                layer.transform = rotationAndPerspectiveTransform
                
                // Status Bar
                //        self.updateStatusBar(originX: 0.0)
            },
            completion:  { (finished: Bool) in
                
                if let block = completion {
                    block()
                }
                
                // Remove detail view overlay from the detail view
                self.navigationOverlayView.removeFromSuperview()
                self.viewAboveNavigationView.isHidden = true
                self.menuIsVisible = false
                
                self.view.removeGestureRecognizer(self.panGesture)
            }
        )
    }
    
    // MARK: - Invite -
    
    @objc func showContacts()
    {
        
        let c = ABPeoplePickerNavigationController()
        c.peoplePickerDelegate = self
        c.displayedProperties = [
            NSNumber(value: kABPersonPhoneProperty as Int32)
        ]
        
        ABAddressBookRequestAccessWithCompletion(c, { success, error in
            if success {
                self.present(c, animated: true, completion: nil)
            }
        })
    }
    
    func showSMS(_ userInfo: NSDictionary)
    {
        let c = MFMessageComposeViewController()
        c.messageComposeDelegate = self
        c.recipients = [userInfo["contact"] as! String]
        c.body = createBodyText(userInfo)
        //    c.view.layer.setValue(userInfo, forKey: "data")
        
        if (MFMessageComposeViewController.canSendText())
        {
            self.present(c, animated: true, completion: nil)
        }
        else
        {
            showAlert("Could not send SMS", text: "Your device could not send SMS.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
        }
    }
    
    
    // MARK: - Show Navigations -
    
    @objc func actionMenu(_ sender: AnyObject) {
        
        if let button = sender as? MenuOptionButton {
            
            //      deselectAllButtons()
            //      button.backgroundColor = UIColor.clearColor()
            
            switch button.tag {
            // Home
            case 1: showHome2()
            // Explore
            case 2: showExplore()
            // Messages
            case 3: showMessages()
            //createGroups
            case 4:showCreateGroups()
            //MyJobs
            case 5:showMyJobs()
            //MyCompany
            case 6: showMyCompany()
            //hall of fame
            case 7:showHallOfFame()
            // Profile
            case 8: showProfile()
            // Friends
            case 9: showFriends()
            // Testimonials
            case 10: showTestimonials()
            // History
            case 11: showHistory()
            // Notifications
            case 12: showNotifications()
            // Settings
            case 13: showSettings()
            // Logout
            case 14: goLogOut()
            // Default
            default: ""
            }
        }
    }
    
    func deselectAllButtons() {
        
        for button in buttonArray {
            
            button.backgroundColor = UIColor.clear
        }
    }
    
    
    // MARK: - Social -
    
    func closeFollowFriendsDialog() {
        
        followFriends.close()
        showInviteView()
    }
    
    @objc func closeInviteDialog() {
        
        inviteView.close()
    }
    
    func showFriendsToFollow() {
        
        self.followFriends = GKFollowSocialFriendsView(frame: self.view.bounds)
        self.followFriends.delegate = self
        self.view.addSubview(self.followFriends)
        
        self.followFriends.parentController = self
        self.followFriends.requestFriendsList()
    }
    
    func showInviteView() {
        
        self.inviteView = GKInviteFriends(frame: self.view.bounds)
        self.inviteView.inviteFacebookButton.addTarget(self, action: #selector(GKMenuViewcontroller.showDialog), for: .touchUpInside)
        self.inviteView.inviteContactsButton.addTarget(self, action: #selector(GKMenuViewcontroller.showContacts), for: .touchUpInside)
        self.inviteView.skipButton.addTarget(self, action: #selector(GKMenuViewcontroller.closeInviteDialog), for: .touchUpInside)
        self.view.addSubview(self.inviteView)
    }
    
    @objc func showDialog() {
        
        let content = FBSDKAppInviteContent()
        content.appLinkURL = URL(string: "https://fb.me/1734736420084534")
        content.appInvitePreviewImageURL = URL(string: "http://www.goodkarms.com/assets/img/invite-friends.png")
        FBSDKAppInviteDialog.show(with: content, delegate: self)
    }
    
    // MARK: - Menu Options -
    
    func showHome(_ loginOrRegister: Bool, recovery: Bool) {
        
        menuButtonsContainer.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
        
        GKNotificationStore.requestMenuCount()
        
        if loginOrRegister {
            
            let user = Static.sharedInstance.userProfile
            var isSocial = false
            if (Static.sharedInstance.isLoginWithFacebook())
            {
                isSocial = true
            }
            
            if isSocial
            {
                showFriendsToFollow()
            }
            else
            {
                showInviteView()
            }
        }
        
        if recovery
        {
            showAlert("Welcome Back!", text: "Have fun!")
            print("RECOVERY")
        }
        
        // Show user's information
        updateData()
        
        // Notification Push
        let application = UIApplication.shared
        
        let userNotificationTypes: UIUserNotificationType = [.alert, .badge, .sound]
        //    let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        let settings = UIUserNotificationSettings(types: userNotificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        // Show Controller
        showHome2()
    }
    
    func showHome2() {
        
        let allShoutViewController = GKHomeViewController(nibName: "GKHomeViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController:
            allShoutViewController)
        
        presentDetailViewController(navigationController)
        self.currentController = "GKHomeViewController"
        
        hideMenuWithFactor(1.0)
    }
    
    func showExplore(){
        
        let allShoutViewController = GKExploreViewController(nibName: "GKExploreViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController:
            allShoutViewController)
        
        presentDetailViewController(navigationController)
        
        //        delegate().selectedTab = "all-vets"
        
        //    self.currentController = "AllShoutViewController"
        
        hideMenuWithFactor(1.0)
    }
    
    func showMessages() {
        
        let inboxViewController = GKInboxViewController(nibName: "GKInboxViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController:
            inboxViewController)
        
        presentDetailViewController(navigationController)
        
        hideMenuWithFactor(1.0)
    }
    //modified
    func showCreateGroups()
    {
        showAlert("Wait!", text: "Work in Progress. Will be released in the next version.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)

//        let createGroupViewController = GKCreateGroupsViewController(nibName: "GKCreateGroupsViewController", bundle: nil)
//        createGroupViewController.isInMenu = true
//        let navigationController = UINavigationController(rootViewController:
//            createGroupViewController)
//
//        presentDetailViewController(navigationController)
//
//        hideMenuWithFactor(1.0)
    }
    //modified
    func showMyJobs()
    {
        let myJobViewController = GKMyJobViewController(nibName: "GKMyJobViewController", bundle: nil)
        myJobViewController.isInMenu = true
        let navigationController = UINavigationController(rootViewController:
            myJobViewController)
        
        presentDetailViewController(navigationController)
        
        hideMenuWithFactor(1.0)
        
    }
    
    func showMyCompany()
    {
        showAlert("Wait!", text: "Work in Progress. Will be released in the next version.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)

//        let addComanyViewController = GKMyCompanyListViewController(nibName: "GKMyCompanyListViewController", bundle: nil)
//        //shivani
//        addComanyViewController.isInMenu = true
//        let navigationController = UINavigationController(rootViewController:
//            addComanyViewController)
//
//        presentDetailViewController(navigationController)
//        hideMenuWithFactor(1.0)
    }
    
    func showConversation(_ userID: Int, fullName: String) {
        
        let inboxViewController = GKInboxViewController(nibName: "GKInboxViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController:
            inboxViewController)
        
        presentDetailViewController(navigationController)
        
        hideMenuWithFactor(1.0)
        
        inboxViewController.openConversation(userID, fullName: fullName)
    }
    
    func showHallOfFame()
    {
        let hallOfFameObject = GKHallOfFameViewController(nibName: "GKHallOfFameViewController", bundle: nil)
        hallOfFameObject.isInMenu = true
        let navigationController = UINavigationController(rootViewController:
            hallOfFameObject)
        
        presentDetailViewController(navigationController)
        
        hideMenuWithFactor(1.0)
    }
    
    
    func showProfile()
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController",
                                                        bundle: nil)
        self.present(UINavigationController(rootViewController: profileController), animated: true, completion: nil)
    }
    
    func showProfile(_ userID: Int)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.anotherUserID = userID
        
        self.present(UINavigationController(rootViewController: profileController), animated: true, completion: nil)
    }
    
    func showFriends()
    {
        let friendViewController = GKFriendsViewController(nibName: "GKFriendsViewController", bundle: nil)
        
        let navigationController = UINavigationController(rootViewController: friendViewController)
        
        presentDetailViewController(navigationController)
        self.currentController = "GKFriendsViewController"
        
        hideMenuWithFactor(1.0)
    }
    
    func showTestimonials()
    {
        let testimonialsViewController = GKProfileListTestimonialsViewController(
            nibName: "GKProfileListTestimonialsViewController", bundle: nil
        )
        
        testimonialsViewController.nameUser = Static.sharedInstance.userFirstName()
        //testimonialsViewController.anotherUserID = Static.sharedInstance.userId()
        testimonialsViewController.fromMenu = true
        
        let navigationController = UINavigationController(rootViewController: testimonialsViewController)
        
        presentDetailViewController(navigationController)
        self.currentController = "GKProfileListTestimonialsViewController"
        
        hideMenuWithFactor(1.0)
    }
    
    func showHistory()
    {
        let historyViewController = GKHistoryViewController(nibName: "GKHistoryViewController", bundle: nil)
        historyViewController.isInMenu = true
        let navigationController = UINavigationController(rootViewController: historyViewController)
        
        presentDetailViewController(navigationController)
        //    self.currentController = "GKHistoryViewController"
        
        hideMenuWithFactor(1.0)
    }
    
    func showHistoryWithTestimonial(_ name: String, relatedId: Int,
                                    type: Int, byClient: Bool)
    {
        let historyViewController = GKHistoryViewController(nibName: "GKHistoryViewController", bundle: nil)
        historyViewController.isInMenu = true
        historyViewController.showTestimonial = true
        historyViewController.testimonialId = relatedId
        historyViewController.testimonialName = name
        historyViewController.testimonialType = type
        historyViewController.testimonialByClient = byClient
        let navigationController = UINavigationController(rootViewController: historyViewController)
        
        presentDetailViewController(navigationController)
        
        hideMenuWithFactor(1.0)
    }
    
    func showDetailPost(_ id: Int, type: Int, isParticipant: Bool)
    {
        let historyViewController = GKHistoryViewController(nibName: "GKHistoryViewController", bundle: nil)
        historyViewController.isInMenu = true
        historyViewController.showDetail = true
        historyViewController.showDetailId = id
        historyViewController.showDetailType = type
        historyViewController.showDetailIsParticipan = isParticipant
        let navigationController = UINavigationController(rootViewController: historyViewController)
        
        self.currentDetailViewController.dismiss(animated: true,
                                                                       completion: nil)
        
        presentDetailViewController(navigationController)
        hideMenuWithFactor(1.0)
    }
    
    func showHistory(_ dataPost: GKDictionary, typePost: Int)
    {
        let historyViewController = GKHistoryViewController(nibName: "GKHistoryViewController", bundle: nil)
        historyViewController.isInMenu = true
        let navigationController = UINavigationController(rootViewController: historyViewController)
        
        presentDetailViewController(navigationController)
        hideMenuWithFactor(1.0)
        
        historyViewController.showPost(dataPost, typePost: typePost)
    }
    
    func showNotifications() {
        
        let notificationsViewController = GKNotificationsViewController(nibName: "GKNotificationsViewController", bundle: nil)
        let navigationController = UINavigationController(rootViewController: notificationsViewController)
        
        presentDetailViewController(navigationController)
        hideMenuWithFactor(1.0)
    }
    
    func showSettings(){
        
        let settingsViewController = GKSettingsViewController(nibName: "GKSettingsViewController", bundle: nil)
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        
        presentDetailViewController(navigationController)
        hideMenuWithFactor(1.0)
    }
    
    func goLogOut(){
        let alertController = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let destroyAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            if (Static.sharedInstance.isLoginWithFacebook())
            {
                logout(true)
            }
            else{
                logout(false)
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated: false, completion: nil)
    }
    
    // MARK!: - Navigations -
    
    //  func homeNavigationController() -> UINavigationController{
    //    //    homeNavigation = UINavigationController(rootViewController: HomeViewController()
    //    //    )
    //
    //    return homeNavigation
    //
    //  }
    //
    //  func profileNavigationController() -> UINavigationController{
    //    
    //    return profileNavigation
    //  }
    //  
    //  func friendsNavigationController() -> UINavigationController{
    //    return friendsNavigation
    //    
    //    //    private var friendsNavigation: UINavigationController!
    //    //    private var messagesNavigation: UINavigationController!
    //    //    private var settingsNavigation: UINavigationController!
    //    //    private var aboutNavigation: UINavigationController!
    //  }
    //  
    //  func messagesNavigationController() -> UINavigationController{
    //    return messagesNavigation
    //  }
    //  
    //  func settingsNavigationController() -> UINavigationController{
    //    return settingsNavigation
    //  }
    //  
    //  func aboutNavigationController() -> UINavigationController{
    //    return aboutNavigation
    //  }
    
    
    // MARK: - Methods -
    
    
    //    func load_image(urlString:String)
    //    {
    //        self.profileView.imageView.loadImageWithUrl(urlString)
    //    }
    
    func updateCounts()
    {
        //          let vetsReceived = (GKStore.sharedInstance.menuCounter?["shouts_received"]
        //              as! NSNumber).integerValue
        //  
        //          if (vetsReceived > 0)
        //          {
        //              self.profileView.shouts.text = "\(vetsReceived) time"
        //                  + (vetsReceived > 1 ? "s" : "")
        //          }
        //          else
        //          {
        //              self.profileView.shouts.text = "0 times"
        //          }
        
        buttonArray[2].updateCount(GKNotificationStore.number_messages)
        buttonArray[7].updateCount(GKNotificationStore.number_notifications)
        //          buttonArray[1].updateCount((GKStore.sharedInstance.menuCounter?["shouts_unread_events"] as! NSNumber).integerValue)
        
        //           Badge Count
        //          self.delegate().updateBadgeCount()
    }
    
    //  private func requestMenuCount()
    //  {
    ////            GKStore.sharedInstance.requestMenuCount()
    ////                {
    ////    //                self.updateCounts()
    ////    //                self.updateData()
    ////            }
    //    var params = GKDictionary()
    //    params["token"] = Static.sharedInstance.token()
    //    
    //    GKServiceClient.menuCounter(params) { (response: AnyObject?) -> () in
    //        
    ////        LoadingView.hideLoadingView(self.navigationController?.view)
    //      
    //        if let res:AnyObject = response {
    //          
    //          let envelope = ResponseSkeleton(res)
    //          
    //          if envelope.getSuccess()
    //          {
    //            if let data = envelope.getResource() as? NSDictionary {
    //              
    //              print(data)
    //              
    //              GKNotificationStore.number_messages      = data["message"] as! Int
    //              GKNotificationStore.number_notifications = data["notification"] as! Int
    //              
    //              self.updateCounts()
    //            }
    //          }
    //      }
    //    }
    //  }
    
    fileprivate func updateData() {
        //      let dic = GKStore.sharedInstance.businessData()
        //      //            profileView.name.text = dic!.objectForKey("name") as? String
        //
        //      let urlString = "http://graph.facebook.com/" +
        //        (dic!.objectForKey("facebook_id") as! String) + "/picture?type=normal"
        //      load_image(urlString)
        
        let user = Static.sharedInstance.userProfile
        //let value = user?.object(forKey: "object") as! GKDictionary
        let lastName = user?.object(forKey: "last_name") as! String
        let firstName = user?.object(forKey: "first_name") as! String
        self.profileView.imageView.loadImageWithUrl(user?["photo_url"] as? String as NSString?)
        self.profileView.nameLabel.text = "\(firstName)" + " " + "\(lastName)"
    }
 
    
    
    @objc func changedProfileInSettingsObserver()
    {
        updateData()
    }
}
