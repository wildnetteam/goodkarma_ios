//
//  GKProfileGratitudeTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKProfileGratitudeTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var timestampLabel: UILabel!
  
  // MARK: - Properties -
  
//  var data: GKDictionary!
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    self.selectionStyle = .none
  }
 
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKProfileGratitudeTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 100.0
  }
  
  // MARK: - Methods -
  
  func loadData(_ data: GKDictionary)
  {
    let descGratitude = (data["desc"] as! String).decodeEmoji()
    
    self.titleLabel.text = descGratitude
    self.timestampLabel.text = secondsToFormatted(data["date_created"] as? Int)
  }
}
