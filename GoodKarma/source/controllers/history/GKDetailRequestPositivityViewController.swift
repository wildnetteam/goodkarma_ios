//
//  GKDetailRequestPositivityViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class GKDetailRequestPositivityViewController: UIViewController, GKTestimonialDelegate, GKShowUserProfileDelegate, UITableViewDataSource, UITableViewDelegate
{
    // MARK: - IBOutlets -
    
    //  @IBOutlet weak var requestPositivityDetailView : UIView!
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var noParticipantsLabel : UILabel!
    @IBOutlet weak var participantsTableView : UITableView!
    
    // MARK: - Properties -
    
    var data: GKDictionary!
    var postId: Int!
    var delegate : GKHistoryParticipantDelegate?
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Positivity Request"
        self.noParticipantsLabel.isHidden = true
        
        self.participantsTableView.register(
            UINib(nibName: GKHistoryParticipantTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKHistoryParticipantTableViewCell.cellIdentifier()
        )
        self.participantsTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        loadData()
        //    requestDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if self.delegate != nil
        {
           
          //   self.refreshData()

        }
       
        //requestDetail()
    }
    
    // MARK: - Protocols
    // MARK: - Protcol GKTestimonialDelegate
    
    func GKLeaveTestimonial(_ clientData: GKDictionary)
    {
        // nothing to do here: testimonial only works for serve & help request
    }
    
    // MARK: - Protocol GKShowUserProfileDelegate -
    
    func GKShowProfile(_ userID: Int)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.anotherUserID = userID
        
        self.present(
            UINavigationController(rootViewController: profileController), animated: true, completion: nil
        )
    }
    
    // MARK: - Protocol UITableViewDataSource -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (self.data != nil)
        {
        //old    return (self.data["clients"] as! [GKDictionary]).count
            //8 dec
           if self.data["clients"] != nil{
                return (self.data["clients"] as! [GKDictionary]).count
            }
            else{
                return 0
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GKHistoryParticipantTableViewCell.cellIdentifier(), for: indexPath) as! GKHistoryParticipantTableViewCell
        
        let client = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        let status = client["status"] as? Int
        var detail = ""
        
        if (status == 1)
        {
            detail = " slide left to accept or reject this message."
        }
        else
        {
            detail = (client["desc"] as? String)!
            detail = detail.decodeEmoji()
        }
        
        cell.delegate = self
        // FIXME: fix type
        cell.loadData(client, detail: detail, postType: 444,
                      postStatus: self.data["status"] as! Int)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        // you need to implement this method too or you can't swipe to display the actions
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        let participantData = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        
        return (participantData["status"] as! Int) != 2
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let participantData = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        let status = participantData["status"] as? Int
        
        if (status > 1)
        {
            let descPostivitity = participantData["desc"] as! String
            
            GKMoreDetailView.show(descPostivitity)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        return GKHistoryParticipantTableViewCell.heightForCell()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let participantData = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        let related_id      = participantData["related_id"] as! Int
        let status          = participantData["status"] as! Int
        
        if status == 1
        {
            // Accept
            let acceptAction = UITableViewRowAction(style: .default, title: "Accept") { (action, indexPath) in
                
                self.acceptParticipant(related_id)
            }
            acceptAction.backgroundColor = UIColor.greenOpaque()
            
            // Deny
            let denyAction = UITableViewRowAction(style: .default, title: "Deny") { (action, indexPath) in
                
                self.denyParticipant(related_id)
            }
            denyAction.backgroundColor = UIColor.red
            
            return [denyAction, acceptAction]
        }
        else if status == 2
        {
            return []
        }
        
        return []
    }
    
    // MARK: - Posts status -
    
    func acceptParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"]      = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 2 as AnyObject?
        params["status"] = 2 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
      //  GKServiceClient.call(GKRouterClient.acceptParticipant(), parameters: params, callback: {
         GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                
                   // self.requestDetail()
                    if self.delegate != nil
                    {
                        
                      
                      //  self.refreshData()
                       
                    }
                    
                }
            }
        })
    }
    
    func denyParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 2 as AnyObject?
        params["status"] = 0 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
      //  GKServiceClient.call(GKRouterClient.removeParticipant(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                   // self.requestDetail()
                    if self.delegate != nil
                    {
                    //     self.refreshData()
                    }
                    
                }
            }
        })
    }
    
    // MARK: - Methods -
    func refreshData()
    {
        let value = self.delegate?.requestOfferedListData()
                                self.data = value
                                self.loadData()
                                self.reloadTableData()
    }
    func loadData()
    {
        if (self.data != nil)
        {
            // Desc
            descLabel.text = (self.data["desc"] as? String)?.decodeEmoji()
            // Date
            dateLabel.text = secondsToFormatted(data["date_created"] as? Int)
        }
    }
    
    func requestDetail()
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["id"]   = fetchPostId() as AnyObject?
        params["type"] = 2 as AnyObject?
        
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.postDetail(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["data"] as? GKDictionary
                    {
                        self.data = data
                        
                        self.loadData()
                        self.reloadTableData()
                    }
                }
            }
        })
    }
    
    func reloadTableData()
    {
        participantsTableView.reloadData()
        
        if (self.data["clients"] as! [GKDictionary]).count > 0
        {
            noParticipantsLabel.isHidden = true
            participantsTableView.isHidden = false
        }
        else
        {
            noParticipantsLabel.isHidden = false
            participantsTableView.isHidden = true
        }
    }
    
    func fetchPostId() -> Int!
    {
        if (self.data != nil)
        {
           return self.data["id"] as! Int
        }
        
        return self.postId
    }
}
