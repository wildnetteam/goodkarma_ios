//
//  GKMyCompanyListTableViewCell.swift
//  GoodKarma
//
//  Created by Shivani on 25/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyCompanyListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewProfile: WebImageView!
    @IBOutlet weak var labelCompanyName: UILabel!
    @IBOutlet weak var labelmembers: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
