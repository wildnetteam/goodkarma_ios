//
//  GKAddPhotoViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/3/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKAddPhotoViewController: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  // MARK: - IBOutlets -
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var addPhotoButton: UIButton!
  @IBOutlet weak var contentView: UIView!
  
  // MARK: - Properties -
  
  var data: NSDictionary!
  var imagePicker = UIImagePickerController()
  var fromFacebook = false
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    cleanBackButton()
    
    self.title = "Sign Up"
    
    // Content Photo
    self.imageView.layer.cornerRadius = self.imageView.bounds.width * 0.5
    self.addPhotoButton.layer.cornerRadius = self.addPhotoButton.bounds.width * 0.5
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = false
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UIActionSheetDelegate -
  
  func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    
    if (buttonIndex <= 1) {
      
      imagePicker.allowsEditing = false
      imagePicker.sourceType = buttonIndex == 0 ? .camera : .photoLibrary
      imagePicker.delegate = self;
      imagePicker.allowsEditing = true;
      
      present(imagePicker, animated: true, completion: nil)
      UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: false)
    }
  }
  
  // MARK: - Protocol UIImagePickerControllerDelegate -
  
  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [String : Any]) {
    let image: UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
    
    self.imageView.image = image
    
    self.dismiss(animated: true, completion: nil)
    UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    self.dismiss(animated: true, completion: nil)
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionSignUp(_ sender: AnyObject) {
    
    if validate() {
      
      LoadingView.showLoadingView(
        "Sending photo...",
        parentView: self.navigationController?.view,
        backColor: true
      )
      
      let imageData = UIImageJPEGRepresentation(self.imageView.image!, 0.5)
      //      let base64String =
      //        imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
      
      var params = [String: String]()
      params["token"]     = self.data["token"] as? String
      
      GKServiceClient.uploadWithAlamofire(params, imageData: imageData!, callback: { (response: AnyObject?) in
        
        LoadingView.hideLoadingView(self.navigationController?.view)
        
        if let res:AnyObject = response {
          let envelope = ResponseSkeleton(res)
          
          if envelope.getSuccess() {
            
            self.requestProfile()
          }
          else {
            showCommonError({ (type: NoInternetResultType) -> () in
              self.actionSignUp(0 as AnyObject)
            })
          }
        }
        else
        {
          showConnectionError({ (type: NoInternetResultType) -> () in
            self.actionSignUp(0 as AnyObject)
          })
        }
      })
    }
  }
  
  @IBAction func actionAddPhoto(_ sender: AnyObject)
  {
    let actionSheet =  UIActionSheet(title: nil, delegate: self,
                                     cancelButtonTitle: nil, destructiveButtonTitle: nil)
    actionSheet.addButton(withTitle: "Take Photo")
    actionSheet.addButton(withTitle: "Camera Roll")
    actionSheet.addButton(withTitle: "Cancel")
    actionSheet.cancelButtonIndex = 2
    actionSheet.actionSheetStyle = UIActionSheetStyle.blackTranslucent
    actionSheet.show(in: self.view)
  }
  
  // MARK: - Methods -
  
  func requestProfile() {
    
    LoadingView.showLoadingView("Sending...",
                                parentView: self.navigationController?.view,
                                backColor: true)
    
    var params = [String: String]()
    params["token"] = self.data["token"] as? String
    
    GKServiceClient.call(GKRouterClient.registerProfile(), parameters: params as GKDictionary, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          let resource = envelope.getResource() as? NSDictionary
          
          let mutable = NSMutableDictionary(dictionary: resource!)
          mutable["token"] = self.data["token"] as? String
          
          Static.sharedInstance.saveUserProfile(mutable)
          Static.sharedInstance.saveLogin(true)
          (UIApplication.shared.delegate as! AppDelegate).showHome(self.navigationController!, loginOrRegister: true, recovery: false) // self.fromFacebook
        }
        else {
          showCommonError({ (type: NoInternetResultType) -> () in
            self.requestProfile()
          })
        }
      }
      else {
        showConnectionError({ (type: NoInternetResultType) -> () in
          self.requestProfile()
        })
      }
    })
  }
  
  func validate() -> Bool {
    
    if imageView.image == nil {
      
      showMessageError("Alert", text: "Add a profile photo so others can see you smile!", callback: { (type: NoInternetResultType) in
        
      })
      
      return false;
    }
    return true
  }
  
}
