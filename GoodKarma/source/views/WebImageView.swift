//
//  WebImageView.swift
//  ShoutOut
//
//  Created by Eliezer de Armas on 3/9/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit
import Haneke

class WebImageView: UIImageView
{
  var activityIndicator: UIActivityIndicatorView!
  var showActivityIndicator = true
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  fileprivate func setup()
  {
    self.backgroundColor = UIColor.superLightGray()
    self.clipsToBounds = true
    self.contentMode = .scaleAspectFill
    
    self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle:.gray)
    self.activityIndicator.stopAnimating()
    self.addSubview(self.activityIndicator)
  }
  
  override func layoutSubviews()
  {
    super.layoutSubviews()
    
    self.activityIndicator.center = CGPoint(x: self.bounds.size.width * 0.5,
                                                y: self.bounds.size.height * 0.5)
  }
  
  func loadImageWithUrl(_ urlString: NSString?)
  {
    guard let url = urlString else {
      
      return
    }
    
    if url.length > 0 {
      
      if self.showActivityIndicator {
        
        self.activityIndicator.startAnimating()
      }
      
      hnk_setImage(from: URL(string: url as String), placeholder: nil, success: { (image) in
        
        self.activityIndicator.stopAnimating()
        self.image = image
        
        }, failure: { (error) in
          
            print(error!.localizedDescription)
          self.activityIndicator.stopAnimating()
      })
    }
  }
  
  func loadFBPicture(_ url: NSString)
  {
    let urlString = "http://graph.facebook.com/"
      + (url as String)
      + "/picture?type=normal"
    
    loadImageWithUrl(urlString as NSString?)
  }
}
