//
//  GKProfileListTestimonialsViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKProfileListTestimonialsViewController: GKViewController, UITableViewDataSource, UITableViewDelegate, GKProfileTestimonialCellDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UILabel!
    
    // MARK: - Properties -
    
    var testimonials: [GKDictionary] = []
    var anotherUserID: Int?
    var nameUser: String!
    var fromMenu: Bool = false
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = self.nameUser + "'s Testimonials"
        
        if (self.fromMenu)
        {
            setMenuBarBadge()
        }
        
        self.tableView.register(
            UINib(nibName: GKProfileTestimonialTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKProfileTestimonialTableViewCell.cellIdentifier()
        )
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        requestData()
    }
    
    // MARK: - Protocols
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return testimonials.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: GKProfileTestimonialTableViewCell.cellIdentifier(), for: indexPath) as! GKProfileTestimonialTableViewCell
        cell.delegate = self
        
        cell.loadData(self.testimonials[indexPath.row])
        
        return cell
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        var testimonialMessage = ""
//        let data = self.testimonials[indexPath.row]
//        let authorName = data["recipient"]!["full_name"] as! String
//
//        if let detail = data["detail"] as? [String] {
//
//            if detail.contains("1")
//            {
//                testimonialMessage += (authorName + " was able to help.\n\n")
//            }
//            if detail.contains("2")
//            {
//                testimonialMessage += (authorName + " is very skilled.\n\n")
//            }
//            if detail.contains("3")
//            {
//                testimonialMessage += (authorName + " is a punctual person.\n\n")
//            }
//
//            if detail.contains("4")
//            {
//                testimonialMessage += (authorName + " is such a friendly person.\n\n")
//            }
//
//            if detail.contains("5")
//            {
//                testimonialMessage += ("I am grateful I met " + authorName + ".\n\n")
//            }
//
//            if detail.contains("6")
//            {
//                testimonialMessage += (authorName + " was thoughtful.\n\n")
//            }
//
//            if detail.contains("7")
//            {
//                testimonialMessage += (authorName + " was able to put my needs first.\n\n")
//            }
//
//            if detail.contains("8")
//            {
//                testimonialMessage += (authorName + " had a kind demeanor.\n\n")
//            }
//
//            if detail.contains("9")
//            {
//                testimonialMessage += (authorName + " was able to pass along helpful thoughts and prayers.\n\n")
//            }
//
//            if detail.contains("10")
//            {
//                testimonialMessage += (authorName + " was able to take time out to go the extra mile for me.\n\n")
//            }
//        }

//        TestimonialView.show(testimonialMessage)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        return GKProfileTestimonialTableViewCell
            .heightForCell(self.testimonials[indexPath.row] as NSDictionary,
                           width: self.view.frame.width - 99)
    }
    
    // MARK: - Methods -
    
    func requestData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page_number"] = 1 as AnyObject?
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        var routeService = ""
        
        if anotherUserID != nil
        {
            routeService = GKRouterClient.profileListOtherTestimonials()
            params["recipient_id "] = anotherUserID! as AnyObject?
        }
        else
        {
            routeService = GKRouterClient.profileListMyTestimonials()
        }
        
        GKServiceClient.call(routeService, parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    if let authorData = envelope.getResource() as? GKDictionary
                    {
                        let data = authorData["result"] as! [GKDictionary]
                        // self.testimonials = data.sorted { ($0["date_created"] as! Double) <
                        //   ($1["date_created"] as! Double)}
                        self.testimonials = data
                        self.tableView.reloadData()
                        //self.emptyView.isHidden = self.testimonials.count > 0
                    }
                }
            }
        })
    }
    
    // MARK: - GKProfileTestimonialCellDelegate
    
    func profileTestimonialCellPictureAction(_ data: NSDictionary)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.withBack = true
        profileController.anotherUserID = data.value(forKeyPath: "author.id") as? Int
        self.navigationController?.pushViewController(profileController, animated: true)
    }
}
