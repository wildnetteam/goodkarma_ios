//
//  FriendsTableViewCell.swift
//  ShoutOut
//
//  Created by Eliezer de Armas on 3/9/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit

protocol FriendsFollowCellDelegate : NSObjectProtocol
{
    func followFriend(_ facebookId: String)
}

class FriendsTableViewCell: UITableViewCell
{
    @IBOutlet weak var pictureImageView : WebImageView!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var followButton     : UIButton!
  
    var data: GKDictionary!
    weak var delegate: FriendsFollowCellDelegate!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width/2
        self.pictureImageView.layer.borderColor = UIColor.colorToPercent(186, green:196, blue:209).cgColor
        self.pictureImageView.layer.borderWidth = 1.3
        self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
        
        // Check Button
//        self.checkButton.hidden = true
    }
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        self.pictureImageView.loadFBPicture((data["id"] as! NSString))
        self.nameLabel.text = (data["name"] as! String)
    }

//    func loadData2(data: GKDictionary, friendsLocation: NSArray)
//    {
//        self.data = data
//        self.pictureImageView.loadFBPicture((data["facebook_id"] as! NSString))
//        self.nameLabel.text = (data["first_name"] as! String) + " " + (data["last_name"] as! String)
//    }
  
    func updateFollow(_ isFollowing: Bool) {
      
        if isFollowing {
          
          followButton.setTitle("Unfollow", for: UIControlState())
        }
        else {
          
          followButton.setTitle("Follow", for: UIControlState())
        }
    }
    
    // MARK: Actions
    
    @IBAction func followAction(_ sender: AnyObject)
    {
        let facebookId = data["id"] as! String
        self.delegate.followFriend(facebookId)
    }
}
