//
//  GKAssignJobViewController.swift
//  GoodKarma
//
//  Created by Shivani on 24/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import Alamofire

class GKAssignJobViewController: UIViewController , GKManageSkillsDelegate , UITextFieldDelegate ,UIImagePickerControllerDelegate , UINavigationControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource , UITextViewDelegate
{
    @IBOutlet weak var heightConstraintContentView: NSLayoutConstraint!
    @IBOutlet weak var skillsUpperView: UIView!
    @IBOutlet weak var virtualCheckBoxButton: UIButton!
    @IBOutlet weak var assignJobButton: UIButton!
    @IBOutlet weak var physicalCheckBoxButton: UIButton!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var needHelpByTextField: UITextField!
    @IBOutlet weak var titleContentTextField: UITextField!
    @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var skillsContainerView: UIView!
    @IBOutlet weak var difficultySliderLabel: UILabel!
    @IBOutlet weak var difficultySlider: UISlider!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    @IBOutlet weak var jobDescriptionTextView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var count = 0
    var imageArray : [UIImage] = []
    var typeOfHelp : Int!
    var userId : Int!
    var manageSkillView: GKManageSkillsView!
    var data : GKDictionary!
    var difficultyValue = 5
    var skillsSelected: [GKModelSkill] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Arya Stark"
        //shivani
        self.typeOfHelp = 1
        titleContentTextField.delegate = self
        jobDescriptionTextView.delegate = self
        physicalCheckBoxButton.layer.borderColor = UIColor.gray.cgColor
        physicalCheckBoxButton.layer.borderWidth = 2.0
        physicalCheckBoxButton.clipsToBounds = true
        virtualCheckBoxButton.layer.borderColor = UIColor.gray.cgColor
        virtualCheckBoxButton.layer.borderWidth = 2.0
        virtualCheckBoxButton.clipsToBounds = true
        jobDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        jobDescriptionTextView.layer.borderWidth = 1.0
        difficultySlider.tintColor = UIColor.darkGray
        difficultySlider.minimumValue = 1.0
        difficultySlider.maximumValue = 5.0
        manageSkillView = GKManageSkillsView(frame: skillsContainerView.bounds, editable: true, delegate: self)
        skillsContainerView.addSubview(manageSkillView)
        //   self.skillsContainerHeightConstraint.constant = 0.0
        needHelpByTextField.delegate = self
        if data != nil
        {
            self.loadData()
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
        showPickerView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if DeviceType.IS_IPHONE_5{
//            heightConstraintContentView.constant = CGFloat(heightConstraintContentView.constant + 190)
//        }
//        else if DeviceType.IS_IPHONE_6
//        {
//            heightConstraintContentView.constant = CGFloat(heightConstraintContentView.constant + 30)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        updateSkillsView()
        // self.filterScrollView.contentSize = CGSize(width: self.filterScrollView.frame.width, height: self.filterFormView.bottomSide)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
       
    }
    // MARK: - UICollectionViewDelegate Methods
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
        cell.imageView.image = imageArray[indexPath.row]
        return cell
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    // MARK: - textViewDelegate Methods
    // MARK: - textFieldDelegate Methods
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.showDatePicker()
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == titleContentTextField
        {
            titleContentTextField.resignFirstResponder()
            jobDescriptionTextView.becomeFirstResponder()
        }
        
        return true
    }
    
    // MARK: - textViewDelegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        
//        self.jobDescriptionPlaceHolderLabel.isHidden = true
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
   
//        self.jobDescriptionPlaceHolderLabel.isHidden = textView.text.characters.count > 0
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
//        self.jobDescriptionPlaceHolderLabel.isHidden = textView.text.countPlainCharacters() > 0
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    // MARK: - IBActions
    @IBAction func actionAssignJob(_ sender: Any)
    {
        let isValid = self.validate()
        if isValid == true
        {
            self.saveAssignJobData()
        }
        
    }
    @IBAction func actionSlider(_ slider: UISlider) {
        
        if slider == difficultySlider {
            
            difficultyValue = Int(slider.value)
        }
        
        
        updateSliders()
    }
    
    
    @IBAction func actionVideo(_ sender: Any)
    {
        
    }
    @IBAction func actionGallery(_ sender: Any)
    {
        self.pickFromGallery()
    }
    @IBAction func actionCamera(_ sender: Any)
    {
        self.pickFromCamera()
    }
    @IBAction func actionCheckButton(_ sender: UIButton)
    {
        //shivani
        let image = UIImage(named : "mark")
        if sender.currentBackgroundImage == nil
        {
            if sender == virtualCheckBoxButton
            {
                physicalCheckBoxButton.setBackgroundImage(nil, for: .normal)
                virtualCheckBoxButton.setBackgroundImage(image, for: .normal)
                self.typeOfHelp = 2
            }
            else
            {
                virtualCheckBoxButton.setBackgroundImage(nil, for: .normal)
                physicalCheckBoxButton.setBackgroundImage(image, for: .normal)
                self.typeOfHelp = 1
            }
        }
        else
        {
            
            if sender == virtualCheckBoxButton
            {
                physicalCheckBoxButton.setBackgroundImage(image, for: .normal)
                virtualCheckBoxButton.setBackgroundImage(nil, for: .normal)
                self.typeOfHelp = 1
            }
            else
            {
                virtualCheckBoxButton.setBackgroundImage(image, for: .normal)
                physicalCheckBoxButton.setBackgroundImage(nil, for: .normal)
                self.typeOfHelp = 2
            }
        }
        
    }
    
    
    // MARK: - Methods
    func loadData()
    {
        let firstName = data["first_name"] as! String
        let last_name = data["last_name"] as! String
        self.title = firstName + " " + last_name
    }
    func updateSliders() {
        
        difficultySliderLabel.text = difficultyToString(difficultyValue)
        difficultySlider.setValue(Float(difficultyValue), animated: false)
        
        
    }
    func updateSkillsView()
    {
        manageSkillView.frame = skillsContainerView.bounds
        manageSkillView.setupSkills(skillsSelected)
        
        self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        skillsSelected = skillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView() {
        
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill]) {
        // Nothing for now
        
    }
    
    
    func validate() -> Bool
    {
        var message : String!
        if titleContentTextField.text == ""{
            message = "please enter title"
        }
        else if jobDescriptionTextView.text == ""{
            message = "Please enter job description"
        }
        else if skillsSelected.isEmpty == true
        {
            message = "Please select skills"
        }
        else if zipCodeTextField.text == ""{
            message = "Please enter zip code"
        }
        else if zipCodeTextField.text?.count != 5
        {
            message = "Zip code should be of five digits"
        }
        else if needHelpByTextField.text == ""
        {
            message = "Please select date"
        }
            
        else if typeOfHelp == nil
        {
            message = "Please select the type of help"
        }
        else if imageArray.isEmpty == true
        {
            message = "please select images"
        }
        else
        {
            return true
        }
        
        let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alertController, animated: false, completion: nil)
        return false
    }
    
    // MARK: - datepicker Methods
    func showDatePicker()
    {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action:  #selector(donePicker(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = NSDate() as Date
        needHelpByTextField.inputView = datePickerView
        needHelpByTextField.inputAccessoryView = toolBar
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
        
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let date = dateFormatter.string(from: sender.date)
        self.needHelpByTextField.text = date
    }
    
    @objc func donePicker(sender: UITextField)
    {
        needHelpByTextField.endEditing(true)
        zipCodeTextField.endEditing(true)
    }
    
    
    // MARK: - UIImagePicker Methods
    
    func pickFromCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func pickFromGallery()
    {
        let imagePicker = UIImagePickerController()
        //  imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let pickedImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        imageArray.append(pickedImage)
        count = imageArray.count
        collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func showPickerView()
    {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action:  #selector(donePicker(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        zipCodeTextField.inputAccessoryView = toolBar
    }
    
    // MARK: - Web Service
    func saveAssignJobData()
    {
        
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["title"] = titleContentTextField.text as AnyObject?
        params["desc"] = jobDescriptionTextView.text as AnyObject?
        params["need_by"] = needHelpByTextField.text as AnyObject?
        params["difficulty"] = difficultyValue as AnyObject?
        params["help_type"] = 1 as AnyObject?
        params["assigned_to"] =  self.userId as AnyObject?
        params["zip_code"] = zipCodeTextField.text as AnyObject?
        
        if skillsSelected.count > 0 {
            let value = skillsSelected.map{ $0.uid } as NSArray?
            let skillsInfo  = value?.componentsJoined(by: ",")
            let info = "[" + "\(skillsInfo!)" + "]"
            params["skills"] = info as AnyObject?
        }
        print(params)
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in params {
                multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
            }
            
            for i in 0 ..< (self.imageArray.count) {
                
                if let imageData = UIImageJPEGRepresentation(self.imageArray[i] , 0.6)
                {
                    multipartFormData.append(imageData, withName: "image[]", fileName: "image\(i).jpeg", mimeType: "image/png")
                }
            }
            print(params)
        }, to: GKRouterClient.addJob(), method: .post, headers: nil,
           encodingCompletion: {
            
            encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                //LoadingView.hideLoadingView(self.navigationController?.view)
                upload.responseJSON { response in
                    //   print(response.result.value!)
                    if response.result.value != nil
                    {
                        self.imageArray.removeAll()
                        LoadingView.hideLoadingView(self.navigationController?.view)
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                break
                
            case .failure(let encodingError):
                LoadingView.hideLoadingView(self.navigationController?.view)
                print(encodingError)
                break
            }
        })
        
        
        
    }
    
}


