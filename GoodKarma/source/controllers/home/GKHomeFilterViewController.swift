//
//  GKHomeFilterViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/10/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class GKHomeFilterViewController: GKViewController, GKManageSkillsDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var filterScrollView: UIScrollView!
    @IBOutlet weak var filterFormView: UIView!
    
    @IBOutlet weak var filterSkillsContainer: UIView!
    
    @IBOutlet weak var filterDifficultySlider: UISlider!
    @IBOutlet weak var filterDifficultyLabel: UILabel!
    
    @IBOutlet weak var filterSeparatorView: UIView!
    @IBOutlet weak var distanceSeparatorView: UIView!
    @IBOutlet weak var filterZipSeparatorView: UIView!
    @IBOutlet weak var filterZipView: UIView!
    
    @IBOutlet weak var filterDateSeparatorView: UIView!
    @IBOutlet weak var filterDistanceField: UITextField!
    
    @IBOutlet weak var filterZipField: UITextField!
    
    @IBOutlet weak var filterFollowingImageView: UIImageView!
    @IBOutlet weak var filterFollowersImageView: UIImageView!
    @IBOutlet weak var filterBothImageView: UIImageView!
    
    @IBOutlet weak var dueDateNumberLabel: UILabel!
    @IBOutlet weak var dueDateTextLabel: UILabel!
    @IBOutlet weak var dueDateView: UIView!
    @IBOutlet weak var viewPostFromView: UIView!
    @IBOutlet weak var bothButton: UIButton!
    @IBOutlet weak var filterPositiveView: UIView!
    @IBOutlet weak var resquestsImageView: UIImageView!
    @IBOutlet weak var responsesImageView: UIImageView!
    @IBOutlet weak var requestsAndResponsesImageView: UIImageView!
    
    // Constraints
    
    @IBOutlet weak var filterSkillsContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterSkillsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterDifficultyViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterDistanceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterZipHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterDataHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterDateseperatorViewTpConstraint: NSLayoutConstraint!
    
    // MARK: - Properties -
    
    var delegate: GKHomeFilterDelegate?
    var manageSkillView: GKManageSkillsView!
    var filterDifficultyValue = 5
    //  var filterDistanceValue   = 1000
    var skillsSelected: [GKModelSkill] = []
    var homeState: GKHomeState?
    
    var isFilterFollowing : Bool = false
    var isFilterFollower  : Bool = false
    
    var dueDateNumberValue   : Int = 0
    var dueDateIntervalValue : Int = 0
    
    var params: GKDictionary = [:]
    
    var isRequestsCheck : Bool = true
    var isResponsesCheck: Bool = false
    
    var isSetting = Bool()

    // MARK: - Override UIViewController -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Filters"
        
        let cancelBarButton = UIBarButtonItem(
            title: "Cancel", style: .plain, target: self, action: #selector(GKHomeFilterViewController.cancel)
        )
        self.navigationItem.leftBarButtonItem = cancelBarButton
        
        let saveBarButton = UIBarButtonItem(
            title: "Save", style: .plain, target: self, action: #selector(GKHomeFilterViewController.save)
        )
        self.navigationItem.rightBarButtonItem = saveBarButton
        
        filterDifficultySlider.tintColor = UIColor.darkGray
        filterDifficultySlider.minimumValue = 1.0
        filterDifficultySlider.maximumValue = 5.0
        
        //    filterDistanceSlider.tintColor = UIColor.darkGrayColor()
        //    filterDistanceSlider.minimumValue = 1.0
        //    filterDistanceSlider.maximumValue = 1000.0
        
        updateSliders()
        
        self.filterZipField.addAccessoryView("Done")
        
        manageSkillView = GKManageSkillsView(frame: filterSkillsContainer.bounds, editable: true, delegate: self)
        filterSkillsContainer.addSubview(manageSkillView)
        
        if let filterState = self.homeState {
            
            if filterState == .Gratitude || filterState == .Positivity {
                
                params.removeValue(forKey: "skills")
                params.removeValue(forKey: "difficulty")
                
                self.filterSeparatorView.isHidden = true
                self.distanceSeparatorView.isHidden = true
                self.filterZipSeparatorView.isHidden = true
                self.filterDateSeparatorView.isHidden = true
                self.dueDateView.isHidden = true
                self.viewPostFromView.frame = CGRectSetY(self.viewPostFromView.frame,
                                                         y: 0)
                
                self.filterSkillsContainerHeightConstraint.constant = 0.0
                self.filterSkillsViewHeightConstraint.constant      = 0.0
                self.filterDifficultyViewHeightConstraint.constant  = 0.0
                self.filterDistanceViewHeightConstraint.constant = 0.0
                self.filterZipHeightConstraint.constant = 0.0
                self.filterDataHeightConstraint.constant = 0.0
            }
            
            self.filterPositiveView.isHidden = filterState != .Positivity
        }
        else if !isSetting
        {
            self.filterPositiveView.isHidden = true
        }
        else{
            self.filterPositiveView.isHidden = true
            self.dueDateView.isHidden = true
            self.filterZipSeparatorView.isHidden = true
            self.filterDataHeightConstraint.constant = 0.0
            self.filterDateseperatorViewTpConstraint.constant = 10
        }
        
        if let skills = params["skills"] as? [Int] {
            
            var params = [String: String]()
            params["token"] = Static.sharedInstance.token()
            
            LoadingView.showLoadingView(
                "Loading...",
                parentView: self.navigationController?.view, backColor: true
            )
            
            GKServiceClient.call(GKRouterClient.listSkills(), parameters: params as GKDictionary, callback: {
                (response: AnyObject?) -> () in
                
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res:AnyObject = response {
                    
                    let envelope = ResponseSkeleton(res)
                    
                    if envelope.getSuccess() {
                        
                        let data = envelope.getResource() as! NSDictionary
                        
                        self.skillsSelected =  []
                        
                        for dic in data["skills"] as! [GKDictionary] {
                            if let uid = dic["id"] as? Int {
                                if skills.contains(uid) {
                                    self.skillsSelected.append(GKModelSkill.readFromDictionary(dic))
                                }
                            }
                        }
                    }
                }
            })
        }
        
        if let difficultyValue = params["difficulty"] as? Int {
            
            filterDifficultyValue = difficultyValue
            updateSliders()
        }
        
        if let distanceValue = params["distance"] as? Int {
            
            filterDistanceField.text = "\(distanceValue)"
        }
        
        if let zipCode = params["zip_code"] as? String {
            
            filterZipField.text = zipCode
        }
        
        if let isFollower = params["follower"] as? Bool {
            
            isFilterFollower = isFollower
        }
        
        if let isFollowing = params["following"] as? Bool {
            
            isFilterFollowing = isFollowing
        }
        
        if let isRequests = params["requests"] as? Bool
        {
            self.isRequestsCheck = isRequests
        }
        
        if let isResponse = params["responses"] as? Bool
        {
            self.isResponsesCheck = isResponse
        }
        
        updateFollowImages()
        updateRequestResponses()
        updateUIAtHomeState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if let filterState = self.homeState {
            
            if !(filterState == .Gratitude || filterState == .Positivity) {
                
                updateSkillsView()
            }
        }
        else {
            
            updateSkillsView()
        }
        
        self.filterScrollView.contentSize = CGSize(width: self.filterScrollView.frame.width, height: self.filterFormView.bottomSide)
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if let filterState = self.homeState
        {
            if filterState == .Gratitude || filterState == .Positivity
            {
                self.viewPostFromView.frame = CGRectSetY(self.viewPostFromView.frame, y: 0)
            }
            
            if filterState == .Positivity
            {
                self.filterPositiveView.frame = CGRectSetY(self.filterPositiveView.frame, y: self.viewPostFromView.frame.maxY)
            }
        }
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKManageSkillsDelegate -
    
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        skillsSelected = skillsSelected.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    
    func GKShowAddSkillsView() {
        
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skillsSelected = self.skillsSelected
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill]) {
        // Nothing for now
        
    }
    
    // MARK: - Protocol UITextFieldDelegate -
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == filterDistanceField {
            
            filterZipField.becomeFirstResponder()
        }
        else if textField == filterZipField {
            
            filterZipField.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Protocols -
    // MARK: - Protocol UIPickerViewDataSource -
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if component == 0
        {
            return 999
        }
        else if component == 1
        {
            return scaleValues().count
        }
        else
        {
            return 0
        }
    }
    
    // MARK: - Protocol UIPickerViewDelegate -
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if component == 0
        {
            return "\(row + 1)"
        }
        else if component == 1
        {
            return scaleValues()[row].1
        }
        else
        {
            return nil
        }
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionSlider(_ slider: UISlider) {
        
        if slider == filterDifficultySlider {
            
            filterDifficultyValue = Int(slider.value)
        }
        //    else if slider == filterDistanceSlider {
        //
        //      filterDistanceValue = Int(slider.value)
        //    }
        
        updateSliders()
    }
    
    @IBAction func actionSelectBoth(_ sender: AnyObject) {
        
        if isFilterFollower && isFilterFollowing {
            
            isFilterFollower  = false
            isFilterFollowing = false
        }
        else {
            
            isFilterFollower  = true
            isFilterFollowing = true
        }
        
        updateFollowImages()
    }
    
    @IBAction func actionSelectFollowers(_ sender: AnyObject) {
        
        isFilterFollower = !isFilterFollower
        
        updateFollowImages()
    }
    
    @IBAction func actionSelectFollowing(_ sender: AnyObject) {
        
        isFilterFollowing = !isFilterFollowing
        
        updateFollowImages()
    }
    
    @IBAction func dueDateAction(_ sender: AnyObject)
    {
        let selectAction = RMAction(title: "Select", style: .done) { (controller) in
            if let picker = controller as? RMActionController<UIPickerView>
            {
                let pickerController = picker as! RMPickerViewController
                self.dueDateNumberValue   = pickerController.picker
                    .selectedRow(inComponent: 0) + 1
                self.dueDateIntervalValue = self.scaleValues()[pickerController
                    .picker.selectedRow(inComponent: 1)].0
                
                self.dueDateNumberLabel.text = "\(self.dueDateNumberValue)"
                self.dueDateTextLabel.text = self.stringFromInterval(self.dueDateIntervalValue)
            }
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .cancel) { (controller) in
        }
        
        let pickerController = RMPickerViewController(style: .default, select: selectAction as! RMAction<UIPickerView>?, andCancel: cancelAction as! RMAction<UIPickerView>?)!
        pickerController.picker.dataSource = self
        pickerController.picker.delegate   = self
        pickerController.disableBlurEffects = true
        pickerController.picker.selectRow(self.dueDateNumberValue - 1, inComponent: 0, animated: false)
        pickerController.picker.selectRow(rowFromInterval(self.dueDateIntervalValue), inComponent: 1, animated: false)
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func actionSelectRequests(_ sender: AnyObject)
    {
        self.isRequestsCheck = !self.isRequestsCheck
        updateRequestResponses()
    }
    
    @IBAction func actionSelectResponses(_ sender: AnyObject)
    {
        self.isResponsesCheck = !self.isResponsesCheck
        updateRequestResponses()
    }
    
    @IBAction func actionSelectRequestsAndResponses(_ sender: AnyObject)
    {
        if (self.isResponsesCheck)
        {
            self.isResponsesCheck = false
        }
        else
        {
            self.isResponsesCheck = true
            self.isRequestsCheck = true
        }
        
        updateRequestResponses()
    }
    
    // MARK: - Methods -
    
    @objc func cancel() {
        
        //    params = [:]
        //
        //    closeAndUpdateParams()
        close()
    }
    
    @objc func save() {
        
        params = [:]
        
        // Difficulty
        params["difficulty"] = filterDifficultyValue as AnyObject?
        
        // Distance
        let distanceValueOptional: Int? = Int(filterDistanceField.text!)
        
        if let distanceValue = distanceValueOptional, distanceValue > 0 {
            
            params["distance"] = distanceValue as AnyObject?
        }
        else {
            
            params["distance"] = 1000 as AnyObject?
        }
        
        //    if let distanceValue = filterDistanceField.text as? Int
        //    {
        //      params["distance"] = distanceValue
        //    }
        //    else
        //    {
        //      params["distance"] = 1000
        //    }
        
        //    params["distance"] = filterDistanceField.text filterDistanceValue
        
        // Zip code
        if filterZipField.text?.count > 1 {
            params["zip_code"] = filterZipField.text as AnyObject?
        }
        else
        {
            params["zip_code"] = "" as AnyObject
        }
        
        // Skills
        if skillsSelected.count > 0 {
            params["skills"] = "\((skillsSelected.map{ $0.uid! }))" as AnyObject?
           // params["skills"] = "\(String(describing: params["skills"]))" as AnyObject?
        }
        else
        {
            params["skills"] = "" as AnyObject
        }
        
        // Follow
        params["follower"]  = isFilterFollower as AnyObject?
        params["following"] = isFilterFollowing as AnyObject?
        
        // Due date
        
        params["number"]   = self.dueDateNumberValue as AnyObject?
        params["interval"] = self.dueDateIntervalValue as AnyObject?
        
        if let filterState = self.homeState
        {
            if (filterState == .Positivity)
            {
                params["requests"] = self.isRequestsCheck as AnyObject?
                params["responses"] = self.isResponsesCheck as AnyObject?
            }
        }
        
        closeAndUpdateParams()
    }
    
    func close()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func closeAndUpdateParams()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
        delegate?.GKFilterUpdateParams(params)
    }
    
    func updateSliders() {
        
        filterDifficultyLabel.text = difficultyToString(filterDifficultyValue)
        filterDifficultySlider.setValue(Float(filterDifficultyValue), animated: false)
        
        //    filterDistanceLabel.text = "\(filterDistanceValue)"
        //    filterDistanceSlider.setValue(Float(filterDistanceValue), animated: false)
    }
    
    func updateSkillsView()
    {
        manageSkillView.frame = filterSkillsContainer.bounds
        manageSkillView.setupSkills(skillsSelected)
        
        self.filterSkillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    
    func updateFollowImages()
    {
        self.filterFollowersImageView.image = UIImage(named:
            self.isFilterFollower  ? "check-on" : "check-off")
        self.filterFollowingImageView.image = UIImage(named:
            self.isFilterFollowing ? "check-on" : "check-off")
        self.filterBothImageView.image = UIImage(named:
            (self.isFilterFollower && self.isFilterFollowing) ?
                "check-on" : "check-off")
    }
    
    func updateRequestResponses()
    {
        self.resquestsImageView.image = UIImage(named:
            self.isRequestsCheck  ? "check-on" : "check-off")
        self.responsesImageView.image = UIImage(named:
            self.isResponsesCheck ? "check-on" : "check-off")
        self.requestsAndResponsesImageView.image = UIImage(named:
            (self.isRequestsCheck && self.isResponsesCheck) ?
                "check-on" : "check-off")
    }
    
    func scaleValues() -> [(Int, String)]
    {
        return [
            ( 1,"Day(s)"),
            ( 7,"Week(s)"),
            (30,"Month(s)"),
            (12,"Year(s)")
        ]
    }
    
    func rowFromInterval(_ interval: Int) -> Int
    {
        switch self.dueDateIntervalValue
        {
        case 1  : return 0
        case 7  : return 1
        case 30 : return 2
        case 12 : return 3
        default : return 0
        }
    }
    
    func stringFromInterval(_ interval: Int) -> String
    {
        switch interval
        {
        case 1  : return scaleValues()[0].1
        case 7  : return scaleValues()[1].1
        case 30 : return scaleValues()[2].1
        case 12 : return scaleValues()[3].1
        default : return ""
        }
    }
    
    func updateUIAtHomeState()
    {
        if (self.homeState == .Positivity || self.homeState == .Gratitude)
        {
            
        }
    }
}
