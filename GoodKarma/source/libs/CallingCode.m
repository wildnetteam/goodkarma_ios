//
//  CallingCode.m
//  GoodKarma
//
//  Created by Paul Aguilar on 6/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

#import "CallingCode.h"

@implementation CallingCode

- (id)init
{
    if ((self = [super init]))
    {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@".plist"];
        codes = [NSDictionary dictionaryWithContentsOfFile:filePath];
    }
    
    return self;
}

- (NSString *)getCountryCallingCode:(NSString *)countryCode
{
    return [codes objectForKey:countryCode][@"dial_code"];
}

- (NSString *)getCountryCallingCodeAtIndex:(NSInteger)index
{
    return [self getCountryCallingCode:[self coutryCodeAtIndex:index]];
}

- (NSString *)coutryCodeAtIndex:(NSInteger)index
{
    NSArray *allCodes = [self allCodes];
    
    return allCodes[index][@"code"];
}

- (NSArray *)allCodes
{
    NSArray *c = [codes allValues];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    c = [c sortedArrayUsingDescriptors:@[sort]];

    return c;
}

- (NSDictionary *)dataAtCode:(NSString *)countryCode
{
    return codes[countryCode];
}

@end
