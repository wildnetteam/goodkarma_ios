//
//  FirebaseManager.swift
//  GoodKarma
//
//  Created by Anuj Jha on 03/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import Foundation
import Firebase

typealias Completion = (Bool) -> Void
typealias onCompletion = (String) -> Void
typealias updateCompletion = (NSMutableArray) -> Void
typealias returnUnreadCount = (Int) -> Void
typealias returnUpdatedUnreadCount = (Int) -> Void

@objc protocol FirebaseContactManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingClassContacts()
    @objc optional func fireBaseManagerDidCompleteFetchingBusContacts()
}
@objc protocol FirebaseMessageManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingMessages()
}
@objc protocol FirebaseRecentChatsManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingRecentChats()
    @objc optional func fireBaseManagerDidCompleteFetchingRecentGroupChats()
}

//Anuj
class FireBaseManager: NSObject {
    // Can't init is singleton
    
    static let defaultManager = FireBaseManager()
    var contactManagerDelegate: FirebaseContactManagerDelegate?
    var messageManagerDelegate: FirebaseMessageManagerDelegate?
    var classContactManagerDelegate: FirebaseContactManagerDelegate?
    var recentChatsManagerDelegate: FirebaseRecentChatsManagerDelegate?
    var classContacts:[ClassContacts] = []
    var busContacts:[BusContacts] = []
    var messages:[Message] = []
    var recentChats:[RecentChats] = []
    var myGroupIdArray:[GroupId] = []
    var groupListArray:[GroupList] = []
    var userStatusRef: DatabaseReference = Database.database().reference().child("UserOnlineStatus")
    var usersRef: DatabaseReference = Database.database().reference().child("Users")
    var msgRef: DatabaseReference = Database.database().reference().child("Message")
    var recentsRef: DatabaseReference = Database.database().reference().child("Recents")
    var userGroupIdRef: DatabaseReference = Database.database().reference().child("UserGroupId")
    var groupRef: DatabaseReference = Database.database().reference().child("Groups")
    var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://good-karma-9ea8c.appspot.com")
    var isChannelPresent = false
    var isMessagePresent: Bool?
    var isGroupPresent: Bool?
    
    //MARK:-Function to create a new user node on Firebase.
    func createNewUser(contact: String, detailDict: NSDictionary){
        
        usersRef.child(contact).setValue(detailDict)
    }
    
    //MARK:-Function to set data for each message under "Messages" node on Firebase.
    func sendNewMsg(atNode: String, detailDict: NSMutableDictionary){
        
        let messageRef = msgRef.child(atNode).childByAutoId()
        detailDict.setValue(messageRef.key, forKey: "objectId")
        messageRef.setValue(detailDict)
        
    }
    
    func setRecentData(atNode: String, recentDict: NSMutableDictionary){
        let channelRef = recentsRef.child(atNode)
        recentDict.setValue(channelRef.key, forKey: "objectId")
        channelRef.setValue(recentDict)
    }
    
    func updateGroupMessage(atNode: String, updateDict: NSMutableDictionary){
        groupRef.child(atNode).updateChildValues(updateDict as! [AnyHashable : Any])
    }
    
    //MARK:-Function to check whether a user exists or not under "Users" node on Firebase.
    func checkUserExistence(contact: String, onCompletion: @escaping Completion) -> (){
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(contact){
                onCompletion(true)
                print("rooms exist")
            }else{
                onCompletion(false)
                print("room doesn't exist")
            }
            
        })
        
    }
    
    func updateSpamCountAtNode(groupName: String, node: String, user: String, onCompletion: @escaping updateCompletion) -> (){
        let updateNode = msgRef.child(groupName).child(node)
        updateNode.observeSingleEvent(of: .value) { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                let spamMarkedByExists = messageData["spamMarkedBy"] as? NSMutableArray != nil
                if spamMarkedByExists{
                    let spamMarkedBy = messageData["spamMarkedBy"] as! NSMutableArray
                    if spamMarkedBy.contains(user){
                        onCompletion(spamMarkedBy)
                    }else{
                        spamMarkedBy.add(user)
                        updateNode.updateChildValues(["spamMarkedBy" : spamMarkedBy])
                        onCompletion(spamMarkedBy)
                    }
                }else{
                    let spamMarkedBy: NSMutableArray = []
                    spamMarkedBy.add(user)
                    updateNode.updateChildValues(["spamMarkedBy" : spamMarkedBy])
                    onCompletion(spamMarkedBy)
                }
            }
        }
    }
    
    //MARK:-Function to upload image in the storage section of Firebase and return the image URL.
    func uploadImageOnFirebase(contact: String, image: UIImage , onCompletion: @escaping onCompletion) -> (){
        
        let createdAt = NSDate().timeIntervalSince1970
        
        let riversRef = storageRef.child(contact).child("images.jpg-\(createdAt)")
        
        // Metadata contains file metadata such as size, content-type, and download URL.
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        DispatchQueue.global().async(execute: {
            riversRef.putData(UIImageJPEGRepresentation(image, 0.3)!, metadata: metadata) { (metadata, error) in
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    let downloadURL = metadata?.downloadURL()!
                    onCompletion((downloadURL?.absoluteString)!)
                    
                })
            }
        })
    }
    
    //MARK:-Function to upload audio in the storage section of Firebase and return the audio URL.
    func uploadAudioOnFirebase(nodeStr: String, ducumentDirectoryUrl: String , onCompletion: @escaping onCompletion)->(){
        let createdAt = NSDate().timeIntervalSince1970
        
        let audioMsgRef = self.storageRef.child(nodeStr).child("\(Int64(createdAt))audio.mp3")
        
        let metadata = StorageMetadata()
        metadata.contentType = "audio/mp3"
        
        let fileUrl = Foundation.URL(string: ducumentDirectoryUrl)
        let data = NSData(contentsOf: fileUrl!)
        
        DispatchQueue.global().async(execute: {
            audioMsgRef.putData((data! as Data), metadata: metadata) { (metadata, error) in
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata?.downloadURL()!
                    onCompletion((downloadURL?.absoluteString)!)
                })
            }
        })
    }
    
    //Mark:- Function to get updated unread count.
    func updateUnreadCount(node: String, id: String, onCompletion: @escaping returnUnreadCount) -> (){
        recentsRef.child(node).observe(.childChanged, with: {(snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let key = snapshot.key
                if key == "unreadCount"{
                    let countDict = snapshot.value as! NSDictionary
                    let count = countDict.object(forKey: id) as! Int
                    print("changedCount---\(String(describing: count))")
                    onCompletion(count)
                }
            }
        })
    }
    
    func setUserOnlineOfflineStatus(id: String, status: Int){
        userStatusRef.child(id).setValue(["status":status])
    }
    
    //MARK:- fetch Online, Offline, Typing.
    func observeOnlineStatus(id: String , onCompletion: @escaping onCompletion)->() {
        
        userStatusRef.child(id).observe(DataEventType.value, with: { (snapshot) in
            
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Int>
                
                let onlineStatus = messageData["status"]! as Int
                if onlineStatus == 0{
                    onCompletion("Offline")
                }else if onlineStatus == 1{
                    onCompletion("Online")
                }else{
                    onCompletion("typing...")
                }
            }
        })
    }
    
    //Mark:- Function to increment unread count.
    func incrementNotificationCount(node: String, id: String, onCompletion: @escaping returnUnreadCount) -> (){
        
        recentsRef.child(node).observeSingleEvent(of: .value, with: {(snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                let countDict = messageData["unreadCount"] as! NSMutableDictionary
                var count = countDict.object(forKey: id) as! Int
                count = count + 1
                countDict.setValue(count, forKey: id)
                self.recentsRef.child(node).updateChildValues(["unreadCount":countDict])
                onCompletion(count)
            }
        })
    }

    //Mark:- Function to set unread count to 0.
    func setUnreadCountToZero(node: String, id: String){
        recentsRef.child(node).observeSingleEvent(of: .value, with: {(snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                let countDict = messageData["unreadCount"] as! NSMutableDictionary
                countDict.setValue(0, forKey: id)
                self.recentsRef.child(node).updateChildValues(["unreadCount":countDict])
            }
        })
    }
    
    func fetchClassContacts(node: String){
        Database.database().reference().child(node).observe(.childAdded, with: { (snapshot) in
            
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                let childFirstName = messageData["childfirst_name"] as! String
                let childLastName = messageData["childlast_name"] as! String
                let momFirstName = messageData["first_name"] as! String
                let momLastName = messageData["last_name"] as! String
                let mobileNo = messageData["mobileNo"] as! NSInteger
                let moderator = messageData["moderator"] as! Bool
                let profileImg = messageData["profileImg"] as! String
                
                //if contactNumber != UserModel.getCurrentCustomerNumber(){
                self.classContacts.append(ClassContacts(childFirstName: childFirstName , childLastName: childLastName, momFirstName: momFirstName, momLastName: momLastName, profileImg: profileImg, moderator: moderator, mobileNo: mobileNo))
                //}
                if self.classContactManagerDelegate != nil{
                    self.classContactManagerDelegate?.fireBaseManagerDidCompleteFetchingClassContacts!()
                }
            }
        })
    }
    
    func fetchBusContacts(node: String){
        Database.database().reference().child(node).observe(.childAdded, with: { (snapshot) in
            
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                let childFirstName = messageData["childfirst_name"] as! String
                let childLastName = messageData["childlast_name"] as! String
                let momFirstName = messageData["first_name"] as! String
                let momLastName = messageData["last_name"] as! String
                let mobileNo = messageData["mobileNo"] as! NSInteger
                let moderator = messageData["moderator"] as! Bool
                let profileImg = messageData["profileImg"] as! String
                
                //if contactNumber != UserModel.getCurrentCustomerNumber(){
                self.busContacts.append(BusContacts(childFirstName: childFirstName , childLastName: childLastName, momFirstName: momFirstName, momLastName: momLastName, profileImg: profileImg, moderator: moderator, mobileNo: mobileNo))
                //}
                if self.contactManagerDelegate != nil{
                    self.contactManagerDelegate?.fireBaseManagerDidCompleteFetchingBusContacts!()
                }
            }
        })
    }
    
    
    //MARK:-Fetch a new message.
    func fetchMessages(node: String, id: String){
        
        msgRef.child(node).queryOrdered(byChild: "createdAt").observe(.childAdded, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                //print("messageData\(messageData)")
                let timeStamp = messageData["createdAt"] as! Double
                //let senderImg = messageData["senderImg"]
                let senderId = messageData["senderId"] as! String
                let senderName = messageData["senderName"] as! String
                let msgText = messageData["text"] as! String
                let msgType = messageData["type"] as! String
                let objectId = messageData["objectId"] as! String
                let imgUrl = messageData["picture"] as! String
                let audioURL = messageData["audio"] as! String!
                let audio_Duration = messageData["audio_duration"] as! Int!
                let status = messageData["status"] as! String

                if id != senderId{
                    
                    if (((AppDelegate.appDelegate().window?.rootViewController as? GKMenuViewcontroller)?.currentDetailViewController) as? UINavigationController)?.topViewController is GKMessagesViewController{
                        self.msgRef.child(node).child(objectId).updateChildValues(["status":"read"])
                    }else{
                        self.msgRef.child(node).child(objectId).updateChildValues(["status":"delivered"])
                    }
                }
                
                let dateString = self.convertTimeStampToDateString(timeStamp:timeStamp)
                self.isMessagePresent = false
                
                if self.messages.count != 0{
                    for index in 0..<self.messages.count {
                        if objectId == self.messages[index].objectId{
                            self.isMessagePresent = true
                            break
                        }
                    }
                    if self.isMessagePresent == false{
                        self.messages.append(Message(text: msgText, senderId: senderId, createdAt: dateString , type: msgType, senderName: senderName, objectId: objectId, picture: imgUrl, status: status, audioURL: audioURL, audio_Duration: audio_Duration))
                    }
                }else{
                    self.messages.append(Message(text: msgText, senderId: senderId, createdAt: dateString , type: msgType, senderName: senderName, objectId: objectId, picture: imgUrl, status: status, audioURL: audioURL, audio_Duration: audio_Duration))
                }
                if self.messageManagerDelegate != nil{
                    self.messageManagerDelegate?.fireBaseManagerDidCompleteFetchingMessages!()
                }
            }
        })
        
        msgRef.child(node).observe(.childChanged, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                print("messageData\(messageData)")
            }
            
        })
        
    }
    
    //MARK:-Fetch the users whom we have contacted.
    func fetchRecentChats(id: String){
        
        self.recentsRef.queryOrdered(byChild: "senderId").queryEqual(toValue: id).observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                
                let channelData = snapshot.value as! Dictionary<String, Any>
                
                let timeStamp = channelData["time"] as! Double
                let lastMsg = channelData["last_msg"] as! String!
                let msgType = channelData["msgType"] as! String!
                let senderName = channelData["senderName"] as! String!
                let senderId = channelData["senderId"] as! String!
                var senderImg = ""
                if let senderImage = channelData["senderImg"] as? String{
                    senderImg = senderImage
                }
                let recieverName = channelData["recieverName"] as! String!
                let recieverId = channelData["recieverId"] as! String!
                var recieverImg = ""
                if let recieverImage = channelData["recieverImg"] as? String{
                    recieverImg = recieverImage
                }
                let node = channelData["objectId"] as! String!
                let time = self.convertTimeStampToDateString(timeStamp: timeStamp)
                let unreadCountDict = channelData["unreadCount"] as! NSMutableDictionary!
                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            
                            self.isChannelPresent = true
                            break
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, unreadCountDict: unreadCountDict))
                        self.isChannelPresent = false
                    }
                    
                }else{
                    self.recentChats.append(RecentChats(msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, unreadCountDict: unreadCountDict))
                    self.isChannelPresent = false
                }
                
                self.recentChats.sort(by: { (channel0, channel1) -> Bool in
                    return channel0.time! > channel1.time!
                })
                
                
                if self.recentChatsManagerDelegate != nil{
                    self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                }
            }
        })
        
        self.recentsRef.queryOrdered(byChild: "recieverId").queryEqual(toValue: id).observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                
                let channelData = snapshot.value as! Dictionary<String, Any>
                
                let timeStamp = channelData["time"] as! Double
                let lastMsg = channelData["last_msg"] as! String!
                let msgType = channelData["msgType"] as! String!
                let senderName = channelData["senderName"] as! String!
                let senderId = channelData["senderId"] as! String!
                var senderImg = ""
                if let senderImage = channelData["senderImg"] as? String {
                    senderImg = senderImage
                }
                let recieverName = channelData["recieverName"] as! String!
                let recieverId = channelData["recieverId"] as! String!
                var recieverImg = ""
                if let recieverImage = channelData["recieverImg"] as? String{
                    recieverImg = recieverImage
                }
                let node = channelData["objectId"] as! String!
                let time = self.convertTimeStampToDateString(timeStamp: timeStamp)
                let unreadCountDict = channelData["unreadCount"] as! NSMutableDictionary!
                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            
                            self.isChannelPresent = true
                            break
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, unreadCountDict: unreadCountDict))
                    }
                    
                }else{
                    self.recentChats.append(RecentChats(msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, unreadCountDict: unreadCountDict))
                    
                }
                
                self.recentChats.sort(by: { (channel0, channel1) -> Bool in
                    return channel0.time! > channel1.time!
                })
                
                if self.recentChatsManagerDelegate != nil{
                    self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                }
            }
        })
        
        self.recentsRef.queryOrdered(byChild: "senderId").queryEqual(toValue: id).observe(.childChanged, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){

                let channelData = snapshot.value as! Dictionary<String, Any>

                let key = snapshot.key
                var changedIndex:NSInteger = -1
                if self.recentChats.count > 0 {
                    for index in 0..<self.recentChats.count {
                        let channelKey = (self.recentChats[index] ).node
                        if key == channelKey {
                            changedIndex = index
                        }
                    }
                }

                if changedIndex != -1{
                    let timeStamp = channelData["time"] as! Double
                    let lastMsg = channelData["last_msg"] as! String!
                    self.recentChats[changedIndex].lastMsg = lastMsg
                    let time = self.convertTimeStampToDateString(timeStamp: timeStamp)
                    self.recentChats[changedIndex].time = time

                    self.recentChats.sort(by: { (channel0, channel1) -> Bool in
                        return channel0.time! > channel1.time!
                    })
                    
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
            }
        })

        self.recentsRef.queryOrdered(byChild: "recieverId").queryEqual(toValue: id).observe(.childChanged, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){

                let channelData = snapshot.value as! Dictionary<String, Any>

                let key = snapshot.key
                var changedIndex:NSInteger = -1
                if self.recentChats.count > 0 {
                    for index in 0..<self.recentChats.count {
                        let channelKey = (self.recentChats[index] ).node
                        if key == channelKey {
                            changedIndex = index
                        }
                    }
                }

                if changedIndex != -1{
                    let timeStamp = channelData["time"] as! Double
                    let lastMsg = channelData["last_msg"] as! String!
                    self.recentChats[changedIndex].lastMsg = lastMsg
                    let time = self.convertTimeStampToDateString(timeStamp: timeStamp)
                    self.recentChats[changedIndex].time = time

                    self.recentChats.sort(by: { (channel0, channel1) -> Bool in
                        return channel0.time! > channel1.time!
                    })
                    
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
            }
        })
    }
    
    func setUserGroup(id: String, groupId: String){
        userGroupIdRef.child(id).childByAutoId().setValue(groupId)
    }
    
    func fetchMyGroups(id: String){
        userGroupIdRef.child(id).queryOrdered(byChild: "time").observe(.childAdded) { (snapshot) in
            if let groupChannelData = snapshot.value, !(groupChannelData is NSNull){
                
                let groupChannelData = snapshot.value as! String
                
                self.myGroupIdArray.append(GroupId(groupId: groupChannelData))
            }
            
            for index in 0..<self.myGroupIdArray.count{
                let groupId = self.myGroupIdArray[index].groupId
                self.groupRef.child(groupId!).observe(.value, with: { (snapshot) in
                    if let groupData = snapshot.value, !(groupData is NSNull){
                        
                        let groupData = snapshot.value as! Dictionary<String, Any>
                        
                        let groupName = groupData["group_name"] as! String
                        let groupImage = groupData["group_image"] as! String
                        let groupId = groupData["group_id"] as! String
                        let lastMsg = groupData["last_msg"] as! String
                        let timestamp = groupData["time"] as! Double
                        var participants = NSDictionary()
                        if let participantsDict = groupData["participants"] as? NSDictionary{
                            participants = participantsDict
                        }
                        
                        let time = self.convertTimeStampToDateString(timeStamp: timestamp)
                        
                        self.isGroupPresent = false
                        
                        if self.groupListArray.count != 0{
                            for index in 0..<self.groupListArray.count{
                                if groupId == self.groupListArray[index].groupId{
                                    self.isGroupPresent = true
                                    break
                                }
                            }
                            if self.isGroupPresent == false{
                                self.groupListArray.append(GroupList(groupName: groupName, groupId: groupId, groupImageURL: groupImage, lastMsg: lastMsg, time: time, participants: participants))
                            }
                        }else{
                            self.groupListArray.append(GroupList(groupName: groupName, groupId: groupId, groupImageURL: groupImage, lastMsg: lastMsg, time: time, participants: participants))
                        }
                        
                        self.groupListArray.sort(by: { (channel0, channel1) -> Bool in
                            return channel0.time! > channel1.time!
                        })
                        
                        if self.recentChatsManagerDelegate != nil{
                            self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentGroupChats!()
                        }
                    }
                })
                
                self.groupRef.queryOrdered(byChild: "time").observe(.childChanged, with: { (snapshot) -> Void in
                    if let channelData = snapshot.value, !(channelData is NSNull){
                        
                        let channelData = snapshot.value as! Dictionary<String, Any>
                        
                        let key = snapshot.key
                        var changedIndex:NSInteger = -1
                        if self.groupListArray.count > 0 {
                            for index in 0..<self.groupListArray.count {
                                let channelKey = (self.groupListArray[index] ).groupId
                                if key == channelKey {
                                    changedIndex = index
                                }
                            }
                        }
                        
                        if changedIndex != -1{
                            let timeStamp = channelData["time"] as! Double
                            let lastMsg = channelData["last_msg"] as! String!
                            self.groupListArray[changedIndex].lastMsg = lastMsg
                            let time = self.convertTimeStampToDateString(timeStamp: timeStamp)
                            self.groupListArray[changedIndex].time = time
                            let participants = channelData["participants"] as! NSDictionary!
                            self.groupListArray[changedIndex].participants = participants
                            
                            self.groupListArray.sort(by: { (channel0, channel1) -> Bool in
                                return channel0.time! > channel1.time!
                            })
                            
                            if self.recentChatsManagerDelegate != nil{
                                self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentGroupChats!()
                            }
                        }
                    }
                })
            }
        }
    }
    
    //MARK:-Convert timestamp to date string.
    func convertTimeStampToDateString(timeStamp: Double) -> String{
        var time = NSDate()
        if String(timeStamp).characters.count == 12{
            time = NSDate(timeIntervalSince1970: timeStamp)
        }else{
            time = NSDate(timeIntervalSince1970: timeStamp/1000)
        }
        
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.medium
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        let dateString = formatter.string(from: time as Date)
        print(dateString)
        
        return dateString
    }
}
