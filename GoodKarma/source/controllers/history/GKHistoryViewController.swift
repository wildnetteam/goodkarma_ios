//
//  GKHistoryViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/6/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKHistoryViewController: GKViewController, GKShowUserProfileDelegate, GKHistoryPostStateDelegate, UITableViewDataSource, UITableViewDelegate,
    UIAlertViewDelegate, UIScrollViewDelegate , GKHistoryParticipantDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var requestedTableView: UITableView!
    @IBOutlet weak var offeredTableView: UITableView!
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    // MARK: - Properties -
    
    var isRequestedViewSelected = true
    var anotherUserID: Int? = nil
    var onlyInProgress = false
    var customTitle: String!
    var otherUserName: String!
    
    var isInMenu : Bool = false
    fileprivate var requestedData : [GKDictionary] = []
    fileprivate var offeredData   : [GKDictionary] = []
    
    var showTestimonial : Bool = false
    var showDetail : Bool = false
    var showDetailType : Int!
    var showDetailId : Int!
    var showDetailIsParticipan: Bool = false
    var testimonialName: String!
    var testimonialId: Int!
    var testimonialType: Int!
    var testimonialByClient: Bool = false
    var canShowTutorial = true
    var tempIndex : Int!
    var tempData : [GKDictionary] = []
    var temValue : GKDictionary!
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if isInMenu
        {
            self.title = "History"
            setMenuBarBadge()
        }
        else
        {
            self.title = "Posts"
        }
        
        if (self.customTitle != nil)
        {
            self.title = customTitle
        }
        
        // Nib
        var classBundle = Bundle(for: GKHistoryRequestedTableViewCell.self)
        var nib = UINib(nibName:"GKHistoryRequestedTableViewCell", bundle:classBundle)
        self.requestedTableView.register(nib, forCellReuseIdentifier:GKHistoryRequestedTableViewCell.cellIdentifier())
        
        classBundle = Bundle(for: GKHistoryOfferedTableViewCell.self)
        nib = UINib(nibName:"GKHistoryOfferedTableViewCell", bundle:classBundle)
        self.offeredTableView.register(nib, forCellReuseIdentifier:GKHistoryOfferedTableViewCell.cellIdentifier())
        
        self.requestedTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.offeredTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        updateViews()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.canShowTutorial = true
        
        if (self.showTestimonial)
        {
            self.showTestimonial = false
            showTestimonialController(self.testimonialName,
                                      relatedId: self.testimonialId,
                                      type:  self.testimonialType,
                                      byClient: self.testimonialByClient)
        }
        else if (self.showDetail)
        {
            self.showDetail = false
            showDetailController(self.showDetailId, type: self.showDetailType, isParticipant: self.showDetailIsParticipan)
        }
        
        requestData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.canShowTutorial = false
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKShowUserProfileDelegate -
    
    func GKShowProfile(_ userID: Int)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.anotherUserID = userID
        
        self.present(
            UINavigationController(rootViewController: profileController), animated: true, completion: nil
        )
    }
    
    // MARK: - Protocols GKHistoryPostState -
    
    func GKHandlePostState(_ postData: GKDictionary)
    {
        let idPost   = postData["id"] as! Int
        let typePost = postData["post_type"] as! Int
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)  // "Finish Post?"
        
        // Cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(cancelAction)
        
        let status = postData["status"] as! Int
        
        if status == 6
        {
            // Unpause
            let unpauseAction = UIAlertAction(title: "Unpause", style: .destructive) { (action) in
                self.unpausePost(idPost, typePost: typePost)
            }
            alertController.addAction(unpauseAction)
        }
        else
        {
            // Pause
            if typePost != 2
            {
                let pauseAction = UIAlertAction(title: "Pause", style: .destructive) { (action) in
                    self.pausePost(idPost, typePost: typePost)
                }
                alertController.addAction(pauseAction)
            }
            // Finish
            let finishAction = UIAlertAction(title: "Finish", style: .destructive) { (action) in
                self.finishPost(idPost, typePost: typePost, postData: postData as NSDictionary)
            }
            alertController.addAction(finishAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func GKHandleParticipationState(_ postData: GKDictionary)
    {
        
        let clients = postData["clients"] as! [GKDictionary]
        //  let relatedID = CLIENTS["id"] as! Int
        let clientData = clients[0] 
        let relatedID = clientData["id"] as! Int
        //     let typePost  = postData["type"] as! Int
        let typePost : Int!
        let valueExists = postData["type"] as? Int != nil
        if valueExists{
            typePost = postData["type"]  as! Int
        }
        else
        {
            typePost = postData["post_type"] as! Int
            
        }
        let alertController = UIAlertController(title: nil, message: "Finish Participation?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        
        let destroyAction = UIAlertAction(title: "Finish", style: .destructive) { (action) in
            self.finishParticipation(relatedID, typePost: typePost, postData: postData as NSDictionary)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Protocol UITableViewDataSource -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == requestedTableView {
            
            return self.requestedData.count
        }
        else {
            
            return self.offeredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == requestedTableView
        {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: GKHistoryRequestedTableViewCell.cellIdentifier(), for: indexPath
                ) as! GKHistoryRequestedTableViewCell
            
            cell.delegate = self
            cell.loadData(requestedData[indexPath.row])
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: GKHistoryOfferedTableViewCell.cellIdentifier(), for: indexPath
                ) as! GKHistoryOfferedTableViewCell
            
            cell.delegate = self
            cell.loadData(offeredData[indexPath.row])
            
            return cell
        }
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard anotherUserID == nil || self.onlyInProgress else
        {
            return
        }
        tempIndex = indexPath.row
        let dataPost = isRequestedViewSelected ? requestedData[indexPath.row] :
            offeredData[indexPath.row]
        let typePost : Int?
        print(dataPost)
       
        let valueExists = dataPost["type"] as? GKDictionary != nil
        if valueExists{
            typePost = (dataPost["type"]  as! Int)
        }
        else
        {
            typePost = (dataPost["post_type"] as! Int)

        }
        
        if (isRequestedViewSelected)
        {
            showPost(dataPost, typePost: typePost!)
        }
        else if (typePost == 1)
        {
           //  let data = dataPost["serve"] as! GKDictionary
            let status = dataPost["status"] as! Int

                            if (status == 1) // status == 4
                        {
            let detailViewController = GKDetailServeViewController(nibName: "GKDetailServeViewController", bundle: nil)
            detailViewController.data  = dataPost
                            detailViewController.delegate = self
            detailViewController.isParticipant = true
            self.navigationController?.pushViewController(detailViewController, animated: true)
                       }
        }
            
        else if (typePost == 3)
        {
          //  let data = dataPost["request_help"] as! GKDictionary
            let status = dataPost["status"] as! Int
 
                        if (status == 1) // status == 4
                        {
            let detailViewController = GKDetailRequestHelpViewController(nibName: "GKDetailRequestHelpViewController", bundle: nil)
            detailViewController.data  = dataPost
            detailViewController.delegate = self
            detailViewController.isParticipant = true
            self.navigationController?.pushViewController(detailViewController,
                                                          animated: true)
                       }
            
        }
        
//        else if (typePost == 5)
//        {
//
//                let detailViewController = GKDetailRequestPositivityViewController(nibName: "GKDetailRequestPositivityViewController", bundle: nil)
//                detailViewController.data  = dataPost
//                self.navigationController?.pushViewController(detailViewController,
//                                                              animated: true)
//
//
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        if tableView == requestedTableView
        {
            return GKHistoryRequestedTableViewCell.heightForCell()
        }
        else
        {
            return GKHistoryOfferedTableViewCell.heightForCell()
        }
    }
    
    // MARK: - UIAlertDelegate
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        if (buttonIndex == 0)
        {
            let data = alertView.layer.value(forKey: "data") as! GKDictionary
            self.temValue = data
            // let postType = data["type"] as! Int
            
            let postType : Int!
            let valueExists = data["type"] as? Int != nil
            if valueExists{
                postType = data["type"]  as! Int
            }
            else
            {
                postType = data["post_type"] as! Int
                
            }
            if (self.isRequestedViewSelected)
            {
                if (postType == 1)
                {
                    loadPostServe(data )
                }
                else if (postType == 3)
                {
                    loadPostHelpRequest(data )
                }
            }
            else
            {
                var author: GKDictionary!
                
                // Serve
                //                if postType == 1
                //                {
                //
                //                    author = (self.temValue["serve"] as? NSDictionary)?["author"] as! GKDictionary
                //                }
                //                    // Request positivity
                //                else if (postType == 5)
                //                {
                //
                //                    author = (temValue["request_positivity"] as? NSDictionary)?["author"] as! GKDictionary
                //                }
                //                    // Request Help
                //                else
                //                {
                //
                //                    author = (self.temValue["request_help"] as? NSDictionary)?["author"] as! GKDictionary
                //                }
                
                let addTestimonialViewController = GKAddTestimonialViewController(nibName: "GKAddTestimonialViewController", bundle: nil)
                addTestimonialViewController.firstName = (data["first_name"] as! String).capitalized +  " " + (data["last_name"] as! String).capitalized
                let clients = data["clients"] as! [GKDictionary]
                //  let relatedID = CLIENTS["id"] as! Int
                let clientData = clients[0]
                addTestimonialViewController.relatedID = clientData["id"] as! Int
                //addTestimonialViewController.relatedID = data["id"] as! Int
                addTestimonialViewController.type = postType
                addTestimonialViewController.byClient = true
                self.navigationController?.pushViewController(addTestimonialViewController, animated: true)
            }
        }
    }
    
    // MARK: - GKHistoryParticipantDelegate
   func requestOfferedListData() -> GKDictionary
   {
     self.requestOfferedData()
     return self.tempData[tempIndex]
    }
    // MARK: - IBActions -
    
    @IBAction func actionSwitchViews(_ sender: UISegmentedControl)
    {
        isRequestedViewSelected = (sender.selectedSegmentIndex == 0)
        self.requestedData.removeAll()
        self.offeredData.removeAll()
        requestData()
    }
    
    // MARK: - Posts -
    
    func showPost(_ dataPost: GKDictionary, typePost: Int)
    {
        guard isAuthor(dataPost["author"] as! GKDictionary) else
        {
            
            isRequestedViewSelected = false
            
            updateViews()
            requestData()
            
            return
        }
        
        if let status = dataPost["status"] as? Int
        {
            guard status != 6 else
            {
                return
            }
        }
        
        // Serve
        if typePost == 1
        {
            loadPostServe(dataPost)
        }
            // Request Positivity
        else if typePost == 2
        {
            loadPostRequestPositivity(dataPost)
        }
            // Request Help
        else if typePost == 3
        {
            loadPostHelpRequest(dataPost)
        }
    }
    
    func showTestimonialController(_ name: String, relatedId: Int, type: Int,
                                   byClient: Bool)
    {
        let addTestimonialViewController = GKAddTestimonialViewController(nibName: "GKAddTestimonialViewController", bundle: nil)
        addTestimonialViewController.firstName = name
        addTestimonialViewController.relatedID = relatedId
        addTestimonialViewController.type = type
        addTestimonialViewController.byClient = byClient
        self.navigationController?.pushViewController(addTestimonialViewController,
                                                      animated: true)
    }
    
    func showDetailController(_ id: Int, type: Int, isParticipant: Bool)
    {
        // Serve
        if type == 1
        {
            let detailViewController = GKDetailServeViewController(nibName: "GKDetailServeViewController", bundle: nil)
            detailViewController.postId  = id
            detailViewController.delegate = self
            detailViewController.isParticipant = isParticipant
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
            // Request Positivity
        else if type == 2
        {
            let detailViewController = GKDetailRequestPositivityViewController(nibName: "GKDetailRequestPositivityViewController", bundle: nil)
            detailViewController.delegate = self
            detailViewController.postId  = id
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
            // Request Help
        else if type == 3
        {
            let detailViewController = GKDetailRequestHelpViewController(nibName: "GKDetailRequestHelpViewController", bundle: nil)
            detailViewController.postId  = id
            detailViewController.delegate = self
            detailViewController.isParticipant = isParticipant
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    func loadPostHelpRequest(_ data: GKDictionary!)
    {
        let detailViewController = GKDetailRequestHelpViewController(nibName: "GKDetailRequestHelpViewController", bundle: nil)
        detailViewController.data  = data
        detailViewController.delegate = self
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func loadPostServe(_ data: GKDictionary)
    {
        let detailViewController = GKDetailServeViewController(nibName: "GKDetailServeViewController", bundle: nil)
        detailViewController.data  = data
        detailViewController.delegate = self
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func loadPostRequestPositivity(_ data: GKDictionary)
    {
        let detailViewController = GKDetailRequestPositivityViewController(nibName: "GKDetailRequestPositivityViewController", bundle: nil)
        detailViewController.data  = data
        detailViewController.delegate = self
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    // MARK: - Methods -
    
    func checkEmptyState(_ count: Int)
    {
        requestedTableView.isHidden = true
        offeredTableView.isHidden   = true
        
        if isRequestedViewSelected
        {
            if (self.onlyInProgress)
            {
                emptyLabel.text = "There are no posts in-progress between you and " +
                    self.otherUserName + "."
            }
            else
            {
                emptyLabel.text = "You have not yet created a post"
            }
            
            emptyLabel.isHidden          = (count != 0)
            requestedTableView.isHidden  = (count == 0)
        }
        else
        {
            if (self.onlyInProgress)
            {
                emptyLabel.text = "There are no posts in-progress between you and " +
                    self.otherUserName + "."
            }
            else
            {
                emptyLabel.text = "You have not yet participated in a post!"
            }
            
            emptyLabel.isHidden       = (count != 0)
            offeredTableView.isHidden = (count == 0)
        }
    }
    
    func isAuthor(_ authorData: GKDictionary) -> Bool
    {
        print( Static.sharedInstance.userId())
                let userID = Static.sharedInstance.userId()
                return (userID == (authorData["id"] as! Int))
       // return false
    }
    
    func pausePost(_ idPost: Int, typePost: Int)
    {
         var params = GKDictionary()
         params["token"] = Static.sharedInstance.token() as AnyObject?
       // params["id"]    = idPost as AnyObject?
         params["id"] = idPost as AnyObject?
       // params["active_posts"]  = 1 as AnyObject
         params["status"] = 6 as AnyObject?
         params["post_type"] = typePost as AnyObject?
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
      //  GKServiceClient.call(GKRouterClient.hostPausePost(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.hostPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            //      print(response)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data")
                if envelope.getSuccess()
                {
                    self.requestData()
                }
            }
        })
    }
    
    func unpausePost(_ idPost: Int, typePost: Int)
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
      //  params["id"]    = idPost as AnyObject?
       params["id"] = idPost as AnyObject?
        params["status"] = 1 as AnyObject?
        params["post_type"]  = typePost as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
      //  GKServiceClient.call(GKRouterClient.hostUnpausePost(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.hostPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            //      print(response)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data")
                if envelope.getSuccess()
                {
                    self.requestData()
                }
            }
        })
    }
    
    func finishPost(_ idPost: Int, typePost: Int, postData: NSDictionary)
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["id"]    = idPost as AnyObject?
        params["post_type"]  = typePost as AnyObject?
        params["status"]  = 4 as AnyObject?
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
    //    GKServiceClient.call(GKRouterClient.hostFinishPost(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.hostPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data")
                
                if envelope.getSuccess()
                {
                    //                    if (typePost == 3 || typePost == 1)
                    //                    {
                    let client: NSArray = (postData["clients"] as? NSArray)!
                    //                        let predicate = NSPredicate(format:"status == 2")
                    //                        client = client.filteredArrayUsingPredicate(predicate)
                    
                    // If have active participants
                    if (client.count > 0)
                    {
                        showAlertWithData("Leave a Testimonial", text: "You marked this job as complete. Please remember to tap on the post to leave testimonials for each participant! Leave a testimonial now?", delegate: self, cancelButtonTitle: "Yes", otherButtonTitle: "No",
                                          data: postData)
                    }
                    //                    }
                    
                    self.requestData()
                }
            }
        })
    }
    
    func finishParticipation(_ relatedID: Int, typePost: Int, postData: NSDictionary)
    {
        var params = GKDictionary()
        params["token"]      = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
      //  params["type"]       = typePost as AnyObject?
         params["type"]       = typePost as AnyObject?
        params["status"] = 4 as AnyObject?
        
        LoadingView.showLoadingView(
            "Sending...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        //GKServiceClient.call(GKRouterClient.clientFinishPost(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data")
                
                if envelope.getSuccess()
                {
                    if (typePost == 3 || typePost == 1)
                    {
                        showAlertWithData("Alert", text: "Congratulations on creating some GoodKarms ! Let the other person know how grateful you are by leaving them a testimonial.", delegate: self, cancelButtonTitle: "Yes", otherButtonTitle: "No",
                                          data: postData)
                    }
                    
                    self.requestData()
                }
            }
        })
    }
    
    func requestData()
    {
        if isRequestedViewSelected
        {
            requestRequestedData()
        }
        else
        {
            requestOfferedData()
        }
    }
    
    func requestRequestedData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        if anotherUserID != nil
        {
            params["requestedUserId"] = anotherUserID! as AnyObject?
        }
        
        if (self.onlyInProgress)
        {
            params["active_posts"] = 1 as AnyObject?
        }
        params["page_number"] = 1 as AnyObject
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.historyRequested(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data")
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["result"] as? [GKDictionary]
                    {
                        self.tempData = data
                        self.requestedData = data
                        self.updateViews()
                        self.checkEmptyState(self.requestedData.count)
                        self.showIntructions(.History)
                    }
                }
            }
        })
    }
    
    func requestOfferedData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        if anotherUserID != nil
        {
            params["requestedUserId"] = anotherUserID! as AnyObject?
        }
        
        if (self.onlyInProgress)
        {
            params["active_posts"] = 1 as AnyObject?
        }
        
        params["page_number"] = 1 as AnyObject
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.historyOffered() , parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "request_history_data" )
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["result"] as? [GKDictionary]
                    {
                        self.offeredData = data
                        self.tempData = data
                        self.updateViews()
                        self.checkEmptyState(self.offeredData.count)
                    }
                }
            }
        })
    }
    
    func updateViews()
    {
        segmentedControl.selectedSegmentIndex = (isRequestedViewSelected ? 0 : 1)
        
        if isRequestedViewSelected
        {
            requestedTableView.reloadData()
        }
        else
        {
            offeredTableView.reloadData()
        }
        
        requestedTableView.isHidden = !isRequestedViewSelected
        offeredTableView.isHidden   =  isRequestedViewSelected
    }
    
    func showIntructions(_ typeInstructions: GKInstructionsType)
    {
        guard Static.sharedInstance.canShowAccessInstructions(typeInstructions) else
        {
            return
        }
        
        let point = cellButtonPosition()
        
        if (point != nil)
        {
            let window = self.navigationController?.view
            
            if (window != nil && self.canShowTutorial)
            {
                self.canShowTutorial = false
                
                let rect = CGRect(x: 0, y: 0, width: window!.frame.width,
                                  height: window!.frame.height)
                
                let instructionsView = GKInstructionsView(frame: rect)
                instructionsView.typeInstructions = typeInstructions
                instructionsView.customPosition = CGPoint(x: 0, y: (point?.y)! + 225)
                window!.addSubview(instructionsView)
                instructionsView.show()
            }
        }
    }
    
    func cellButtonPosition() -> CGPoint!
    {
        let visibleCells = self.requestedTableView.visibleCells
        
        for c in visibleCells
        {
            let cell = c as! GKHistoryRequestedTableViewCell
            
            if (cell.withButton)
            {
                var indexPath: IndexPath!
                indexPath = self.requestedTableView.indexPath(for: cell)
                var rect = self.requestedTableView.rectForRow(at: indexPath)
                rect = CGRect(x: rect.origin.x,
                              y: rect.origin.y - self.requestedTableView.contentOffset.y,
                              width: rect.size.width,
                              height: rect.size.height)
                
                if (rect.origin.y + 350 < self.view.frame.height)
                {
                    return rect.origin;
                }
            }
        }
        return nil
    }
    
    // MARK - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if (self.isRequestedViewSelected)
        {
            showIntructions(.History)
        }
    }
    
}

