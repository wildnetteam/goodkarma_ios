//
//  GKExploreHelpTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/22/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKExploreHelpTableViewCell: UITableViewCell, UICollectionViewDelegateFlowLayout {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var containerView : UIView!
    
    @IBOutlet weak var userImageView : WebImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var followButton : UIButton!
    
    @IBOutlet weak var checkImage : UIImageView!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var difficultyLabel : UILabel!
    
    @IBOutlet weak var skillsContainer: UIView!
    
    @IBOutlet weak var dateLabel : UILabel!
    
    @IBOutlet weak var descTextView : UITextView!
    @IBOutlet weak var timestampLabel : UILabel!
    
    @IBOutlet weak var likeButton : UIButton!
    @IBOutlet weak var likeCountLabel : UILabel!
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var collectionViewPost: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Properties -
    
    var manageSkillView: GKManageSkillsView?
    var skills: [GKModelSkill] = []
    var data: GKDictionary!
    var delegate: (GKShowUserProfileDelegate & GKLikePostDelegate & GKFollowDelegate & GKHelpRequestParticipateDelegate & GKStartMessageDelegate & GKShareDelegate)?
    var popTip: AMPopTip!
    var tapGesture: UITapGestureRecognizer!
    
    static var isImages = Bool()

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        userImageView.layer.cornerRadius = userImageView.bounds.height * 0.5
        userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapImageView)))
        userImageView.isUserInteractionEnabled = true
        
        // Tap Gesture
        self.tapGesture = UITapGestureRecognizer(target: self,
                                                 action: #selector(self.tapImageView))
        
        self.userNameLabel.addGestureRecognizer(self.tapGesture)
        self.userNameLabel.isUserInteractionEnabled = true
        
        // Follow
        followButton.layer.cornerRadius = 4.0
        followButton.layer.borderColor = UIColor.baseColor().cgColor
        followButton.layer.borderWidth = 1.0
        
        self.selectionStyle = .none
        
        self.manageSkillView?.removeFromSuperview()
        
        self.manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: false)
        self.skillsContainer.addSubview(manageSkillView!)
        
        // PopTip
        self.popTip = AMPopTip()
        self.popTip.popoverColor = UIColor.black
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionViewPost.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
    }
    
    deinit
    {
        self.userNameLabel.removeGestureRecognizer(self.tapGesture)
        self.tapGesture = nil;
    }
    
    // MARK: - IBActions -
    
    @IBAction func buttonShareClicked(_ sender: Any) {
        if delegate != nil
        {
            delegate?.GKSharePost(share_text: self.titleLabel.attributedText!)
        }
    }
    
    @IBAction func followAction(_ sender: AnyObject)
    {
        let authorData = self.data["author"] as! GKDictionary
        
        if (self.data["follow"] as! Bool) {
            
            self.delegate?.GKUnfollow(authorData["id"] as! Int, name: (authorData["first_name"] as! String).capitalized +  " " + (authorData["last_name"] as! String).capitalized)
        }
        else {
            
            self.delegate?.GKFollow(authorData["id"] as! Int)
        }
    }
    
    @IBAction func sendLike(_ sender: AnyObject) {
        
        let postID = self.data["id"] as! Int
        
        self.delegate?.GKSendLike(3, postID: postID)
    }
    
    @IBAction func participateInHelpRequest(_ sender: AnyObject) {
        
        self.delegate?.GKParticipateInHelpRequest(self.data)
    }
    
    @IBAction func sendMessage(_ sender: AnyObject) {
        
        let authorData = data["author"] as! GKDictionary
        
        let userID = authorData["id"] as! Int
        let userName = (authorData["first_name"] as! String).capitalized +  " " + (authorData["last_name"] as! String).capitalized
        
        delegate?.GKSendMessage(userID, fullName: userName)
    }
    
    @IBAction func tooltipAction(_ sender: AnyObject)
    {
        popTip.showText("Seeking Help", direction: .left, maxWidth: 200, in: self,
                        fromFrame: (sender as! UIView).frame, duration: 2)
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKExploreHelpTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat {
        
        if isImages {
            return 600.0
        }
        return 600.0
    }
    
    // MARK: - UICollectionView Methods
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewPost.delegate = dataSourceDelegate
        collectionViewPost.dataSource = dataSourceDelegate
        collectionViewPost.tag = row
        //collectionViewPost.invalidateFlowLayoutDelegateMetrics = true
        collectionViewPost.reloadData()
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        self.popTip.hideForced(true)
        
        let author = self.data["author"] as! GKDictionary
        
        if let first = author["first_name"] as? String{
            if let last = author["last_name"] as? String{
                self.userNameLabel.text = first.capitalized +  " " + last.capitalized
            }
        }
        if let photo = author["photo_url"] as? String{
            self.userImageView.loadImageWithUrl(photo as NSString?)
        }
 
        if let difficulty = author["difficulty"] as? Int{
            difficultyLabel.text = difficultyToString(difficulty)
        }
        
        // Skills
        var skillsData: [GKModelSkill] = []
        
        for skillData in (self.data["skills"] as! [GKDictionary]) {
            
            skillsData.append(GKModelSkill.readFromExploreDictionary(skillData))
        }
        
        self.skills = skillsData
        
        self.manageSkillView?.frame = skillsContainer.bounds
        self.manageSkillView?.setupSkills(self.skills, hasLimits: true)
        
        // Date
        timestampLabel.text = secondsToFormatted(data["date_created"] as? Int)
        
        // Date completion
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date: Date! = formatter.date(from: data["date_completion"] as! String)
        
        if (date != nil)
        {
            formatter.dateFormat = "MM/dd/yyyy"
            self.dateLabel.text = formatter.string(from: date)
        }
        else
        {
            self.dateLabel.text = ""
        }
        
        if let title = data["title"] as? String{
            titleLabel.text   = title.decodeEmoji()
        }
        // Desc
        descTextView.text = (data["desc"] as! String).decodeEmoji()
        // Follow
        self.updateFollow(self.data["follow"] as! Bool)
        // Like
        self.updateLike(self.data["like_status"] as! Bool, likeCount: self.data["like_count"] as! Int)
        
        // Zip Code
        let helpType: Int? = self.data["help_type"] as? Int
        let zipCode: String? = self.data["zip_code"] as? String
        
        if (helpType != nil && helpType == 1 && zipCode != nil)
        {
            self.zipView.isHidden = false
            self.zipLabel.text = zipCode
        }
        else
        {
            self.zipView.isHidden = true
        }
        
        // Disable buttons
        let isMePost = Static.sharedInstance.isMe(author as NSDictionary)
        enabledView(self.followButton, enabled: !isMePost)
        enabledView(self.playButton, enabled: !isMePost)
        enabledView(self.messageButton, enabled: !isMePost)
    }
    
    @objc func tapImageView()
    {
        let authorData = self.data["author"] as! GKDictionary
        self.delegate?.GKShowProfile(authorData["id"] as! Int)
    }
    
    func updateLike(_ isLike: Bool, likeCount: Int) {
        
        likeCountLabel.text = "\(likeCount)"
        
        if isLike
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-on"), for: UIControlState())
            likeCountLabel.textColor = UIColor.white
        }
        else
        {
            likeButton.setImage(UIImage(named: "explore-bar-like-off"), for: UIControlState())
            likeCountLabel.textColor = UIColor.red
        }
    }
    
    func updateFollow(_ isFollowing: Bool) {
        
        if isFollowing {
            
            checkImage.isHidden = false
            
            followButton.setTitleColor(UIColor.white, for: UIControlState())
            followButton.backgroundColor   = UIColor.baseColor()
            followButton.layer.borderColor = UIColor.clear.cgColor
            followButton.setTitle("FOLLOWING", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        }
        else {
            checkImage.isHidden = true
            
            followButton.setTitleColor(UIColor.baseColor(), for: UIControlState())
            followButton.backgroundColor   = UIColor.white
            followButton.layer.borderColor = UIColor.baseColor().cgColor
            followButton.setTitle("FOLLOW", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsets.zero
        }
    }
}
