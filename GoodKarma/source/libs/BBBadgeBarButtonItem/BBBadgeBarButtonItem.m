//
//  BBBadgeBarButtonItem.m
//
//  Created by Tanguy Aladenise on 07/02/14.
//  Copyright (c) 2014 Riverie, Inc. All rights reserved.
//

#import "BBBadgeBarButtonItem.h"

@interface BBBadgeBarButtonItem()

@property (nonatomic) UILabel *badge_label;

@end

@implementation BBBadgeBarButtonItem

#pragma mark - Init methods

- (BBBadgeBarButtonItem *)initWithTarget:(NSObject *)target andSelector:(SEL)selector;
{
    self.menu_button = [UIButton new];
    self.menu_button.frame = CGRectMake(0, 0, 20, 20);
    [self.menu_button setImage:[UIImage imageNamed:@"menu-ico"] forState:UIControlStateNormal];
    [self.menu_button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    self = [self initWithCustomView:self.menu_button];
    if (self) {
        [self initializer];
    }
    
    return self;
}

- (void)initializer
{
    self.badgeOriginX   = 7;
    self.badgeOriginY   = -9;
    self.customView.clipsToBounds = NO;
}

#pragma mark - Methods

- (void)removeBadge
{
    [self.badge_label removeFromSuperview];
    self.badge_label = nil;
}

- (void)showBadge: (NSInteger)number
{
  
  [self removeBadge];
  
    // When changing the badge value check if we need to remove the badge
    if (number > 0) {
        // Create a new badge because not existing
        self.badge_label = [[UILabel alloc] initWithFrame:CGRectMake(self.badgeOriginX, self.badgeOriginY, 20, 20)];
        self.badge_label.backgroundColor = [UIColor redColor];
        self.badge_label.layer.cornerRadius = 10.0;
        self.badge_label.layer.masksToBounds = YES;
        // Label attributes
        self.badge_label.font = [UIFont systemFontOfSize: 14];
        self.badge_label.text = [NSString stringWithFormat:@"%ld", (long)number];
        self.badge_label.textAlignment = NSTextAlignmentCenter;
        self.badge_label.textColor = [UIColor whiteColor];
        // Add label
        [self.customView addSubview:self.badge_label];
    } else {
        [self removeBadge];
    }
}

@end
