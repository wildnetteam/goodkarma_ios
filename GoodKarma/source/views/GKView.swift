//
//  GKView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/20/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class PGCView: UIView
{
    struct Metrics
    {
        static var pad: CGFloat
        {
            return (Metrics.screen.width) * 0.04
        }
        
        static var screen: CGRect
        {
            return UIScreen.main.bounds
      }
      static var paddingHeight:CGFloat{
        return (Metrics.screen.height) * 0.04
      }
    }
  
    // MARK: - Methods -
    
    func closeView()
    {
        self.removeFromSuperview()
    }
    
    func showView()
    {
        UIApplication.shared.keyWindow?.addSubview(self)
    }
}
