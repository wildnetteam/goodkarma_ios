//
//  SendPhotoMsgCell.swift
//  SuperMom
//
//  Created by Anuj Jha on 22/09/17.
//  Copyright © 2017 Shatakshi. All rights reserved.
//

import UIKit

protocol zoomImageDelegate {
    
    func zoomImage(image:UIImage!)
}

class SendPhotoMsgCell: UITableViewCell {
    
    @IBOutlet weak var tickImageVIewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var zoomImageButton: UIButton!
    @IBOutlet weak var sendPhotoMsgImageView: UIImageView!
    @IBOutlet weak var photoMsgBackgroundView: UIView!
    @IBOutlet weak var tickMsgImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    var delegate: zoomImageDelegate?
    //var deleteImageDelegate: deleteMsgDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photoMsgBackgroundView.layer.cornerRadius = 15
        photoMsgBackgroundView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)
        coverView.backgroundColor = UIColor(red: 55.0/255, green: 162.0/255, blue: 244.0/255, alpha: 1.0)
    }
    
    
    @IBAction func zoomImageButtonTapped(_ sender: Any) {
        
        if delegate != nil{
            delegate?.zoomImage(image: sendPhotoMsgImageView.image)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func deleteMessageActionTapped(sender:Any){
//        if deleteImageDelegate != nil{
//            deleteImageDelegate?.deleteMsg()
//        }
        
    }
    
}

