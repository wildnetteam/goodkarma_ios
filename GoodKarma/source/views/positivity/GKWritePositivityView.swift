//
//  GKWritePositivityView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKWritePositivityView: UIView, UITextViewDelegate
{
  
  // MARK: - IBOutlets -
  
  @IBOutlet var view: UIView!
  
  @IBOutlet weak var messageContentView: UIView!
  @IBOutlet weak var customMessageContentField: UITextView!
  
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var sendButton: UIButton!
  
  @IBOutlet weak var optionFirstImageView: UIImageView!
  @IBOutlet weak var optionSecondImageView: UIImageView!
  @IBOutlet weak var optionThirdImageView: UIImageView!
  @IBOutlet weak var optionFourthImageView: UIImageView!
  
  // MARK: - Properties -
  
  var optionSelected = 0
  var requestPositivityID: Int!
  
  var errorView: ErrorView!
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UITextViewDelegate -
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
  {
    if text == "\n"
    {
      textView.resignFirstResponder()
      return false
    }
    
    return true;
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionSend(_ sender: AnyObject)
  {
    self.view.endEditing(true)
    self.errorView.hide()
    
    if validate()
    {
      LoadingView.showLoadingView(
        "Loading...",
        parentView: self.view, backColor: true
      )
      
      let params = getParams()
      print(params)
      GKServiceClient.call(GKRouterClient.createPositivity(), parameters: params, callback: {
        (response: AnyObject?) -> () in
        print(response!)
        LoadingView.hideLoadingView(self.view)
        
        if let res:AnyObject = response
        {
          let envelope = ResponseSkeleton(res)
          
          if envelope.getSuccess()
          {
            self.removeFromSuperview()
          }
          else
          {
            showCommonError({ (type: NoInternetResultType) -> () in
              self.actionSend(0 as AnyObject)
            })
          }
        }
        else
        {
          showConnectionError({ (type: NoInternetResultType) -> () in
            self.actionSend(0 as AnyObject)
          })
        }
      })
    }
  }
  
  @IBAction func actionCancel(_ sender: AnyObject)
  {
    self.removeFromSuperview()
  }
  
  @IBAction func actionFirstOption(_ sender: AnyObject)
  {
    optionSelected = sender.tag
    clearSelections()
    updateMessage("God bless you.")
  }
  
  @IBAction func actionSecondOption(_ sender: AnyObject)
  {
    optionSelected = sender.tag
    clearSelections()
    updateMessage("You can do it.")
  }
  
  @IBAction func actionThirdOption(_ sender: AnyObject)
  {
    optionSelected = sender.tag
    clearSelections()
    updateMessage("Stay positive.")
  }
  
  @IBAction func actionFourthOption(_ sender: AnyObject)
  {
    optionSelected = sender.tag
    clearSelections()
    updateMessage("All good things come to those who wait.")
  }
  // MARK: - Methods -
  
  
  func clearSelections()
  {
    self.optionFirstImageView.image = UIImage(named: (optionSelected == 1) ? "check-on" : "check-off")
    self.optionSecondImageView.image = UIImage(named: (optionSelected == 2) ? "check-on" : "check-off")
    self.optionThirdImageView.image = UIImage(named: (optionSelected == 3) ? "check-on" : "check-off")
    self.optionFourthImageView.image = UIImage(named: (optionSelected == 4) ? "check-on" : "check-off")
  }
  
  func updateMessage(_ message: String)
  {
    customMessageContentField.text = message
  }
  
  func getParams() -> GKDictionary
  {
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["desc"]  = customMessageContentField.text?.encodeEmoji() as AnyObject?
    params["request_positivity"] = requestPositivityID as AnyObject?
    
    return params
  }
  
  func validate() -> Bool
  {
    let baseY = self.errorView.frame.size.height;
    
    if self.customMessageContentField.text!.count < 10
    {
      self.errorView.showInPos(
        CGPoint(
          x: self.customMessageContentField.frame.minX - 8,
          y: self.customMessageContentField.frame.minY - baseY
        ),
        text: "Message should contain at least 10 characters."
      )
      
      return false;
    }
    
    return true
  }
  
  // MARK: - Setup -
  
  func setup()
  {
    view = Bundle.main.loadNibNamed("GKWritePositivityView", owner: self,
                                              options: nil)?[0] as! UIView
    view.frame = self.bounds
    view.clipsToBounds = true
    view.frame = self.bounds
    self.addSubview(view)
    
    self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
    self.messageContentView.layer.cornerRadius = 8.0
    
    self.clearSelections()
    
    self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
    self.messageContentView.addSubview(self.errorView)
  }
}
