//
//  GKProfileTestimonialTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/16/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

protocol GKProfileTestimonialCellDelegate : NSObjectProtocol
{
    func profileTestimonialCellPictureAction(_ data: NSDictionary)
}

class GKProfileTestimonialTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var pictureImageView : WebImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var clockIcon: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    
    // MARK: - Properties -
    var data : NSDictionary!
    weak var delegate : GKProfileTestimonialCellDelegate!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
        self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
        
        self.selectionStyle = .none
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKProfileTestimonialTableViewCell"
    }
    
    internal class func heightForCell(_ data: NSDictionary, width: CGFloat) -> CGFloat
    {
        let height = stringHeight(GKProfileTestimonialTableViewCell.testimonialText(data), width: Int(width), font: UIFont(name: "Roboto-Light", size: 13)!)
        
        return max(CGFloat(height) + 80, 80)
    }
    
    // MARK: - Methods -
    
    static func textAtIndex(_ index: Int, name: String) -> String!
    {
        switch index
        {
        case 1:
            return "I am grateful " + name + " was able to help me."
            
        case 2:
            return "I am grateful " + name + " is very skilled."
            
        case 3:
            return "I am grateful " + name + " is a punctual person."
            
        case 4:
            return "I am grateful " + name + " is such a friendly person."
            
        case 5:
            return "I am grateful I met " + name + "."
            
        case 6:
            return "I am grateful " + name + " was thoughtful."
            
        case 7:
            return "I am grateful " + name + " was able to put my needs first."
            
        case 8:
            return "I am grateful " + name + " had a kind demeanor."
            
        case 9:
            return "I am grateful " + name + " was able to pass along helpful thoughts and prayers."
            
        case 10:
            return "I am grateful " + name + " was able to take time out to go the extra mile for me."
            
        default:
            return nil
        }
    }
    
    static func testimonialText(_ data: NSDictionary) -> String
    {
        //let name = (data["gkarm_user"] as! NSDictionary).value(forKey: "first_name")
        let name = (data["author_first_name"])
        var testimonials : NSArray = []
        if let detail =  data["detail"] as? NSArray{
            testimonials = detail
        }
        var resultText = ""
        
        for testimonialIndex in testimonials
        {
            let index = Int(testimonialIndex as! String)
            let text = GKProfileTestimonialTableViewCell.textAtIndex(index!, name: name as! String)
            resultText = resultText + (resultText.count != 0 ? "\n" : "")
                + text!
        }
        
        return resultText
    }
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data as NSDictionary!
        print(self.data)
        
        // let author = data["author"] as! GKDictionary
        
        // Image
        self.pictureImageView.loadImageWithUrl(data["author_photo_url"] as? String as NSString?)
        
        // Name
        self.authorLabel.text = shortName((data["author_first_name"] as! String).capitalized +  " " + (data["author_last_name"] as! String).capitalized)
        self.authorLabel.sizeToFit()
        
        // Clock
        self.clockIcon.frame = CGRectSetX(self.clockIcon.frame,
                                          x: self.authorLabel.frame.maxX + 7)
        
        // Time
        self.timestampLabel.frame = CGRectSetX(self.timestampLabel.frame,
                                               x: self.clockIcon.frame.maxX + 5)
         self.timestampLabel.text = secondsToFormatted(data["createdAt"] as? Int)
        
        // Content
        self.contentLabel.text = GKProfileTestimonialTableViewCell.testimonialText(data as NSDictionary)
        //        self.contentLabel.sizeToFit()
    }
    
    // MARK: - Actions -
    
    @IBAction func pictureAction(_ sender: UIButton)
    {
        self.delegate.profileTestimonialCellPictureAction(self.data)
    }
}
