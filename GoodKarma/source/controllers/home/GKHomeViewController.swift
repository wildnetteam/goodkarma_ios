 //
 //  GKHomeViewController.swift
 //  GoodKarma
 //
 //  Created by Paul Aguilar on 6/27/16.
 //  Copyright © 2016 Teclalabs. All rights reserved.
 //
 
 import UIKit
 import AddressBookUI
 import MessageUI
 import FirebaseAnalytics
 
 class GKHomeViewController: GKViewController, GKHomeButtonContainerDelegate, GKHomeFilterDelegate, GKStartMessageDelegate,
    GKCreatePostDelegate,GKMainFeedDelegate,ABPeoplePickerNavigationControllerDelegate,GKSosAddContactDelegate,deleteSosData , UIScrollViewDelegate
 {
    // MARK: - IBOutlets -
    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var buttonContainerView: GKHomeButtonContainerView!
    @IBOutlet weak var mainFeedContainer: GKMainFeedTableView!
    @IBOutlet weak var emptyContainerView: UIView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var emptyButton: UIButton!
    
    let titleButton = UIButton()
    
    // MARK: - Properties -
    var info: GKDictionary = [:]
    var data  = [GKDictionary]()
    var homeCurrentState: GKHomeState?
    var filterParams: GKDictionary = [:]
    var firstTime = true
    var isFilter = false
    var urlString : String!
    var delegate : GKShowSosContact!
    var pageNo = 1
    let addContactsViewObject = Bundle.main.loadNibNamed(
        "GKSosAddContactsView", owner: self, options: nil)?[0] as! GKSosAddContactsView
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        
        Analytics.logEvent("Home", parameters: [
            "Screen_Name": "Home" as NSObject,
            "User_Email": Static.sharedInstance.email() as NSObject
            ])
        
        super.viewDidLoad()
        //   emptyImageView.isHidden = true
        //    self.title = "Home"
        addContactsViewObject.delegate = self
        mainFeedContainer.deleteDelegate = self
        
        setMenuBarBadge()
        
        self.buttonContainerView.delegate = self
        self.mainFeedContainer.parentController = self
        self.mainFeedContainer.delegate = self
        self.mainFeedContainer.setupTableView()
        self.mainFeedContainer.cleanData()
        self.mainFeedContainer.requestData()
        
        emptyButton.layer.cornerRadius = 5.0
        
        updateRightBarItems()
        //    cleanBackButton()
        
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(GKHomeViewController.refreshData),
                                               name:NSNotification.Name(rawValue: "UpdatedBlocked"),
                                               object:nil)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        titleButton.bounds = CGRect(x: 0.0, y: 0.0, width: 100.0, height: 40.0)
        titleButton.titleLabel?.font = UIFont.fontRoboto(.Medium, size: 18)
        titleButton.setTitle("Home", for: UIControlState())
        titleButton.setTitleColor(UIColor.black, for: UIControlState())
        titleButton.addTarget(
            self,
            action: #selector(GKHomeViewController.showInitialHomeView),
            for: .touchUpInside
        )
        self.navigationItem.titleView = titleButton
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKHomeFilterDelegate -
    
    func GKFilterUpdateParams(_ filterParams: GKDictionary) {
        
        self.filterParams = filterParams
        isFilter = true
        self.mainFeedContainer.cleanData()
        self.mainFeedContainer.requestData()
    }
    
    // MARK: - Protocol GKMainFeedDelegate -
    
    func passRequest(request : String)
    {
        urlString = request
    }
    
    func GKRequestData(_ completion: @escaping ([GKDictionary]) -> ())
    {
        changeNavigationBarTitle()
        
        var params = GKDictionary()
        var urlString : String!
        
        var value : Int!
        if let postType = homeCurrentState
        {
            value = postStateParam(postType)
        }
        if value == 5
        {
            isSos = true
            urlString = GKRouterClient.listSOS()
            params["token"] = Static.sharedInstance.token() as AnyObject?
        }
        else{
            isSos = false
            urlString = GKRouterClient.home()
            if isFilter == true
            {
                params = filterParams
            }
            else{
                params["interval"] = "" as AnyObject
                params["distance"] = "" as AnyObject
                params["zip_code"] = "" as AnyObject
                params["difficulty"] = "" as AnyObject
                params["following"] = 1 as AnyObject
                params["follower"] = 1 as AnyObject as AnyObject
                params["skills"] = "" as AnyObject?
                params["number"] = ""  as AnyObject?
                params["post_type"] = "" as AnyObject? as AnyObject
            }
            if self.mainFeedContainer.isSearch{
                self.pageNo = self.mainFeedContainer.currentPageSearch!
            }
            else{
                self.pageNo = self.mainFeedContainer.currentPage!
            }
            params["token"] = Static.sharedInstance.token() as AnyObject?
            params["is_home"] = 1 as AnyObject
            params["page_number"] = self.pageNo  as AnyObject?
            params["is_search"] = mainFeedContainer.searchBarMainFeed.text  as AnyObject?

            if let postType = homeCurrentState
            {
                params["post_type"] = postStateParam(postType) as AnyObject?
            }
        }
  
        print(params)
        if (self.navigationController != nil)
        {
            if self.pageNo==1 {
                LoadingView.showLoadingView(
                    "Loading...",
                    parentView: self.navigationController?.view, backColor: true
                )
            }
        }
        
        GKServiceClient.call(urlString, parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            if (self.navigationController != nil)
            {
                LoadingView.hideLoadingView(self.navigationController?.view)
            }
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                print(res)
                if envelope.getSuccess()
                {
                    self.data = [GKDictionary]()
                    print(self.data)
                    if isSos == true
                    {
                        let object = response?.object(forKey: "object") as! [GKDictionary]
                        isSos = true
                        self.checkEmptyState(object.count)
                        
                        self.data = object
                        
                        completion(object )
                    }
                    else if let data = envelope.getResource()["result"] as? [GKDictionary]
                    {
                        let items = (data as NSArray) //self.removeLiveStream(data as NSArray) 

                        if self.mainFeedContainer.isSearch{
                            self.mainFeedContainer.currentPageSearch = self.mainFeedContainer.currentPageSearch! + 1
                            self.mainFeedContainer.totalCountSearch = envelope.getResource()["count"] as! NSInteger?

                            if data.count == 0{
                                showAlert("Sorry!", text: "No data found.")
                                self.mainFeedContainer.tableView.tableFooterView = nil
                            }
                            else{
                                self.mainFeedContainer.tableView.tableFooterView = self.mainFeedContainer.activityView
                            }
                        }
                        else{
                            self.checkEmptyState(items.count)
                            if items.count > 0{
                                self.mainFeedContainer.currentPage = self.mainFeedContainer.currentPage! + 1
                                self.mainFeedContainer.totalCount = envelope.getResource()["count"] as! NSInteger?
                                self.mainFeedContainer.tableView.tableFooterView = self.mainFeedContainer.activityView
                            }
                        }
                        self.checkInstructions(items.count)
                        
                        completion(items as! [GKDictionary])
                    }
                }
                else{
                    showAlert("Sorry!", text: "An error has occured.")
                }
            }
        })
    }
    
    // MARK: - Protocol GKHomeButtonContainerDelegate -
    
    func GKDidSelectButtonSection(_ section: GKHomeState) {
        
        homeCurrentState = section
        
        self.mainFeedContainer.searchBarMainFeed.text = ""
        self.mainFeedContainer.searchBarMainFeed.resignFirstResponder()
        self.mainFeedContainer.searchBarMainFeed.setShowsCancelButton(false, animated: true)
        self.mainFeedContainer.cleanData()
        self.mainFeedContainer.requestData()
        
        updateRightBarItems()
    }
    
    // MARK: - Protocol GKStartMessageDelegate -
    
    func GKSendMessage(_ userID: Int, fullName: String)
    {
        let messageViewController = GKMessagesViewController(nibName:
            GKMessagesViewController.xibName(), bundle: nil)
        
        let anotherUser = GKMessageAnotherUser(uid: userID, fullName: fullName)
        messageViewController.anotherUser = anotherUser
        self.navigationController?.pushViewController(messageViewController, animated:true)
    }
    
    // MARK: - IBActions -
    
    //  @IBAction func actionRequest(sender: AnyObject)
    //  {
    //    showCreateRequestHelpView()
    //  }
    //
    //  @IBAction func actionServe(sender: AnyObject)
    //  {
    //    showCreateServeView()
    //  }
    
    @IBAction func actionEmpty(_ sender: AnyObject)
    {
        compose()
    }
    
    // MARK: - Bar Actions -
    
    @objc func showFilters() {
        
        let filterController = GKHomeFilterViewController(nibName: "GKHomeFilterViewController", bundle: nil)
        filterController.homeState = homeCurrentState
        filterController.delegate = self
        filterController.isSetting = false
        filterController.params = filterParams
        
        self.navigationController?.present(UINavigationController(rootViewController: filterController), animated: true, completion: nil)
    }
    
    func search() {
        print("SEARCH")
    }
    
    @objc func compose() {
        if let homeState = homeCurrentState {
            switch homeState {
            case .Positivity  : showCreateRequestPositive()
            case .Gratitude   : showCreateGratitudeView()
            case .HelpRequest : showCreateServeView()
            case .Serve       : showCreateRequestHelpView()
            case .SOS : showSOSView()
            }
        }
    }
    
    @objc func showInitialHomeView() {
        
        buttonContainerView.cleanSelections()
        
        homeCurrentState = nil
        
        self.mainFeedContainer.cleanData()
        self.mainFeedContainer.requestData()
        
        updateRightBarItems()
    }
    
    // MARK: - Methods -
    
    func changeNavigationBarTitle(){
        if homeCurrentState == .Positivity {
            
            titleButton.setTitle("Positivity", for: UIControlState())
        }
        else if homeCurrentState == .Gratitude {
            
            titleButton.setTitle("Gratitude", for: UIControlState())
        }
        else if homeCurrentState == .HelpRequest {
            
            titleButton.setTitle("Offer Help", for: UIControlState())
        }
        else if homeCurrentState == .Serve {
            
            titleButton.setTitle("Seek Help", for: UIControlState())
        }
        else  if homeCurrentState == .SOS{
            
            titleButton.setTitle("SOS", for: UIControlState())
        }
    }
    
    func showSOSView()
    {
        if self.data.count < 2
        {
            addContactClicked()
        }
        else
        {
            let alertController = UIAlertController(title: "Message", message: "Maximum two contacts can be added" , preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showCreateRequestPositive()
    {
        let requestController = GKRequestPositiveAndHelpViewController(nibName: "GKRequestPositiveAndHelpViewController", bundle: nil)
        requestController.delegate = self
        requestController.barTitle = "Request Positivity"
        requestController.isOnlyPositive = true
        self.navigationController?.present(UINavigationController(rootViewController: requestController), animated: true, completion: nil)
    }
    
    func showCreateGratitudeView()
    {
        let createGratitudeController = GKCreateGratitudeViewController(nibName: "GKCreateGratitudeViewController", bundle: nil)
        createGratitudeController.delegate = self
        self.navigationController?.present(UINavigationController(rootViewController: createGratitudeController), animated: true, completion: nil)
    }
    
    func showCreateRequestHelpView()
    {
        let requestController = GKRequestPositiveAndHelpViewController(nibName: "GKRequestPositiveAndHelpViewController", bundle: nil)
        requestController.delegate = self
        self.navigationController?.present(UINavigationController(rootViewController: requestController), animated: true, completion: nil)
    }
    
    func showCreateServeView()
    {
        let serveController = GKCreateServeViewController(nibName: "GKCreateServeViewController", bundle: nil)
        serveController.delegate = self
        self.navigationController?.present(UINavigationController(rootViewController: serveController), animated: true, completion: nil)
    }
    
    func showPositivityInstructions()
    {
        guard Static.sharedInstance.canShowAccessPositivityInstructions() else {
            return
        }
        
        if let res = self.filterParams["responses"] as? Bool
        {
            if (res)
            {
                return
            }
        }
        
        let window = self.navigationController?.view
        
        let rect = CGRect(
            x: 0, y: 64,
            width: window!.frame.width, height: window!.frame.height
        )
        
        let positivityInstructions = GKPositivityInstructionView(frame: rect)
        window!.addSubview(positivityInstructions)
        positivityInstructions.show()
    }
    
    func showIntructions(_ typeInstructions: GKInstructionsType) {
        
        guard Static.sharedInstance.canShowAccessInstructions(typeInstructions) else {
            return
        }
        
        let window = self.navigationController?.view
        
        if (window != nil)
        {
            let rect = CGRect(
                x: 0, y: 0,
                width: window!.frame.width, height: window!.frame.height
            )
            
            let instructionsView = GKInstructionsView(frame: rect)
            instructionsView.typeInstructions = typeInstructions
            window!.addSubview(instructionsView)
            instructionsView.show()
        }
    }
    
    func checkInstructions(_ dataCount: Int) {
        
        if homeCurrentState == .Positivity && dataCount > 0 {
            
            showPositivityInstructions()
        }
        else if homeCurrentState == .Gratitude {
            
            showIntructions(GKInstructionsType.Gratitude)
        }
        else if homeCurrentState == .HelpRequest {
            
            showIntructions(GKInstructionsType.OfferHelp)
        }
        else if homeCurrentState == .Serve {
            
            showIntructions(GKInstructionsType.SeekHelp)
        }
    }
    
    func checkEmptyState(_ count: Int)
    {
        guard count == 0 else {
            
            emptyContainerView.isHidden = true
            mainFeedContainer.isHidden  = false
            
            return
        }
        
        emptyContainerView.isHidden = false
        mainFeedContainer.isHidden  = true
        
        // Main Home
        guard let homeState = homeCurrentState else {
            
            emptyButton.isHidden = true
            emptyLabel.isHidden = false
            
            emptyLabel.text = "Nothing new in your area. Change your location settings to expand your reach."
            return
        }
        
        // Positivity, Gratitude, Help Request and Serve
        emptyButton.isHidden = false
        emptyLabel.isHidden = false
        
        switch homeState
        {
        case .Positivity:
            emptyLabel.text = "Check back to see if any of your buddies need some positivity sent their way!"
            emptyButton.isHidden = true
            emptyImageView.isHidden =  true
            
        case .Gratitude:
            emptyLabel.text = "Show others what you are grateful for and motivate them to do the same!"
            emptyButton.setTitle("Gratitude", for: UIControlState())
            emptyImageView.isHidden = true
            
        case .HelpRequest:
            emptyLabel.text = "Can't find someone who needs help? Create a help offer."
            emptyButton.setTitle("Serve", for: UIControlState())
            emptyImageView.isHidden = true
            
        case .Serve:
            emptyLabel.text = "Can't find the help you need? Create a help request"
            emptyButton.setTitle("Request", for: UIControlState())
            emptyImageView.isHidden = true
            
        case .SOS :
            emptyLabel.text = "Add contacs for SOS"
            emptyButton.setTitle("+ Add Contacts", for: UIControlState())
            emptyImageView.isHidden = false
            emptyImageView.image  = UIImage(named : "Add Contacts")
        }
    }
    
    func postStateParam(_ homeState: GKHomeState) -> Int
    {
        switch homeState
        {
        case .Positivity  : return 2
        case .Gratitude   : return 4
        case .HelpRequest : return 1
        case .Serve       : return 3
        case .SOS : return 5
        }
    }
    
    func updateRightBarItems()
    {
        // State
        //new
        if let state = homeCurrentState
        {
            self.navigationItem.rightBarButtonItem  = nil
            self.navigationItem.rightBarButtonItems = []
            
            let composeImage = UIImage(named: "add")?.withRenderingMode(.alwaysOriginal)
            let filterImage  = UIImage(named: "filter")?.withRenderingMode(.alwaysOriginal)
            
            let composeButton = UIButton(type: .custom)
            composeButton.frame = CGRect(x: 0.0, y: 0.0, width: 22.0, height: 22.0)
            composeButton.setImage(composeImage, for: UIControlState())
            composeButton.addTarget(
                self,
                action: #selector(GKHomeViewController.compose),
                for: .touchUpInside
            )
            
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: 0.0, y: 0.0, width: 22.0, height: 22.0)
            filterButton.setImage(filterImage, for: UIControlState())
            filterButton.addTarget(
                self,
                action: #selector(GKHomeViewController.showFilters),
                for: .touchUpInside
            )
            
            let filterBarButton  = UIBarButtonItem(customView: filterButton)
            let composeBarButton = UIBarButtonItem(customView: composeButton)
            let value = postStateParam(state) 
            //new
            if value == 5
            {
                self.navigationItem.rightBarButtonItems = [composeBarButton]
            }
            else
            {
                self.navigationItem.rightBarButtonItems = [composeBarButton, filterBarButton]
            }
        }
            // Main
        else
        {
            //      let searchImage = UIImage(named: "search")?.imageWithRenderingMode(.AlwaysOriginal)
            let filterImage = UIImage(named: "filter")?.withRenderingMode(.alwaysOriginal)
            
            //      let searchButton = UIButton(type: .Custom)
            //      searchButton.frame = CGRectMake(0.0, 0.0, 22.0, 22.0)
            //      searchButton.setImage(searchImage, forState: .Normal)
            //      searchButton.addTarget(
            //        self,
            //        action: #selector(GKHomeViewController.search),
            //        forControlEvents: .TouchUpInside
            //      )
            
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: 0.0, y: 0.0, width: 22.0, height: 22.0)
            filterButton.setImage(filterImage, for: UIControlState())
            filterButton.addTarget(
                self,
                action: #selector(GKHomeViewController.showFilters),
                for: .touchUpInside
            )
            
            let filterBarButton = UIBarButtonItem(customView: filterButton)
            //      let searchBarButton = UIBarButtonItem(customView: searchButton)
            
            self.navigationItem.rightBarButtonItem  = nil
            self.navigationItem.rightBarButtonItems = []
            //      self.navigationItem.rightBarButtonItems = [filterBarButton, searchBarButton]
            self.navigationItem.rightBarButtonItem = filterBarButton
        }
    }
    
    @objc func refreshData()
    {
        self.mainFeedContainer.cleanData()
        self.mainFeedContainer.requestData()
    }
    
    func removeLiveStream(_ data: NSArray) -> NSArray
    {
        let result = NSMutableArray()
        
        for item in data
        {
            let d = item as! NSDictionary
            let type = (d["liveStreamingStatus"] as! NSNumber).int32Value
            var add = true
            
            if (type == 1)
            {
                add = false
            }
            if (add)
            {
                result.add(item)
            }
        }
        return result
    }
    
    func removeDueDate(_ data: NSArray) -> NSArray
    {
        let result = NSMutableArray()
        
        for item in data
        {
            let d = item as! NSDictionary
            let type = (d["post_type"] as! NSNumber).int32Value
            var add = true
            
            if (type == 3)
            {
                if let dateCompletion = d["date_completion"] as? String
                {
                    if dateCompletion == "Invalid date" || (dateCompletion.contains("1970"))
                    {
//                        if (isDateExpired(dateCompletion))
//                        {
                            add = false
//                        }
                    }
                }
                else if (type == 3)
                {
                    add = false
                }
            }
            if (add)
            {
                result.add(item)
            }
        }
        return result
    }
    
    func isDateExpired(_ dateStr: String) -> Bool
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date: Date! = formatter.date(from: dateStr)
        
        if (Date().differenceInDaysWithDate(date) < 0)
        {
            return true
        }
        return false
    }
    
    
    // MARK: - ABAddressBook Methods
    
    func addContactClicked() {
        let c = ABPeoplePickerNavigationController()
        c.peoplePickerDelegate = self
        c.displayedProperties = [
            NSNumber(value: kABPersonPhoneProperty as Int32)
        ]
        
        ABAddressBookRequestAccessWithCompletion(c, { success, error in
            if success {
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(c, animated: true, completion: nil)
            }
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier)
    {
        peoplePicker.dismiss(animated: true, completion: nil)
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        let contact = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
        let nameCFString: CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
        let name: NSString = nameCFString as NSString

        checkNumberFromContactBook(contact: contact, name: name as String)
    }
    
    func checkNumberFromContactBook(contact: String, name: String){
        
        var fullContact = (((contact.replacingOccurrences(of: " ", with: "")).replacingOccurrences(of: "-", with: "")).replacingOccurrences(of: "(", with: "")).replacingOccurrences(of: ")", with: "")
        print(contact, name, fullContact)
        
        if fullContact.contains("+") && fullContact.countPlainCharacters() > 10 {
            var info = GKDictionary()
            info["phn_no"] = (fullContact.plainText().components(separatedBy: CharacterSet.decimalDigits.inverted).joined().suffix(10)) as AnyObject?
            info["country_code"] = ((fullContact.replacingOccurrences(of: fullContact.plainText().components(separatedBy: CharacterSet.decimalDigits.inverted).joined().suffix(10), with: "")).plainText()) as AnyObject?
            info["name"] = name as AnyObject?
            self.passDataToBeAdded(info: info)
        }
        else if fullContact.countPlainCharacters() >= 10{
            openSOSView(contact: String((fullContact.plainText().components(separatedBy: CharacterSet.decimalDigits.inverted).joined().suffix(10))), name: name)
        }
        else{
                openSOSView(contact: "", name: name)
        }

    }
    
    func openSOSView(contact: String, name: String){
        self.addContactsViewObject.frame = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!
        self.addContactsViewObject.frame.size.height = ((UIApplication.shared.delegate as! AppDelegate).window?.frame)!.size.height
        self.addContactsViewObject.frame.origin.x = 0.0
        self.addContactsViewObject.frame.origin.y = 0.0
        self.addContactsViewObject.setUp()
        self.addContactsViewObject.phoneNumberTextField.text = contact
        self.addContactsViewObject.nameTextField.text = name
        self.view.addSubview(self.addContactsViewObject)
    }
    
    // MARK: - Protocol GKCreatePostDelegate
    
    func createPostDidFinish()
    {
        self.mainFeedContainer.exploreList = []
        self.mainFeedContainer.isSearch = false
        self.mainFeedContainer.currentPage = 1
        self.mainFeedContainer.lastPageBool = false
        self.mainFeedContainer.currentPageSearch = 1
        self.mainFeedContainer.lastPageBoolSearch = false
        self.mainFeedContainer.totalCountSearch = 0
        self.mainFeedContainer.totalCount = 0
        
        self.mainFeedContainer.requestData()
    }
    
    func passDataToBeAdded(info : GKDictionary)
    {
        self.info = (info as GKDictionary?)!
        self.uploadSOSContact()
            {
                (isUpdated) in
                self.data.append(info)
                self.checkEmptyState(self.data.count)
                self.mainFeedContainer.updateData(data : self.data)
                isSos = true
        }
    }
    
    func delete(index : Int)
    {
        self.data.remove(at: index)
        //new
        if self.data.count == 0
        {
            self.checkEmptyState(0)
        }
    }
    
    func uploadSOSContact(onCompletion: @escaping addSosServiceResponse) -> ()
    {
        var params = GKDictionary()
        isSos = true
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["name"] = info["name"] as AnyObject?
        params["country_code"] = info["country_code"] as AnyObject?
        params["phn_number"] =  info["phn_no"]  as AnyObject?
        if params["phn_number"] == nil
        {
            params["phn_no"] = " " as AnyObject
        }
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        GKServiceClient.call(GKRouterClient.addSOS(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response!)
                    onCompletion(true)
                }
                else
                {
                    let error = envelope.getErrors()
                    //new
                    let errorMessage = error!["object"] as! String
                    //new
                    let alertController = UIAlertController(title: "Message", message: errorMessage , preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                showConnectionError({ (type: NoInternetResultType) -> () in
                })
            }
        })
    }
 }
 
 extension GKHomeViewController
 {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height{
            //pageNo = pageNo+1
            self.pageNo = self.pageNo + 1
            //call API.
        }
    }
 }
