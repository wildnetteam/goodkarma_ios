//
//  GKModelSkill.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/30/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKModelSkill: GKModelProtocol {
    
    var uid: ID!
    var name: String!
    var categoryID: ID!
    var otherText: String!
    
    func text() -> String
    {
        if (self.otherText.count > 0)
        {
            return self.name + ": " + self.otherText
        }
        
        return self.name
    }
    
    static func readFromDictionary(_ dictionary: GKDictionary) -> GKModelSkill {
        
        return GKModelSkill(
            uid: dictionary["id"] as! Int,
            name: dictionary["name"] as! String,
            categoryID: dictionary["category_id"] as! Int,
            otherText: ""
        )
    }
    
    static func readFromExploreDictionary(_ dictionary: GKDictionary) -> GKModelSkill {
        
        return GKModelSkill(
            uid: dictionary["id"] as! Int,
            name: dictionary["desc"] as! String,
            categoryID: dictionary["category"] as! Int,
            otherText: ""
        )
    }
    
    static func readFromListDictionary(_ listDictionary: [GKDictionary]) -> [GKModelSkill] {
        
        var skills: [GKModelSkill] = []
        print(listDictionary)
        for dic in listDictionary {
            
            skills.append(GKModelSkill.readFromDictionary(dic))
        }
        
        return skills
    }
    
    //updated
    static func readForProfile(dict : [GKDictionary]) -> [GKModelSkill]
    {
        var skills: [GKModelSkill] = []
        print(dict)
        for dic in dict {
            let value = dic["gkarm_skill"] as! GKDictionary
            skills.append(GKModelSkill.readFromDictionary(value))
        }

        return skills
    }
    static func readFromDictionaryGetProfile(_ listDictionary: [GKDictionary]) -> [GKModelSkill] {
        
        var skills: [GKModelSkill] = []
        for dic in listDictionary {
            print(dic)
            if (dic["gkarm_skill"] as? [String:Any]) != nil {
                skills.append(GKModelSkill.readFromDictionary(dic["gkarm_skill"] as! GKDictionary))
            }
        }
        
        return skills
    }
}
