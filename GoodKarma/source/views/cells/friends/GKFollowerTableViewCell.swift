//
//  GKFollowerTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/21/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKFollowerTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var pictureImageView : WebImageView!
  @IBOutlet weak var nameLabel        : UILabel!
  @IBOutlet weak var followButton     : UIButton!
  @IBOutlet weak var checkImage       : UIImageView!
  
  // MARK: - Properties -
  
  var dataUser: GKDictionary!
  var delegate: GKFollowDelegate?
  var mutual_follow: Bool!
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    // Picture
    self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
    self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    
    // Check Button
    //        self.checkButton.hidden = true
    
    self.selectionStyle = .none
    // Follow button
    self.followButton.layer.borderWidth = 1.0
    self.followButton.layer.cornerRadius = 5.0
    
    self.checkImage.contentMode = .scaleAspectFit
  }
  
  // MARK: - IBActions -
  
  @IBAction func followAction(_ sender: AnyObject)
  {
    if self.mutual_follow!
    {
      
      self.delegate?.GKUnfollow(dataUser["id"] as! Int, name: "\(self.dataUser["first_name"] as! String)" + "\(self.dataUser["last_name"] as! String)")
    }
    else
    {
      self.delegate?.GKFollow(dataUser["id"] as! Int)
    }
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKFollowerTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 85.0
  }
  
  // MARK: - Methods -
  
  func loadData(_ dataUser: GKDictionary, mutual_follow: Bool)
  {
    self.dataUser      = dataUser
    self.mutual_follow = mutual_follow
    
    self.nameLabel.text = "\(self.dataUser["first_name"] as! String)".capitalized + "\(self.dataUser["last_name"] as! String)".capitalized
    self.pictureImageView.loadImageWithUrl(self.dataUser["photo_url"] as? String as NSString?)
    
    updateFollow(mutual_follow)
  }
  
  func updateFollow(_ isFollowing: Bool)
  {
    if isFollowing
    {
      checkImage.isHidden = false
      
      followButton.setTitleColor(UIColor.white, for: UIControlState())
      followButton.backgroundColor = UIColor.baseColor()
      followButton.layer.borderColor = UIColor.clear.cgColor
      followButton.setTitle("FOLLOWING", for: UIControlState())
      
      followButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
    }
    else
    {
      checkImage.isHidden = true
      
      followButton.setTitleColor(UIColor.baseColor(), for: UIControlState())
      followButton.backgroundColor = UIColor.white
      followButton.layer.borderColor = UIColor.baseColor().cgColor
      followButton.setTitle("FOLLOW", for: UIControlState())
      
      followButton.titleEdgeInsets = UIEdgeInsets.zero
    }
  }
}
