//
//  GKBlockedUserTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 10/6/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKBlockedUserTableViewCell: UITableViewCell {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var pictureImageView : WebImageView!
  @IBOutlet weak var nameLabel        : UILabel!
  @IBOutlet weak var unBlockButton    : UIButton!
  
  // MARK: - Properties -
  
  var dataUser: GKDictionary!
  var delegate: GKUnblockDelegate?
  
  override func awakeFromNib()
  {
    super.awakeFromNib()
    
    self.selectionStyle = .none
    
    // Picture
    self.pictureImageView.layer.cornerRadius = self.pictureImageView.frame.width * 0.5
    self.pictureImageView.activityIndicator.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
    
    // Unblock button
    self.unBlockButton.layer.cornerRadius = 5.0
  }
  
  // MARK: - IBActions -
  
  @IBAction func unblockAction(_ sender: AnyObject)
  {
    self.delegate?.GKUnblock(dataUser["id"] as! Int)
  }
  
  // MARK: - Class Methods -
  
  internal class func cellIdentifier() -> String {
    
    return "GKBlockedUserTableViewCell"
  }
  
  internal class func heightForCell() -> CGFloat {
    
    return 85.0
  }
  
  // MARK: - Methods -
  
  func loadData(_ dataUser: GKDictionary)
  {
    self.dataUser      = dataUser
    self.nameLabel.text = "\(self.dataUser["first_name"] as! String)".capitalized + "\(self.dataUser["last_name"] as! String)".capitalized
    self.pictureImageView.loadImageWithUrl(self.dataUser["photo_url"] as? String as NSString?)
  }
}
