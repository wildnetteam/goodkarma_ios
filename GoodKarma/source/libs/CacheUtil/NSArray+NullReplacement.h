//
//  NSArray+NullReplacement.h
//  Hawkr
//
//  Created by Eliezer de Armas on 15/07/15.
//  Copyright (c) 2015 TeclaLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (NullReplacement)

- (NSArray *)arrayByReplacingNullsWithBlanks;

@end
