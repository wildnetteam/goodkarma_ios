//
//  GKPostFacebookView.swift
//  GoodKarma
//
//  Created by Eliezer de Armas on 3/11/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

protocol GKPostFacebookDelegate: NSObjectProtocol
{
    func postFacebookYes(_ postFacebookView: GKPostFacebookView)
    func postFacebookNo(_ postFacebookView: GKPostFacebookView)
}

class GKPostFacebookView: UIView
{
    @IBOutlet var view: UIView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    weak var delegate: GKPostFacebookDelegate!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    fileprivate func setup()
    {
        // View
        view = Bundle.main.loadNibNamed("GKPostFacebookView", owner: self,
                                                  options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.backgroundColor = UIColor.clear
        
        self.addSubview(view)
        self.alpha = 0
        self.backgroundColor = UIColor.clear
        
        // Content View
        self.contentView.layer.cornerRadius = 12;
        
        // Yes Button
        self.yesButton.layer.cornerRadius = 4;
        
        // No Button
        self.noButton.layer.cornerRadius = 4;
    }
    
    func show() 
    {
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                self.alpha = 1
            },
                                   completion:
            { _ in
        })
    }
    
    func hide()
    {
        UIView.animate(withDuration: 0.5, delay:0,
                                   options:UIViewAnimationOptions(),
                                   animations:
            {
                self.alpha = 0
            },
                                   completion:
            { _ in
                self.removeFromSuperview()
         })
    }
    
    // MARK: - Actions
    
    @IBAction func buttonAction(_ sender: AnyObject)
    {
    }
    
    @IBAction func yesAction(_ sender: AnyObject)
    {
        self.delegate.postFacebookYes(self)
    }
    
    @IBAction func noAction(_ sender: AnyObject)
    {
        self.delegate .postFacebookNo(self)
        self.hide()
    }
}
