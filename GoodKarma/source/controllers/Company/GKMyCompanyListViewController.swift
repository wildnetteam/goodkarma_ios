//
//  GKMyCompanyListViewController.swift
//  GoodKarma
//
//  Created by Shivani on 25/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyCompanyListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var buttonAddNewCompany: UIButton!
    
    var arrayCompanyList : NSArray = []
    var isInMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Companies"
        if isInMenu
        {
            setMenuBarBadge()
        }
        
        let nib = UINib(nibName: "GKMyCompanyListTableViewCell", bundle: nil)
        self.table.register(nib, forCellReuseIdentifier: "GKMyCompanyListTableViewCellIdentifier")
        table.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getMyCompnayList()
    }
    
    // MARK: - IBActions
    
    @IBAction func buttonAddNewCompanyClicked(_ sender: Any) {
        let GKAddComanyViewControllerObject = GKAddComanyViewController(nibName: "GKAddComanyViewController", bundle: nil)
        GKAddComanyViewControllerObject.parentObject = self
        self.navigationController?.present(
            UINavigationController(rootViewController: GKAddComanyViewControllerObject), animated: true, completion: nil
        )
    }
    
    // MARK: - UITableView Delegates

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return arrayCompanyList.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GKMyCompanyListTableViewCellIdentifier", for: indexPath) as! GKMyCompanyListTableViewCell
        
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width / 2
        cell.labelCompanyName.text = ((arrayCompanyList[indexPath.row] as! NSDictionary).value(forKey: "companyName") as! String)
        cell.imageViewProfile.loadImageWithUrl(((arrayCompanyList[indexPath.row] as! NSDictionary).value(forKey: "image") as! String as NSString))
        
        return cell
    }

    // MARK: - Method - Webservices
    
    func getMyCompnayList()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page_number"] = 1 as AnyObject
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.getCompanyList(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    self.arrayCompanyList = (envelope.getResource() as! NSDictionary).value(forKey: "result") as! NSArray
                    print(self.arrayCompanyList)
                    self.table.reloadData()
                }
                else if (res["error"] as? NSDictionary != nil) {
                    showMessageError("Alert", text: (res["error"] as? NSDictionary)?.value(forKey: "object") as! String, callback: { (type: NoInternetResultType) -> () in
                    })
                }
                else {
                    showCommonErrorWithoutSignOut({ (type: NoInternetResultType) -> () in
                        self.getMyCompnayList()
                    })
                }
            }
        })
    }
    
}
