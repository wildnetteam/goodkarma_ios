//
//  GKInviteFriends.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/10/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKInviteFriends: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var inviteFacebookButton: UIButton!
    @IBOutlet weak var inviteContactsButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var facebookButtonHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Initializer -
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK: - Methods -
    
    func close() {
        
        self.removeFromSuperview()
    }
    
    func setup() {
        
        var isSocial = false
        
        if (Static.sharedInstance.isLoginWithFacebook())
        {
            isSocial = true
        }
 
        view = Bundle.main.loadNibNamed("GKInviteFriends", owner: self,
                                                  options: nil)?[0] as! UIView
        view.frame = self.bounds
        view.clipsToBounds = true
        view.frame = self.bounds
        self.addSubview(view)
        
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        contentView.layer.cornerRadius = 8.0
        
        // Header Image
        self.headerImage.layer.cornerRadius = 5
        
        if !isSocial {
            inviteFacebookButton.isUserInteractionEnabled = false
            inviteFacebookButton.alpha = 0.2
            inviteContactsButton.setTitleColor(UIColor.white, for: UIControlState())
            inviteContactsButton.backgroundColor = UIColor.baseColor()
        }
    }
    
}
