//
//  GKCompanyProfileViewController.swift
//  GoodKarma
//
//  Created by Shivani on 25/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import FloatRatingView

class GKCompanyProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
    
    @IBOutlet weak var scrollViewDetail: UIScrollView!
    
    @IBOutlet weak var collectionViewHeader: UICollectionView!
    @IBOutlet weak var collectionViewPhotos: UICollectionView!
    @IBOutlet weak var collectionViewVideos: UICollectionView!
    
    @IBOutlet weak var tableViewPosts: UITableView!
    
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var viewAbout: UIView!
    @IBOutlet weak var viewPhotos: UIView!
    @IBOutlet weak var viewVideos: UIView!
    @IBOutlet weak var viewRateUs: UIView!
    
    @IBOutlet weak var imageViewProfile: UIImageView!
    
    @IBOutlet weak var labelMembersCount: UILabel!
    @IBOutlet weak var labelMembersName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var labelAccountType: UILabel!
    
    @IBOutlet weak var buttonSeeAll: UIButton!
    @IBOutlet weak var buttonSeeAllVideos: UIButton!
    
    @IBOutlet weak var ratingViewDetail: FloatRatingView!
    @IBOutlet weak var ratingViewEdit: FloatRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collectionViewHeader.register(CompanyDetailCollectionViewCell.self, forCellWithReuseIdentifier: "CompanyDetailCollectionViewCell")

    }

    //MARK: TABLE VIEW DELEGATES

//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 2
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        <#code#>
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionViewHeader {
            return 6
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewHeader {
            return CGSize(width: (ScreenSize.SCREEN_WIDTH/3), height: 85)

        }
        else{
            return CGSize(width: ScreenSize.SCREEN_WIDTH == 320 ? 145 : (ScreenSize.SCREEN_WIDTH == 375 ? 172 : 190) , height: ScreenSize.SCREEN_HEIGHT == 320 ? 85 : (ScreenSize.SCREEN_HEIGHT == 375 ? 85 : 85))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //if collectionView == collectionViewHeader {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompanyDetailCollectionViewCell", for: indexPath) as! CompanyDetailCollectionViewCell
            
            cell.labelTitle.text = "Home"
            return cell
       // }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {

    }
    
    @IBAction func buttonSeeAllClicked(_ sender: Any) {
    }
    
    @IBAction func buttonSeeAllVideosClicked(_ sender: Any) {
    }
    
}
