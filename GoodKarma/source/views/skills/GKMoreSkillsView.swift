//
//  GKMoreSkillsView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKMoreSkillsView: UIView, UITableViewDataSource, UITableViewDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet var view: UIView!
  @IBOutlet var containerView: UIView!
  @IBOutlet var tableView: UITableView!
  
  // MARK: - Properties -
  
  var skills: [GKModelSkill] = [] {
    didSet {
      self.reloadData()
    }
  }
  
  // MARK: - Initializer -
  
  override init(frame: CGRect)
  {
    super.init(frame: frame)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  // MARK: - Protocols -
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return self.skills.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(
      withIdentifier: "SkillCell", for: indexPath)
    
    //    cell.loadData(self.friends[indexPath.row])
    cell.textLabel?.text = skills[indexPath.row].name
    cell.selectionStyle = .none
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt
    indexPath: IndexPath) -> CGFloat
  {
    return 55
  }
  
  // MARK: - IBActions -
  
  @IBAction func closeAction(_ sender: AnyObject) {
    UIView.animate(withDuration: 0.3, delay:0,
                               options:UIViewAnimationOptions(),
                               animations:
      {
        self.alpha = 0
      },
                               completion:
      { _ in
        self.removeFromSuperview()
    })
  }
  
  // MARK: - Static Methods -
  static func show(_ skills: [GKModelSkill]) {
    
    // TODO: show skills
    //    print(skills)
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let window = appDelegate.window!
    
    let moreSkillView = GKMoreSkillsView(frame: CGRect(x: 0.0, y: 0.0, width: window.frame.width, height: window.frame.height))
    moreSkillView.alpha = 0
    moreSkillView.skills = skills
    
    window.addSubview(moreSkillView)
    
    UIView.animate(withDuration: 0.5, delay:0,
                               options:UIViewAnimationOptions(),
                               animations:
      {
        moreSkillView.alpha = 1
      },
                               completion:
      { _ in
    })
  }
  
  
  // MARK: - Methods -
  
  func reloadData() {
    
    tableView.reloadData()
  }
  
  fileprivate func setup()
  {
    view = Bundle.main.loadNibNamed("GKMoreSkillsView", owner: self,
                                              options: nil)?[0] as! UIView
    view.frame = self.bounds
    view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    self.addSubview(view)
    
    self.backgroundColor = UIColor.clear
    
    // ContentView
    self.containerView.layer.cornerRadius = 6
    
    // TableView
    self.tableView.register(
      UITableViewCell.self,
      forCellReuseIdentifier: "SkillCell"
    )
    self.tableView.tableFooterView = UIView(frame: CGRect.zero)
  }
  
}
