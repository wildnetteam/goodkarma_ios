//
//  RecieveTextMsgCell.swift
//  SuperMom
//
//  Created by Anuj Jha on 19/09/17.
//  Copyright © 2017 Shatakshi. All rights reserved.
//

import UIKit
protocol recievingSpamDelegate {
    
    func markSpam(tag: Int)
}

class RecieveTextMsgCell: UITableViewCell {

    @IBOutlet weak var userNameLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var spamButton: UIButton!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var recieverMessageLabel: UILabel!
    var delegate: recievingSpamDelegate?
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        userImage.layer.cornerRadius = userImage.frame.size.width/2
//        userImage.layer.masksToBounds = true
        recieverMessageLabel.sizeToFit()
        innerView.layer.cornerRadius = 15
        innerView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
        coverView.backgroundColor = UIColor(red: 222.0/255, green: 222.0/255, blue: 222.0/255, alpha: 1.0)
        //userNameLabel.font = UIFont(name: "Biko", size: 17.0)
    }
    
    @IBAction func spamButtonTapped(_ sender: UIButton) {
//        if delegate != nil{
//            delegate?.markSpam(tag: sender.tag)
//        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func deleteMessageActionTapped(sender:Any){
//        if delegate != nil{
//            delegate?.deleteMsg()
//        }
        
    }
    
}
