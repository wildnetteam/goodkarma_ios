//
//  MessagesRightCell.swift
//  ShoutOut
//
//  Created by Eliezer de Armas on 16/9/15.
//  Copyright (c) 2015 cesar. All rights reserved.
//

import UIKit

class MessagesRightCell: UITableViewCell
{
//    @IBOutlet weak var pictureImageView: WebImageView!
    @IBOutlet weak var messageContent : UIView!
    @IBOutlet weak var contentBubble  : UIView!
    @IBOutlet weak var messageText    : UILabel!
    @IBOutlet weak var timestamp: UILabel!
    var data: NSDictionary!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
//        self.pictureImageView.layer.cornerRadius
//            = CGRectGetWidth(self.pictureImageView.frame)/2
//        self.pictureImageView.layer.borderColor = UIColor
//            .colorToPercent(187, green:193, blue:198).CGColor
//        self.pictureImageView.layer.borderWidth = 1.3
//        self.pictureImageView.activityIndicator.transform =
//            CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7)
      
        // Message Content
        self.messageContent.layer.cornerRadius = 7
        
        // Content Bubble
//        self.contentBubble.backgroundColor = UIColor.clearColor()
    }
    
    func loadData(_ data: NSDictionary)
    {
        self.data = data
      
//        self.pictureImageView.loadFBPicture(dicUser["facebook_id"] as! String)
      
        // Message
        self.messageText.text = (data["detail"] as? String)?.decodeEmoji()
        
        // Timestamp
//        let date = data.objectForKey("date_created") as! String
//        self.timestamp.text = Common.vetTime(date)
    }
  
}
