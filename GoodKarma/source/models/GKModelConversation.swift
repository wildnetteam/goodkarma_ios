////
////  GKModelConversation.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/24/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation
//
//struct GKModelConversation: GKModelProtocol {
//  
//  // MARK: - Properties -
//  
//  var uid:  String!
//  var last_message: GKModelMessage!
//  var last_update: String?
//  var another_user: GKModelUser!
//  
//  // MARK: - Methods -
//  // MARK: - Class Methods -
//  
//  static func readFromDictionary(dictionary: GKDictionary) -> GKModelConversation {
//    
//    var conversation = GKModelConversation()
//    conversation.uid          = dictionary["_id"] as! String
//    conversation.last_message = GKModelMessage.readFromDictionary(dictionary["last_message"] as! [String: AnyObject])
//    
//    var user_key = "user_a"
//    // Check who is the another person
//    if let user_a = dictionary[user_key] as? [String: AnyObject] {
//      let user_a_uid = user_a["_id"] as! String
////      if user_a_uid == ALServiceUser.instance.fetchCredential(ALServiceUser.Credential_Types.Uid) {
////        user_key = "user_b"
////      }
//    }
//    
//    // Last Update
//    if let date_str = dictionary["last_update"] as? String {
//      conversation.last_update = NSDate.parseDateWhitFormat(date_str)
//    }
//    
//    conversation.another_user = GKModelUser.readFromDictionaryConversation(dictionary[user_key] as! [String: AnyObject])
//    
//    return conversation
//  }
//
//  
//  
//}