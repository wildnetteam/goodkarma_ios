//
//  StringExtension.swift
//  Faveo
//
//  Created by Cesar Velasquez on 23/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit
import Foundation

extension String {
  
  func isEmailValid() -> Bool {
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
    
    return emailTest.evaluate(with: self)
  }
  
  func isEmpty() -> Bool {
    
    return !(self.plainText().count > 0)
  }
  
  func countPlainCharacters() -> Int {
    
    return self.plainText().count
  }
  
  func plainText() -> String {
    
    return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
  }
  
  func sizeInWidth(_ aWidth: CGFloat, font: UIFont) -> CGSize {
    
    let sizeAllowed = CGSize(width: aWidth, height: 999.0)
    
    return self.boundingRect(with: sizeAllowed,
      options: .usesLineFragmentOrigin,
      attributes: [NSAttributedStringKey.font: font],
      context: nil).size
  }
    
    func encodeEmoji() -> String
    {
        let strText: NSString = self as NSString
        let unicodedData: Data = strText.data(using: String.Encoding.nonLossyASCII.rawValue,
                                                             allowLossyConversion: true)!
        
        return NSString(data: unicodedData, encoding: String.Encoding.utf8.rawValue)! as String
        
    }
    
    func decodeEmoji() -> String
    {
        let strText: NSString = self as NSString
        let unicodedData: Data! = strText.data(using: String.Encoding.utf8.rawValue,
                                                              allowLossyConversion: true)!
        
        if (unicodedData != nil)
        {
            if let string = NSString(data: unicodedData, encoding: String.Encoding.nonLossyASCII.rawValue)
            {
                return string as String
            }
        }
        
        return self
    }
}
