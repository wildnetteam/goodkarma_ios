//
//  GKMyJobProfileViewController.swift
//  GoodKarma
//
//  Created by Shivani on 10/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyJobProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        self.title = "Arya Stark"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

   

}
