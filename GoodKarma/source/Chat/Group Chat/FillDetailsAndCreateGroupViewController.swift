//
//  FillDetailsAndCreateGroupViewController.swift
//  GoodKarma
//
//  Created by Anuj Jha on 05/12/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//
import Alamofire
import UIKit

class FillDetailsAndCreateGroupViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var groupImageButton: UIButton!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var participantsCountLabel: UILabel!
    
    var selectedMembersIdArray: NSMutableArray? = []
    var selectedMembersDetailedArray: NSMutableArray? = []
    var imagePicker = UIImagePickerController()
    var selectedImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        participantsCountLabel.text = "\(String(describing: selectedMembersDetailedArray!.count)) OF 200"
        
        collectionView.register(UINib.init(nibName: "SelectedUserCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "selectedUserCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        groupImageButton.layer.cornerRadius = groupImageButton.frame.size.width/2
        groupImageButton.layer.borderWidth = 0.5
        groupImageButton.layer.borderColor = UIColor.gray.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createGroupButtonTapped(_ sender: Any) {
        if groupNameTextField.text == "" || selectedImage == nil {
            showAlert("Good Karma", text: "Please fill all the details.")
        }else if (selectedMembersDetailedArray?.count)!<2{
            showAlert("Good Karma", text: "Please select more than 2 members to create a group.")
        }else{
            createGroupWebService()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == groupNameTextField{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func createGroupWebService(){
        
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["group_name"] = groupNameTextField.text as AnyObject?
        //params["group_image"] = selectedImage as AnyObject?
        //params["group_user"] = "[\(String(describing: selectedMembersIdArray! ))]" as AnyObject?
        
        let skillsInfo  = selectedMembersIdArray?.componentsJoined(by: ",")
        let info = "[" + "\(skillsInfo!)" + "]"
        params["group_user"] = info as AnyObject?
        
        print(params)
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in params {
                multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
            }
            if  let imageData = UIImageJPEGRepresentation(self.selectedImage! , 0.6)
            {
                multipartFormData.append(imageData, withName: "group_image", fileName: "image", mimeType: "image/png")
            }
            
        }, to: GKRouterClient.createGroup(), method: .post, headers: nil,
           encodingCompletion: {
            
            encodingResult in
            switch encodingResult
            {
                
            case .success(let upload, _, _):
                LoadingView.hideLoadingView(self.navigationController?.view)
                upload.responseJSON { response in
                    
                    if response.result.value! is NSDictionary, !(response.result.value is NSNull){
                        print(response.result.value!)
                        let res = (response.result.value! as! NSDictionary).object(forKey: "object") as! NSDictionary
                        let groupId = res.object(forKey: "groupId") as! String
                        let userArray = res.object(forKey: "users") as! NSArray
                        for index in 0..<userArray.count{
                            let userId = (userArray[index] as! NSDictionary).object(forKey: "id") as! Int
                            FireBaseManager.defaultManager.setUserGroup(id: "\(userId)", groupId: groupId)
                            
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
                break
                
            case .failure(let encodingError):
                LoadingView.hideLoadingView(self.navigationController?.view)
                print(encodingError)
                break
            }
        })
    }
    
    @IBAction func groupImageButtonTapped(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        let cameraButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(cameraButton)
        
        let galleryButton = UIAlertAction(title: "Photo Library", style: .default)
        { _ in
            print("Photo & Video Library")
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(galleryButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion:nil)
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage?
        
        if (selectedImage != nil){
            groupImageButton.setImage(selectedImage, for: .normal)
        }
    }
    
    @objc func crossButtonTapped(sender: UIButton){
        print("sender.tag---\(sender.tag)")
        
        selectedMembersIdArray?.remove(String((selectedMembersDetailedArray?.object(at: sender.tag) as! NSDictionary).object(forKey: "id") as! Int))
        selectedMembersDetailedArray?.removeObject(at: sender.tag)
        
        collectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        participantsCountLabel.text = "\((selectedMembersDetailedArray?.count)!) OF 200"
        return (selectedMembersDetailedArray?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell = UICollectionViewCell()
        
        let selectedUserCell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectedUserCell", for: indexPath) as! SelectedUserCollectionViewCell
        selectedUserCell.crossButton.tag = indexPath.item
        selectedUserCell.crossButton.addTarget(self, action: #selector(crossButtonTapped), for: .touchUpInside)
        selectedUserCell.userImageView.sd_setImage(with: URL(string: (selectedMembersDetailedArray![indexPath.row] as! NSDictionary).object(forKey: "photo_url") as! String))
        selectedUserCell.userNameLabel.text = (selectedMembersDetailedArray![indexPath.row] as! NSDictionary).object(forKey: "first_name") as? String
        cell = selectedUserCell
        return cell
    }
    
}

