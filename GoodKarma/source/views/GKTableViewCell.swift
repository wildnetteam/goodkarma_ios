//
//  GKTableViewCell.swift
//  Version3
//
//  Created by Paul Aguilar on 8/3/15.
//  Copyright (c) 2015 Alumnify Inc. All rights reserved.
//

import UIKit

class GKTableViewCell: UITableViewCell {
    
    var index: IndexPath!

    // MARK: - Initializer -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        selectionStyle = .none
        clipsToBounds = true
    }
    
    // MARK: - Methods -
    
    // MARK: - Class Methods -
    
    class func cellIdentifier() -> String {
        let identifier = NSStringFromClass(self)
//        if identifier.rangeOfString("Version3.") != nil {
//            identifier = identifier.stringByReplacingOccurrencesOfString("Version3.", withString: "")
//        }
      
      print(identifier)
        return identifier
    }
    
    class func heightForCell() -> CGFloat {
        // subclasses override this
        return 0.0
    }
    
//    class func nibName() -> String {
//        return ALTableViewCell.cellIdentifier()
//    }
}
