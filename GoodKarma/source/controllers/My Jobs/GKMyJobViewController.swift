//
//  GKMyJobViewController.swift
//  GoodKarma
//
//  Created by Shivani on 06/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyJobViewController: UIViewController,UITableViewDelegate, UITableViewDataSource
{
    var pageNo = 1
    var count = 0
    var result = [GKDictionary]()
    var isInMenu = false
    var assignedJob = "JobAssignedToMe"
    @IBOutlet weak var segmenyControl: UISegmentedControl!
    @IBOutlet weak var noDataFoundView: UIView!
    
    @IBOutlet weak var tabel: UITableView!
    
    
    override func viewDidLoad()
    {
        noDataFoundView.isHidden = true
        super.viewDidLoad()
        
        if isInMenu
        {
            self.title = "My Participation"
            setMenuBarBadge()
        }
        else
        {
            self.title = "Posts"
        }
        let nib = UINib(nibName: "GKMyJobTableViewCell", bundle: nil)
        self.tabel.register(nib, forCellReuseIdentifier: "GKMyJobTableViewCell")
        //self.noDataFoundView.isHidden = false
        self.requestJobAssignedToMeList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func segmentAction(_ sender: Any)
    {
      self.result.removeAll()
        if segmenyControl.selectedSegmentIndex == 0
        {
            assignedJob = "JobAssignedToMe"
            requestJobAssignedToMeList()
            
        }
        else{
            assignedJob = "JobAssgnedByMe"
            requestJobAssignedByMeList()
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        if segmenyControl.selectedSegmentIndex == 0
        {
            assignedJob = "JobAssignedToMe"
            requestJobAssignedToMeList()
            
        }
        else{
            assignedJob = "JobAssgnedByMe"
            requestJobAssignedByMeList()
        }
    }
    // MARK: - Protocol UITableViewDataDource -
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.result.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GKMyJobTableViewCell", for: indexPath) as! GKMyJobTableViewCell
        cell.loadData(result[indexPath.row] as AnyObject)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  GKMyJobTableViewCell.heightForCell()
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let detailViewController = GKJobDetailViewController(nibName: "GKJobDetailViewController", bundle: nil)
        if assignedJob == "JobAssignedToMe"
        {
            detailViewController.isJobAssignedByMe = false
        }
        else
        {
            detailViewController.isJobAssignedByMe = true
        }
        detailViewController.dictData = result[indexPath.row]
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    // MARK: - Method - Webservices
    
    func requestJobAssignedToMeList()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page_number"] = pageNo as AnyObject
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.listJobsAssignedToMe(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    let value = res.object(forKey: "object") as AnyObject
                    
                    if self.pageNo == 1
                    {
                        self.result.removeAll()
                        self.result = (value.object(forKey: "result") as AnyObject) as! [GKDictionary]
                        if self.result.count == 0
                        {
                           // self.noDataFoundView.isHidden = true
                            
                        }
                    }
//                    else
//                    {
//                        let array = (value.object(forKey: "result") as AnyObject) as! [GKDictionary]
//                        if array.count  != 0
//                        {
//                            self.result.append(value.object(forKey: "result") as AnyObject as! GKDictionary)
//
//                        }
//                      else
//                        {
//                            self.pageNo = self.pageNo - 1
//                        }
//                    }
                    
                   
                        
                    
                    self.tabel.reloadData()
                }
                else
                {
                  //  self.noDataFoundView.isHidden = true
                }
            }
            else
            {
                let alertController = UIAlertController(title: "Sorry", message: "Server time out", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                self.present(alertController, animated: false, completion: nil)
            }
        })
    }
    
    func requestJobAssignedByMeList()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["page_number"] = pageNo as AnyObject
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.listJobsAssignedByMe(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    let value = res.object(forKey: "object") as AnyObject
                    
                    if self.pageNo == 1
                    {
                        self.result.removeAll()
                        self.result = (value.object(forKey: "result") as AnyObject) as! [GKDictionary]
                        if self.result.count == 0
                        {
                           // self.noDataFoundView.isHidden = true
                            
                        }
                    }
                   // else
//                    {
//                        let array = (value.object(forKey: "result") as AnyObject) as! [GKDictionary]
//                        if array.count  != 0
//                        {
//                            let tempVal = (value.object(forKey: "result") as AnyObject) as! GKDictionary
//                            self.result.append(tempVal)
//                           //self.result.append(value as! GKDictionary)
//                        }
//                   else
//                        {
//                            self.pageNo = self.pageNo - 1
//                        }
//                    }
               
                    self.tabel.reloadData()
                }
              
            }
            else
            {
                let alertController = UIAlertController(title: "Sorry", message: "Server time out", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                self.present(alertController, animated: false, completion: nil)
            }
        })
    }
}


extension GKMyJobViewController
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height{
            //pageNo = pageNo+1
            self.pageNo = self.pageNo + 1
            //call API.
            if assignedJob == "JobAssignedToMe"
            {
                requestJobAssignedToMeList()
            }
            else
            {
                requestJobAssignedByMeList()
            }
            
            
        }
    }
}
