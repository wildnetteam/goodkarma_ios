//
//  GKNewMessageContactViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 7/18/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKNewMessageContactViewController: GKViewController, UISearchBarDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var search_bar: UISearchBar!
  @IBOutlet weak var searchContainer: UIView!
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Properties -
  
  var searchActive = false
  var inboxViewController: GKInboxViewController!
  
  var contacts: [GKDictionary] = []
  var filteredContacts: [GKDictionary] = []
  
  deinit
  {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Override UIViewController -
  
    override func viewDidLoad() {
      
      super.viewDidLoad()
      
      self.title = "Message Friend"
      
      self.tableView.register(
        UINib(nibName: GKNewMessageContactTableViewCell.cellIdentifier(),bundle: nil),
        forCellReuseIdentifier: GKNewMessageContactTableViewCell.cellIdentifier()
      )
//      self.tableView.tableFooterView = UIView(frame: CGRect.zero)
//      self.tableView.separatorStyle = .none
//
      // Load Data
      self.reloadData()
      
//      self.navigationItem.rightBarButtonItem = UIBarButtonItem(
//        barButtonSystemItem: .Search,
//        target: self,
//        action: #selector(self.showSearchContacts)
//      )
      
//      search_bar.backgroundColor = UIColor.whiteColor()
      search_bar.backgroundColor = UIColor.superLightGray()
      search_bar.barTintColor =  UIColor.white
      search_bar.showsCancelButton = true
//      search_bar.delegate = self
//      search_bar.frame = CGRectMake(
//        10.0, 10.0,
//        bar_container.bounds.size.width - 2 * 10.0,
//        55.0
//      )
      search_bar.layer.borderColor = UIColor.superLightGray().cgColor
      search_bar.layer.borderWidth = 1;
//      search_bar.placeholder = "Search alumni"
      
      NotificationCenter.default.addObserver(
        self,
        selector:#selector(self.keyboardWillShow(_:)),
        name:NSNotification.Name.UIKeyboardWillShow,
        object:nil)
      
      NotificationCenter.default.addObserver(
        self,
        selector:#selector(self.keyboardWillHide(_:)),
        name:NSNotification.Name.UIKeyboardWillHide,
        object:nil)
      
      cleanBackButton()
  }
  
  override func viewDidAppear(_ animated: Bool)
  {
    super.viewDidAppear(animated)
    
    // Request Data
    requestContactsList()
  }
  
  // MARK: - Protocols
  // MARK: - Protocol UISearchBarDelegate -
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    
//    searchActive = true;
    
//    filteredContacts = contacts
    
    self.reloadData()
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//    searchActive = false;
    self.reloadData()
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    
    searchActive = false
    
    searchBar.text = ""
    searchBar.resignFirstResponder()
    
    self.reloadData()
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
//    searchActive = true;
    
    searchBar.resignFirstResponder()
    
    self.reloadData()
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
    if searchText.count == 0 {
      
      searchActive = false
    }
    else {
      
      searchActive = true
      
      filteredContacts = contacts.filter {
        
        let tmp: NSString = $0["full_name"] as! NSString
        let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
        return range.location != NSNotFound
      }
    }
    
//    if filteredContacts.count == 0
//    {
//      searchActive = false
//    }
//    else
//    {
//      searchActive = true
//    }
    
    self.reloadData()
  }

  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    if searchActive {

        return 5
      //return filteredContacts.count
    }
    else {
return 5
      //return self.contacts.count
    }
   
  }
  
  func tableView(_ tableView: UITableView, cellForRowAtIndexPath
    indexPath: IndexPath) -> UITableViewCell
  {
    let cell = tableView.dequeueReusableCell(
      withIdentifier: GKNewMessageContactTableViewCell.cellIdentifier(), for: indexPath) as! GKNewMessageContactTableViewCell
    
    if searchActive {
    
      cell.loadData(self.filteredContacts[indexPath.row])
    }
    else {
      
      cell.loadData(self.contacts[indexPath.row])
    }
    
    return cell
  }
  
  // MARK: - Protocol UITableViewDelegate -
  
  func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
  {
    var contact = GKDictionary()
    
    if searchActive
    {
      contact = self.filteredContacts[indexPath.row]
    }
    else
    {
      contact = self.contacts[indexPath.row]
    }
    
    self.openConversation(contact["id"] as! Int, fullName: (contact["first_name"] as! String) +  " " + (contact["last_name"] as! String))
  }
  
  func tableView(_ tableView: UITableView, heightForRowAtIndexPath
    indexPath: IndexPath) -> CGFloat
  {
    return GKNewMessageContactTableViewCell.heightForCell()
  }
  
  // MARK: - Notifications -
  
  @objc func keyboardWillShow(_ notification: Notification)
  {
    if let userInfo = notification.userInfo
    {
      if let keyboardSize =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
      {
        UIView.animate(withDuration: 0.3, animations: {
          self.tableView.frame = CGRectSetHeight(
            self.tableView.frame,
            h: self.view.bounds.size.height - (self.searchContainer.bounds.size.height + keyboardSize.height + 64.0))  //- 60)
          }, completion: { _ in
            
//            self.reloadAndScroll()
        })
      }
    }
  }
  
  @objc func keyboardWillHide(_ notification: Notification)
  {
//    self.reloadAndScroll()
    
    if let userInfo = notification.userInfo
    {
      if let _ =  (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
      {
        UIView.animate(withDuration: 0.3, animations: {
          
          self.tableView.frame = CGRectSetHeight(
            self.tableView.frame,
            h: self.view.bounds.size.height - (self.searchContainer.bounds.size.height + 64.0))  // - 60
          }, completion: { _ in
            
        })
      }
    }
  }
  
  // MARK: - Methods -

//  func showSearchContacts() {
//    
//  }
  
  func openConversation(_ userID: Int, fullName: String)
  {  
    let messageViewController = GKMessagesViewController(nibName:
      GKMessagesViewController.xibName(), bundle: nil)
    
    let anotherUser = GKMessageAnotherUser(uid: userID, fullName: fullName)
    messageViewController.anotherUser = anotherUser
    //    messageViewController.data = self.messages[indexPath.row]
    self.navigationController?.pushViewController(messageViewController, animated:true)
  }
  
  func requestContactsList() {
    
    contacts = []
    
    var params = GKDictionary()
    params["token"] = Static.sharedInstance.token() as AnyObject?
    params["page"] = 1 as AnyObject?
    
    GKServiceClient.call(GKRouterClient.friends(), parameters: params, callback: {
      (response: AnyObject?) -> () in
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          if let data = envelope.getResource() as? [GKDictionary] {
            
            self.contacts = data
            
            self.reloadData()
          }
        }
      }
    })
    
    reloadData()
  }

  func reloadData()
  {
   // self.tableView.reloadData()
  }

}
