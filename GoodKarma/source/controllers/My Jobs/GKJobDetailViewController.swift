//
//  GKJobDetailViewController.swift
//  GoodKarma
//
//  Created by Shivani on 10/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKJobDetailViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , updateStatusDelegate , GKManageSkillsDelegate , GKSendThankYouDelegate
{
    
    @IBOutlet weak var jobCompletedStatusButton: UIButton!
    @IBOutlet weak var jobStatusLabel: UILabel!
    @IBOutlet weak var jobStatusButton: UIButton!
    @IBOutlet weak var cancelJobButton: UIButton!
    @IBOutlet weak var jobDescriptionContentLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
     @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var deadlineContentLabel: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
     @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: WebImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var skillsContainerView: UIView!
    var job_status : Int!
    var dictData : GKDictionary!
    var reason : String!
    var count = 0
    var imageData : [NSDictionary]!
    var userId : Int!
    var isJobAssignedByMe = false
    var manageSkillView: GKManageSkillsView!
    var skills: [GKModelSkill] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        jobStatusButton.isHidden = false
        
        let nib = UINib(nibName: "GKMyJobDetailCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "jobDetailCell")
        self.loadData(data: dictData)
        collectionView.delegate  = self
        collectionView.dataSource = self
        
        //profile image
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2
         manageSkillView = GKManageSkillsView(frame: skillsContainerView.bounds, editable: false, delegate: self as? GKManageSkillsDelegate)
         skillsContainerView.addSubview(manageSkillView)
        
        self.showJobStatus(job_status : dictData["job_status"] as! Int)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        updateSkillsView()
        // self.filterScrollView.contentSize = CGSize(width: self.filterScrollView.frame.width, height: self.filterFormView.bottomSide)
    }
    
    func cancelJob()
    {
        
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jobDetailCell", for: indexPath) as! GKMyJobDetailCollectionViewCell
       
        let image_url = imageData[indexPath.row].value(forKey: "image_name") as! String
        cell.imageView.loadImageWithUrl(image_url as NSString)
        return cell
    }
    
    func updateSkillsView()
    {
        manageSkillView.frame = skillsContainerView.bounds
        manageSkillView.setupSkills(skills)
        
        self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    func GKRemoveSkill(_ skill: GKModelSkill)
    {
        skills = skills.filter{ $0.uid != skill.uid }
        updateSkillsView()
    }
    func GKShowAddSkillsView() {
        
        let skillsController = GKSkillCategoriesViewController(
            nibName: "GKSkillCategoriesViewController", bundle: nil)
        skillsController.skills = self.skills
        
        self.navigationController?.pushViewController(skillsController, animated: true)
    }
    func GKShowMoreSkillsInView(_ skills: [GKModelSkill])
    {

        
    }
    func loadData(data : GKDictionary)
    {
        //title
        self.lblTitle.text = data["title"] as? String
        
        //jobDescription
        self.jobDescriptionContentLabel.text = data["desc"] as? String
        
        //difficulty
        switch(data["difficulty"] as! Int)
        {
        case 1:
            self.difficultyLabel.text = "Very Easy"
            break
        case 2:
            self.difficultyLabel.text = "Easy"
            break
        case 3:
            self.difficultyLabel.text = "Average"
            break
        case 4:
            self.difficultyLabel.text = "Hard"
            break
        case 5:
            self.difficultyLabel.text = "Very Hard"
            break
        default:
            self.difficultyLabel.text = ""
            break
        }
        let value : GKDictionary!
        let valueExists = data["job_assigned_by"] as? GKDictionary != nil
        var message : String!
        if valueExists{
            value = data["job_assigned_by"]  as! GKDictionary
            message =  " has assigned you a new job"
   
 
        }
        else
        {
            value = data["job_assigned_to"] as! GKDictionary
            message = " has assigned a job by you"
   
           
       
        }
        //time
        self.timeLabel.text = secondsToFormatted(data["createdAt"] as? Int)
        //Job Status
        job_status = data["job_status"] as! Int
        jobStatusLabel.textColor = UIColor.greenOpaque()
        if job_status == 0
        {
            jobStatusLabel.text = "Job Created"
        }
        else if job_status == 1
        {
            jobStatusLabel.text = "In Progress"
        }
        else if job_status == 2
        {
            jobStatusLabel.text = "Cancelled"
            jobStatusLabel.textColor = UIColor.red
        }
            
        else if job_status == 3
        {
            jobStatusLabel.text = "Flake"
            
        }
            
        else if job_status == 4
        {
            jobStatusLabel.text = "Waiting confirmation"
            //
        }
        else if job_status == 5
        {
            jobStatusLabel.text = "Completed"
        }
        else
        {
            jobStatusLabel.text = "Thank You"
        }
        
        //username
        
        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.darkGray ]
        let myAttrString = NSAttributedString(string: message, attributes: myAttribute)
        
        let first_name = (value["first_name"] as! String).capitalized + " "
        let first_nameAtrributedString = NSAttributedString(string: first_name, attributes: nil)
        
        let last_name = (value["last_name"] as! String).capitalized
        let last_nameAttributedString = NSAttributedString(string: last_name, attributes: nil)
        
        let valueMessage = NSMutableAttributedString()
        valueMessage.append(first_nameAtrributedString)
        valueMessage.append(last_nameAttributedString)
        valueMessage.append(myAttrString)
        userNameLabel.attributedText = valueMessage
        //Profile Photo
        
        self.profileImageView.loadImageWithUrl(value["photo_url"] as? String as NSString?)
        
        
        
        //deadline
        let dateString = showDate(actualString: (data["need_by"] as? String)!)
        //new
        self.deadlineContentLabel.text  = dateString
        
        
        //image
        count = (data["gkarm_job_images"]?.count)!
        imageData = (data["gkarm_job_images"] as! [NSDictionary])
        collectionView.reloadData()
        
        
        //userid
        self.userId = data["id"] as! Int
        
        //skills
        skills = GKModelSkill.readForProfile(dict: data["gkarm_job_skills"] as! [GKDictionary])
        
    }
    
    func showJobStatus(job_status : Int)
    {
        if isJobAssignedByMe == true
        {
            jobCompletedStatusButton.isHidden = true
            
            if job_status  == 0
            {
                jobCompletedStatusButton.isHidden = false
                jobCompletedStatusButton.setTitle("Cancel Job", for: .normal)
              
            }
                
            else if  job_status  == 1 || job_status  == 0 || job_status  == 3
            {
                jobStatusButton.setTitle("Mark As Flake", for: .normal)
                jobStatusButton.backgroundColor = UIColor.red
            }
            else if job_status  == 2
            {
                jobCompletedStatusButton.backgroundColor = UIColor.red
                jobCompletedStatusButton.setTitle("Job Cancelled", for: .normal)
                jobCompletedStatusButton.isHidden = false
            }
            else if job_status  == 4
            {
                cancelJobButton.setTitle("Confirm Job", for: .normal)
                cancelJobButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
                jobStatusButton.setTitle("Mark As Flake", for: .normal)
                jobStatusButton.backgroundColor = UIColor.red
            }
                
            else
            {
                jobCompletedStatusButton.isHidden = false
                jobCompletedStatusButton.setTitle("Send Thank You", for: .normal)
                jobCompletedStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
        }
        else if isJobAssignedByMe == false
        {
            
            jobCompletedStatusButton.isHidden = true
            
            if job_status  == 0
            {
                jobStatusButton.setTitle("Accept Job", for: .normal)
                jobStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            else if job_status  == 2
            {
                jobCompletedStatusButton.backgroundColor = UIColor.red
                jobCompletedStatusButton.setTitle("Job Cancelled", for: .normal)
                //            jobStatusButton.isHidden = true
                jobCompletedStatusButton.isHidden = false
            }
            else if     job_status  == 1 || job_status  == 3
            {
                jobStatusButton.setTitle("Complete Job", for: .normal)
                jobStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            else if job_status  == 4
            {
                jobCompletedStatusButton.isHidden = false
                jobCompletedStatusButton.setTitle("Waiting Confirmation", for: .normal)
                jobCompletedStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            else
            {
                // cancelJobButton.isHidden = true
                jobCompletedStatusButton.isHidden = false
                jobCompletedStatusButton.setTitle("Job Completed", for: .normal)
                jobCompletedStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            
        }
    }
    @IBAction func actionStatus(_ sender: Any)
    {
        self.showJobStatus(job_status: self.job_status)
        self.reason = " "
        if isJobAssignedByMe == true
        {
            self.job_status = 3
        }
        else
        {
            if jobStatusButton.titleLabel?.text == "Accept Job"
            {
                 self.job_status = 1
            }
           else
            {
                 self.job_status = 4
            }
        }
        self.uploadJobStatus()
    }
    
    @IBAction func actionCancelJob(_ sender: Any)
    {
        self.showJobStatus(job_status: self.job_status)
        if dictData["job_status"] as! Int == 2
        {
            
        }
            else if self.cancelJobButton.titleLabel?.text == "Confirm Job"
        {
            self.reason = " "
            self.job_status = 5
            self.uploadJobStatus()
        }
            
        else
        {
            let GKCancelMyJobViewControllerObject = GKCancelMyJobViewController(nibName: "GKCancelMyJobViewController", bundle: nil)
            GKCancelMyJobViewControllerObject.delegate = self
            self.present(GKCancelMyJobViewControllerObject, animated: true, completion: nil)
        }
       
    }
    
    
    @IBAction func actionJobCompleted(_ sender: Any)
    {
        
        if self.jobCompletedStatusButton.titleLabel?.text == "Cancel Job"
        {
            let GKCancelMyJobViewControllerObject = GKCancelMyJobViewController(nibName: "GKCancelMyJobViewController", bundle: nil)
            GKCancelMyJobViewControllerObject.delegate = self
            self.present(GKCancelMyJobViewControllerObject, animated: true, completion: nil)
            
        }
        else if self.jobCompletedStatusButton.titleLabel?.text == "Confirm Job"
        {
            self.job_status = 5
            self.uploadJobStatus()
        }
        else if self.jobCompletedStatusButton.titleLabel?.text == "Send Thank You"
        {
             let GKThankYouViewControllerObject = GKThankYouViewController(nibName: "GKThankYouViewController", bundle: nil)
             GKThankYouViewControllerObject.delegate = self
             self.navigationController?.pushViewController(GKThankYouViewControllerObject, animated: true)
            
        }
        
    }
    
    @IBAction func imageButtonClicked(_ sender: Any)
    {
         
    }

    func sendThankYou()
    {
        self.reason = " "
        self.job_status = 6
        self.uploadJobStatus()
    }
    func updateStatus(reason : String)
    {
        self.reason = reason
        self.job_status = 2
        self.uploadJobStatus()
    }
    func uploadJobStatus()
    {
        var params =  GKDictionary()
        params["token"] =  Static.sharedInstance.token() as AnyObject
        params["reason"] = self.reason as AnyObject
        params["status"] = self.job_status as AnyObject?
        params["id"] = self.userId as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.jobStatus(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    _ = res.object(forKey: "object") as AnyObject
                     self.jobCompletedStatusButton.isHidden = true
                    self.showJobStatus(job_status: self.job_status)
                    if self.job_status == 2
                    {
                        self.jobStatusLabel.text = "Cancelled"
                        self.jobStatusLabel.textColor = UIColor.red
                        self.jobCompletedStatusButton.isHidden = false
                        self.jobCompletedStatusButton.backgroundColor = UIColor.red
                        self.jobCompletedStatusButton.setTitle("Job Cancelled", for: .normal)
                    }
                   else if self.job_status == 4
                    {
                        self.jobCompletedStatusButton.isHidden = false
                        self.jobCompletedStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
                        if self.isJobAssignedByMe == true
                        {
                             self.jobCompletedStatusButton.setTitle("Confirm Job", for: .normal)
                        }
                       else
                        {
                             self.jobCompletedStatusButton.setTitle("Waiting confirmation", for: .normal)
                        }
                        self.jobStatusLabel.textColor = UIColor.greenOpaque()
                        self.jobStatusLabel.text = "Waiting confirmation"

                    }
                   else if self.job_status == 3
                    {

                        self.jobStatusLabel.textColor = UIColor.greenOpaque()
                        self.jobStatusLabel.text = "Flake"
                    }
                   else if self.job_status == 1
                    {
                        self.jobStatusLabel.textColor = UIColor.greenOpaque()
                        self.jobStatusLabel.text = "In Progress"
                        if self.isJobAssignedByMe == false
                        {
                           self.jobStatusButton.titleLabel?.text = "Complete Job"
                        }
                    }
                   else if self.job_status == 5
                    {
                        self.jobStatusLabel.textColor = UIColor.greenOpaque()
                        self.jobStatusLabel.text = "Completed"
                        self.jobCompletedStatusButton.isHidden = false
                        if self.isJobAssignedByMe == true
                        {
                            self.jobCompletedStatusButton.titleLabel?.text = "Send Thank You"

                        }
                        else
                        {
                            self.jobCompletedStatusButton.titleLabel?.text = "Job Completed"
                        }
                         self.jobCompletedStatusButton.backgroundColor = UIColor(red: 51.0/255.0, green: 122.0/255.0, blue: 222.0/255.0, alpha: 1.0)
                    }
                    print("success")
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: " ", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    self.present(alertController, animated: false, completion: nil)
                }
                
            }
                else
                {
                    let alertController = UIAlertController(title: "Sorry", message: "Server time out", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    self.present(alertController, animated: false, completion: nil)
                }
            }
        )
    }
   
}
