//
//  GKStoreMenu.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation

struct GKStoreMenu {
  
  static func menuTitles() -> [String] {
    
    return [
        "Home",
        "Explore",
        "Chat",
        "My Groups",
        "My Participation",
        //new
        "My Company",
        "Hall of Fame",
        "Profile",
        "Friends",
        "Testimonials",
        "History",
        "Notifications",
        "Settings",
        "Log-out"
    ]

  }
  
}
