//
//  FacebookHandler.swift
//  Faveo
//
//  Created by Eliezer de Armas on 3/11/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FBSDKLoginKit

struct FacebookHandler
{
    static func publishInFB(_ content: String)
    {
        let properties = [
            "og:type"        : "_vetted:vett",
            "og:title"       : "Psy Name says:",
            "og:description" : content,
            "og:other"       : "personal",
            "og:author"      : "Psy Name",
            "og:url"         : "https://fb.me/1734736420084534"
        ]
        
        let object = FBSDKShareOpenGraphObject(properties: properties as
            [AnyHashable: Any])
        
        let action = FBSDKShareOpenGraphAction(type: "_vetted:received",
                                               object: object,
                                               key: "vett")
        action?.actionType = "_vetted:received"
        action?.setString("true", forKey: "fb:explicitly_shared")
        
        let content = FBSDKShareOpenGraphContent()
        content.action = action
        content.previewPropertyName = "vett"
        
        let shareApi = FBSDKShareAPI()
        //    shareApi.delegate = self
        shareApi.shareContent = content
        shareApi.share()
    }
    
    static func login(_ parentViewController: UIViewController?,
                      callback: @escaping (NSDictionary?) ->()) -> ()
    {
        LoadingView.showLoadingView("Loading...",
                                    parentView: parentViewController?.view,
                                    backColor: true)
        
        if FBSDKAccessToken.current() == nil
        {
            let permissions = ["public_profile", "email", "user_friends"]
            
            FBSDKLoginManager().logIn(withReadPermissions: permissions,
                                                         handler:
                {
                    (result: FBSDKLoginManagerLoginResult?, error: Error?) in
                    
                    if error != nil
                    {
                        FBSDKLoginManager().logOut()
                        LoadingView.hideLoadingView(parentViewController?.view)
                        
                        showConnectionError({ (type: NoInternetResultType) -> () in
                            
                            FacebookHandler.login(parentViewController, callback: callback)
                        })
                        
                        callback(nil)
                    }
                    else if !(result?.isCancelled)!
                    {
                        var allPermsGranted = true
                        let perms = (result!.grantedPermissions)!
                        let grantedPermissions = ((perms as NSSet)).allObjects.map({"\($0)"})
                        
                        for permission in permissions
                        {
                            if !grantedPermissions.contains(permission)
                            {
                                allPermsGranted = false
                                break
                            }
                        }
                        
                        if allPermsGranted
                        {
                            FacebookHandler.requestDataFromFacebook(parentViewController!, callback: callback)
                        }
                        else
                        {
                            FBSDKLoginManager().logOut()
                            LoadingView.hideLoadingView(parentViewController?.view)
                            
                            showConnectionError({ (type: NoInternetResultType) -> () in
                                
                                FacebookHandler.login(parentViewController, callback: callback)
                            })
                            
                            callback(nil)
                        }
                    }
                    else
                    {
                        FBSDKLoginManager().logOut()
                        LoadingView.hideLoadingView(parentViewController?.view)
                        
                        showConnectionError({ (type: NoInternetResultType) -> () in
                            
                            FacebookHandler.login(parentViewController, callback: callback)
                        })
                        callback(nil)
                    }
            })
        }
        else
        {
            FacebookHandler.requestDataFromFacebook(parentViewController!, callback: callback)
        }
    }
    
    static func readPermissions(_ parentViewController: UIViewController?,
                                callback: @escaping (Bool) ->()) -> ()
    {
        let permissions = ["publish_actions"]
        
        FBSDKLoginManager()
            .logIn(withPublishPermissions: permissions,
                                         handler:
                {
                    (result: FBSDKLoginManagerLoginResult?, error: Error?) -> Void in
                    
                    if error != nil
                    {
                        callback(false)
                    }
                    else if !(result?.isCancelled)!
                    {
                        var allPermsGranted = true
                        let perms = (result!.grantedPermissions)!
                        let grantedPermissions = (perms as NSSet).allObjects.map({"\($0)"})
                        
                        for permission in permissions
                        {
                            if !grantedPermissions.contains(permission)
                            {
                                allPermsGranted = false
                                break
                            }
                        }
                        
                        if allPermsGranted
                        {
                            callback(true)
                        }
                        else
                        {
                            callback(false)
                        }
                    }
                    else
                    {
                        callback(false)
                    }
            })
    }
    
    static func requestDataFromFacebook(_ parentViewController: UIViewController,
                                        callback: @escaping (NSDictionary?) ->()) -> ()
    {
        if (FBSDKAccessToken.current() != nil)
        {
            let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me?fields=id,picture,first_name,last_name,email", parameters: ["fields":"id,picture,first_name,last_name,email,gender"],
                                                                    tokenString: FBSDKAccessToken.current().tokenString,
                                                                    version: nil, httpMethod: nil)
            
            graphRequest.start(completionHandler: {(connection, result, error) -> Void in
                
                LoadingView.hideLoadingView(parentViewController.view)
                
                if (error != nil)
                {
                    showConnectionError({ (type: NoInternetResultType) -> () in
                        
                        FacebookHandler.login(parentViewController, callback: callback)
                    })
                    
                    callback(nil)
                }
                else
                {
                    print(result as! NSDictionary)
                    callback(result as! NSDictionary?)
                }
            })
        }
    }
    
    static func publishGratitude(_ parentViewController: UIViewController,
                                   content: String,
                                   callback: @escaping (Bool) ->()) -> ()
    {
        if (FBSDKAccessToken.current() != nil && FBSDKAccessToken.current().hasGranted("publish_actions"))
        {
            let title = NSString(format: "%@  is grateful for:",
                                 Static.sharedInstance.userProfile["first_name"] as! String)
            let properties = ["og:type" : "_goodkarma:post",
                              "og:title": title,
                              "og:description" : content] as [String : Any]
            
            let object = FBSDKShareOpenGraphObject(properties: properties as
                [AnyHashable: Any])
            
            let action = FBSDKShareOpenGraphAction(type: "_goodkarma:post",
                                                   object: object,
                                                   key: "post")
            action?.actionType = "_goodkarma:post"
            action?.setString("true", forKey: "fb:explicitly_shared")
            
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "post"
            
            let shareApi = FBSDKShareAPI()
            shareApi.delegate = parentViewController as! FBSDKSharingDelegate
            shareApi.shareContent = content
            shareApi.share()
            
            callback(true)
        }
        else
        {
            FacebookHandler.readPermissions(parentViewController, callback: { (result) in
                if (result)
                {
                   FacebookHandler.publishHelpRequest(parentViewController,
                    content: content,
                    callback: callback)
                }
                else
                {
                    callback(false)
                }
            })
        }
    }
    
    static func publishHelpRequest(_ parentViewController: UIViewController,
                                   content: String,
                                   callback: @escaping (Bool) ->()) -> ()
    {
        if (FBSDKAccessToken.current() != nil && FBSDKAccessToken.current().hasGranted("publish_actions"))
        {
            let title = NSString(format: "%@ says:",
                                 Static.sharedInstance.userProfile["first_name"] as! String)
            let properties = ["og:type" : "_goodkarma:post",
                              "og:title": title,
                              "og:description" : content] as [String : Any]
            
            let object = FBSDKShareOpenGraphObject(properties: properties as
                [AnyHashable: Any])
            
            let action = FBSDKShareOpenGraphAction(type: "_goodkarma:post",
                                                   object: object,
                                                   key: "post")
            action?.actionType = "_goodkarma:helprequest"
            action?.setString("true", forKey: "fb:explicitly_shared")
            
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "post"
            
            let shareApi = FBSDKShareAPI()
            shareApi.delegate = parentViewController as! FBSDKSharingDelegate
            shareApi.shareContent = content
            shareApi.share()
            
            callback(true)
        }
        else
        {
            FacebookHandler.readPermissions(parentViewController, callback: { (result) in
                if (result)
                {
                    FacebookHandler.publishHelpRequest(parentViewController,
                        content: content,
                        callback: callback)
                }
                else
                {
                    callback(false)
                }
            })
        }
    }
    
    static func publishPositivity(_ parentViewController: UIViewController,
                                   content: String,
                                   callback: @escaping (Bool) ->()) -> ()
    {
        if (FBSDKAccessToken.current() != nil &&
            FBSDKAccessToken.current().hasGranted("publish_actions"))
        {
            let title = NSString(format: "%@ says:",
                                 Static.sharedInstance.userProfile["first_name"] as! String)
            let properties = ["og:type" : "_goodkarma:post",
                              "og:title": title,
                              "og:description" : content] as [String : Any]
            
            let object = FBSDKShareOpenGraphObject(properties: properties as
                [AnyHashable: Any])
            
            let action = FBSDKShareOpenGraphAction(type: "_goodkarma:post",
                                                   object: object,
                                                   key: "post")
            action?.actionType = "_goodkarma:positivity"
            action?.setString("true", forKey: "fb:explicitly_shared")
            
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "post"
            
            let shareApi = FBSDKShareAPI()
            shareApi.delegate = parentViewController as! FBSDKSharingDelegate
            shareApi.shareContent = content
            shareApi.share()
            
            callback(true)
        }
        else
        {
            FacebookHandler.readPermissions(parentViewController, callback: { (result) in
                if (result)
                {
                    FacebookHandler.publishHelpRequest(parentViewController,
                        content: content,
                        callback: callback)
                }
                else
                {
                    callback(false)
                }
            })
        }
    }
    
    static func publishServe(_ parentViewController: UIViewController,
                                  content: String,
                                  callback: @escaping (Bool) ->()) -> ()
    {
        if (FBSDKAccessToken.current() != nil && FBSDKAccessToken.current().hasGranted("publish_actions"))
        {
            let title = NSString(format: "%@ says:",
                                 Static.sharedInstance.userProfile["first_name"] as! String)
            let properties = ["og:type" : "_goodkarma:post",
                              "og:title": title,
                              "og:description" : content] as [String : Any]
            
            let object = FBSDKShareOpenGraphObject(properties: properties as
                [AnyHashable: Any])
            let action = FBSDKShareOpenGraphAction(type: "_goodkarma:post",
                                                   object: object,
                                                   key: "post")
            action?.actionType = "_goodkarma:serve"
            action?.setString("true", forKey: "fb:explicitly_shared")
            
            let content = FBSDKShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "post"
            
            let shareApi = FBSDKShareAPI()
            shareApi.delegate = parentViewController as! FBSDKSharingDelegate
            shareApi.shareContent = content
            shareApi.share()
            
            callback(true)
        }
        else
        {
            FacebookHandler.readPermissions(parentViewController, callback: { (result) in
                if (result)
                {
                    FacebookHandler.publishHelpRequest(parentViewController,
                        content: content,
                        callback: callback)
                }
                else
                {
                    callback(false)
                }
            })
        }
    }
}
