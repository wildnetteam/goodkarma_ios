//
//  GKServiceProtocol.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import Foundation
import Alamofire

typealias GKDictionary = [String: AnyObject]
typealias GKMutableDictionary = NSMutableArray

typealias GKCallbackDictionary = (_ success: Bool, _ data:  GKDictionary?,  _ errorMessage: String?)   -> ()
typealias GKCallbackArray      = (_ success: Bool, _ data: [GKDictionary]?, _ errorMessage: String?) -> ()

struct GKParamKV {
  
  var title: String
  var value: String
  
  init(title: String, value: String) {
    
    self.title = title
    self.value = value
  }
}

protocol GKServiceProtocol {
  
  static func checkStatusResponse(_ res: DataResponse<AnyObject>) -> String?
}

extension GKServiceProtocol {
  
  static func checkStatusResponse(_ res: DataResponse<AnyObject>) -> String? {
    
    // TODO: add error message
    // TODO: delete prints
    
    guard res.result.isSuccess else {
      
        print("URL    : \(String(describing: res.request?.urlRequest?.url))")
      return "Please try again later."
    }
    
    guard let statusCode = res.response?.statusCode else {
      
        print("URL    : \(String(describing: res.request?.urlRequest?.url))")
      return "Please try again later."
    }
    
    // Error
    if let errorMessage = res.result.value?["message"] as? String {
      
        print("URL   : \(String(describing: res.request?.urlRequest?.url))")
      print("ERROR : \(errorMessage)")
      return errorMessage
    }
    
    // Status code
    guard String(statusCode).first == "2" else {
      
        print("URL    : \(String(describing: res.request?.urlRequest?.url))")
      print("STATUS : \(statusCode)")
      return "Please try again later."
    }
    
    return nil
  }
  
}
