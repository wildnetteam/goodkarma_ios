//
//  GKHallOfFameViewController.swift
//  GoodKarma
//
//  Created by Shivani on 23/11/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKHallOfFameViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var noDataFoundView: UIView!
    @IBOutlet weak var collectionViewFame: UICollectionView!
    
    var responseArray : [GKDictionary] = []
    var isInMenu = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "GKHallOfFameCollectionViewCell", bundle: nil)
        collectionViewFame.register(nib, forCellWithReuseIdentifier: "GKHallOfFameCollectionViewCell")
        collectionViewFame.delegate  = self
        collectionViewFame.dataSource = self
     
        self.title = "Hall Of Flame"
        if isInMenu
        {
            setMenuBarBadge()
        }
        noDataFoundView.isHidden = true
        listHallOfFame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CollectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ScreenSize.SCREEN_WIDTH == 320 ? 142 : (ScreenSize.SCREEN_WIDTH == 375 ? 170 : 190) , height: ScreenSize.SCREEN_WIDTH == 320 ? 148 : (ScreenSize.SCREEN_WIDTH == 375 ? 172 : 190))
    }
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.responseArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GKHallOfFameCollectionViewCell", for: indexPath) as! GKHallOfFameCollectionViewCell
        
        cell.data = responseArray[indexPath.row]
        cell.loadData()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let profileViewControllerObject = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        
        let tempData = responseArray[indexPath.row]
        profileViewControllerObject.anotherUserID = tempData["id"] as? Int
        self.present(
            UINavigationController(rootViewController: profileViewControllerObject), animated: true, completion: nil
        )
    }
   
    
    // MARK: - WebServices
    
    func listHallOfFame()
    {
        var params =  GKDictionary()
        params["token"] =  Static.sharedInstance.token() as AnyObject
        
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.listHallOfFame(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                if envelope.getSuccess()
                {
                    print(response!)
                    let object = res.object(forKey: "object") as AnyObject
                    self.responseArray = object as! [GKDictionary]
                    if self.responseArray.count == 0
                    {
                       self.noDataFoundView.isHidden = false
                    }
                    else
                    {
                       self.noDataFoundView.isHidden = true
                        self.collectionViewFame.reloadData()
                    }
                }
            }
            else
            {
                
            }
        }
        )
    }
}
