//
//  GKProfileViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/12/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit
import MessageUI

class GKProfileViewController: GKViewController, OpenPostViewDelegate, MFMailComposeViewControllerDelegate
{
    // MARK: - IBOutlets -
    //modified
    @IBOutlet weak var assignJobButton: UIButton!
    @IBOutlet weak var cancelledJobConstantLabel: UILabel!
    @IBOutlet weak var inProcessJobConstantLabel: UILabel!
    @IBOutlet weak var incompletedJobConstantLabel: UILabel!
    @IBOutlet weak var completedJobConstantLabel: UILabel!
    @IBOutlet weak var jobsViewHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var jobsView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileContainer: UIView!
    @IBOutlet weak var profileTopView: UIView!
    
    @IBOutlet weak var userImageView: WebImageView!
    
    @IBOutlet weak var followCheckImage: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var aboutTitle: UILabel!
    //  @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    
    @IBOutlet weak var collegeLabel: UILabel!
    
    @IBOutlet weak var skillsContainer: UIView!
    
    @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var blockArrowImageView: UIImageView!
    
    @IBOutlet weak var gratitudesButton   : UIButton!
    @IBOutlet weak var jobsButton         : UIButton!
    @IBOutlet weak var testimonialsButton : UIButton!
    @IBOutlet weak var jobsLineView: UIView!
    @IBOutlet weak var gratitudeLineView: UIView!
    @IBOutlet weak var testimonialsLineView: UIView!
    @IBOutlet weak var worldTextView: UITextView!
    @IBOutlet weak var abuseButton: UIButton!
    
    var messageButton: UIButton!
    var openPostsView: GKOpenPostView!
    
    // MARK: - Properties -
    
    var manageSkillView: GKManageSkillsView!
    var skills: [GKModelSkill] = []
    var userData: GKDictionary!
    var anotherUserID: Int? = nil
    var isMe: Bool = false
    var withBack = false
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.gratitudesButton.isEnabled = true
        if (anotherUserID == nil || anotherUserID == Static.sharedInstance.userId())
        {
            //modified
            self.isMe = true
            jobsView.isHidden = true
            assignJobButton.isHidden = true
            jobsViewHeighConstraint.constant = 0.0
        }
        else
        {
            assignJobButton.isHidden = false
            assignJobButton.layer.cornerRadius = 4.0
            assignJobButton.layer.borderColor = UIColor.baseColor().cgColor
            assignJobButton.layer.borderWidth = 1.0
        }
        
        self.title = ""
        
        let closeImage = UIImage(named: "arrow-l")?.withRenderingMode(.alwaysOriginal)
        
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x: 0.0, y: 0.0, width: 13.0, height: 22.0)
        closeButton.addTarget(
            self,
            action: #selector(self.close),
            for: .touchUpInside
        )
        closeButton.setImage(closeImage, for: UIControlState())
        
        let closeBarButton = UIBarButtonItem(customView: closeButton)
        self.navigationItem.leftBarButtonItem = closeBarButton
        
        //    let closeButtonItem = UIBarButtonItem(
        //      title: "Close",
        //      style: .Plain,
        //      target: self,
        //      action: #selector(self.close)
        //    )
        //    closeButtonItem.tintColor = UIColor.baseColor()
        //    self.navigationItem.leftBarButtonItem = closeButtonItem
        
        // Right button item depends if this is my profile or another user's profile
        
        if self.isMe
        {
            let editButtonItem = UIBarButtonItem(
                title: "Edit",
                style: .plain,
                target: self,
                action: #selector(self.editProfile)
            )
            editButtonItem.tintColor = UIColor.baseColor()
            self.navigationItem.rightBarButtonItem = editButtonItem
        }
        else
        {
            let messageImage = UIImage(named: "explore-bar-comment")?.withRenderingMode(.alwaysOriginal)
            
            self.messageButton = UIButton(type: .custom)
            self.messageButton.frame = CGRect(x: 0.0, y: 0.0, width: 22.0, height: 22.0)
            self.messageButton.setImage(messageImage, for: UIControlState())
            self.messageButton.addTarget(
                self,
                action: #selector(self.messsageUser),
                for: .touchUpInside
            )
            
            let messageBarButton = UIBarButtonItem(customView: messageButton)
            self.navigationItem.rightBarButtonItem = messageBarButton
        }
        
        userImageView.layer.cornerRadius = userImageView.bounds.size.height * 0.5
        
        if self.isMe
        {
            followButton.isHidden = true
            followCheckImage.isHidden = true
        }
        else
        {
            followButton.layer.cornerRadius = 4.0
            followButton.layer.borderColor = UIColor.baseColor().cgColor
            followButton.layer.borderWidth = 1.0
        }
        
        manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: false)
        skillsContainer.addSubview(manageSkillView)
        
        // Buttons
        customizeButton(self.jobsButton)
        customizeButton(self.gratitudesButton)
        customizeButton(self.testimonialsButton)
        customizeButton(self.blockButton)
        
        // GKOpenPostView
        self.openPostsView = GKOpenPostView(frame: CGRect(x: 0, y: 0,
            width: self.view.frame.width, height: 44))
        self.openPostsView.delegate = self
        self.openPostsView.autoresizingMask = .flexibleWidth
        self.view.addSubview(self.openPostsView)
        
        // Abuse Button
        self.abuseButton.isHidden = self.isMe
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        requestData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        updateScrollContentHeight()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        self.openPostsView.hide()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if (self.userData != nil)
        {
            
            let gratitude = self.userData["count_gratitude"] as! Int > 0
            let testimonials = self.userData["count_testimonials"] as! Int > 0
            //            self.jobsButton.frame =
            //                CGRectSetY(self.jobsButton.frame, y: self.skillsContainer.frame.maxY)
            //            self.jobsButton.frame =
            //                CGRectSetHeight(self.jobsButton.frame, h: jobs ? 50 : 0)
            self.gratitudesButton.frame =
                CGRectSetY(self.gratitudesButton.frame,
                           y: self.jobsButton.frame.maxY)
            self.gratitudesButton.frame =
                CGRectSetHeight(self.gratitudesButton.frame,
                                h: gratitude ? 50 : 0)
            self.testimonialsButton.frame =
                CGRectSetY(self.testimonialsButton.frame,
                           y: self.gratitudesButton.frame.maxY)
            self.testimonialsButton.frame =
                CGRectSetHeight(self.testimonialsButton.frame,
                                h: testimonials ? 50 : 0)
            if self.testimonialsButton.frame.size.height == 0.0
                
            {
                self.testimonialsButton.isHidden = true
            }
            else
            {
                self.testimonialsButton.isHidden = false
            }
            self.blockButton.frame =
                CGRectSetY(self.blockButton.frame,
                           y: self.testimonialsButton.frame.maxY)
            if (self.userData["completed_jobs"] as! Int) != 0 || (self.userData["incompleted_jobs"] as! Int) != 0 || (self.userData["inprogress_jobs"] as! Int) != 0 || (self.userData["cancelled_jobs"] as! Int) != 0
            {
                
                //modified
                jobsView.isHidden = false
                
                jobsViewHeighConstraint.constant = 154.0
                completedJobConstantLabel.text = String(describing: self.userData["completed_jobs"]! as! Int)
                incompletedJobConstantLabel.text = String(describing: self.userData["incompleted_jobs"]! as! Int)
                inProcessJobConstantLabel.text = String(describing: self.userData["inprogress_jobs"]! as! Int)
                cancelledJobConstantLabel.text = String(describing: self.userData["cancelled_jobs"]! as! Int)
            }
            else
            {
                jobsView.isHidden = true
                jobsViewHeighConstraint.constant = 0.0
            }
        }
    }
    
    // MARK: - Protocols
    // MARK: - Protocol GKFollowDelegate -
    
    func GKFollow(_ userID: Int)
    {
        var params = GKDictionary()
        params["token"]   = Static.sharedInstance.token() as AnyObject?
        params["user_id"] = anotherUserID! as AnyObject?
        
        LoadingView.showLoadingView("Loading...", parentView: self.view, backColor: true)
        
        GKServiceClient.call(GKRouterClient.follow(), parameters: params) { (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "profile")
                
                if envelope.getSuccess()
                {
                    self.requestData()
                }
            }
        }
    }
    
    func GKUnfollow(_ userID: Int, name: String)
    {
        let alertController = UIAlertController(title: nil, message: "Unfollow \(name)?", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let destroyAction = UIAlertAction(title: "Unfollow", style: .destructive)
        { (action) in
            self.unfollow(userID)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(destroyAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionAssignJob(_ sender: Any)
    {
        let GKAssignJobViewControllerObject = GKAssignJobViewController(nibName: "GKAssignJobViewController", bundle: nil)
        if anotherUserID != nil
        {
            GKAssignJobViewControllerObject.userId = self.anotherUserID as AnyObject as! Int;
            GKAssignJobViewControllerObject.data = userData
        }
        self.navigationController?.pushViewController(GKAssignJobViewControllerObject, animated: true)
    }
    @IBAction func actionFollow(_ sender: AnyObject)
    {
        let following = ((userData["follower"] as! Int) > 0 ? true :false)
        
        if following
        {
            self.GKUnfollow(anotherUserID!, name: (userData["first_name"] as! String) +  " " + (userData["last_name"] as! String))
        }
        else
        {
            self.GKFollow(anotherUserID!)
        }
    }
    
    @IBAction func actionGratitude(_ sender: AnyObject)
    {
        let gratitudesViewController = GKProfileListGratitudeViewController(
            nibName: "GKProfileListGratitudeViewController", bundle: nil
        )
        gratitudesViewController.anotherUserID = self.anotherUserID
        self.navigationController?.pushViewController(gratitudesViewController, animated:true)
    }
    
    @IBAction func actionJobs(_ sender: AnyObject)
    {
        if self.isMe
        {
            let jobsViewController = GKHistoryViewController(
                nibName: "GKHistoryViewController", bundle: nil
            )
            jobsViewController.isInMenu = false
            self.navigationController?.pushViewController(jobsViewController, animated:true)
        }
        else
        {
            let jobsViewController = GKExploreViewController(
                nibName: "GKExploreViewController", bundle: nil
            )
            jobsViewController.isInMenu = false
            jobsViewController.anotherUserID = self.anotherUserID
            jobsViewController.onlyFinalized = true
            self.navigationController?.pushViewController(jobsViewController, animated:true)
        }
    }
    
    @IBAction func actionTestimonials(_ sender: AnyObject)
    {
        if self.userData != nil{
            if (self.userData["first_name"] as? String) != nil{
                let testimonialsViewController = GKProfileListTestimonialsViewController(
                nibName: "GKProfileListTestimonialsViewController", bundle: nil
                )
            
                testimonialsViewController.nameUser = (self.userData["first_name"] as! String).capitalized
                testimonialsViewController.anotherUserID = self.anotherUserID
                self.navigationController?.pushViewController(testimonialsViewController, animated:true)
            }
        }
        else{
            showConnectionError()
        }
    }
    
    @IBAction func actionBlock(_ sender: AnyObject)
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["recipient"] = anotherUserID! as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.profileBlock(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            print(response!)
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "UpdatedBlocked"),
                        object: nil)
                    self.requestData()
                }
            }
        })
    }
    
    
    @IBAction func abuseAction(_ sender: AnyObject)
    {
        let c = MFMailComposeViewController()
        c.mailComposeDelegate = self
        c.setToRecipients(["support@goodkarms.com"])
        
        if (MFMailComposeViewController.canSendMail())
        {
            self.present(c, animated: true, completion: nil)
        }
        else
        {
            showAlert("Could not send Email", text: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitle: nil)
        }
    }
    
    // MARK: - Methods -
    
    @objc func editProfile()
    {
        let editProfileController = GKEditProfileViewController(
            nibName: "GKEditProfileViewController", bundle: nil
        )
        self.navigationController?.pushViewController(editProfileController, animated:true)
    }
    
    @objc func close()
    {
        if (self.withBack)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
           self.navigationController?.dismiss(animated: true, completion: nil) 
        }
    }
    
    func loadData()
    {
        print(userData)
        //let deleted = self.userData["enabled"] as! Bool == false
        let deleted = false
        
        // Update header
        if self.isMe
        {
            headerContainerHeightConstraint.constant = 210.0
            blockButton.isHidden = true
            blockArrowImageView.isHidden = true
        }
        else
        {
            headerContainerHeightConstraint.constant = 240.0
            
            if (self.userData["blocked"] as! Bool == true)
            {
                blockButton.setTitle("Unblock User", for: UIControlState())
                self.messageButton.isEnabled = false
                self.followButton.isEnabled = false
                self.followButton.alpha = 0.3
                self.assignJobButton.isEnabled = false
                self.assignJobButton.alpha = 0.3
            }
            else
            {
                blockButton.setTitle("Block User", for: UIControlState())
                self.messageButton.isEnabled = true
                self.followButton.isEnabled = true
                self.followButton.alpha = 1
                self.assignJobButton.isEnabled = true
                self.assignJobButton.alpha = 1
            }
            blockButton.isHidden = false
            blockArrowImageView.isHidden = true
        }
        
//        if (self.userData["blocked"] as! Bool)
//        {
//            blockButton.isEnabled = false
//            blockButton.backgroundColor = UIColor.megaLightGray()
//        }
        
        // Full name
        
        let firstName = (userData["first_name"] as! String).capitalized
        let lastName = userData["last_name"] as! String
        let userName = "\(firstName)" + " " + "\(lastName)"
        self.title = userName
        
        // Image
        if (deleted)
        {
            self.userImageView.image = UIImage(named: "user-placeholder")
            self.title = "Anonymous User"
        }
        else
        {
            self.userImageView.loadImageWithUrl(self.userData["photo_url"] as? String as NSString?)
        }
        
        // About Title
        if self.isMe
        {
            aboutTitle.text = "About me"
        }
        else
        {
            aboutTitle.text = "About " + (self.userData["first_name"] as! String).capitalized
        }
        
        if (deleted)
        {
            aboutTitle.text = "About Anonymous"
        }
        
        // Description
        if let aboutString = self.userData["description"] as? String
        {
            if aboutString.countPlainCharacters() > 0 && !deleted
            {
                aboutTextView.text = aboutString
            }
            else
            {
                aboutTextView.text = "Not Available"
            }
        }
        else
        {
            aboutTextView.text = "Not Available"
        }
        
        // Save World
        if let saveWorldString = self.userData["save_world"] as? String
        {
            if saveWorldString.countPlainCharacters() > 0 && !deleted
            {
                worldTextView.text = saveWorldString
            }
            else
            {
                worldTextView.text = "Not Available"
            }
        }
        else
        {
            worldTextView.text = "Not Available"
        }
        
        // College
        if let collegeString = self.userData["college"] as? String
        {
            if collegeString.countPlainCharacters() > 0 && !deleted
            {
                collegeLabel.text = collegeString
            }
            else
            {
                collegeLabel.text = "Not Available"
            }
        }
        else
        {
            collegeLabel.text = "Not Available"
        }
        
        if (!deleted)
        {
            // Skills
            skills = GKModelSkill.readForProfile(dict: userData["gkarm_skill_users"] as! [GKDictionary])
            //skills = GKModelSkill.readFromListDictionary(userData["gkarm_skill_users"] as! [GKDictionary])
            updateSkillsView()
        }
        
        // Follow
        if anotherUserID != nil && !deleted
        {
            updateFollow((userData["follower"] as! Int) > 0 ? true :false)
        }
        
        // Gratitude
        if userData["count_gratitude"] as? Int == 0
        {
            gratitudesButton.backgroundColor = UIColor.megaLightGray()
            gratitudesButton.isEnabled = false
        }
        // Jobs
        if userData["count_jobs"] as? Int == 0
        {
            jobsButton.backgroundColor = UIColor.megaLightGray()
            jobsButton.isEnabled = false
        }
        // Testimonials
        if userData["count_testimonials"] as? Int == 0
        {
            testimonialsButton.backgroundColor = UIColor.megaLightGray()
            testimonialsButton.isEnabled = false
        }
        
        if (deleted)
        {
            gratitudesButton.backgroundColor = UIColor.megaLightGray()
            gratitudesButton.isEnabled = false
            jobsButton.backgroundColor = UIColor.megaLightGray()
            jobsButton.isEnabled = false
            testimonialsButton.backgroundColor = UIColor.megaLightGray()
            testimonialsButton.isEnabled = false
            blockButton.setTitle("Block User", for: UIControlState())
            blockButton.isEnabled = false
            blockButton.backgroundColor = UIColor.megaLightGray()
        }
        
        if let hallOfFame = userData["hallOfFame"] as? Bool{
            if hallOfFame{
                profileTopView.applyGradient(colours: [UIColor(rgb: 0x44c8c9), UIColor(rgb: 0x00a0e7)])
                assignJobButton.setTitleColor(UIColor.baseColor(), for: .normal)
                assignJobButton.backgroundColor = UIColor.white
            }
        }
        
        //    updateScrollContentHeight()
    }
    
    @objc func messsageUser()
    {
        let messageViewController = GKMessagesViewController(nibName:
            GKMessagesViewController.xibName(), bundle: nil)
        
        let anotherUser = GKMessageAnotherUser(uid: anotherUserID!, fullName: ((userData["first_name"]) as! String).capitalized + (userData["first_name"] as! String).capitalized)
        messageViewController.anotherUser = anotherUser
        //    messageViewController.data = self.messages[indexPath.row]
        self.navigationController?.pushViewController(messageViewController, animated:true)
    }
    
    func requestData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        if anotherUserID != nil
        {
            params["user_id"] = anotherUserID! as AnyObject?
        }
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.profileEdit(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "profile")
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource() as? GKDictionary
                    {
                        self.userData = data
                        self.loadData()
                      //  let image_value = (driver_detail[0])["image"] as! String
//                        let url:URL = data["photo_url"] as! URL
//                        let value = try? Data(contentsOf: url)
//                        self.userImageView.image =  UIImage(data: value!)!
                        if (!self.isMe)
                        {
                            let activePosts = data["active_posts"] as! Int
                            if (activePosts > 0)
                            {
                                self.openPostsView.show("" + String(activePosts) + " Job" + (activePosts > 1 ? "s" : "") +  " In Progress")
                            }
                            else
                            {
                                self.openPostsView.hide()
                            }
                        }
                    }
                }
            }
        })
    }
    
    func unfollow(_ userID: Int)
    {
        var params = GKDictionary()
        params["token"]   = Static.sharedInstance.token() as AnyObject?
        params["user_id"] = userID as AnyObject?
        
        GKServiceClient.call(GKRouterClient.unfollow(), parameters: params) { (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string : "profile")
                
                if envelope.getSuccess()
                {
                    self.requestData()
                }
            }
        }
    }
    
    func updateFollow(_ isFollowing: Bool)
    {
        if isFollowing
        {
            followCheckImage.isHidden = false
            
            followButton.setTitleColor(UIColor.white, for: UIControlState())
            followButton.backgroundColor   = UIColor.baseColor()
            followButton.layer.borderColor = UIColor.clear.cgColor
            followButton.setTitle("FOLLOWING", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0)
        }
        else
        {
            followCheckImage.isHidden = true
            
            followButton.setTitleColor(UIColor.baseColor(), for: UIControlState())
            followButton.backgroundColor   = UIColor.white
            followButton.layer.borderColor = UIColor.baseColor().cgColor
            followButton.setTitle("FOLLOW", for: UIControlState())
            
            followButton.titleEdgeInsets = UIEdgeInsets.zero
        }
    }
    
    func updateScrollContentHeight()
    {
        if anotherUserID != nil
        {
            self.scrollView.contentSize = CGSize(
                width: self.scrollView.frame.width,
                height: self.blockButton.bottomSide
            )
        }
        else
        {
            self.scrollView.contentSize = CGSize(
                width: self.scrollView.frame.width,
                height: self.blockButton.frame.origin.y
            )
        }
    }
    
    func updateSkillsView()
    {
        manageSkillView.frame = skillsContainer.bounds
        manageSkillView.setupSkills(skills)
        
        self.skillsContainerHeightConstraint.constant = manageSkillView.bounds.size.height
    }
    
    func customizeButton(_ button: UIButton)
    {
        // Line
        let line = UIView(frame: CGRect(x: 0, y: 0, width: button.frame.width, height: 1))
        line.autoresizingMask = .flexibleWidth
        line.backgroundColor = RGBCOLOR(242, green: 242, blue: 242)
        button.addSubview(line)
        
        // Arrow
        let arrow = UIImageView(image: UIImage(named: "arrow-r"))
        arrow.frame = CGRect(x: self.jobsButton.frame.width - 26,
                                 y: 15, width: 20, height: 20);
        arrow.contentMode = .scaleAspectFit;
        arrow.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin,
                                  .flexibleBottomMargin]
        button.addSubview(arrow)
    }
    
    // MARK: - OpenPostViewDelegate
    
    func openPostViewDidTap()
    {
        let jobsViewController = GKHistoryViewController(nibName: "GKHistoryViewController", bundle: nil)
        jobsViewController.isInMenu = false
        jobsViewController.anotherUserID = self.anotherUserID
        jobsViewController.customTitle = "Posts Between Us"
        jobsViewController.onlyInProgress = true
        jobsViewController.otherUserName = (self.userData["first_name"] as! String).capitalized
        self.navigationController?.pushViewController(jobsViewController, animated:true)
    }
    
    // MARK: - MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        switch result.rawValue
        {
        case MFMailComposeResult.sent.rawValue :
            let text = "Abuse Confirmation"
            
            showAlert("Abuse sent",
                      text: text as String,
                      delegate: nil,
                      cancelButtonTitle: "Ok",
                      otherButtonTitle: nil)
            
        case MFMailComposeResult.cancelled.rawValue:
            NSLog("Email Canceled")
            
        case MFMailComposeResult.failed.rawValue :
            showConnectionError()
            
        default:
            NSLog("")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

