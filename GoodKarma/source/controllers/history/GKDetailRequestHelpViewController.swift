//
//  GKDetailRequestHelpViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/24/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKDetailRequestHelpViewController: UIViewController, GKTestimonialDelegate, GKShowUserProfileDelegate, UITableViewDataSource, UITableViewDelegate
{
    // GKManageSkillsDelegate
    // MARK: - IBOutlets -
    
    //  @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var containerView : UIView!
    
    @IBOutlet weak var titleLabel : UILabel!
    //  @IBOutlet weak var difficultyLabel : UILabel!
    
    //  @IBOutlet weak var skillsContainer: UIView!
    //  @IBOutlet weak var skillsContainerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    
    @IBOutlet weak var noParticipantsLabel : UILabel!
    @IBOutlet weak var participantsTableView : UITableView!
    
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var testimonialView: UIView!
    @IBOutlet weak var testimonialButton: UIButton!
    
    // MARK: - Properties -
    
    var data: GKDictionary!
    var isParticipant: Bool = false
    var postId: Int!
     var delegate : GKHistoryParticipantDelegate?
    //  var manageSkillView: GKManageSkillsView!
    //  var skills: [GKModelSkill] = []
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Help Request"
        self.noParticipantsLabel.isHidden = true
        self.testimonialView.isHidden = true
        
        //    self.manageSkillView = GKManageSkillsView(frame: skillsContainer.bounds, editable: false, delegate: self)
        //    self.skillsContainer.addSubview(manageSkillView!)
        
        self.participantsTableView.register(
            UINib(nibName: GKHistoryParticipantTableViewCell.cellIdentifier(),bundle: nil),
            forCellReuseIdentifier: GKHistoryParticipantTableViewCell.cellIdentifier()
        )
        self.participantsTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        loadData(data)
        
        if (self.isParticipant)
        {
            self.participantsLabel.isHidden = true
            self.participantsTableView.isHidden = true
            self.testimonialView.isHidden = false
            
            updateTestimonialButton()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    //    requestDetail()
        if self.delegate != nil
        {

          //    self.refreshData()

        }
      
    }
    
    // MARK: - Protocols
    // MARK: - Protcol GKTestimonialDelegate -
    
    func GKLeaveTestimonial(_ clientData: GKDictionary)
    {
        showAddTestimonial((clientData["first_name"] as! String) +  " " + (clientData["last_name"] as! String),
                           relatedId: clientData["related_id"] as! Int,
                           byClient: false)
    }
    
    // MARK: - Protocol GKShowUserProfileDelegate -
    
    func GKShowProfile(_ userID: Int)
    {
        let profileController = GKProfileViewController(nibName: "GKProfileViewController", bundle: nil)
        profileController.anotherUserID = userID
        
        self.present(
            UINavigationController(rootViewController: profileController), animated: true, completion: nil
        )
    }
    
    // MARK!: - Protocol GKManageSkillsDelegate -
    
    //  func GKRemoveSkill(skill: GKModelSkill) {
    //    // Nothing for now
    //  }
    //
    //  func GKShowAddSkillsView() {
    //    // Nothing for now
    //  }
    //
    //  func GKShowMoreSkillsInView(skills: [GKModelSkill]) {
    //
    //    GKMoreSkillsView.show(skills)
    //  }
    
    // MARK: - Protocol UITableViewDataSource -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (self.data == nil)
        {
            return 0;
        }
        else if self.data["clients"] != nil{
            return (self.data["clients"] as! [GKDictionary]).count
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: GKHistoryParticipantTableViewCell.cellIdentifier(), for: indexPath) as! GKHistoryParticipantTableViewCell
        
        let client = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        let status = client["status"] as! Int
        
        cell.delegate = self
        
        if status == 1
        {
            cell.loadData(client, detail: " Wants to offer you help.", postType: 3,
                          postStatus: self.data["status"] as! Int)
        }
        else if status == 2
        {
            cell.loadData(client, detail: " is helping you out!", postType: 3,
                          postStatus: self.data["status"] as! Int)
        }
        else
        {
            cell.loadData(client, detail: " participated in this job.", postType: 3,
                          postStatus: self.data["status"] as! Int)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        // you need to implement this method too or you can't swipe to display the actions
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt
        indexPath: IndexPath) -> CGFloat
    {
        return GKHistoryParticipantTableViewCell.heightForCell()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let participantData = (self.data["clients"] as! [GKDictionary])[indexPath.row]
        let related_id = participantData["related_id"] as! Int
        let status = participantData["status"] as! Int
        
        var editActions: [UITableViewRowAction] = []
        
        if status == 1
        {
            print("CREATED")
            // Accept
            let acceptAction = UITableViewRowAction(style: .default, title: "Accept") { (action, indexPath) in
                
                self.acceptParticipant(related_id)
            }
            acceptAction.backgroundColor = UIColor.greenOpaque()
            
            // Deny
            let denyAction = UITableViewRowAction(style: .default, title: "Deny") { (action, indexPath) in
                
                self.denyParticipant(related_id)
            }
            denyAction.backgroundColor = UIColor.red
            
            editActions = [denyAction, acceptAction]
        }
        else if status == 2
        {
            // Flake
            let flakeAction = UITableViewRowAction(style: .default, title: "Flake") { (action, indexPath) in
                
                self.markFlakeParticipant(related_id)
            }
            flakeAction.backgroundColor = UIColor.greenOpaque()
            
            // Report
            let reportParticipant = UITableViewRowAction(style: .default, title: "Report") { (action, indexPath) in
                
                self.reportParticipant(related_id)
            }
            reportParticipant.backgroundColor = UIColor.blue
            
            editActions = [reportParticipant, flakeAction]
        }
        else
        {
            print("NOTHING TO DO HERE!!!")
        }
        
        return editActions
    }
    
    // MARK: - Posts status -
    
    func acceptParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 3 as AnyObject?
        params["status"] = 2 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
       // GKServiceClient.call(GKRouterClient.acceptParticipant(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                //    self.requestDetail()
                    if self.delegate != nil
                    {
                       //  self.refreshData()
                       
                    }
                   
                }
            }
        })
    }
    
    func denyParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 3 as AnyObject?
        params["status"] = 0 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
      //  GKServiceClient.call(GKRouterClient.removeParticipant(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                   // self.requestDetail()
                    if self.delegate != nil
                    {
                        
                   //       self.refreshData()
//
                    }
                  
                }
            }
        })
    }
    
    func markFlakeParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 3 as AnyObject?
        params["status"] = 3 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
      //  GKServiceClient.call(GKRouterClient.markFlakeParticipant(), parameters: params, callback: {
         GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
                  //  self.requestDetail()
                    if self.delegate != nil
                    {
                       
                         // self.refreshData()
//
                    }
                  
                }
            }
        })
    }
    
    func reportParticipant(_ relatedID: Int)
    {
        var params = GKDictionary()
        
        params["token"] = Static.sharedInstance.token() as AnyObject?
        params["related_id"] = relatedID as AnyObject?
        params["type"]       = 3 as AnyObject?
        params["status"] = 5 as AnyObject?
        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
        
     //   GKServiceClient.call(GKRouterClient.reportParticipant(), parameters: params, callback: {
        GKServiceClient.call(GKRouterClient.clientPost(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess()
                {
               //     self.requestDetail()
                    if self.delegate != nil
                    {
                     
                        // self.refreshData()

                    }
                   
                }
            }
        })
    }
    
    // MARK: - Methods -
    
    
    func refreshData()
    {
        let value = self.delegate?.requestOfferedListData()
        self.data = value
        self.loadData(self.data)
        self.reloadTableData()
        if (!self.isParticipant)
        {
            if (self.canShowInstructions(.AcceptOrCancel))
            {
                self.showIntructions(.AcceptOrCancel)
            }
            else if (self.canShowInstructions(.FlakeOrSOS))
            {
                self.showIntructions(.FlakeOrSOS)
            }
        }
    }
    func loadData(_ data: GKDictionary!)
    {
        if (data != nil)
        {
            self.data = data
            
            let titleString = (self.data["title"] as! String).decodeEmoji()
            titleLabel.text = titleString
            //    difficultyLabel.text = difficultyToString(self.data["difficulty"] as! Int)
            
            dateLabel.text = secondsToFormatted(data["date_created"] as? Int)
            
            let descString  = (data["desc"] as! String).decodeEmoji()
            descLabel.text = descString
            
            let status = data["status"] as! Int
            
            if (status == 4)
            {
                self.testimonialView.isUserInteractionEnabled = true
                self.testimonialView.alpha = 1
            }
            else
            {
                self.testimonialView.isUserInteractionEnabled = false
                self.testimonialView.alpha = 0.3
            }
        }
    }
    
    //  func loadSkills() {
    //
    //    var skillsData: [GKModelSkill] = []
    //
    //    for skillData in (self.data["skills"] as! [GKDictionary]) {
    //
    //      skillsData.append(GKModelSkill.readFromExploreDictionary(skillData))
    //    }
    //
    //    self.skills = skillsData
    //  }
    
    func fetchPostId() -> Int!
    {
        if (self.data != nil)
        {
            return self.data["id"] as! Int
        }
        
        return self.postId
    }
    
    
    
 //   func requestDetail()
//    {
//        var params = GKDictionary()
//
//        params["token"] = Static.sharedInstance.token() as AnyObject?
//        params["id"]    = fetchPostId() as AnyObject?
//        params["type"]  = 3 as AnyObject?
//
//        LoadingView.showLoadingView("Sending...", parentView: self.navigationController?.view, backColor: true)
//
//       GKServiceClient.call(GKRouterClient.postDetail(), parameters: params, callback: {
//         //GKServiceClient.call(GKRouterClient.hostPost(), parameters: params, callback: {
//            (response: AnyObject?) -> () in
//
//            LoadingView.hideLoadingView(self.navigationController?.view)
//
//            if let res:AnyObject = response
//            {
//                let envelope = ResponseSkeleton(res)
//
//                if envelope.getSuccess()
//                {
//                    if let data = envelope.getResource()["data"] as? GKDictionary
//                    {
//                        self.data = data
//
//                        self.loadData(self.data)
//                        self.reloadTableData()
//
//                        if (!self.isParticipant)
//                        {
//                            if (self.canShowInstructions(.AcceptOrCancel))
//                            {
//                                self.showIntructions(.AcceptOrCancel)
//                            }
//                            else if (self.canShowInstructions(.FlakeOrSOS))
//                            {
//                                self.showIntructions(.FlakeOrSOS)
//                            }
//                        }
//                    }
//                }
//            }
//        })
//    }
    
    func reloadTableData()
    {
        participantsTableView.reloadData()
        
        if (self.data["clients"] as! [GKDictionary]).count > 0
        {
            noParticipantsLabel.isHidden = true
            participantsTableView.isHidden = false
        }
        else
        {
            noParticipantsLabel.isHidden = false
            participantsTableView.isHidden = true
        }
        
        if (self.isParticipant)
        {
            self.participantsTableView.isHidden = true
            self.noParticipantsLabel.isHidden = true
            updateTestimonialButton()
        }
    }
    
    func updateTestimonialButton()
    {
        let testimonial = participantTestimonial()
        
        if (testimonial != nil)
        {
            self.testimonialButton.setTitle("View Testimonial", for: UIControlState())
        }
        else
        {
            self.testimonialButton.setTitle("Leave Testimonial", for: UIControlState())
        }
    }
    
    func participantTestimonial() -> NSDictionary!
    {
        let participant = participantData()
        
        if (participant != nil)
        {
            if let testimonial = participant?["participant_testimonial"] as? NSDictionary
            {
                return testimonial
            }
        }
        
        return nil
    }
    
    func participantData() -> NSDictionary!
    {
        if (self.data != nil)
        {
            if let clients = self.data["clients"] as? NSArray
            {
                let c: NSArray! = clients.filter{ ($0 as! NSDictionary)["id"] as? Int == Static.sharedInstance.userId()} as NSArray!
                
                if (c != nil && c.count > 0)
                {
                    return c[0] as! NSDictionary
                }
            }
        }
        
        return nil
    }
    
    func showAddTestimonial(_ name: String, relatedId: Int, byClient: Bool)
    {
        let addTestimonialViewController = GKAddTestimonialViewController(nibName: "GKAddTestimonialViewController", bundle: nil)
        addTestimonialViewController.firstName = name
        addTestimonialViewController.relatedID = relatedId
        addTestimonialViewController.type = 3
        addTestimonialViewController.byClient = byClient
        self.navigationController?.pushViewController(addTestimonialViewController, animated: true)
    }
    
    func showTestimonial()
    {
        let authorName = self.data["author"]!["full_name"] as! String
        var testimonialMessage = ""
        let testimonial = participantTestimonial()
        
        if let detail = testimonial?["detail"] as? [String]
        {
            if detail.contains("1")
            {
                testimonialMessage += (authorName + " was able to help.\n\n")
            }
            if detail.contains("2")
            {
                testimonialMessage += (authorName + " is very skilled.\n\n")
            }
            if detail.contains("3")
            {
                testimonialMessage += (authorName + " is a punctual person.\n\n")
            }
            
            if detail.contains("4")
            {
                testimonialMessage += (authorName + " is such a friendly person.\n\n")
            }
            
            if detail.contains("5")
            {
                testimonialMessage += ("I am grateful I met " + authorName + ".\n\n")
            }
            
            if detail.contains("6")
            {
                testimonialMessage += (authorName + " was thoughtful.\n\n")
            }
            
            if detail.contains("7")
            {
                testimonialMessage += (authorName + " was able to put my needs first.\n\n")
            }
            
            if detail.contains("8")
            {
                testimonialMessage += (authorName + " had a kind demeanor.\n\n")
            }
            
            if detail.contains("9")
            {
                testimonialMessage += (authorName + " was able to pass along helpful thoughts and prayers.\n\n")
            }
            
            if detail.contains("10")
            {
                testimonialMessage += (authorName + " was able to take time out to go the extra mile for me.\n\n")
            }
        }
        
        TestimonialView.show(testimonialMessage)
    }
    
    func canShowInstructions(_ typeInstructions: GKInstructionsType) -> Bool
    {
        if (self.data == nil)
        {
            return false;
        }
        
        let clients : [GKDictionary]! = self.data["clients"] as! [GKDictionary]
        
        if (clients != nil && clients.count > 0)
        {
            let d = clients[0]
            let status = d["status"] as! Int
            
            if (typeInstructions == .AcceptOrCancel)
            {
                return status == 1
            }
            else if (typeInstructions == .FlakeOrSOS)
            {
                return status == 2
            }
        }
        
        return false
    }
    
    func showIntructions(_ typeInstructions: GKInstructionsType)
    {
        guard Static.sharedInstance.canShowAccessInstructions(typeInstructions) else
        {
            return
        }
        
            let window = self.navigationController?.view
            
            if (window != nil)
            {
                let rect = CGRect(x: 0, y: 0, width: window!.frame.width,
                                      height: window!.frame.height)
                
                let instructionsView = GKInstructionsView(frame: rect)
                instructionsView.typeInstructions = typeInstructions
//                instructionsView.customPosition = CGPointMake(0, point.y + 225)
                window!.addSubview(instructionsView)
                instructionsView.show()
            }
    }
    
    // MARK: - Methods -
    
    @IBAction func testimonialAction(_ sender: AnyObject)
    {
        let testimonial = participantTestimonial()
        
        if (testimonial != nil)
        {
            showTestimonial()
        }
        else
        {
            let clientData = participantData()
            
            if (clientData != nil)
            {
                showAddTestimonial(self.data["author"]!["full_name"] as! String,
                                   relatedId: clientData?["related_id"] as! Int,
                                   byClient: true)
            }
        }
    }
}
