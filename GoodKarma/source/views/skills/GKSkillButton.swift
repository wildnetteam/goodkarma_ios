//
//  GKSkillButton.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/30/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSkillButton: UIButton
{
  
  // MARK: - Properties -
  
  var removeButton: UIButton?
  var skill: GKModelSkill!
  
  // MARK: - Initializer -
  
  init(frame: CGRect, editable: Bool, skill: GKModelSkill)
  {
    super.init(frame: frame)
    
    self.skill = skill
    
    self.backgroundColor = UIColor.lightBlue()
    
    if editable
    {
      self.contentHorizontalAlignment = .left
      self.contentEdgeInsets = UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)
    }
    
    self.layer.cornerRadius = 5.0
    self.titleLabel?.font = UIFont.fontRoboto(.Regular, size: 12)
    
    self.setTitleColor(UIColor.blueOpaque(), for: UIControlState())
    
    if editable
    {
      setupRemoveButton()
    }
    else
    {
      removeButton?.removeFromSuperview()
    }
    
    setTitle(skill.text(), for: UIControlState())
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
  }
  
  // MARK: - Setup -
  
  func setupRemoveButton()
  {
    removeButton = UIButton()
    removeButton?.backgroundColor = UIColor.clear
    removeButton?.frame = CGRect(
      x: self.bounds.size.width - 20.0 - 5.0,
      y: (self.bounds.size.height - 20.0) * 0.5,
      width: 20.0,
      height: 20.0
    )
    self.addSubview(removeButton!)
    
    let removeImageView = UIImageView()
    removeImageView.center = CGPoint(x: removeButton!.bounds.midX, y: removeButton!.bounds.midY)
    removeImageView.bounds = CGRect(
      x: 0, y: 0,
      width: removeButton!.bounds.width * 0.5,
      height: removeButton!.bounds.height * 0.5
    )
    removeImageView.image = UIImage(named: "remove-tag")
    removeButton?.addSubview(removeImageView)
  }
}
