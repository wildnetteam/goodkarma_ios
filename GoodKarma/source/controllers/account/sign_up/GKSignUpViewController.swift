//
//  GKSignUpViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/1/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSignUpViewController: UIViewController,UITextFieldDelegate {
  
  // MARK: - IBOutlets -
  
  @IBOutlet weak var fbButton: UIButton!
  @IBOutlet weak var emailTextField: UITextField!
  
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var repasswordTextField: UITextField!
  
  @IBOutlet weak var containerTextField: UIView!
  @IBOutlet weak var scrollView: UIScrollView!
  
  // MARK: - Properties -
  
  var errorView: ErrorView!
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    self.title = "Sign Up"
    
    cleanBackButton()
    
    fbButton.layer.cornerRadius = 5.0
    self.automaticallyAdjustsScrollViewInsets = false
    
    self.errorView = ErrorView(frame: CGRect(x: 0, y: 0, width: 179, height: 50))
    self.containerTextField.addSubview(self.errorView)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    
    self.navigationController?.isNavigationBarHidden = false
  }
  
  // MARK: - Protocol UITextFieldDelegate -
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool
  {
    
    if textField == emailTextField
    {
        self.passwordTextField.becomeFirstResponder()
    }
    else if textField == passwordTextField
    {
        self.repasswordTextField.becomeFirstResponder()
    }
    else
    {
        self.repasswordTextField.resignFirstResponder()
        self.actionNext(0 as AnyObject)
    }
//    if NSObject.isEqual(self.emailTextField) {
//
//      self.passwordTextField.becomeFirstResponder()
//    }
//    else if NSObject.isEqual(self.passwordTextField) {
//
//      self.repasswordTextField.becomeFirstResponder()
//    }
//    else {
//
//      self.actionNext(0 as AnyObject)
//    }

    return false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    self.errorView.hide()
  }
  
  // MARK: - Protocol UIScrollViewDelegate -
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    self.errorView.hide()
  }
  
  // MARK: - IBActions -
  
  @IBAction func actionFB(_ sender: AnyObject) {
    
    self.view.endEditing(true)
    self.errorView.hide()
    
    FacebookHandler.login(
      
      self.navigationController, callback: { (data: NSDictionary?) -> () in
        
        if data != nil {
          
          self.loginWithFacebook(data!)
        }
        else {
          
          showConnectionError({ (type: NoInternetResultType) -> () in
            
            self.actionFB(0 as AnyObject)
          })
        }
    })
  }
  
  
    @IBAction func actionNext(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        self.errorView.hide()
        
        if validate() {
            
            LoadingView.showLoadingView(
                "Creating Account...", parentView: self.navigationController?.view, backColor: true)
            
            var params = [String: String]()
            params["email"]    = self.emailTextField.text
            params["password"] = self.passwordTextField.text
            params["uid"]      = UIDevice.current.identifierForVendor!.uuidString
            params["device_type"] = "ios"
            
            GKServiceClient.call(GKRouterClient.signUp(), parameters: params as GKDictionary, callback: {
                (response: AnyObject?) -> () in
                
                LoadingView.hideLoadingView(self.navigationController?.view)
                
                if let res:AnyObject = response {
                    
                    let envelope = ResponseSkeleton(res)
                    let data = envelope.getResource() as? NSDictionary
                    
                    print(res)
                    if envelope.getSuccess() {
                        
                        //            Static.sharedInstance.saveEmail(self.emailTextField.text!)
                        //30 nov
                        UserDefaults.standard.set((data?["id"] as AnyObject), forKey: "id")
                        self.showCompleteInfo(data)
                    }
                    else if (res["error"] as? NSDictionary != nil) {
                        
                        showMessageError("Alert", text: (res["error"] as? NSDictionary)?.value(forKey: "object") as! String, callback: { (type: NoInternetResultType) -> () in
                        })
                    }
                    else {
                        showCommonError({ (type: NoInternetResultType) -> () in
                            self.actionNext(0 as AnyObject)
                        })
                    }
                }
                else
                {
                    showConnectionError({ (type: NoInternetResultType) -> () in
                        self.actionNext(0 as AnyObject)
                    })
                }
            })
        }
    }
  
  // MARK: - Methods -
  
  func loginWithFacebook(_ data: NSDictionary)
  {
    LoadingView.showLoadingView("Loading...",
                                parentView: self.navigationController?.view,
                                backColor: true)
    
    var params = [String: String]()
    params["first_name"] = (data["first_name"] as? String)?.capitalized
    params["last_name"] = (data["last_name"] as? String)?.capitalized
    params["facebook_id"] = data["id"] as? String
    params["uid"] = UIDevice.current.identifierForVendor!.uuidString
    params["email"] = data["email"] as? String
//    params["type"] = "Client"
    params["device_type"] = "ios" as String?

    let isSocial = true
    print(params)
    GKServiceClient.call(GKRouterAuth.signWithFacebook(), parameters:params as GKDictionary, callback: {
      (response: AnyObject?) -> () in
      
      LoadingView.hideLoadingView(self.navigationController?.view)
      
      if let res:AnyObject = response {
        
        let envelope = ResponseSkeleton(res)
        
        if envelope.getSuccess() {
          
          let data = envelope.getResource() as? NSDictionary
            UserDefaults.standard.set((data?["id"] as AnyObject), forKey: "id") // added
            UserDefaults.standard.set((data?["facebook_id"] as AnyObject), forKey: "isFacebookId") // added

          let hasPhoto         = (data?["has_photo"] as! NSNumber).boolValue
          let hasProfile       = (data?["has_profile"] as! NSNumber).boolValue
          let hasPhoneVerified = (data?["has_phone_verified"] as! NSNumber).boolValue
          
          if (!hasProfile) {
            let completeInfo = GKRegistrationViewController(nibName: "GKRegistrationViewController",
              bundle: nil)
            completeInfo.data = data
            completeInfo.fromFacebook = isSocial
            self.navigationController?.pushViewController(completeInfo, animated: true)
          }
          else if !hasPhoneVerified {
            let confirmCodeController = GKConfirmCodeViewController(nibName: "GKConfirmCodeViewController", bundle: nil)
            confirmCodeController.data = data
            confirmCodeController.fromFacebook = isSocial
            self.navigationController?.pushViewController(confirmCodeController, animated: true)
          }
          else if (!hasPhoto) {
            let photoController = GKAddPhotoViewController(nibName: "GKAddPhotoViewController",
              bundle: nil)
            photoController.data = data
            photoController.fromFacebook = isSocial
            self.navigationController?.pushViewController(photoController, animated: true)
          }
          else {
            Static.sharedInstance.saveUserProfile(envelope.getResource() as? NSDictionary)
            Static.sharedInstance.saveLogin(true)
            (UIApplication.shared.delegate as! AppDelegate)
              .showHome(self.navigationController!, loginOrRegister: true, recovery: false) //isSocial
          }
        }
        else {
          
          showCommonError({ (type: NoInternetResultType) -> () in
            
            self.actionFB(0 as AnyObject)
          })
        }
      }
      else {
        
        showConnectionError({ (type: NoInternetResultType) -> () in
          
          self.actionFB(0 as AnyObject)
        })
      }
    })
  }
  
  func showCompleteInfo(_ data: NSDictionary?) {
    
    let completeInfoViewController = GKRegistrationViewController(
      nibName: "GKRegistrationViewController", bundle: nil
    )
    completeInfoViewController.data         = data
    completeInfoViewController.fromFacebook = false
    self.navigationController?.pushViewController(completeInfoViewController, animated: true)
  }
  
  func validate() -> Bool {
    
    let baseY = self.errorView.frame.size.height;
    
    if (!self.emailTextField.text!.isEmailValid()) {
      
      self.errorView.showInPos(CGPoint(x: self.emailTextField.frame.minX - 8,
        y: self.emailTextField.frame.minY - baseY),
                               text: "Need to set a valid email.")
      return false;
    }
    
    if (self.passwordTextField.text!.count < 6)
    {
      self.errorView
        .showInPos(CGPoint(x: self.passwordTextField.frame.minX - 8,
          y: self.passwordTextField.frame.minY - baseY),
                   text: "Pasword should contain at least 6 characters.")
      return false;
    }
    
    if (self.repasswordTextField.text != self.passwordTextField.text)
    {
      self.errorView
        .showInPos(CGPoint(x: self.repasswordTextField.frame.minX - 8,
          y: self.repasswordTextField.frame.minY - baseY),
                   text: "Password confirmation doesn't match Password.")
      
      return false;
    }
    return true
  }
    
  
}
