//
//  GKManageSkillsView.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/30/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKManageSkillsView: UIView
{
  // MARK: - Properties -
  
  fileprivate var editable: Bool!
  
  fileprivate var skills: [GKModelSkill] = []
  fileprivate var skillButtons: [GKSkillButton] = []
  
  fileprivate var noSkillLabel: UILabel?
  
  internal var addButton  : UIButton?
  internal var moreButton : UIButton?
  internal var delegate: GKManageSkillsDelegate?
  
  // MARK: - Initializer -
  
  init(frame: CGRect, editable: Bool, delegate: GKManageSkillsDelegate? = nil)
  {
    super.init(frame: frame)
    
    self.editable = editable
    self.delegate = delegate

    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
  }
  
  // MARK: - Methods -
  
  fileprivate func setup()
  {
    self.backgroundColor = UIColor.clear
    
    if editable!
    {
      addButton = UIButton()
      addButton?.backgroundColor = UIColor.baseColor()
      addButton?.titleLabel?.font = UIFont.fontRoboto(.Regular, size: 12)
      addButton?.frame = CGRect(x: 15.0, y: 15.0, width: 80.0, height: 30.0)
      addButton?.setTitle("Add+", for: UIControlState())
      addButton?.setTitleColor(UIColor.white, for: UIControlState())
      addButton?.layer.cornerRadius = 5.0
      addButton?.addTarget(
        self, action: #selector(self.showAddSkills),
        for: .touchUpInside
      )
      self.addSubview(addButton!)
    }
    else
    {
      noSkillLabel = UILabel()
//      noSkillLabel?.backgroundColor = UIColor.redColor()
      noSkillLabel?.font   = UIFont.fontRoboto(.Regular, size: 15)
      noSkillLabel?.frame  = CGRect(x: 25.0, y: 15.0, width: 70.0, height: 30.0)
      noSkillLabel?.text   = "No Skills"
      noSkillLabel?.textColor = UIColor.gray
      noSkillLabel?.isHidden = true
      self.addSubview(noSkillLabel!)
      
      addButton?.removeFromSuperview()
    }
  }
  
  fileprivate func cleanButtons()
  {
    for button in skillButtons
    {
      button.removeFromSuperview()
    }
  }

  func setupSkills(_ skills: [GKModelSkill], hasLimits: Bool = false)
  {
    self.skills = skills.sorted { $0.name < $1.name }
    
    cleanButtons()
    
    let selfWidth = self.bounds.size.width
    
    var originX:      CGFloat = 15.0
    var originY:      CGFloat = 15.0
    let heightButton: CGFloat = 30.0
    var rows = 1
    
    if let addButtonRightSide = addButton?.rightSide
    {
      originX = addButtonRightSide + 5.0
    }
    
    skillButtons = []
    
    // Remove More Button
    moreButton?.removeFromSuperview()
    
    for (index, skill) in self.skills.enumerated()
    {
      var widthButton = skill.text().sizeInWidth(999, font: UIFont.fontRoboto(.Regular, size: 13)).width
      
      if widthButton < 60.0
      {
        widthButton = 60.0
      }
      // button metrics = pad + label + close button
      widthButton += (5.0 + 20.0) // pad + Close button
      
      if (originX + widthButton + 5.0) > selfWidth
      {
        originX = 15.0
        originY += (heightButton + 5.0)
        rows += 1
      }
      
      if rows == 2 && hasLimits
      {
        moreButton?.removeFromSuperview()
        
        moreButton = UIButton(
          frame: CGRect(
            origin: CGPoint(x: originX, y: originY) ,
            size: CGSize(width: 80.0, height: heightButton)
          )
        )
        moreButton?.backgroundColor = UIColor.baseColor()
        moreButton?.titleLabel?.font = UIFont.fontRoboto(.Regular, size: 12)
        moreButton?.setTitle("More", for: UIControlState())
        moreButton?.setTitleColor(UIColor.white, for: UIControlState())
        moreButton?.layer.cornerRadius = 5.0
        moreButton?.addTarget(
          self, action: #selector(self.showMoreSkills),
          for: .touchUpInside
        )
        self.addSubview(moreButton!)
        
        break
      }
      else
      {
        let frameButton = CGRect(
          origin: CGPoint(x: originX, y: originY) ,
          size: CGSize(width: widthButton, height: heightButton)
        )
        
        let skillButton = GKSkillButton(frame: frameButton, editable: editable, skill: skill)
        skillButton.removeButton?.tag = index
        skillButton.removeButton?.addTarget(
          self,
          action: #selector(self.removeSkillInView(_:)),
          for: .touchUpInside
        )
        skillButtons.append(skillButton)
        
        originX = skillButton.rightSide + 5.0
      }
    }
    
    for skillButton in skillButtons
    {
      self.addSubview(skillButton)
    }
    
    noSkillLabel?.isHidden = (editable || skills.count > 0)
    
    updateHeight(originY + heightButton + 15.0)
  }
  
  @objc func removeSkillInView(_ skillButton: UIButton)
  {
    delegate?.GKRemoveSkill(skills[skillButton.tag])
  }
  
  @objc func showAddSkills()
  {
    delegate?.GKShowAddSkillsView()
  }
  
  @objc func showMoreSkills()
  {
    delegate?.GKShowMoreSkillsInView(skills)
  }

  fileprivate func updateHeight(_ lastPointY: CGFloat)
  {
    var selfFrame = self.frame
    selfFrame.size.height = lastPointY
    self.frame = selfFrame
  }
}
