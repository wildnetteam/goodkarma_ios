////
////  GKNotification.swift
////  GoodKarma
////
////  Created by Paul Aguilar on 5/24/16.
////  Copyright © 2016 Teclalabs. All rights reserved.
////
//
//import Foundation
//
//enum GKNotificationKind: String {
//  case Invite = "invite"
//  case Accept = "accept"
//  case Cancel = "cancel"
//}
//
//enum GKNotificationReference: String {
//  case Connect = "connect"
//  case Meetup  = "meetup"
//}
//
//struct GKModelNotification: GKModelProtocol {
//  
//  // MARK: - Properties -
//  
//  var uid: String!
//  var sender_user: GKModelUser!
//  var reference: GKNotificationReference!
//  var reference_invite_uid: String?
//  var kind: GKNotificationKind!
//  var date_created: String?
//  
//  // MARK: - Methods
//  // MARK: - Class Methods -
//  
//  
//  static func readFromDictionary(dictionary: GKDictionary) -> GKModelNotification {
//    var notification = GKModelNotification()
//    
//    // Reference
//    if let ref = dictionary["reference"] as? String {
//      if let reference = GKNotificationReference(rawValue: ref) {
//        notification.reference = reference
//      }
//      else {
//        print("WRONG REFERENCE")
//      }
//    }
//    // Kind
//    if let kind = dictionary["kind"] as? String {
//      if let noti_kind = GKNotificationKind(rawValue: kind) {
//        notification.kind = noti_kind
//      }
//      else {
//        print("WRONG KIND")
//      }
//    }
//    
//    // Sender user
//    let sender_user_dic = dictionary["sender_user"] as! [String: AnyObject]
//    notification.sender_user = GKModelUser.readFromSmallDictionary(sender_user_dic)
//    
//    // Kind: 'Invite' has 'reference_meetup' or 'reference_request'
//    if notification.kind == GKNotificationKind.Invite {
//      if let reference_meetup = dictionary["reference_meetup"] as? String {
//        notification.reference_invite_uid = reference_meetup
//      }
//      else if let reference_request = dictionary["reference_request"] as? String {
//        notification.reference_invite_uid = reference_request
//      }
//    }
//    // Date Created
//    if let date_str = dictionary["date_created"] as? String {
//      
//      notification.date_created = NSDate.parseDateWhitFormat(date_str)
//    }
//    
//    return notification
//  }
//  
//}