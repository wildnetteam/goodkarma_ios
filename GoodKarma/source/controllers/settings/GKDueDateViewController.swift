//
//  GKDueDateViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 9/27/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKDueDateViewController: GKViewController, UIPickerViewDataSource, UIPickerViewDelegate
{
    // MARK: - IBOutlets -
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var intervalLabel: UILabel!
    @IBOutlet weak var editDueDateButton: UIButton!
    
    // MARK: - Properties -
    
    var numberValue  = 1
    var intervalValue = 1
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Due Date"
        
        let editBarButton = UIBarButtonItem(
            title: "Edit", style: .plain, target: self, action: #selector(self.editDueDate)
        )
        editBarButton.tintColor = UIColor.baseColor()
        self.navigationItem.rightBarButtonItem = editBarButton
        
        requestData()
    }
    
    // MARK: - Protocols -
    // MARK: - Protocol UIPickerViewDataSource -
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if component == 0
        {
            return 999
        }
        else if component == 1
        {
            return scaleValues().count
        }
        else
        {
            return 0
        }
    }
    
    // MARK: - Protocol UIPickerViewDelegate -
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if component == 0
        {
            return "\(row + 1)"
        }
        else if component == 1
        {
            return scaleValues()[row].1
        }
        else
        {
            return nil
        }
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionsave(_ sneder: AnyObject)
    {
        var params = GKDictionary()
        params["token"]    = Static.sharedInstance.token() as AnyObject?
        params["number"]   = "\(numberValue)" as String? as AnyObject
        params["interval"] = "\(intervalValue)" as String? as AnyObject
        
        print(params)
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.settingsDueDate(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                print(res)
                let envelope = ResponseSkeleton(res , string  : "due_settings")
                
                if envelope.getSuccess()
                {
                    if envelope.getResource() is GKDictionary
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        })
    }
    
    // MARK: - Methods -
    
    @objc func editDueDate()
    {
        openNumberPickerView()
    }
    
    func openNumberPickerView()
    {
        
        let selectAction = RMAction(title: "Select", style: .done) { (controller) in
            if let picker = controller as? RMActionController<UIPickerView>
            {
                let pickerController = picker as? RMPickerViewController
                self.numberValue   = (pickerController?.picker.selectedRow(inComponent: 0))! + 1
                self.intervalValue = self.scaleValues()[(pickerController?.picker.selectedRow(inComponent: 1))!].0
                self.valueLabel.text = "\(self.numberValue)"
                self.intervalLabel.text = self.stringFromInterval(self.intervalValue)
            }
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .cancel) { (controller) in
        }
        
        let pickerController = RMPickerViewController(style: .default, select: selectAction as! RMAction<UIPickerView>?, andCancel: cancelAction as! RMAction<UIPickerView>?)!
        pickerController.picker.dataSource = self
        pickerController.picker.delegate   = self
        pickerController.disableBlurEffects = true
        pickerController.picker.selectRow(self.numberValue - 1, inComponent: 0, animated: false)
        pickerController.picker.selectRow(rowFromInterval(self.intervalValue), inComponent: 1, animated: false)
        pickerController.picker.selectRow(0, inComponent: 0, animated: false)
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func requestData()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject?
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.settingsDueDate(), parameters: params, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response
            {
                let envelope = ResponseSkeleton(res , string  : "due_settings")
                
                if envelope.getSuccess()
                {
                    if let data = envelope.getResource()["object"] as? GKDictionary
                    {
                        self.numberValue   = data["number"] as! Int
                        self.intervalValue = data["interval"] as! Int
                        
                        self.updateValues()
                    }
                }
            }
        })
    }
    
    func scaleValues() -> [(Int, String)]
    {
        return [
            ( 0,"Day(s)"),
            ( 1,"Week(s)"),
            (2,"Month(s)"),
            (3,"Year(s)")
        ]
    }
    
    func rowFromInterval(_ interval: Int) -> Int
    {
        switch self.intervalValue
        {
        case 0  : return 0
        case 1  : return 1
        case 2 : return 2
        case 3 : return 3
        default : return 0
        }
    }
    
    func stringFromInterval(_ interval: Int) -> String
    {
        switch interval
        {
        case 0  : return scaleValues()[0].1
        case 1  : return scaleValues()[1].1
        case 2 : return scaleValues()[2].1
        case 3 : return scaleValues()[3].1
        default : return ""
        }
    }
    
    func updateValues()
    {
        valueLabel.text    = "\(numberValue)"
        
        intervalLabel.text = self.stringFromInterval(self.intervalValue)
    }
}

