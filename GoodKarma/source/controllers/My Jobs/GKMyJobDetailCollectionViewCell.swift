//
//  GKMyJobDetailCollectionViewCell.swift
//  GoodKarma
//
//  Created by Shivani on 10/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit

class GKMyJobDetailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: WebImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = 4.0
        imageView.backgroundColor = UIColor.black
        
        // Initialization code
    }

}
