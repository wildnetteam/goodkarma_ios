//
//  GKHistoryRequestedTableViewCell.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 8/19/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKHistoryRequestedTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var statusArrowImageView: UIImageView!
    
    @IBOutlet weak var detailLabel : UILabel!
    @IBOutlet weak var timestampLabel : UILabel!
    
    @IBOutlet weak var postTypeImageView : UIImageView!
    
    @IBOutlet weak var createdCountLabel : UILabel!
    @IBOutlet weak var progressCountLabel : UILabel!
    
    // MARK: - Properties -
    
    var data: GKDictionary!
    var delegate: GKHistoryPostStateDelegate?
    var withButton = false
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Picture
        //    userImageView.layer.cornerRadius = CGRectGetHeight(userImageView.bounds) * 0.5
        
        self.selectionStyle = .none
    }
    
    // MARK: - IBActions -
    
    @IBAction func actionShowStatus(_ sender: AnyObject)
    {
        delegate?.GKHandlePostState(self.data)
    }
    
    // MARK: - Class Methods -
    
    internal class func cellIdentifier() -> String {
        
        return "GKHistoryRequestedTableViewCell"
    }
    
    internal class func heightForCell() -> CGFloat {
        
        return 135.0
    }
    
    // MARK: - Methods -
    
    func loadData(_ data: GKDictionary)
    {
        self.data = data
        print(self.data)
        let type = data["post_type"] as! Int
        let value : GKDictionary!
        let valueExists = data["request_positivity"] as? GKDictionary != nil
        if valueExists{
            value = data["request_positivity"] as? GKDictionary
        }
        else{
            value = data["request_help"] as? GKDictionary
        }
        //let desc = value["desc"] as! String
        let status = data["like_status"] as! Int
        self.withButton = false
        
        // Started
        if status == 1
        {
            statusButton.isHidden = false
            statusButton.setTitle("In-progress", for: UIControlState())
            statusButton.isUserInteractionEnabled = true
            statusArrowImageView.isHidden = false
        }
            // Pause
        else if status == 6
        {
            statusButton.isHidden = false
            statusButton.setTitle("Paused", for: UIControlState())
            statusButton.isUserInteractionEnabled = true
            statusArrowImageView.isHidden = false
        }
            // Finished
        else
        {
            statusButton.isHidden = false
            statusButton.isUserInteractionEnabled = false
            statusButton.setTitle("Finished", for: UIControlState())
            statusArrowImageView.isHidden = true
        }
        
        // Serve
        if type == 1
        {
            postTypeImageView.image = UIImage(named: "home-serve-on")
        }
            // Request Positivity
        else if type == 2
        {
            postTypeImageView.image = UIImage(named: "home-positive-on")
            statusButton.isUserInteractionEnabled = false
            statusButton.isHidden = true
            statusArrowImageView.isHidden = true
        }
            // Request Help
        else if type == 3
        {
            postTypeImageView.image = UIImage(named: "home-request-on")
        }
        else if type == 4
        {
            postTypeImageView.image = UIImage(named: "home-gratitude-on")
            statusButton.isUserInteractionEnabled = false
            statusButton.isHidden = true
            statusArrowImageView.isHidden = true
        }
        
        if let des = self.data["desc"] as? String{
            self.detailLabel.text = des.decodeEmoji()
        }
       // self.detailLabel.text = (value["desc"]! as! String).decodeEmoji()
        // self.detailLabel.text    = ""//desc
        
        
//        let clientData = data["clients"] as! [GKDictionary]
//        let tempData = clientData[0] as! GKDictionary
        self.timestampLabel.text = secondsToFormatted(data["createdAt"] as? Int)
        
        if (type != 4 && type != 2)
        {
            //        let createdCount   = self.data["created_count"] as! Int
            //        let progressCount  = self.data["inprogress_count"] as! Int
            //        let completedCount = self.data["completed_count"] as! Int
            //
            //        createdCountLabel.isHidden = (createdCount == 0)
            //        createdCountLabel.text   = "\(createdCount)"
            //
            //        progressCountLabel.text  = "\(completedCount)/\(progressCount + completedCount)"
            //        progressCountLabel.isHidden = false;
            
            progressCountLabel.isHidden = true
            createdCountLabel.isHidden = true
        }
        else
        {
            progressCountLabel.isHidden = true
            createdCountLabel.isHidden = true
        }
        
        self.withButton = statusButton.isUserInteractionEnabled
    }
}

