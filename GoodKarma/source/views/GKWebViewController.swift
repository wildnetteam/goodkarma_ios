//
//  GKTermsViewController.swift
//  Faveo
//
//  Created by Eliezer de Armas on 27/10/15.
//  Copyright (c) 2015 teclalabs. All rights reserved.
//

import UIKit

class GKWebViewController: GKViewController, UIWebViewDelegate
{
  
  // MARK: - IBOutlets -
  
//  @IBOutlet weak var scrollView: UIScrollView!
//  @IBOutlet weak var textView: UITextView!
    var url: String = ""
  
  // MARK: - Override UIViewController -
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.title = ""
    cleanBackButton()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    
    LoadingView.showLoadingView(
        "Loading",
        parentView: self.navigationController?.view, backColor: true
    )
    
    self.navigationController?.isNavigationBarHidden = false
//    self.scrollView.contentSize = CGSizeMake(
//      CGRectGetWidth(self.scrollView.frame),
//      CGRectGetMaxY(self.textView.frame) + 20
//    )
//    webView.hidden = true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    super.viewDidAppear(animated)
    
    let webView = UIWebView(
      frame: CGRect(
        x: 0.0,
        y: 64.0,
        width: self.view.bounds.size.width,
        height: self.view.bounds.size.height - 64
      )
    )
    let width: CGFloat = webView.frame.size.width;
    let height: CGFloat = webView.frame.size.height;
    
    let index1 = url.range(of: "/", options: .backwards)?.lowerBound
    let substringId = url.substring(from: index1!)
    
    let embedHTML = String(format: "<html><head><style type=\"text/css\">body {background-color: transparent;color: black;}</style></head><body style=\"margin:10\"><iframe src=\"//player.vimeo.com/video%@?autoplay=1\" width=\"%f\" height=\"%f\" frameborder=\"10\" allowfullscreen></iframe>",substringId , width * 2.8, height * 3)
    
    webView.delegate = self
    
    let urlll: NSURL = NSURL(string: (String(format: "https://player.vimeo.com/video%@?autoplay=1", substringId)))!
    webView.loadHTMLString(embedHTML as String, baseURL:urlll as URL )

    webView.scalesPageToFit = true
    self.view.addSubview(webView)
  }
    
    //MARK:------WebView delegates--------
    
    public func webViewDidStartLoad(_ webView: UIWebView){
        LoadingView.hideLoadingView(self.navigationController?.view)
        
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        LoadingView.hideLoadingView(self.navigationController?.view)
        
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
   LoadingView.hideLoadingView(self.navigationController?.view)
        
    }
}
