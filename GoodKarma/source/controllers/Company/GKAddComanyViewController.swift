//
//  GKAddComanyViewController.swift
//  GoodKarma
//
//  Created by Shivani on 25/10/17.
//  Copyright © 2017 Teclalabs. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI
import AddressBookUI

class GKAddComanyViewController: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate  , UITextFieldDelegate, ABPeoplePickerNavigationControllerDelegate
{
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var nameOfOeganizationTextField: UITextField!
    @IBOutlet weak var basicInfoTextView: UITextView!
    @IBOutlet weak var addressTextview: UITextView!
    @IBOutlet weak var photoImageView: UIImageView!
    
    var parentObject: Any!
    var accountType = 0
    var isInMenu = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isInMenu
        {
            setMenuBarBadge()
        }
        self.title = "Add Company"

        let closeImage = UIImage(named: "arrow-l")?.withRenderingMode(.alwaysOriginal)
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x: 0.0, y: 0.0, width: 13.0, height: 22.0)
        closeButton.addTarget(
            self,
            action: #selector(self.close),
            for: .touchUpInside
        )
        closeButton.setImage(closeImage, for: UIControlState())
        
        let closeBarButton = UIBarButtonItem(customView: closeButton)
        self.navigationItem.leftBarButtonItem = closeBarButton
        
        photoImageView.layer.cornerRadius = photoImageView.frame.size.height / 2
//        addressTextview.layer.borderWidth = 1.0
//        addressTextview.layer.borderColor = UIColor.lightGray.cgColor
//        basicInfoTextView.layer.borderColor = UIColor.lightGray.cgColor
//        basicInfoTextView.layer.borderWidth = 1.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func close()
    {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionProfileImage(_ sender: Any)
    {
        let alertController = UIAlertController(title: "Pick Image", message: nil, preferredStyle: .actionSheet)
        let cameraButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            self.pickFromCamera()
        }
        let galleryButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { action -> Void in
            self.pickFromGallery()
        }
        let cancelButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
            
        }
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(cancelButton)
        self.present(alertController, animated: false, completion: nil)
    }
    @IBAction func addContacts(_ sender: Any)
    {
        let c = ABPeoplePickerNavigationController()
        c.peoplePickerDelegate = self
        c.displayedProperties = [
            NSNumber(value: kABPersonPhoneProperty as Int32)
        ]
        
        ABAddressBookRequestAccessWithCompletion(c, { success, error in
            if success {
                self.present(c, animated: true, completion: nil)
            }
        })
    }
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier)
    {
        peoplePicker.dismiss(animated: true, completion: nil)
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        let index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        let contact = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as! String
        let nameCFString: CFString = ABRecordCopyCompositeName(person).takeRetainedValue()
        let name: NSString = nameCFString as NSString
        let firstName = name.components(separatedBy: " ")[0]
        let lastName = name.components(separatedBy: " ")
        self.phoneNumberTextField.text = contact
    }
    func validate() -> Bool
    {
        var message : String!
        let image = UIImage(named : "plus-photo")
        if photoImageView.image == image
        {
            message = "Please select profile image"
        }
        else if nameOfOeganizationTextField.text == ""
        {
            message = "Please enter the name of organization"
        }
        else if phoneNumberTextField.text == ""
        {
            message = "Please enter phone number"
            
        }
        else if addressTextview.text == ""
        {
            message = "Please ente rthe address"
            
        }
        else if basicInfoTextView.text == ""
        {
            message = "Please enter basic info"
        }
        else
        {
            return true
        }
        
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alertController, animated: false, completion: nil)
        return false
    }
    
    @IBAction func accountTypeSegmentAction(_ sender: UISegmentedControl)
    {
        if sender.selectedSegmentIndex == 0
        {
            accountType = 0
        }
        else{
            accountType = 1
        }
    }
    
    @IBAction func actionNext(_ sender: Any)
    {
        let isValid = self.validate()
        if isValid == true
        {
            self.uploadCompanyProfile()
//            let alertController = UIAlertController(title: "Wait!", message: "Work in progress", preferredStyle: UIAlertControllerStyle.alert)
//            alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
//            self.present(alertController, animated: false, completion: nil)
        }
    }
    
    func pickFromCamera()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func pickFromGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let pickedImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        self.photoImageView.image = pickedImage
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadCompanyProfile()
    {
        var params = GKDictionary()
        params["token"] = Static.sharedInstance.token() as AnyObject
        params["companyName"] = nameOfOeganizationTextField.text as AnyObject?
        params["phoneNumber"] = phoneNumberTextField.text as AnyObject
        params["companyAddress"] =  addressTextview.text as AnyObject
        params["basicInformation"] = basicInfoTextView.text as AnyObject
        params["accountType"] = accountType as AnyObject
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in params {
                multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
            }
            if  let imageData = UIImageJPEGRepresentation(self.photoImageView.image! , 0.6)
            {
                multipartFormData.append(imageData, withName: "image", fileName: "image", mimeType: "image/png")
            }
            
        }, to: GKRouterClient.addCompany(), method: .post, headers: nil,
           encodingCompletion: {
            encodingResult in
            switch encodingResult
            {
            case .success(let upload, _, _):
                LoadingView.hideLoadingView(self.navigationController?.view)
                upload.responseJSON { response in
                    print(response.result.value!)
                    let res:AnyObject = response.result.value as AnyObject
                    let envelope = ResponseSkeleton(res)
                    
                    if envelope.getSuccess()
                    {
                        print(envelope)
                        let addPeopleController = GKCompanyAddPeopleViewController(nibName: "GKCompanyAddPeopleViewController", bundle: nil)
                        addPeopleController.parentObject = self.parentObject
                        addPeopleController.companyId = String(describing: (envelope.getResource() as! NSDictionary).value(forKey: "id") as! NSNumber)
                        addPeopleController.referenceCode = (envelope.getResource() as! NSDictionary).value(forKey: "referenceCode") as! String
                        self.navigationController?.pushViewController(addPeopleController, animated: true)
                    }
                    else if (res["error"] as? NSDictionary != nil) {
                        
                        showMessageError("Alert", text: (res["error"] as? NSDictionary)?.value(forKey: "object") as! String, callback: { (type: NoInternetResultType) -> () in
                        })
                    }
                    else {
                        showCommonErrorWithoutSignOut({ (type: NoInternetResultType) -> () in
                            self.uploadCompanyProfile()
                        })
                    }
                }
                break
                
            case .failure(let encodingError):
                LoadingView.hideLoadingView(self.navigationController?.view)
                print(encodingError)
                break
            }
        }
        )
    }
    
}
