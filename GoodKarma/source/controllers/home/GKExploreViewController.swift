//
//  GKExploreViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 5/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKExploreViewController: GKViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.title = "Explore"
      
      enableMenuButton()
    }
  
//  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    
//    let controller = GKWalkthroughViewController(nibName: "GKWalkthroughViewController", bundle: nil)
//    
//    
//    
//    
//    self.navigationController?.presentViewController(controller, animated: true, completion: nil)
    
  }

  // TODO: add buttons for these actions
//  func showRequest() {
//    
//    let controller = GKRequestPositiveAndHelpViewController(nibName: "GKRequestPositiveAndHelpViewController", bundle: nil)
//    let navigationController = UINavigationController(rootViewController: controller)
//    
//    presentDetailViewController(navigationController)
//    
//    hideMenuWithFactor(1.0)
//  }
//  
//  func showServe() {
//    
//    let controller = GKCreateServeViewController(nibName: "GKCreateServeViewController", bundle: nil)
//    let navigationController = UINavigationController(rootViewController: controller)
//    
//    presentDetailViewController(navigationController)
//    
//    hideMenuWithFactor(1.0)
//  }
//  
  
}
