//
//  GKSkillCategoriesViewController.swift
//  GoodKarma
//
//  Created by Paul Aguilar on 6/23/16.
//  Copyright © 2016 Teclalabs. All rights reserved.
//

import UIKit

class GKSkillCategoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties -
    
    var categories = [GKDictionary]()
    var skills         = [GKModelSkill]()
    var skillsSelected: [GKModelSkill]!
    var creatingPos: Bool = false
    
    // MARK: - Override UIViewController -
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Categories"
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        requestSkillsData()
        
        cleanBackButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        if self.navigationController?.viewControllers.last is GKRegistrationViewController
        {
            (self.navigationController?.viewControllers.last as! GKRegistrationViewController).skillsSelected = self.skillsSelected
        }
        else if self.navigationController?.viewControllers.last is GKRequestPositiveAndHelpViewController
        {
            (self.navigationController?.viewControllers.last as! GKRequestPositiveAndHelpViewController).helpSkillsSelected = self.skillsSelected
        }
        else if self.navigationController?.viewControllers.last is GKEditProfileViewController
        {
            (self.navigationController?.viewControllers.last as! GKEditProfileViewController).skillsSelected = self.skillsSelected
        }
        else if self.navigationController?.viewControllers.last is GKCreateServeViewController
        {
            (self.navigationController?.viewControllers.last as! GKCreateServeViewController).skillsSelected = self.skillsSelected
        }
        else if self.navigationController?.viewControllers.last is GKHomeFilterViewController
        {
            (self.navigationController?.viewControllers.last as! GKHomeFilterViewController).skillsSelected = self.skillsSelected
        }
            //modified
        else if self.navigationController?.viewControllers.last is GKAssignJobViewController
        {
            (self.navigationController?.viewControllers.last as! GKAssignJobViewController).skillsSelected = self.skillsSelected
        }
    }
    
    //  MARK: - Protocols
    //  MARK: - Protocol UITableViewDataSource -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = self.categories[indexPath.row]["name"] as? String
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = UIColor.baseColor()
        
        return cell
    }
    
    // MARK: - Protocol UITableViewDelegate -
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let category   = categories[indexPath.row]
        let categoryID = category["id"] as! Int
        
        let subcategories = skills.filter {
            $0.categoryID == categoryID
        }
        
        let subcategoriesController = GKSkillSubCategoriesViewController(
            nibName: "GKSkillSubCategoriesViewController", bundle: nil)
        subcategoriesController.category         = category
        subcategoriesController.subcategories    = subcategories
        subcategoriesController.skillsSelected = self.skillsSelected
        subcategoriesController.creatingPos = self.creatingPos
        self.present(subcategoriesController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
    
    // MARK: - Methods -
    
    func requestSkillsData() {
        
        var params = [String: String]()
        params["token"] = Static.sharedInstance.token()
        
        LoadingView.showLoadingView(
            "Loading...",
            parentView: self.navigationController?.view, backColor: true
        )
        
        GKServiceClient.call(GKRouterClient.listSkills(), parameters: params as GKDictionary, callback: {
            (response: AnyObject?) -> () in
            
            LoadingView.hideLoadingView(self.navigationController?.view)
            
            if let res:AnyObject = response {
                
                let envelope = ResponseSkeleton(res)
                
                if envelope.getSuccess() {
                    
                    let data = envelope.getResource() as! NSDictionary
                    print(data)
                    self.skills =  []
                    
                    for dic in data["skills"] as! [GKDictionary] {
                        self.skills.append(GKModelSkill.readFromDictionary(dic))
                    }
                    
                    self.categories = data["categories"] as! [GKDictionary]
                    
                    self.reloadTableData()
                }
                else {
                    
                    showCommonError({ (type: NoInternetResultType) -> () in
                        self.requestSkillsData()
                    })
                }
            }
            else {
                
                showConnectionError({ (type: NoInternetResultType) -> () in
                    self.requestSkillsData()
                })
            }
            
        })
    }
    
    func reloadTableData() {
        
        tableView.reloadData()
    }
    
}
