//
//  BBBadgeBarButtonItem.h
//
//  Created by Tanguy Aladenise on 07/02/14.
//  Copyright (c) 2014 Riverie, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BBBadgeBarButtonItem : UIBarButtonItem

#pragma mark - Properties

// @property (nonatomic) BOOL showBadge;
@property (nonatomic) CGFloat badgeOriginX;
@property (nonatomic) CGFloat badgeOriginY;
@property (nonatomic) UIButton *menu_button;

#pragma mark - Methods

- (BBBadgeBarButtonItem *)initWithTarget:(NSObject *)target andSelector:(SEL)selector;
- (void)showBadge: (NSInteger)number;

@end